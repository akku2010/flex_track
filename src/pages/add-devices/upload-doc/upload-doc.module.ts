import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadDocPage } from './upload-doc';

@NgModule({
  declarations: [
    UploadDocPage,
  ],
  imports: [
    IonicPageModule.forChild(UploadDocPage),
  ],
})
export class UploadDocPageModule {}
