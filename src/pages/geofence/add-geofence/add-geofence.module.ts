import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddGeofencePage } from './add-geofence';
// import { NativeGeocoder } from '@ionic-native/native-geocoder';

@NgModule({
  declarations: [
    AddGeofencePage,
  ],
  imports: [
    IonicPageModule.forChild(AddGeofencePage)
  ],
  providers: [
    // NativeGeocoder
  ]
})
export class AddGeofencePageModule {}
