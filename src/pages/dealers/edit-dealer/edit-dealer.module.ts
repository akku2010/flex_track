import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditDealerPage } from './edit-dealer';

@NgModule({
  declarations: [
    EditDealerPage,
  ],
  imports: [
    IonicPageModule.forChild(EditDealerPage),
  ],
})
export class EditDealerPageModule {}
