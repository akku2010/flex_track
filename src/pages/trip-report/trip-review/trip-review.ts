import { Component, OnInit } from "@angular/core";
import { NavParams, AlertController, IonicPage } from "ionic-angular";
import { ApiServiceProvider } from "../../../providers/api-service/api-service";
import { LatLngBounds, GoogleMaps, Marker, LatLng, Spherical } from "@ionic-native/google-maps";

@IonicPage()
@Component({
    selector: 'page-trip-review',
    templateUrl: 'trip-review.html'
})
export class TripReviewPage implements OnInit {
    tripData: any;
    Device_Id: any;
    points: any;
    mapData: any[];
    dataArrayCoords: any[];
    playbackData: any[];
    speed1: any;
    // map: any;
    playing: boolean;
    coordreplaydata: any;
    speed: number;
    speedMarker: any;
    updatetimedate: any;
    target: number = 0;
    allData: any = {};
    startPos: any[];

    constructor(
        public navParams: NavParams,
        public apiCall: ApiServiceProvider,
        public alertCtrl: AlertController) {
        console.log("pramas=> " + JSON.stringify(navParams.get("params")));
        this.tripData = navParams.get("params");
        this.Device_Id = navParams.get("device_id");
        console.log(this.Device_Id);
    }

    ngOnInit() {
        localStorage.removeItem("tripTarget");
        this.triphistory();
    }

    onShare() {

        let tempalert = this.alertCtrl.create({
            title: 'Share Trip',
            message: 'Enter your Mobile Number so that we can share you a trip',
            inputs: [
                {
                    name: 'mobno',
                    placeholder: 'Mobile Number',
                    type: 'number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Share',
                    handler: data => {
                        var sNumber = data.mobno.toString();
                        if (sNumber.length == 10) {
                            console.log(data.mobno)
                        } else {
                            alert("Plaese provide valid mobile number")
                        }
                    }
                }
            ]
        });
        tempalert.present();
        console.log("shareTrip");
    }

    triphistory() {
        let that = this;
        that.apiCall.startLoading().present();
        that.apiCall.tripReviewCall(that.Device_Id, that.tripData.start_time, that.tripData.end_time)
            .subscribe(data => {
                that.apiCall.stopLoading();
                // console.log("resp data=> ", data)
                that.points = data;
                that.mapData = [];
                that.mapData = that.points.map(function (d) {
                    return { lat: d.latDecimal, lng: d.longDecimal };
                });
                that.points.reverse();

                // console.log('data of latlanf', that.mapData);
                let latLngBounds = new LatLngBounds(that.mapData);


                that.dataArrayCoords = [];
                // for (var i = that.points.length - 1; i > 0; i--) {
                for (var i = that.points.length - 1; i > 0; i--) {

                    if (that.points[i].latDecimal && that.points[i].longDecimal) {
                        var arr = [];
                        var startdatetime = new Date(that.points[i].insertionTime);
                        arr.push(that.points[i].latDecimal);
                        arr.push(that.points[i].longDecimal);
                        arr.push({ "time": startdatetime.toLocaleString() });
                        arr.push({ "speed": that.points[i].speed });
                        that.dataArrayCoords.push(arr);
                    }
                }

                setInterval(() => {
                    that.dataArrayCoords;
                }, 0);
                // console.log(that.dataArrayCoords);

                that.playbackData = [];

                for (var j = 0; j < that.points.length; j++) {
                    var startdatetime1 = new Date(that.points[j].insertionTime);

                    that.playbackData.push({ "lat": that.points[j].latDecimal, "lng": that.points[j].longDecimal, 'speed': that.points[j].speed + "Km/hr", 'isettiontime': startdatetime1.toLocaleString() });

                    that.speed1 = that.playbackData[j].speed;

                    setInterval(() => {
                        that.speed1;
                    }, 0);
                }

                if (that.allData.map != undefined) {
                    that.allData.map.remove();
                }
                that.allData.map = GoogleMaps.create('trip_map_canvas');

                that.allData.map.moveCamera({
                    'target': latLngBounds
                });


                that.allData.map.addMarker({
                    title: 'S',
                    position: that.mapData[0],
                    icon: 'green',
                    styles: {
                        'text-align': 'center',
                        'font-style': 'italic',
                        'font-weight': 'bold',
                        'color': 'red'
                    },
                    // icon: 'http://www.googlemapsmarkers.com/v1/FF0000/'
                }).then((marker: Marker) => {
                    marker.showInfoWindow();

                    that.allData.map.addMarker({
                        title: 'D',
                        position: that.mapData[that.mapData.length - 1],
                        icon: 'red',
                        styles: {
                            'text-align': 'center',
                            'font-style': 'italic',
                            'font-weight': 'bold',
                            'color': 'green'
                        },
                        // icon: 'http://www.googlemapsmarkers.com/v1/009900/'
                    }).then((marker: Marker) => {
                        marker.showInfoWindow();
                        // animateMarker(marker, dataArrayCoords, speed, trackerType)
                    });
                });

                // console.log("latlang.............", that.mapData)
                that.allData.map.addPolyline({
                    points: that.mapData,
                    color: 'blue',
                    width: 4,
                    geodesic: true
                })

            },
                err => {
                    console.log(err);
                    that.apiCall.stopLoading();
                });
    };

    Playback() {
        let that = this;
        if (localStorage.getItem("tripTarget") != null) {
            that.target = JSON.parse(localStorage.getItem("tripTarget"));
        }

        console.log("target=> " + that.target)
        that.playing = !that.playing; // This would alternate the state each time
        console.log("playing=> " + that.playing)
        // that.dataArrayCoords.reverse();

        var coord = that.dataArrayCoords[that.target];
        // console.log("coords=> " + coord);

        console.log("datacorrd=> ", that.dataArrayCoords[that.target])

        that.coordreplaydata = coord;
        var lat = coord[0];
        var lng = coord[1];

        that.startPos = [lat, lng];
        that.speed = 200; // km/h
        // var delay = 100;
        if (that.playing) {
            console.log("if initialize=> ", that.playing)

            that.allData.map.setCameraTarget({ lat: lat, lng: lng });
            if (that.allData.mark == undefined) {
                that.allData.map.addMarker({
                    icon: './assets/imgs/vehicles/runningcar.png',
                    styles: {
                        'text-align': 'center',
                        'font-style': 'italic',
                        'font-weight': 'bold',
                        'color': 'green'
                    },
                    position: new LatLng(that.startPos[0], that.startPos[1]),
                }).then((marker: Marker) => {

                    that.allData.mark = marker;
                    // marker.showInfoWindow();
                    that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
                });
            } else {
                that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
                that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
            }

        } else {
            that.allData.mark.setPosition(new LatLng(that.startPos[0], that.startPos[1]));
        }
    }

    liveTrack(map, mark, coords, target, startPos, speed, delay, ) {
        let that = this;
        var target = target;
        if (!startPos.length)
            coords.push([startPos[0], startPos[1]]);

        function _gotoPoint() {
            if (target > coords.length)
                return;

            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;

            var step = (speed * 1000 * delay) / 3600000;
            if (coords[target] == undefined)
                return;
            var dest = new LatLng(coords[target][0], coords[target][1]);
            var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); //in meters
            var numStep = distance / step;
            var i = 0;

            var deltaLat = (coords[target][0] - lat) / numStep;
            var deltaLng = (coords[target][1] - lng) / numStep;

            function changeMarker(mark, deg) {
                // console.log("deg=> ", deg)
                // mark.setIcon($("img[src='" + imgPath + "']").css("transform", "rotate(" + deg + "deg)"));
                mark.setRotation(deg);
            }

            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));

                if (i < distance) {
                    // var head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
                    if ((head != 0) || (head == NaN)) {
                        // icons.rotation = head;
                        changeMarker(mark, head);
                    }
                    // mark.setIcon(icons);
                    mark.setPosition(new LatLng(lat, lng));
                    // map.setCameraTarget(new LatLng(lat, lng))
                    // map.setCenter({ lat: lat, lng: lng });
                    setTimeout(_moveMarker, delay);
                } else {
                    // var head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
                    if ((head != 0) || (head == NaN)) {
                        // icons.rotation = head;
                        changeMarker(mark, head);
                    }
                    // mark.setIcon(icons);
                    mark.setPosition(dest);
                    // map.setCameraTarget(dest);
                    // map.setCenter({ lat: lat, lng: lng });
                    target++;
                    setTimeout(_gotoPoint, delay);
                }
            }
            a++
            if (a > coords.length) {

            } else {
                that.speedMarker = coords[target][3].speed;
                that.updatetimedate = coords[target][2].time;

                if (that.playing) {
                    // console.log("if playing=> "+ playing)
                    _moveMarker();
                    target = target;
                    localStorage.setItem("tripTarget", target);

                } else {
                    // marker.setIcon(icons[trackerType]);
                    // target = target;
                    // console.log("target102=> ", target)
                }
                // km_h = km_h;
            }

        }
        var a = 0;
        _gotoPoint();
    }

    inter(fastforwad) {
        let that = this;
        console.log("fastforwad=> " + fastforwad);
        if (fastforwad == 'fast') {
            that.speed = 2 * that.speed;
            console.log("speed=> " + that.speed)
        }
        else if (fastforwad == 'slow') {
            if (that.speed > 50) {
                that.speed = that.speed / 2;
                console.log("speed=> " + that.speed)
            }
            else {
            }
        }
        else {
            that.speed = 200;
        }
    }
}