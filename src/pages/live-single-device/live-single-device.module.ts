import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LiveSingleDevice } from './live-single-device';
import { IonBottomDrawerModule } from 'ion-bottom-drawer';

@NgModule({
  declarations: [
    LiveSingleDevice,
  ],
  imports: [
    IonicPageModule.forChild(LiveSingleDevice),
    IonBottomDrawerModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LiveSingleDeviceModule { }
