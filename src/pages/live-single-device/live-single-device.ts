import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ModalController, IonicPage, ViewController, ToastController, AlertController, Events } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as io from 'socket.io-client';
import * as moment from 'moment';
import * as _ from "lodash";
import { GoogleMaps, Marker, LatLng, Spherical, GoogleMapsEvent, GoogleMapsMapTypeId, Polyline } from '@ionic-native/google-maps';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { DrawerState } from 'ion-bottom-drawer';
import { Subscription } from 'rxjs/Subscription';
import { mapStyle } from '../live/map-style.model';
declare var google;

@IonicPage()
@Component({
  selector: 'page-live',
  templateUrl: 'live-single-device.html',
})
export class LiveSingleDevice implements OnInit, OnDestroy {

  shouldBounce = true;
  dockedHeight = 80;
  distanceTop = 200;
  drawerState = DrawerState.Docked;
  states = DrawerState;
  minimumHeight = 50;
  transition = '0.85s ease-in-out';

  drawerState1 = DrawerState.Docked;
  dockedHeight1 = 150;
  distanceTop1 = 378;
  minimumHeight1 = 0;

  ongoingGoToPoint: any = {};
  ongoingMoveMarker: any = {};
  service = new google.maps.DistanceMatrixService();
  data: any = {};
  _io: any;
  socketSwitch: any = {};
  socketChnl: any = [];
  socketData: any = {};
  selectedFlag: any = { _id: '', flag: false };
  allData: any = {};
  userdetails: any;
  showBtn: boolean;
  SelectVehicle: string;
  selectedVehicle: any;
  titleText: any;
  portstemp: any;
  showActionSheet: boolean;
  gpsTracking: any;
  power: any;
  currentFuel: any;
  last_ACC: any;
  today_running: string;
  today_stopped: string;
  timeAtLastStop: string;
  distFromLastStop: any;
  lastStoppedAt: string;
  fuel: any;
  total_odo: any;
  todays_odo: any;
  vehicle_speed: any;
  liveVehicleName: any;
  onClickShow: boolean;
  address: any;
  isEnabled: boolean = false;
  liveDataShare: any;
  showShareBtn: boolean = false;
  resToken: any;
  mapData: any[];
  menuActive: boolean;
  motionActivity: string;
  mapHideTraffic: boolean = false;
  locateme: boolean = false;
  latlngCenter: any;
  last_ping_on: any;
  deviceDeatils: any = {};
  tempArray: any[] = [];
  mapTrail: boolean = false;
  showFooter: boolean;
  condition: string = 'gpsc';
  condition1: string = 'light';
  condition2: string = 'light';
  tttime: number;
  acModel: any;
  showaddpoibtn: boolean = false;
  impkey: any;
  btnString: string = "Create Trip";
  showIcon: boolean = false;
  _id: any;
  expectation: any = {};
  polyLines: any[] = [];
  marksArray: any[] = [];
  shwBckBtn: boolean = false;
  recenterMeLat: any;
  recenterMeLng: any;
  mapKey: any;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apiCall: ApiServiceProvider,
    public actionSheetCtrl: ActionSheetController,
    public elementRef: ElementRef,
    public platform: Platform,
    private event: Events,
    public modalCtrl: ModalController,
    private socialSharing: SocialSharing,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public storage: Storage,
    public plt: Platform,
    private nativePageTransitions: NativePageTransitions,
    private viewCtrl: ViewController,
    private geocoderApi: GeocoderProvider
  ) {
    // Environment.setBackgroundColor("black");
    var selectedMapKey;
    if (localStorage.getItem('MAP_KEY') != null) {
      selectedMapKey = localStorage.getItem('MAP_KEY');
      if (selectedMapKey == 'Hybrid') {
        this.mapKey = 'MAP_TYPE_HYBRID';
      } else if (selectedMapKey == 'Normal') {
        this.mapKey = 'MAP_TYPE_NORMAL';
      } else if (selectedMapKey == 'Terrain') {
        this.mapKey = 'MAP_TYPE_TERRAIN';
      } else if (selectedMapKey == 'Satellite') {
        this.mapKey = 'MAP_TYPE_SATELLITE';
      }
    } else {
      this.mapKey = 'MAP_TYPE_NORMAL';
    }
    this.event.subscribe("tripstatUpdated", (data) => {
      if (data) {
        this.checktripstat();
      }
    })
    this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> " + JSON.stringify(this.userdetails));

    if (navParams.get("device") != null) {
      this.deviceDeatils = navParams.get("device");
    }
    this.motionActivity = 'Activity';
    this.menuActive = false;
  }

  car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
  bike = "M419 1754 l3 -44 -37 0 c-21 0 -34 4 -30 10 3 6 -1 10 -9 10 -19 0 -66 -21 -66 -30 0 -18 44 -44 71 -42 16 1 29 0 29 -2 0 -12 -24 -56 -30 -56 -4 0 -7 -6 -7 -12 0 -7 -8 -29 -16 -48 l-15 -35 -7 30 c-4 17 -8 36 -11 43 -6 19 -40 14 -69 -11 l-27 -23 16 -134 c12 -99 23 -149 41 -186 28 -57 25 -84 -10 -84 -34 0 -51 -25 -31 -45 9 -9 16 -33 16 -53 0 -30 -9 -46 -44 -84 -24 -27 -42 -48 -40 -48 2 0 -10 -21 -26 -47 -17 -27 -30 -61 -30 -79 0 -35 35 -189 52 -232 6 -15 10 -41 9 -59 -3 -34 19 -53 58 -53 12 0 21 -6 21 -14 0 -12 -7 -13 -32 -5 -44 12 -70 11 -66 -2 2 -6 21 -12 41 -14 21 -1 43 -7 49 -13 17 -17 23 -119 8 -137 -10 -12 -9 -19 3 -38 l15 -23 -46 23 c-29 15 -63 23 -94 23 -56 0 -61 -14 -17 -48 24 -20 42 -24 97 -24 63 -1 69 -3 109 -39 22 -21 38 -41 35 -44 -4 -4 -2 -5 3 -4 6 1 30 -3 55 -10 54 -16 142 -9 176 14 13 8 24 13 24 10 0 -3 16 13 36 34 35 39 36 40 103 39 59 -1 73 2 99 23 45 35 41 49 -17 49 -32 0 -64 -8 -97 -25 -27 -14 -42 -20 -33 -13 10 8 14 23 11 38 -10 54 -7 139 6 152 7 7 30 13 52 13 27 0 40 4 40 13 0 11 -9 13 -36 8 -19 -4 -42 -10 -50 -13 -10 -3 -12 0 -8 10 3 8 18 17 33 20 23 5 30 13 35 41 4 18 13 47 20 62 8 16 26 77 41 136 l26 106 -45 90 c-24 49 -56 102 -70 119 -14 17 -23 33 -19 37 3 3 1 6 -5 6 -15 0 -16 57 -1 62 22 7 -3 38 -31 38 -19 0 -31 6 -34 18 -7 20 38 114 49 104 5 -4 5 -2 2 4 -4 7 0 67 9 135 8 68 13 134 10 145 -8 29 -60 51 -81 34 -8 -7 -15 -22 -15 -35 0 -43 -13 -33 -46 36 -19 39 -34 73 -34 76 0 3 13 3 30 1 22 -3 35 2 50 17 27 29 17 49 -26 53 -22 1 -33 -2 -29 -8 4 -6 -8 -10 -29 -10 -34 0 -35 1 -38 43 -3 42 -3 42 -43 43 l-40 1 4 -43z m-140 -187 c13 -16 5 -27 -21 -27 -14 0 -18 -5 -14 -17 8 -29 26 -196 24 -220 -5 -48 -19 -2 -32 106 -16 127 -15 133 3 155 16 20 25 20 40 3z m402 -3 c7 -9 14 -22 15 -30 4 -26 -25 -237 -34 -246 -12 -12 -12 12 0 127 6 55 16 103 22 107 6 4 -2 12 -22 19 -26 9 -30 14 -21 25 15 18 24 18 40 -2z m-123 -226 c48 -23 54 -30 46 -51 -5 -13 -10 -14 -39 -1 -50 21 -163 18 -207 -5 -35 -17 -37 -17 -41 0 -7 24 23 49 85 70 58 20 89 18 156 -13z m28 -469 c39 -36 56 -76 56 -134 2 -169 -205 -230 -284 -85 -26 47 -21 155 7 176 10 8 15 14 10 14 -13 0 27 41 50 51 11 5 46 7 77 6 45 -3 64 -9 84 -28z m131 -37 c7 -8 10 -19 7 -23 -3 -5 -1 -9 4 -9 17 0 21 -41 10 -103 -10 -59 -28 -100 -28 -64 0 14 -44 137 -75 209 -6 15 -2 16 32 11 21 -3 43 -12 50 -21z m-455 -49 c-15 -38 -30 -71 -35 -74 -5 -3 -8 -9 -8 -13 3 -21 0 -36 -8 -36 -5 0 -11 30 -13 68 -5 61 -3 69 20 95 14 15 36 27 48 27 l22 0 -26 -67z m377 -317 c19 14 25 14 32 3 13 -21 12 -23 -21 -42 -16 -10 -30 -23 -30 -28 0 -6 -4 -8 -8 -5 -4 2 -9 -9 -10 -25 -2 -22 2 -29 16 -29 14 0 21 -12 29 -44 10 -43 9 -45 -13 -43 -13 1 -29 10 -37 21 -13 17 -15 14 -37 -40 -13 -33 -30 -65 -38 -72 -9 -6 -12 -12 -7 -12 4 0 0 -9 -10 -20 -35 -39 -107 -11 -117 45 -1 11 -9 26 -15 33 -12 12 -17 33 -15 54 1 3 -5 11 -13 18 -11 9 -16 7 -25 -9 -12 -23 -50 -29 -50 -7 0 44 14 76 32 76 17 0 19 4 13 23 -4 12 -7 30 -8 39 -1 9 -4 15 -8 12 -4 -2 -19 6 -33 18 -19 16 -23 27 -17 37 8 12 13 12 34 -2 l25 -16 7 39 c4 22 8 40 9 40 1 0 18 -9 39 -19 l37 -19 -28 -8 c-15 -4 -30 -7 -33 -6 -3 0 -6 -11 -5 -26 l1 -27 125 0 c120 0 125 1 128 21 2 16 -5 25 -27 34 l-31 12 33 22 32 22 10 -43 c10 -43 10 -44 34 -27z m-161 11 c2 -10 -3 -17 -12 -17 -18 0 -29 16 -21 31 9 14 29 6 33 -14z m-58 -11 c0 -16 -18 -31 -27 -22 -8 8 5 36 17 36 5 0 10 -6 10 -14z m108 -10 c3 -12 -1 -17 -10 -14 -7 3 -15 13 -16 22 -3 12 1 17 10 14 7 -3 15 -13 16 -22z m-148 -5 c0 -14 -18 -23 -31 -15 -8 4 -7 9 2 15 18 11 29 11 29 0z m198 -3 c-3 -7 -11 -13 -18 -13 -7 0 -15 6 -17 13 -3 7 4 12 17 12 13 0 20 -5 18 -12z m-430 -224 c37 -10 25 -23 -14 -14 -19 5 -38 11 -41 14 -8 8 26 7 55 0z m672 -3 c-8 -5 -35 -11 -60 -15 l-45 -6 35 14 c39 16 96 21 70 7z m-454 -113 l29 -30 -30 15 c-17 8 -38 24 -48 35 -18 20 -18 20 1 15 11 -2 33 -18 48 -35z m239 15 c-11 -8 -33 -21 -50 -30 l-30 -15 30 31 c16 17 35 29 42 27 7 -3 13 0 13 6 0 6 3 8 7 4 5 -4 -1 -14 -12 -23z"
  truck = 'M180 1725 c0 -8 -21 -25 -47 -37 -27 -12 -58 -31 -70 -42 l-23 -20 0 -495 0 -496 23 -18 c12 -10 31 -24 42 -32 20 -13 20 -14 -2 -37 -21 -22 -23 -34 -23 -131 0 -65 -4 -107 -10 -107 -5 0 -10 -7 -10 -15 0 -8 5 -15 10 -15 6 0 10 -31 10 -74 0 -44 5 -78 11 -82 8 -4 7 -9 -2 -15 -38 -23 96 -89 181 -89 85 0 219 66 181 89 -9 6 -10 11 -2 15 6 4 11 38 11 82 0 43 4 74 10 74 6 0 10 7 10 15 0 8 -4 15 -10 15 -6 0 -10 43 -10 108 0 96 -2 108 -20 120 -30 18 -24 38 20 69 l40 28 0 491 0 491 -22 24 c-22 24 -86 59 -106 59 -5 0 -12 9 -15 20 -5 18 -14 20 -91 20 -70 0 -86 -3 -86 -15z m115 -101 c21 -8 35 -45 28 -72 -7 -29 -47 -44 -78 -30 -29 13 -38 36 -29 72 7 30 43 44 79 30z m-148 -726 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m250 0 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m-102 326 c21 -9 35 -46 28 -74 -12 -44 -93 -46 -108 -1 -17 54 27 95 80 75z m20 -436 c2 -2 6 -15 10 -30 10 -38 -26 -72 -68 -64 -37 8 -35 6 -43 43 -8 42 18 67 64 59 19 -3 36 -6 37 -8z m18 -245 c-36 -16 -102 -16 -130 0 -18 10 -7 12 68 12 80 0 87 -1 62 -12z m-203 -69 c0 -21 -4 -33 -10 -29 -5 3 -10 19 -10 36 0 16 5 29 10 29 6 0 10 -16 10 -36z m300 1 c0 -24 -5 -35 -15 -35 -15 0 -22 50 -8 63 15 15 23 5 23 -28z m-300 -87 c0 -24 -5 -50 -10 -58 -7 -11 -10 2 -10 43 0 31 4 57 10 57 6 0 10 -19 10 -42z m300 -20 c0 -43 -3 -58 -10 -48 -13 20 -23 87 -15 100 15 25 25 5 25 -52z m-201 -44 c31 -4 80 -2 108 4 59 10 70 5 75 -43 3 -29 2 -30 -57 -38 -58 -8 -199 -2 -229 9 -12 5 -12 11 2 45 11 28 20 38 30 34 9 -3 40 -8 71 -11z m147 -222 c-14 -15 -15 -13 -3 17 8 23 13 28 15 16 2 -10 -3 -25 -12 -33z m-213 18 c3 -11 1 -20 -4 -20 -5 0 -9 9 -9 20 0 11 2 20 4 20 2 0 6 -9 9 -20z m267 -25 c0 -2 -10 -10 -22 -16 -21 -11 -22 -11 -9 4 13 16 31 23 31 12z'
  carIcon = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [10, 20],
  };
  busIcon = {
    path: this.car,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    fillColor: 'blue',
    anchor: [12.5, 12.5],
  };
  bikeIcon = {
    path: this.bike,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    anchor: [12.5, 12.5],
  };

  truckIcon = {
    path: this.truck,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    anchor: [10, 20],
  };

  userIcon = {
    path: this.truck,
    labelOrigin: [25, 50],
    strokeColor: 'black',
    strokeWeight: .10,
    fillOpacity: 1,
    anchor: [12.5, 12.5],
  }

  icons = {
    "car": this.carIcon,
    "bike": this.bikeIcon,
    "truck": this.truckIcon,
    "bus": this.busIcon,
    "user": this.userIcon,
    "jcb": this.carIcon,
    "tractor": this.carIcon
  }

  ngOnInit() {
    let that = this;
    that.tempArray = [];
    this.checktripstat();
  }


  setDocHeight() {
    let that = this;
    that.distanceTop = 200;
    that.drawerState = DrawerState.Top;
  }

  setDocHeightAtFirst() {
    let that = this;
    that.distanceTop = 80;
    that.drawerState = DrawerState.Top;
  }

  goBack() {
    if (this.navCtrl.canGoBack()) {
      let options: NativeTransitionOptions = {
        direction: 'left',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 60

      };

      this.nativePageTransitions.slide(options);
      this.navCtrl.pop();
    } else {
      let options: NativeTransitionOptions = {
        duration: 700
      };
      this.nativePageTransitions.fade(options);
      this.navCtrl.setRoot('DashboardPage');
    }
  }

  refreshMe() {
    this.ngOnDestroy();
    this.ngOnInit();
    this.ionViewDidLoad();
  }

  reCenterMe() {
    // console.log("getzoom level: " + this.allData.map.getCameraZoom());
    this.allData.map.moveCamera({
      target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
      zoom: this.allData.map.getCameraZoom()
    }).then(() => {

    })
  }

  newMap() {
    let mapOptions = {
      camera: { zoom: 10 },
      gestures: {
        rotate: false,
        tilt: false
      }
    };
    let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
    // Environment.setBackgroundColor("black");
    return map;
  }

  isNight() {
    //Returns true if the time is between
    //7pm to 5am
    let time = new Date().getHours();
    return (time > 5 && time < 19) ? false : true;
  }

  checktripstat() {
    var url = this.apiCall.mainUrl + "user_trip/getLastTrip?device=" + this.deviceDeatils._id + "&status=Started&tripInfo=last_trip";

    this.apiCall.getSOSReportAPI(url)
      .subscribe(data => {
        // debugger
        if (!data.message) {
          this.allData.tripStat = data[0].trip_status;
          if (this.allData.tripStat == 'Started') {
            var sources = new LatLng(parseFloat(data[0].start_lat), parseFloat(data[0].start_long));
            var dest = new LatLng(parseFloat(data[0].end_lat), parseFloat(data[0].end_long));
            this.calcRoute(sources, dest);
          }
        }
      })
  }

  calcRoute(start, end) {

    this.allData.AIR_PORTS = [];
    var directionsService = new google.maps.DirectionsService();
    let that = this;
    var request = {
      origin: start,
      destination: end,
      // waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: google.maps.TravelMode.DRIVING
    };

    directionsService.route(request, function (response, status) {

      if (status == google.maps.DirectionsStatus.OK) {
        var path = new google.maps.MVCArray();
        for (var i = 0, len = response.routes[0].overview_path.length; i < len; i++) {
          path.push(response.routes[0].overview_path[i]);

          that.allData.AIR_PORTS.push({
            lat: path.j[i].lat(), lng: path.j[i].lng()
          });
          // debugger
          if (that.allData.AIR_PORTS.length > 1) {
            that.allData.map.addMarker({
              title: 'Start',
              position: start,
              icon: 'green'
            }).then((mark: Marker) => {
              that.marksArray.push(mark);
            })
            that.allData.map.addMarker({
              title: 'Destination',
              position: end,
              icon: 'red'
            }).then((mark: Marker) => {
              that.marksArray.push(mark);
            })
            that.allData.map.addPolyline({
              'points': that.allData.AIR_PORTS,
              'color': '#4aa9d5',
              'width': 4,
              'geodesic': true,
            }).then((poly: Polyline) => {
              that.polyLines.push(poly);
              that.getTravelDetails(start, end);
              //   that.showBtn = true;
            })
          }
        }
        that.apiCall.stopLoading();
      }
    });
  }

  getTravelDetails(source, dest) {
    let that = this;
    this._id = setInterval(() => {
      if (localStorage.getItem("livepagetravelDetailsObject") != null) {
        if (that.expectation.distance == undefined && that.expectation.duration == undefined) {
          that.expectation = JSON.parse(localStorage.getItem("livepagetravelDetailsObject"));
          console.log("expectation: ", that.expectation)
        } else {
          clearInterval(this._id);
        }
      }
    }, 3000)
    that.service.getDistanceMatrix({
      origins: [source],
      destinations: [dest],
      travelMode: 'DRIVING'
    }, that.callback);
  }

  callback(response, status) {
    let travelDetailsObject;
    if (status == 'OK') {
      var origins = response.originAddresses;
      for (var i = 0; i < origins.length; i++) {
        var results = response.rows[i].elements;
        for (var j = 0; j < results.length; j++) {
          var element = results[j];
          var distance = element.distance.text;
          var duration = element.duration.text;
          travelDetailsObject = {
            distance: distance,
            duration: duration
          }
        }
      }
      localStorage.setItem("livepagetravelDetailsObject", JSON.stringify(travelDetailsObject));
    }
  }

  diff(start, end) {
    start = start.split(":");
    end = end.split(":");
    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
    var diff = endDate.getTime() - startDate.getTime();
    var hours = Math.floor(diff / 1000 / 60 / 60);
    diff -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff / 1000 / 60);

    // If using time pickers with 24 hours format, add the below line get exact hours
    if (hours < 0)
      hours = hours + 24;

    return (hours <= 9 ? "0" : "") + hours + " hours " + (minutes <= 9 ? "0" : "") + minutes + " mins";
  }

  callendtripfunc(res) {
    var endtime = moment(new Date(), 'DD/MM/YYY').format('hh:mm');
    var starttime = moment(new Date(res.doc.start_time), 'DD/MM/YYYY').format('hh:mm');
    var duration = this.diff(starttime, endtime);
    var dest = new LatLng(res.doc.start_lat, res.doc.start_long);
    var dest2 = new LatLng(res.doc.end_lat, res.doc.end_long);
    var distance = Spherical.computeDistanceBetween(dest, dest2); // in meters

    var bUrl = this.apiCall.mainUrl + "user_trip/updatePlantrip";
    var payload = {
      "user": res.doc.user,
      "device": res.doc.device,
      "start_loc": {
        "lat": res.doc.start_lat,
        "long": res.doc.start_long
      },
      "end_loc": {
        "lat": res.doc.end_lat,
        "long": res.doc.end_long,
      },
      "duration": duration,
      "distance": distance,
      "trip_status": "completed",
      "trip_id": res.doc._id
    }
    this.apiCall.startLoading().present();
    this.apiCall.urlpasseswithdata(bUrl, payload)
      .subscribe(data => {
        this.apiCall.stopLoading();
        console.log("tri end data: ", data)
        // alert("trip ended succesfully");
        let toast = this.toastCtrl.create({
          message: 'Trip has ended succesfully.',
          duration: 1500,
          position: 'bottom'
        })
        toast.present();
        let that = this;
        // debugger
        that.allData.tripStat = 'completed';
        if (that.polyLines.length > 0) {
          for (var t = 0; t < that.polyLines.length; t++) {
            that.polyLines[t].remove();
          }
        }

        if (that.marksArray.length > 0) {
          for (var e = 0; e < that.marksArray.length; e++) {
            that.marksArray[e].remove();
          }
        }
        if (localStorage.getItem("livepagetravelDetailsObject") != null) {
          localStorage.removeItem("livepagetravelDetailsObject");
        }
        this.checktripstat();
      })

  }

  endTrip() {
    let alert = this.alertCtrl.create({
      title: 'Confirm',
      message: 'Do you want to end the trip?',
      buttons: [
        {
          text: 'YES PROCEED',
          handler: () => {
            this.storage.get("TRIPDATA").then((res) => {
              if (res) {
                this.callendtripfunc(res);
              }
            })
          }
        },
        {
          text: 'Back'
        }
      ]

    })
    alert.present()
  }

  resumeListener: Subscription = new Subscription();
  ionViewDidLoad() {
    // this.setDocHeightAtFirst();
    this._io = io.connect(this.apiCall.mainUrl + 'gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
    this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
    this.showShareBtn = true;
    var paramData = this.navParams.get("device");

    this.titleText = paramData.Device_Name;
    this.temp(paramData);
    this.showActionSheet = true;
  }
  ionViewWillEnter() {
    if (this.plt.is('ios')) {
      this.shwBckBtn = true;
      this.viewCtrl.showBackButton(false);
    }

    this.platform.ready().then(() => {
      this.resumeListener = this.platform.resume.subscribe(() => {
        var today, Christmas;
        today = new Date();
        Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
        var diffMs = (today - Christmas); // milliseconds between now & Christmas
        var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        if (diffMins >= 5) {
          localStorage.removeItem("backgroundModeTime");
          this.alertCtrl.create({
            message: 'Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?',
            buttons: [
              {
                text: 'YES PROCEED',
                handler: () => {
                  this.refreshMe();
                }
              },
              {
                text: 'Back',
                handler: () => {
                  this.navCtrl.setRoot('DashboardPage');
                }
              }
            ]
          }).present();
        }
      })
    })
  }

  ionViewWillLeave() {
    this.platform.ready().then(() => {
      this.resumeListener.unsubscribe();
    })
  }

  ngOnDestroy() {
    let that = this;
    for (var i = 0; i < that.socketChnl.length; i++)
      that._io.removeAllListeners(that.socketChnl[i]);
    that._io.on('disconnect', () => {
      that._io.open();
    })
    if (localStorage.getItem("livepagetravelDetailsObject") != null) {
      localStorage.removeItem("livepagetravelDetailsObject");
    }
  }

  onClickMap(maptype) {
    let that = this;
    if (maptype == 'SATELLITE') {
      that.allData.map.setMapTypeId(GoogleMapsMapTypeId.SATELLITE);
    } else {
      if (maptype == 'TERRAIN') {
        that.allData.map.setMapTypeId(GoogleMapsMapTypeId.TERRAIN);
      } else {
        if (maptype == 'NORMAL') {
          that.allData.map.setMapTypeId(GoogleMapsMapTypeId.NORMAL);
        } else {
          if (maptype == 'HYBRID') {
            that.allData.map.setMapTypeId(GoogleMapsMapTypeId.HYBRID);
          }
        }
      }
    }
  }

  createTrip() {
    console.log("create trip data: ", this.deviceDeatils)
    this.navCtrl.push('CreateTripPage', {
      'paramData': this.deviceDeatils,
    })
  }

  trafficFunc() {
    let that = this;
    that.isEnabled = !that.isEnabled;
    if (that.isEnabled == true) {
      that.allData.map.setTrafficEnabled(true);
    } else {
      that.allData.map.setTrafficEnabled(false);
    }
  }

  shareLive() {
    let that = this;
    that.showActionSheet = false;
    // that.drawerHidden = false;
    that.drawerState1 = DrawerState.Docked;
    that.showFooter = true;
  }

  sharedevices(param) {
    let that = this;
    if (param == '15mins') {
      that.condition = 'gpsc';
      that.condition1 = 'light';
      that.condition2 = 'light';
      that.tttime = 15;
      // that.tttime  = (15 * 60000); //for miliseconds
    } else {
      if (param == '1hour') {
        that.condition1 = 'gpsc';
        that.condition = 'light';
        that.condition2 = 'light';
        that.tttime = 60;
        // that.tttime  = (1 * 3600000); //for miliseconds
      } else {
        if (param == '8hours') {
          that.condition2 = 'gpsc';
          that.condition = 'light';
          that.condition1 = 'light';
          that.tttime = (8 * 60);
          // that.tttime  = (8 * 3600000);
        }
      }
    }
  }

  shareLivetemp() {
    let that = this;
    if (that.tttime == undefined) {
      that.tttime = 15;
    }
    var data = {
      id: that.liveDataShare._id,
      imei: that.liveDataShare.Device_ID,
      sh: this.userdetails._id,
      ttl: that.tttime   // set to 1 hour by default
    };
    this.apiCall.startLoading().present();
    this.apiCall.shareLivetrackCall(data)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.resToken = data.t;
        this.liveShare();
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  liveShare() {
    let that = this;
    var link = this.apiCall.mainUrl + "share/liveShare?t=" + that.resToken;
    that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
    that.showActionSheet = true;
    that.showFooter = false;
    that.tttime = undefined;
  }

  parseMillisecondsIntoReadableTime(milliseconds) {
    //Get hours from milliseconds
    var hours = milliseconds / (1000 * 60 * 60);
    var absoluteHours = Math.floor(hours);
    var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;

    //Get remainder from hours and convert to minutes
    var minutes = (hours - absoluteHours) * 60;
    var absoluteMinutes = Math.floor(minutes);
    var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;

    //Get remainder from minutes and convert to seconds
    var seconds = (minutes - absoluteMinutes) * 60;
    var absoluteSeconds = Math.floor(seconds);
    var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;

    // return h + ':' + m;
    return h + ':' + m + ':' + s;
  }

  settings() {
    let that = this;
    let profileModal = this.modalCtrl.create('DeviceSettingsPage', {
      param: that.deviceDeatils
    });
    profileModal.present();

    profileModal.onDidDismiss(() => {
      for (var i = 0; i < that.socketChnl.length; i++)
        that._io.removeAllListeners(that.socketChnl[i]);

      var paramData = this.navParams.get("device");
      this.temp(paramData);
    })
  }

  getAddress(coordinates) {
    // debugger
    let that = this;
    if (!coordinates) {
      that.address = 'N/A';
      return;
    }
    // this.geocoderApi.geocoderResult(coordinates.lat, coordinates.long)
    // .then(res => {
    //   that.address = res;
    // })
    this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        that.address = str;
      })
  }

  socketInit(pdata, center = false) {

    this._io.emit('acc', pdata.Device_ID);
    this.socketChnl.push(pdata.Device_ID + 'acc');
    let that = this;
    this._io.on(pdata.Device_ID + 'acc', (d1, d2, d3, d4, d5) => {
      if (d4 != undefined)
        console.log("ping data: => ", d4);
      (function (data) {
        if (data == undefined) {
          return;
        }
        localStorage.setItem("pdata", JSON.stringify(data))

        if (data._id != undefined && data.last_location != undefined) {
          var key = data._id;
          that.impkey = data._id;
          that.deviceDeatils = data;
          let ic = _.cloneDeep(that.icons[data.iconType]);
          if (!ic) {
            return;
          }
          ic.path = null;

          if (data.status.toLowerCase() === 'running' && data.last_speed > 0) {
            if (that.plt.is('ios')) {
              ic.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
            } else if (that.plt.is('android')) {
              ic.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
            }
          } else {
            if (data.status.toLowerCase() === 'running' && data.last_speed === 0) {
              if (that.plt.is('ios')) {
                ic.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
              } else if (that.plt.is('android')) {
                ic.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
              }
            } else {
              if (that.plt.is('ios')) {
                ic.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
              } else if (that.plt.is('android')) {
                ic.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
              }
            }
          }
          that.vehicle_speed = data.last_speed;
          that.todays_odo = data.today_odo;
          that.total_odo = data.total_odo;
          if (that.userdetails.fuel_unit == 'LITRE') {
            that.fuel = data.currentFuel;
          } else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
            that.fuel = data.fuel_percent;
          } else {
            that.fuel = data.currentFuel;
          }

          that.last_ping_on = data.last_ping_on;

          var tempvar = new Date(data.lastStoppedAt);
          if (data.lastStoppedAt != null) {
            var fd = tempvar.getTime();
            var td = new Date().getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            that.lastStoppedAt = rhours + ':' + rminutes;
          } else {
            that.lastStoppedAt = '00' + ':' + '00';
          }

          that.distFromLastStop = data.distFromLastStop;
          if (!isNaN(data.timeAtLastStop)) {
            that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
          } else {
            that.timeAtLastStop = '00:00:00';
          }

          that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
          that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
          that.last_ACC = data.last_ACC;
          that.acModel = data.ac;
          that.currentFuel = data.currentFuel;
          that.power = data.power;
          that.gpsTracking = data.gpsTracking;
          that.recenterMeLat = data.last_location.lat;
          that.recenterMeLng = data.last_location.long;
          if (that.allData[key]) {
            that.socketSwitch[key] = data;
            console.log("check mark is avalible or not? ", that.allData[key].mark)
            if (that.allData[key].mark !== undefined) {
              that.allData[key].mark.setIcon(ic);
              that.allData[key].mark.setPosition(that.allData[key].poly[1]);
              var temp = _.cloneDeep(that.allData[key].poly[1]);
              that.allData[key].poly[0] = _.cloneDeep(temp);
              that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };

              var speed = data.status == "RUNNING" ? data.last_speed : 0;
              that.getAddress(data.last_location);
              // debugger
              that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
            } else {
              return;
            }
          }
          else {
            that.allData[key] = {};
            that.socketSwitch[key] = data;
            that.allData[key].poly = [];
            if (data.sec_last_location) {
              that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
            } else {
              that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
            }
            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
            if (data.last_location != undefined) {

              that.allData.map.addMarker({
                title: data.Device_Name,
                position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                icon: ic.url,
              }).then((marker: Marker) => {

                that.allData[key].mark = marker;
                marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)
                  .subscribe(e => {
                    that.liveVehicleName = data.Device_Name;
                    that.showActionSheet = true;
                    that.showaddpoibtn = true;

                    that.getAddressTitle(marker);

                  });
                that.getAddress(data.last_location);
                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                that.liveTrack(that.allData.map, that.allData[key].mark, ic, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
              })
            }
          }
        }
      })(d4)
    })
  }

  getAddressTitle(marker) {
    this.geocoderApi.reverseGeocode(marker.getPosition().lat, marker.getPosition().lng)
      .then(res => {
        var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
        marker.setSnippet(str);
      })
  }

  addPOI() {
    let that = this;
    let modal = this.modalCtrl.create(PoiPage, {
      param1: that.allData[that.impkey]
    });
    modal.onDidDismiss((data) => {
      console.log(data)
      let that = this;
      that.showaddpoibtn = false;
    });
    modal.present();
  }

  liveTrack(map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {
    var target = 0;

    clearTimeout(that.ongoingGoToPoint[id]);
    clearTimeout(that.ongoingMoveMarker[id]);
    if (center) {
      map.setCameraTarget(coords[0]);
    }

    function _goToPoint() {
      if (target > coords.length)
        return;

      var lat = mark.getPosition().lat;
      var lng = mark.getPosition().lng;

      var step = (speed * 1000 * delay) / 3600000; // in meters
      if (coords[target] == undefined)
        return;

      var dest = new LatLng(coords[target].lat, coords[target].lng);
      var distance = Spherical.computeDistanceBetween(dest, mark.getPosition()); // in meters
      var numStep = distance / step;
      var i = 0;
      var deltaLat = (coords[target].lat - lat) / numStep;
      var deltaLng = (coords[target].lng - lng) / numStep;

      function changeMarker(mark, deg) {
        mark.setRotation(deg);
        // mark.setIcon(icons);
      }
      function _moveMarker() {
        // debugger
        lat += deltaLat;
        lng += deltaLng;
        i += step;
        var head;
        if (i < distance) {

          head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.mapTrail) {
            that.showTrail(lat, lng, map);
          } else {
            if (that.allData.polylineID) {
              that.tempArray = [];
              that.allData.polylineID.remove();
            }
          }
          map.setCameraTarget(new LatLng(lat, lng));
          that.latlngCenter = new LatLng(lat, lng);
          mark.setPosition(new LatLng(lat, lng));
          that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
        }
        else {
          head = Spherical.computeHeading(mark.getPosition(), dest);
          head = head < 0 ? (360 + head) : head;
          if ((head != 0) || (head == NaN)) {
            changeMarker(mark, head);
          }
          if (that.mapTrail) {
            that.showTrail(dest.lat, dest.lng, map);
          }
          else {
            if (that.allData.polylineID) {
              that.tempArray = [];
              that.allData.polylineID.remove();
            }
          }
          map.setCameraTarget(dest);
          that.latlngCenter = dest;
          mark.setPosition(dest);
          target++;
          that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
        }
      }
      _moveMarker();
    }
    _goToPoint();
  }

  zoomin() {
    let that = this;
    that.allData.map.moveCameraZoomIn();
  }
  zoomout() {
    let that = this;
    that.allData.map.animateCameraZoomOut();
  }

  temp(data) {

    let that = this;
    that.showShareBtn = true;

    that.liveDataShare = data;
    that.onClickShow = false;

    if (that.allData.map != undefined) {
      that.allData.map.remove();
    }

    for (var i = 0; i < that.socketChnl.length; i++)
      that._io.removeAllListeners(that.socketChnl[i]);
    that.allData = {};
    that.socketChnl = [];
    that.socketSwitch = {};
    let style = [];

    //Change Style to night between 7pm to 5am
    if (this.isNight()) {
      style = mapStyle
    }
    if (data) {
      if (data.last_location) {
        if (that.plt.is('android')) {
          let mapOptions = {
            backgroundColor: 'white',
            controls: {
              compass: true,
              zoom: false,
            },
            gestures: {
              rotate: false,
              tilt: false
            },
            mapType: that.mapKey,
            styles: style
          }
          let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
          // Environment.setBackgroundColor("black");
          map.animateCamera({
            target: { lat: 20.5937, lng: 78.9629 },
            zoom: 18,
            duration: 3000,
            padding: 0  // default = 20px
          });

          map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
          that.allData.map = map;
        } else {
          if (that.plt.is('ios')) {
            let mapOptions = {
              backgroundColor: 'white',
              controls: {
                compass: true,
                zoom: false,
              },
              gestures: {
                rotate: false,
                tilt: false
              },
              camera: {
                target: { lat: 20.5937, lng: 78.9629 },
                zoom: 18,
              },
              mapType: that.mapKey,
              styles: style
            }
            let map = GoogleMaps.create('map_canvas_single_device', mapOptions);
            // Environment.setBackgroundColor("black");
            that.allData.map = map;
          }
        }

        that.socketInit(data);
      } else {
        that.allData.map = that.newMap();
        that.socketInit(data);
      }
    }
  }

  showTrail(lat, lng, map) {
    let that = this;
    that.tempArray.push({ lat: lat, lng: lng });

    if (that.tempArray.length === 2) {
      map.addPolyline({
        points: that.tempArray,
        color: '#0260f7',
        width: 5,
        geodesic: true
      }).then((polyline: Polyline) => {
        that.allData.polylineID = polyline;
        // that.polylineID.push(polyline);
      });
    } else if (that.tempArray.length > 2) {
      that.tempArray.shift();
      map.addPolyline({
        points: that.tempArray,
        color: '#0260f7',
        width: 5,
        geodesic: true
      }).then((polyline: Polyline) => {
        that.allData.polylineID = polyline;
        // that.allData.polylineID.push(polyline);
      });
    }
  }

  onClickMainMenu(item) {
    this.menuActive = !this.menuActive;
  }

  onSelectMapOption(type) {
    let that = this;
    if (type == 'locateme') {
      that.allData.map.setCameraTarget(that.latlngCenter);
    } else {
      if (type == 'mapHideTraffic') {
        this.mapHideTraffic = !this.mapHideTraffic;
        if (this.mapHideTraffic) {
          that.allData.map.setTrafficEnabled(true);
        } else {
          that.allData.map.setTrafficEnabled(false);
        }
      } else {
        if (type == 'mapTrail') {
          this.mapTrail = !this.mapTrail;
        }
      }
    }
  }
}

@Component({
  templateUrl: './poi.html',
  selector: 'page-device-settings'
})
export class PoiPage implements OnInit {
  poi_name: any;
  address: any;
  params: any;
  lat: any;
  lng: any;
  islogin: any;

  constructor(public apicall: ApiServiceProvider, public toastCtrl: ToastController, public navparam: NavParams, public viewCtrl: ViewController) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    this.params = this.navparam.get("param1");
    this.lat = this.params.mark.getPosition().lat;
    this.lng = this.params.mark.getPosition().lng;
  }

  ngOnInit() {
    let that = this;
    that.address = undefined;
    var geocoder = new google.maps.Geocoder;
    var latlng = new google.maps.LatLng(this.lat, this.lng);
    var request = { "latLng": latlng };

    geocoder.geocode(request, function (resp, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (resp[0] != null) {
          that.address = resp[0].formatted_address;
        } else {
          console.log("No address available")
        }
      }
      else {
        that.address = 'N/A';
      }
    })
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  save() {
    if (this.poi_name == undefined || this.address == undefined) {
      let toast = this.toastCtrl.create({
        message: "POI name is required!",
        duration: 1500,
        position: "bottom"
      })
      toast.present();

    } else {
      var payload = {
        "poi": [{
          "location": {
            "type": "Point",
            "coordinates": [
              this.lng,
              this.lat
            ]
          },
          "poiname": this.poi_name,
          "status": "Active",
          "address": this.address,
          "user": this.islogin._id
        }]
      };
      this.apicall.startLoading().present();
      this.apicall.addPOIAPI(payload)
        .subscribe(data => {
          this.apicall.stopLoading();
          let toast = this.toastCtrl.create({
            message: "POI added successfully!",
            duration: 2000,
            position: 'top'
          });
          toast.present();
          this.viewCtrl.dismiss();
        },
          err => {
            this.apicall.stopLoading();
          })
    }
  }
}


