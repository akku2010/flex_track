import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { MyApp } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { NetworkProvider } from '../providers/network/network';
import { MenuProvider } from '../providers/menu/menu';

import { GoogleMaps, Spherical } from '@ionic-native/google-maps';
import { PopoverPage } from '../pages/add-devices/add-devices';
import { IonBottomDrawerModule } from '../../node_modules/ion-bottom-drawer';
import { Push } from '@ionic-native/push';
import { UpdateGroup } from '../pages/groups/update-group/update-group';
import { FilterPage } from '../pages/all-notifications/filter/filter';
// Custom components
import { SideMenuContentComponent } from '../../shared/side-menu-content/side-menu-content.component';
import { Geolocation } from '@ionic-native/geolocation';
import { ServiceProviderPage, UpdatePasswordPage } from '../pages/profile/profile';
import { AppVersion } from '@ionic-native/app-version';
import { SocialSharing } from '@ionic-native/social-sharing';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { CallNumber } from '@ionic-native/call-number';
import { Pro } from '@ionic/pro';
import { Injectable, Injector } from '@angular/core';

import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { PoiPage } from '../pages/live-single-device/live-single-device';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { ViewDoc } from '../pages/add-devices/upload-doc/view-doc/view-doc';
import { DocPopoverPage } from '../pages/add-devices/upload-doc/upload-doc';
import { ACDetailPage } from '../pages/ac-report/ac-report';
import { FuelEntryPage } from '../pages/fuel/fuel-consumption/fuel-consumption';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { GeocoderProvider } from '../providers/geocoder/geocoder';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { ModalPage } from '../pages/history-device/modal';

Pro.init('d2e7bf2e', {
  appVersion: '0.0.1'
})

@Injectable()
export class MyErrorHandler implements ErrorHandler {
  ionicErrorHandler: IonicErrorHandler;

  constructor(injector: Injector) {
    try {
      this.ionicErrorHandler = injector.get(IonicErrorHandler);
    } catch (e) {
      // Unable to get the IonicErrorHandler provider, ensure
      // IonicErrorHandler has been added to the providers list below
    }
  }

  handleError(err: any): void {
    Pro.monitoring.handleNewError(err);
    // Remove this if you want to disable Ionic's auto exception handling
    // in development mode.
    this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
  }
}

@NgModule({
  declarations: [
    MyApp,
    SideMenuContentComponent,
    PopoverPage,
    UpdateGroup,
    FilterPage,
    ServiceProviderPage,
    UpdatePasswordPage,
    PoiPage,
    ViewDoc,
    DocPopoverPage,
    ACDetailPage,
    FuelEntryPage,
    ModalPage
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    IonBottomDrawerModule,
    SelectSearchableModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PopoverPage,
    UpdateGroup,
    FilterPage,
    ServiceProviderPage,
    UpdatePasswordPage,
    PoiPage,
    ViewDoc,
    DocPopoverPage,
    ACDetailPage,
    FuelEntryPage,
    ModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    IonicErrorHandler,
    [{ provide: ErrorHandler, useClass: MyErrorHandler }],
    // { provide: ErrorHandler, useClass: IonicErrorHandler },
    ApiServiceProvider,
    Network,
    NetworkProvider,
    MenuProvider,
    GoogleMaps,
    Geolocation,
    Spherical,
    Push,
    AppVersion,
    SocialSharing,
    TextToSpeech,
    Transfer,
    TransferObject,
    File,
    FileTransfer,
    FileTransferObject,
    Camera,
    FilePath,
    NativeGeocoder,
    CallNumber,
    AndroidPermissions,
    GeocoderProvider,
    NativePageTransitions
  ]
})
export class AppModule { }
