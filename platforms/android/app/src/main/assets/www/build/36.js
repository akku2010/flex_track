webpackJsonp([36],{

/***/ 1026:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DistanceReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DistanceReportPage = /** @class */ (function () {
    function DistanceReportPage(navCtrl, navParams, apicallDistance, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallDistance = apicallDistance;
        this.toastCtrl = toastCtrl;
        this.distanceReportData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // this.datetimeStart = moment({ hours: 0 }).format();
        // this.datetimeEnd = moment().format();//new Date(a).toISOString();
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).subtract(1, 'days').format(); // yesterday date with 12:00 am
        console.log("today time: ", this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format(); // today date and time with 12:00am
    }
    DistanceReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    DistanceReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicallDistance.startLoading().present();
        this.apicallDistance.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicallDistance.stopLoading();
            _this.devices = data;
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apicallDistance.stopLoading();
            console.log(err);
        });
    };
    DistanceReportPage.prototype.getdistancedevice = function (selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.Ignitiondevice_id = selectedVehicle._id;
    };
    DistanceReportPage.prototype.getDistanceReport = function () {
        var _this = this;
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        var outerthis = this;
        this.apicallDistance.startLoading().present();
        this.apicallDistance.getDistanceReportApi(new Date(outerthis.datetimeStart).toISOString(), new Date(outerthis.datetimeEnd).toISOString(), this.islogin._id, this.Ignitiondevice_id)
            .subscribe(function (data) {
            _this.apicallDistance.stopLoading();
            _this.distanceReport = data;
            if (_this.distanceReport.length > 0) {
                _this.innerFunc(_this.distanceReport);
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Report(s) not found for selected dates/Vehicle.',
                    duration: 1500,
                    position: 'bottom'
                });
                toast.present();
            }
        }, function (error) {
            _this.apicallDistance.stopLoading();
            console.log(error);
        });
    };
    DistanceReportPage.prototype.innerFunc = function (distanceReport) {
        var outerthis = this;
        var i = 0, howManyTimes = distanceReport.length;
        function f() {
            outerthis.distanceReportData.push({
                'distance': outerthis.distanceReport[i].distance,
                'Device_Name': outerthis.distanceReport[i].device.Device_Name
            });
            if (outerthis.distanceReport[i].endLat != null && outerthis.distanceReport[i].startLat != null) {
                var latEnd = outerthis.distanceReport[i].endLat;
                var lngEnd = outerthis.distanceReport[i].endLng;
                var latlng = new google.maps.LatLng(latEnd, lngEnd);
                var latStart = outerthis.distanceReport[i].startLat;
                var lngStart = outerthis.distanceReport[i].startLng;
                var lngStart1 = new google.maps.LatLng(latStart, lngStart);
                var geocoder = new google.maps.Geocoder();
                var request = {
                    latLng: latlng
                };
                var request1 = {
                    latLng: lngStart1
                };
                geocoder.geocode(request, function (data, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (data[1] != null) {
                            outerthis.locationEndAddress = data[1].formatted_address;
                        }
                    }
                    outerthis.distanceReportData[outerthis.distanceReportData.length - 1].EndLocation = outerthis.locationEndAddress;
                });
                geocoder.geocode(request1, function (data, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (data[1] != null) {
                            outerthis.locationAddress = data[1].formatted_address;
                        }
                    }
                    outerthis.distanceReportData[outerthis.distanceReportData.length - 1].StartLocation = outerthis.locationAddress;
                });
            }
            else {
                outerthis.distanceReportData[outerthis.distanceReportData.length - 1].StartLocation = 'N/A';
                outerthis.distanceReportData[outerthis.distanceReportData.length - 1].EndLocation = 'N/A';
            }
            console.log(outerthis.distanceReportData);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    DistanceReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-distance-report',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\distance-report\distance-report.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Distance Report</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getdistancedevice(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right;">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getDistanceReport();"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n  <ion-card *ngFor="let item of distanceReportData">\n\n\n\n    <ion-item style="border-bottom: 2px solid #dedede;">\n\n      <ion-avatar item-start>\n\n        <img src="assets/imgs/car_red_icon.png">\n\n      </ion-avatar>\n\n      <ion-label>{{item.Device_Name }}</ion-label>\n\n      <ion-badge item-end color="gpsc">{{item.distance | number : \'1.0-2\'}}{{\'Kms\'}}</ion-badge>\n\n    </ion-item>\n\n    <ion-card-content>\n\n      <ion-row style="padding-top: 5px;">\n\n        <ion-col col-1>\n\n          <ion-icon ios="ios-pin" md="md-pin" style="color:#33c45c;font-size:15px;"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-11 style="color:gray;font-size:11px;font-weight: 400;">\n\n          {{item.StartLocation}}\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row style="padding-top: 5px;">\n\n        <ion-col col-1>\n\n          <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:15px;"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-11 style="color:gray;font-size:11px;font-weight: 400;">\n\n          {{item.EndLocation}}\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-content>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\distance-report\distance-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], DistanceReportPage);
    return DistanceReportPage;
}());

//# sourceMappingURL=distance-report.js.map

/***/ }),

/***/ 518:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DistanceReportPageModule", function() { return DistanceReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__distance_report__ = __webpack_require__(1026);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DistanceReportPageModule = /** @class */ (function () {
    function DistanceReportPageModule() {
    }
    DistanceReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__distance_report__["a" /* DistanceReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__distance_report__["a" /* DistanceReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], DistanceReportPageModule);
    return DistanceReportPageModule;
}());

//# sourceMappingURL=distance-report.module.js.map

/***/ })

});
//# sourceMappingURL=36.js.map