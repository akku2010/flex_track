webpackJsonp([25],{

/***/ 1067:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LivePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash__ = __webpack_require__(416);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_Subscription__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_Subscription___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_Subscription__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__live_single_device_live_single_device__ = __webpack_require__(387);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var LivePage = /** @class */ (function () {
    function LivePage(navCtrl, navParams, apiCall, actionSheetCtrl, elementRef, socialSharing, alertCtrl, modalCtrl, plt, viewCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.elementRef = elementRef;
        this.socialSharing = socialSharing;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.plt = plt;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.shouldBounce = true;
        this.dockedHeight = 52;
        this.distanceTop = 150;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.states = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */];
        this.minimumHeight = 52;
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.data = {};
        this.socketSwitch = {};
        this.socketChnl = [];
        this.socketData = {};
        this.allData = {};
        this.isEnabled = false;
        this.showMenuBtn = false;
        this.mapHideTraffic = false;
        this.mapData = [];
        this.geodata = [];
        this.geoShape = [];
        this.locations = [];
        this.shwBckBtn = false;
        this.deviceDeatils = {};
        this.showaddpoibtn = false;
        this.car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
        this.carIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [12.5, 12.5],
        };
        this.icons = {
            "car": this.carIcon,
            "bike": this.carIcon,
            "truck": this.carIcon,
            "bus": this.carIcon,
            "user": this.carIcon,
            "jcb": this.carIcon,
            "tractor": this.carIcon
        };
        this.resumeListener = new __WEBPACK_IMPORTED_MODULE_8_rxjs_Subscription__["Subscription"]();
        var selectedMapKey;
        if (localStorage.getItem('MAP_KEY') != null) {
            selectedMapKey = localStorage.getItem('MAP_KEY');
            if (selectedMapKey == 'Hybrid') {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
            else if (selectedMapKey == 'Normal') {
                this.mapKey = 'MAP_TYPE_NORMAL';
            }
            else if (selectedMapKey == 'Terrain') {
                this.mapKey = 'MAP_TYPE_TERRAIN';
            }
            else if (selectedMapKey == 'Satellite') {
                this.mapKey = 'MAP_TYPE_SATELLITE';
            }
        }
        else {
            this.mapKey = 'MAP_TYPE_NORMAL';
        }
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> " + JSON.stringify(this.userdetails));
        this.menuActive = false;
    }
    LivePage.prototype.goBack = function () {
        this.navCtrl.pop({ animate: true, direction: 'forward' });
    };
    LivePage.prototype.onClickMainMenu = function (item) {
        this.menuActive = !this.menuActive;
    };
    LivePage.prototype.refreshMe = function () {
        this.ngOnDestroy();
        this.ngOnInit();
    };
    LivePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.plt.is('ios')) {
            this.shwBckBtn = true;
            this.viewCtrl.showBackButton(false);
        }
        this.plt.ready().then(function () {
            _this.resumeListener = _this.plt.resume.subscribe(function () {
                var today, Christmas;
                today = new Date();
                Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
                var diffMs = (today - Christmas); // milliseconds between now & Christmas
                var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
                if (diffMins >= 5) {
                    localStorage.removeItem("backgroundModeTime");
                    _this.alertCtrl.create({
                        message: 'Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?',
                        buttons: [
                            {
                                text: 'YES PROCEED',
                                handler: function () {
                                    // this.getdevices();
                                }
                            },
                            {
                                text: 'Back',
                                handler: function () {
                                    _this.navCtrl.setRoot('DashboardPage');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    };
    LivePage.prototype.ionViewWillLeave = function () {
        var _this = this;
        this.plt.ready().then(function () {
            _this.resumeListener.unsubscribe();
        });
    };
    LivePage.prototype.newMap = function () {
        var mapOptions = {
            controls: {
                zoom: false
            },
            mapTypeControlOptions: {
                mapTypeIds: [__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].ROADMAP, 'map_style']
            },
            gestures: {
                rotate: false,
                tilt: false
            },
            mapType: this.mapKey
        };
        var map = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas', mapOptions);
        if (this.plt.is('android')) {
            map.animateCamera({
                target: { lat: 20.5937, lng: 78.9629 },
                duration: 2000,
            });
            map.setPadding(20, 20, 20, 20);
        }
        return map;
    };
    LivePage.prototype.onClickMap = function (maptype) {
        var that = this;
        if (maptype == 'SATELLITE') {
            that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].SATELLITE);
        }
        else {
            if (maptype == 'TERRAIN') {
                that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].TERRAIN);
            }
            else {
                if (maptype == 'NORMAL') {
                    that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].NORMAL);
                }
            }
        }
    };
    LivePage.prototype.settings = function () {
        var _this = this;
        var that = this;
        var profileModal = this.modalCtrl.create('DeviceSettingsPage', {
            param: that.deviceDeatils
        });
        profileModal.present();
        profileModal.onDidDismiss(function () {
            for (var i = 0; i < that.socketChnl.length; i++)
                that._io.removeAllListeners(that.socketChnl[i]);
            var paramData = _this.navParams.get("device");
            _this.temp(paramData);
        });
    };
    LivePage.prototype.addPOI = function () {
        var _this = this;
        var that = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__live_single_device_live_single_device__["b" /* PoiPage */], {
            param1: that.allData[that.impkey]
        });
        modal.onDidDismiss(function (data) {
            console.log(data);
            var that = _this;
            that.showaddpoibtn = false;
        });
        modal.present();
    };
    LivePage.prototype.onSelectMapOption = function (type) {
        var that = this;
        if (type == 'locateme') {
            that.allData.map.setCameraTarget(that.latlngCenter);
        }
        else {
            if (type == 'mapHideTraffic') {
                that.mapHideTraffic = !that.mapHideTraffic;
                if (that.mapHideTraffic) {
                    that.allData.map.setTrafficEnabled(true);
                }
                else {
                    that.allData.map.setTrafficEnabled(false);
                }
            }
            else {
                if (type == 'showGeofence') {
                    console.log("Show Geofence");
                    if (that.generalPolygon != undefined) {
                        that.generalPolygon.remove();
                        that.generalPolygon = undefined;
                    }
                    else {
                        that.callGeofence();
                    }
                }
            }
        }
    };
    LivePage.prototype.callGeofence = function () {
        var _this = this;
        this.geoShape = [];
        this.apiCall.startLoading().present();
        this.apiCall.getGeofenceCall(this.userdetails._id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("geofence data=> " + data.length);
            if (data.length > 0) {
                _this.geoShape = data.map(function (d) {
                    return d.geofence.coordinates[0];
                });
                for (var g = 0; g < _this.geoShape.length; g++) {
                    for (var v = 0; v < _this.geoShape[g].length; v++) {
                        _this.geoShape[g][v] = _this.geoShape[g][v].reverse();
                    }
                }
                for (var t = 0; t < _this.geoShape.length; t++) {
                    _this.drawPolygon(_this.geoShape[t]);
                }
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    message: 'No gofence found..!!',
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LivePage.prototype.drawPolygon = function (polyData) {
        var _this = this;
        var that = this;
        that.geodata = [];
        that.geodata = polyData.map(function (d) {
            return { lat: d[0], lng: d[1] };
        });
        console.log("geodata=> ", that.geodata);
        // let bounds = new LatLngBounds(that.geodata);
        // that.allData.map.moveCamera({
        // target: bounds
        // });
        var GORYOKAKU_POINTS = that.geodata;
        console.log("GORYOKAKU_POINTS=> ", GORYOKAKU_POINTS);
        that.allData.map.addPolygon({
            'points': GORYOKAKU_POINTS,
            'strokeColor': '#AA00FF',
            'fillColor': '#00FFAA',
            'strokeWidth': 2
        }).then(function (polygon) {
            // polygon.on(GoogleMapsEvent.POLYGON_CLICK).subscribe((param) => {
            //   console.log("polygon data=> " + param)
            //   console.log("polygon data=> " + param[1])
            // })
            _this.generalPolygon = polygon;
        });
    };
    LivePage.prototype.ngOnInit = function () {
        if (localStorage.getItem("SCREEN") != null) {
            if (localStorage.getItem("SCREEN") === 'live') {
                this.showMenuBtn = true;
            }
        }
        this._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"]('https://www.oneqlik.in/gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        // this.drawerHidden = true;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Bottom;
        this.onClickShow = false;
        this.showBtn = false;
        this.SelectVehicle = "Select Vehicle";
        this.selectedVehicle = undefined;
        this.userDevices();
    };
    LivePage.prototype.ngOnDestroy = function () {
        var _this = this;
        for (var i = 0; i < this.socketChnl.length; i++)
            this._io.removeAllListeners(this.socketChnl[i]);
        this._io.on('disconnect', function () {
            _this._io.open();
        });
    };
    LivePage.prototype.shareLive = function () {
        var _this = this;
        var data = {
            id: this.liveDataShare._id,
            imei: this.liveDataShare.Device_ID,
            sh: this.userdetails._id,
            ttl: 60 // set to 1 hour by default
        };
        this.apiCall.startLoading().present();
        this.apiCall.shareLivetrackCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.resToken = data.t;
            _this.liveShare();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LivePage.prototype.getPOIs = function () {
        var _this = this;
        this.allData._poiData = [];
        var _burl = "https://www.oneqlik.in/poi/getPois?user=" + this.userdetails._id;
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(_burl)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.allData._poiData = data;
            _this.plotPOI();
        });
    };
    LivePage.prototype.plotPOI = function () {
        var toast = this.toastCtrl.create({
            message: "Plotting POI's please wait...",
            position: 'middle'
        });
        for (var v = 0; v < this.allData._poiData.length; v++) {
            toast.present();
            if (this.allData._poiData[v].poi.location.coordinates !== undefined) {
                this.allData.map.addMarker({
                    title: this.allData._poiData[v].poi.poiname,
                    position: {
                        lat: this.allData._poiData[v].poi.location.coordinates[1],
                        lng: this.allData._poiData[v].poi.location.coordinates[0]
                    },
                    icon: './assets/imgs/poiFlag.png',
                    animation: __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["c" /* GoogleMapsAnimation */].BOUNCE
                });
            }
        }
        toast.dismiss();
    };
    LivePage.prototype.liveShare = function () {
        var that = this;
        var link = "https://www.oneqlik.in/share/liveShare?t=" + that.resToken;
        that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
    };
    LivePage.prototype.zoomin = function () {
        var that = this;
        that.allData.map.moveCameraZoomIn();
    };
    LivePage.prototype.zoomout = function () {
        var that = this;
        that.allData.map.animateCameraZoomOut();
    };
    LivePage.prototype.userDevices = function () {
        var baseURLp;
        var that = this;
        if (that.allData.map != undefined) {
            that.allData.map.remove();
            that.allData.map = that.newMap();
        }
        else {
            that.allData.map = that.newMap();
        }
        baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;
        // baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUserDropdown?email=' + this.userdetails.email + '&id=' + this.userdetails._id;
        if (this.userdetails.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.userdetails._id;
        }
        else {
            if (this.userdetails.isDealer == true) {
                baseURLp += '&dealer=' + this.userdetails._id;
            }
        }
        that.apiCall.startLoading().present();
        // that.apiCall.getdevicesApi(that.userdetails._id, that.userdetails.email)
        that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (resp) {
            that.apiCall.stopLoading();
            that.portstemp = resp.devices;
            console.log("list of vehicles :", that.portstemp);
            that.mapData = [];
            that.mapData = that.portstemp.map(function (d) {
                if (d.last_location != undefined) {
                    return { lat: d.last_location['lat'], lng: d.last_location['long'] };
                }
            });
            var bounds = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["g" /* LatLngBounds */](that.mapData);
            that.allData.map.moveCamera({
                target: bounds,
                zoom: 10
            });
            // this.innerFunc(that.portstemp);
            for (var i = 0; i < resp.devices.length; i++) {
                console.log("status: ", resp.devices[i].status);
                if (resp.devices[i].status != "Expired")
                    that.socketInit(resp.devices[i]);
            }
        }, function (err) {
            that.apiCall.stopLoading();
            console.log(err);
        });
    };
    LivePage.prototype.parseMillisecondsIntoReadableTime = function (milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;
        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
        // return h + ':' + m;
        return h + ':' + m + ':' + s;
    };
    LivePage.prototype.socketInit = function (pdata, center) {
        if (center === void 0) { center = false; }
        var that = this;
        that._io.emit('acc', pdata.Device_ID);
        that.socketChnl.push(pdata.Device_ID + 'acc');
        that._io.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        return;
                    }
                    if (data._id != undefined && data.last_location != undefined) {
                        var key = data._id;
                        that.impkey = data._id;
                        that.deviceDeatils = data;
                        var ic_1 = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](that.icons[data.iconType]);
                        if (!ic_1) {
                            return;
                        }
                        ic_1.path = null;
                        if (data.status.toLowerCase() === 'running' && data.last_speed > 0) {
                            if (that.plt.is('ios')) {
                                ic_1.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
                            }
                            else if (that.plt.is('android')) {
                                ic_1.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
                            }
                        }
                        else {
                            if (data.status.toLowerCase() === 'running' && data.last_speed === 0) {
                                if (that.plt.is('ios')) {
                                    ic_1.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                                }
                                else if (that.plt.is('android')) {
                                    ic_1.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                                }
                            }
                            else {
                                if (that.plt.is('ios')) {
                                    ic_1.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                                }
                                else if (that.plt.is('android')) {
                                    ic_1.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                                }
                            }
                        }
                        console.log("ic url=> " + ic_1.url);
                        that.vehicle_speed = data.last_speed;
                        that.todays_odo = data.today_odo;
                        that.total_odo = data.total_odo;
                        // that.fuel = data.currentFuel;
                        if (that.userdetails.fuel_unit == 'LITRE') {
                            that.fuel = data.currentFuel;
                        }
                        else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
                            that.fuel = data.fuel_percent;
                        }
                        else {
                            that.fuel = data.currentFuel;
                        }
                        that.last_ping_on = data.last_ping_on;
                        if (data.lastStoppedAt != null) {
                            var fd = new Date(data.lastStoppedAt).getTime();
                            var td = new Date().getTime();
                            var time_difference = td - fd;
                            var total_min = time_difference / 60000;
                            var hours = total_min / 60;
                            var rhours = Math.floor(hours);
                            var minutes = (hours - rhours) * 60;
                            var rminutes = Math.round(minutes);
                            that.lastStoppedAt = rhours + ':' + rminutes;
                        }
                        else {
                            that.lastStoppedAt = '00' + ':' + '00';
                        }
                        that.distFromLastStop = data.distFromLastStop;
                        if (!isNaN(data.timeAtLastStop)) {
                            that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
                        }
                        else {
                            that.timeAtLastStop = '00:00:00';
                        }
                        that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
                        that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
                        that.last_ACC = data.last_ACC;
                        that.acModel = data.ac;
                        that.currentFuel = data.currentFuel;
                        that.power = data.power;
                        that.gpsTracking = data.gpsTracking;
                        that.recenterMeLat = data.last_location.lat;
                        that.recenterMeLng = data.last_location.long;
                        if (that.allData[key]) {
                            that.socketSwitch[key] = data;
                            if (that.allData[key].mark != undefined) {
                                that.allData[key].mark.setIcon(ic_1);
                                that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                                var temp = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](that.allData[key].poly[1]);
                                that.allData[key].poly[0] = __WEBPACK_IMPORTED_MODULE_5_lodash__["cloneDeep"](temp);
                                that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                that.liveTrack(that.allData.map, that.allData[key].mark, ic_1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                            }
                            else {
                                return;
                            }
                        }
                        else {
                            that.allData[key] = {};
                            that.socketSwitch[key] = data;
                            that.allData[key].poly = [];
                            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            if (data.last_location != undefined) {
                                that.allData.map.addMarker({
                                    title: data.Device_Name,
                                    position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                                    icon: ic_1,
                                }).then(function (marker) {
                                    that.allData[key].mark = marker;
                                    // if (that.selectedVehicle == undefined) {
                                    //   marker.showInfoWindow();
                                    // }
                                    // if (that.selectedVehicle != undefined) {
                                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                                        .subscribe(function (e) {
                                        if (that.selectedVehicle == undefined) {
                                            that.getAddressTitle(marker);
                                        }
                                        else {
                                            that.liveVehicleName = data.Device_Name;
                                            // that.drawerHidden = false;
                                            that.showaddpoibtn = true;
                                            that.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Docked;
                                            that.onClickShow = true;
                                        }
                                    });
                                    // }
                                    var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                    that.liveTrack(that.allData.map, that.allData[key].mark, ic_1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                                });
                            }
                        }
                        // if (that.selectedVehicle != undefined) {
                        //   var geocoder = new google.maps.Geocoder;
                        //   var latlng = new google.maps.LatLng(data.last_location['lat'], data.last_location['long']);
                        //   var request = {
                        //     "latLng": latlng
                        //   };
                        //   geocoder.geocode(request, function (resp, status) {
                        //     if (status == google.maps.GeocoderStatus.OK) {
                        //       if (resp[0] != null) {
                        //         that.address = resp[0].formatted_address;
                        //       } else {
                        //         console.log("No address available")
                        //       }
                        //     }
                        //     else {
                        //       that.address = 'N/A';
                        //     }
                        //   })
                        // }
                    }
                })(d4);
        });
    };
    LivePage.prototype.reCenterMe = function () {
        // console.log("getzoom level: " + this.allData.map.getCameraZoom());
        this.allData.map.moveCamera({
            target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
            zoom: this.allData.map.getCameraZoom()
        }).then(function () {
        });
    };
    LivePage.prototype.getAddressTitle = function (marker) {
        var payload = {
            "lat": marker.getPosition().lat,
            "long": marker.getPosition().lng,
            "api_id": "1"
        };
        var addr;
        this.apiCall.getAddressApi(payload)
            .subscribe(function (data) {
            console.log("got address: " + data.results);
            if (data.results[2] != undefined || data.results[2] != null) {
                addr = data.results[2].formatted_address;
            }
            else {
                addr = 'N/A';
            }
            marker.setSnippet(addr);
        });
    };
    LivePage.prototype.addCluster = function () {
        var that = this;
        console.log("locations length=> " + that.locations.length);
        console.log("locations=> " + that.locations);
        var icicon;
        if (that.plt.is('ios')) {
            icicon = "www/assets/imgs/maps/m1.png";
        }
        else if (that.plt.is('android')) {
            icicon = "./assets/imgs/maps/m1.png";
        }
        that.allData.map.addMarkerCluster({
            markers: that.locations,
            icons: [
                { min: 50, max: 100, url: icicon, anchor: { x: 16, y: 16 } }
            ]
        })
            .then(function (markerCluster) {
            // markerCluster.on(GoogleMapsEvent.CLUSTER_CLICK).subscribe((cluster: any) => {
            //   console.log('cluster was clicked.');
            // });
        });
    };
    LivePage.prototype.liveTrack = function (map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {
        var target = 0;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        if (center) {
            map.setCameraTarget(coords[0]);
        }
        function _goToPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](coords[target].lat, coords[target].lng);
            var distance = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
                mark.setIcon(icons);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.selectedVehicle != undefined) {
                        map.setCameraTarget(new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    }
                    that.latlngCenter = new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng);
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    head = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.selectedVehicle != undefined) {
                        map.setCameraTarget(dest);
                    }
                    that.latlngCenter = dest;
                    mark.setPosition(dest);
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    };
    LivePage.prototype.temp = function (data) {
        var _this = this;
        // debugger
        if (data.status == 'Expired') {
            var profileModal = this.modalCtrl.create('ExpiredPage');
            profileModal.present();
            profileModal.onDidDismiss(function () {
                _this.selectedVehicle = undefined;
            });
        }
        else {
            var that = this;
            that.liveDataShare = data;
            // that.drawerHidden = true;
            that.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Bottom;
            that.onClickShow = false;
            if (that.allData.map != undefined) {
                // that.allData.map.clear();
                that.allData.map.remove();
            }
            console.log("on select change data=> " + JSON.stringify(data));
            for (var i = 0; i < that.socketChnl.length; i++)
                that._io.removeAllListeners(that.socketChnl[i]);
            that.allData = {};
            that.socketChnl = [];
            that.socketSwitch = {};
            if (data) {
                if (data.last_location) {
                    var mapOptions = {
                        // backgroundColor: 'white',
                        controls: {
                            compass: true,
                            zoom: false,
                            mapToolbar: true
                        },
                        gestures: {
                            rotate: false,
                            tilt: false
                        },
                        mapType: that.mapKey
                    };
                    var map = __WEBPACK_IMPORTED_MODULE_6__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas', mapOptions);
                    map.animateCamera({
                        target: { lat: 20.5937, lng: 78.9629 },
                        zoom: 15,
                        duration: 1000,
                        padding: 0 // default = 20px
                    });
                    map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
                    map.setPadding(20, 20, 20, 20);
                    // map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
                    that.allData.map = map;
                    that.socketInit(data);
                }
                else {
                    that.allData.map = that.newMap();
                    that.socketInit(data);
                }
                if (that.selectedVehicle != undefined) {
                    // that.drawerHidden = false;
                    that.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Docked;
                    that.onClickShow = true;
                }
            }
            that.showBtn = true;
        }
    };
    LivePage.prototype.info = function (mark, cb) {
        mark.addListener('click', cb);
    };
    LivePage.prototype.setDocHeight = function () {
        var that = this;
        that.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Top;
    };
    LivePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-live',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\live\live.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button *ngIf="showMenuBtn" ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-buttons start *ngIf="shwBckBtn">\n\n      <button ion-button small (click)="goBack()">\n\n        <ion-icon name="arrow-back"></ion-icon>Back\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>Live Tracking\n\n      <span *ngIf="titleText">for {{ titleText }}</span>\n\n    </ion-title>\n\n    <ion-buttons end *ngIf="showBtn">\n\n      <button ion-button (click)="ngOnInit()">\n\n        All Vehicles\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content style="background-color: #f2f1f0">\n\n  <ion-item *ngIf="!titleText">\n\n    <ion-label>{{ SelectVehicle }}</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="temp(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n  <div id="map_canvas">\n\n    <ion-row *ngIf="selectedVehicle != undefined">\n\n      <ion-col>\n\n        <ion-fab top left>\n\n          <button ion-fab color="light" mini (click)="onClickMainMenu()">\n\n            <ion-icon color="gpsc" name="map"></ion-icon>\n\n          </button>\n\n          <ion-fab-list side="bottom">\n\n            <button style="margin: 0 2px;" ion-fab mini (click)="onClickMap(\'SATELLITE\')" color="gpsc">\n\n              S\n\n            </button>\n\n            <button style="margin: 1px 0px 1px 0px;" ion-fab mini (click)="onClickMap(\'HYBRID\')" color="gpsc">\n\n              H\n\n            </button>\n\n            <button style="margin: 1px 0px 1px 0px;" ion-fab mini (click)="onClickMap(\'TERRAIN\')" color="gpsc">\n\n              T\n\n            </button>\n\n            <button style="margin: 1px 0px 1px 0px;" ion-fab mini (click)="onClickMap(\'NORMAL\')" color="gpsc">\n\n              N\n\n            </button>\n\n          </ion-fab-list>\n\n        </ion-fab>\n\n      </ion-col>\n\n      <ion-col>\n\n        <p class="blink" style="text-align: center;font-size:24px;color:cornflowerblue;font-weight: 600;"\n\n          *ngIf="!vehicle_speed">\n\n          0\n\n          <span style="font-size:16px;">Km/hr</span>\n\n        </p>\n\n        <p class="blink" style="text-align: center;font-size:24px;color: green;font-weight: 600;"\n\n          *ngIf="vehicle_speed <= 60">\n\n          {{ vehicle_speed }}\n\n          <span style="font-size:16px;">Km/hr</span>\n\n        </p>\n\n        <p class="blink" style="text-align: center;font-size:24px;color: red;font-weight: 600;"\n\n          *ngIf="vehicle_speed > 60">\n\n          {{ vehicle_speed }}\n\n          <span style="font-size:16px;">Km/hr</span>\n\n        </p>\n\n      </ion-col>\n\n      <ion-col>\n\n        <ion-fab top right>\n\n          <button ion-fab color="gpsc" mini>\n\n            <ion-icon name="arrow-dropdown"></ion-icon>\n\n          </button>\n\n          <ion-fab-list side="bottom">\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="shareLive($event)">\n\n              <ion-icon name="share"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="onSelectMapOption(\'mapTrail\')">\n\n              <ion-icon name="eye"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="onSelectMapOption(\'mapHideTraffic\')">\n\n              <ion-icon name="walk"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="settings()">\n\n              <ion-icon name="cog"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="addPOI()">\n\n              <ion-icon name="flag"></ion-icon>\n\n            </button>\n\n          </ion-fab-list>\n\n        </ion-fab>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-fab right style="margin-top:59%" *ngIf="selectedVehicle != undefined">\n\n      <button ion-fab color="gpsc" mini (click)="reCenterMe()">\n\n        <ion-icon name="locate"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab right style="margin-top:72%" *ngIf="selectedVehicle != undefined">\n\n      <button color="gpsc" ion-fab (click)="zoomin()" mini>\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab right style="margin-top:85%" *ngIf="selectedVehicle != undefined">\n\n      <button color="gpsc" ion-fab (click)="zoomout()" mini>\n\n        <ion-icon name="remove"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n    <ion-fab left style="margin-top:85%" *ngIf="selectedVehicle != undefined">\n\n      <button ion-fab color="gpsc" mini (click)="refreshMe()">\n\n        <ion-icon name="refresh"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab top left *ngIf="selectedVehicle == undefined">\n\n      <button ion-fab color="light" mini>\n\n        <ion-icon color="gpsc" name="map"></ion-icon>\n\n      </button>\n\n      <ion-fab-list side="bottom">\n\n        <button ion-fab (click)="onClickMap(\'SATELLITE\')" color="gpsc">\n\n          S\n\n        </button>\n\n        <button ion-fab (click)="onClickMap(\'TERRAIN\')" color="gpsc">\n\n          T\n\n        </button>\n\n        <button ion-fab (click)="onClickMap(\'NORMAL\')" color="gpsc">\n\n          N\n\n        </button>\n\n      </ion-fab-list>\n\n    </ion-fab>\n\n\n\n    <ion-fab top right *ngIf="selectedVehicle == undefined">\n\n      <button ion-fab mini (click)="onSelectMapOption(\'mapHideTraffic\')"\n\n        color="{{ mapHideTraffic ? \'light\' : \'gpsc\' }}">\n\n        <ion-icon name="walk" color="{{ mapHideTraffic ? \'dark-grey\' : \'black\' }}"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 15%"\n\n      *ngIf="selectedVehicle == undefined">\n\n      <button ion-fab mini (click)="onSelectMapOption(\'showGeofence\')" color="gpsc">\n\n        <img src="assets/imgs/geo.png" />\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 28%"\n\n      *ngIf="selectedVehicle == undefined">\n\n      <button ion-fab mini (click)="getPOIs()" color="gpsc">\n\n        <ion-icon name="pin"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <!-- <ion-fab\n\n      style="right: calc(10px + env(safe-area-inset-right)); margin-top: 73%"\n\n      *ngIf="selectedVehicle != undefined"\n\n    >\n\n      <button ion-fab mini (click)="zoomin()" color="gpsc">\n\n        <ion-icon name="add" color="black"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab\n\n      style="right: calc(10px + env(safe-area-inset-right)); margin-top: 85%"\n\n      *ngIf="selectedVehicle != undefined"\n\n    >\n\n      <button ion-fab mini (click)="zoomout()" color="gpsc">\n\n        <ion-icon name="remove" color="black"></ion-icon>\n\n      </button>\n\n    </ion-fab> -->\n\n\n\n    <!-- <ion-fab top right *ngIf="selectedVehicle != undefined">\n\n      <button ion-fab mini color="light">\n\n        <ion-icon name="arrow-dropdown-circle" color="gpsc"></ion-icon>\n\n      </button>\n\n      <ion-fab-list side="bottom">\n\n        <button\n\n          ion-fab\n\n          (click)="onSelectMapOption(\'mapHideTraffic\')"\n\n          color="{{ mapHideTraffic ? \'light\' : \'gpsc\' }}"\n\n        >\n\n          <ion-icon\n\n            name="walk"\n\n            color="{{ mapHideTraffic ? \'dark-grey\' : \'black\' }}"\n\n          ></ion-icon>\n\n        </button>\n\n      </ion-fab-list>\n\n    </ion-fab> -->\n\n  </div>\n\n</ion-content>\n\n<div *ngIf="onClickShow" class="divPlan">\n\n  <ion-bottom-drawer [(state)]="drawerState" [dockedHeight]="dockedHeight" [shouldBounce]="shouldBounce"\n\n    [distanceTop]="distanceTop" [minimumHeight]="minimumHeight" (click)="setDocHeight()">\n\n    <div class="drawer-content">\n\n      <p padding-left style="font-size: 13px; padding-bottom: 3px; color:cornflowerblue;">\n\n        Last Updated On &mdash;\n\n        {{ last_ping_on | date: "medium" }}\n\n      </p>\n\n      <!-- <p padding-left style="font-size: 13px" *ngIf="!address">N/A</p>\n\n      <p padding-left style="font-size: 13px" *ngIf="address">{{ address }}</p> -->\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/ign_red.png" *ngIf="last_ACC == \'0\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ign_green.png" *ngIf="last_ACC == \'1\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ign_idle.png" *ngIf="last_ACC == null" width="20" height="20" />\n\n          <p>IGN</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/ac_na.png" *ngIf="acModel == null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ac_present.png" *ngIf="acModel == \'1\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ac_empty.png" *ngIf="acModel == \'0\'" width="20" height="20" />\n\n          <p>AC</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/fuel_available.png" *ngIf="currentFuel != null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/fuel_empty.png" *ngIf="currentFuel == null" width="20" height="20" />\n\n          <p>Fuel</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/power_na.png" *ngIf="power == null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/pwer_disconnected.png" *ngIf="power == \'0\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/power_connected.png" *ngIf="power == \'1\'" width="20" height="20" />\n\n          <p>Power</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/gps_na.png" *ngIf="gpsTracking == null" width="30" height="20" />\n\n          <img src="assets/imgs/statusIcons/gps_notPresent.png" *ngIf="gpsTracking == \'0\'" width="30" height="20" />\n\n          <img src="assets/imgs/statusIcons/gps_present.png" *ngIf="gpsTracking == \'1\'" width="30" height="20" />\n\n          <p>GPS</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!total_odo">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="total_odo">\n\n            {{ total_odo | number: "1.0-2" }}\n\n          </p>\n\n          <p style="font-size: 13px">Odometer</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!vehicle_speed">\n\n            0km/h\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="vehicle_speed">\n\n            {{ vehicle_speed }}km/h\n\n          </p>\n\n          <p style="font-size: 13px">\n\n            Speed\n\n          </p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="fuel">\n\n            {{ fuel }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!fuel">N/A</p>\n\n          <p style="font-size: 13px">Fuel</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!distFromLastStop">\n\n            0Kms\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="distFromLastStop">\n\n            {{ distFromLastStop | number: "1.0-2" }}Kms\n\n          </p>\n\n          <p style="font-size: 13px">From Last Stop</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/milestone-png-1.png" width="30" height="35" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!todays_odo">\n\n            0Kms\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="todays_odo">\n\n            {{ todays_odo | number: "1.0-2" }}Kms\n\n          </p>\n\n          <p style="font-size: 13px">Total</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!timeAtLastStop">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="timeAtLastStop">\n\n            {{ timeAtLastStop }}\n\n          </p>\n\n          <p style="font-size: 13px">At Last Stop</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/parkinglogo.png" width="30" height="30" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!today_stopped">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="today_stopped">\n\n            {{ today_stopped }}\n\n          </p>\n\n          <p style="font-size: 13px">Total</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!lastStoppedAt">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="lastStoppedAt">\n\n            {{ lastStoppedAt }}\n\n          </p>\n\n          <p style="font-size: 13px">From Last Stop</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/2530791.png" width="30" height="35" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!today_running">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="today_running">\n\n            {{ today_running }}\n\n          </p>\n\n          <p style="font-size: 13px">Total</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    </div>\n\n  </ion-bottom-drawer>\n\n</div>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\live\live.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], LivePage);
    return LivePage;
}());

//# sourceMappingURL=live.js.map

/***/ }),

/***/ 560:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LivePageModule", function() { return LivePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__live__ = __webpack_require__(1067);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ion_bottom_drawer__ = __webpack_require__(123);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var LivePageModule = /** @class */ (function () {
    function LivePageModule() {
    }
    LivePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__live__["a" /* LivePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__live__["a" /* LivePage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__node_modules_ion_bottom_drawer__["b" /* IonBottomDrawerModule */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], LivePageModule);
    return LivePageModule;
}());

//# sourceMappingURL=live.module.js.map

/***/ })

});
//# sourceMappingURL=25.js.map