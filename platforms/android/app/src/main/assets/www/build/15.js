webpackJsonp([15],{

/***/ 1045:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupOtpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_android_permissions__ = __webpack_require__(411);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SignupOtpPage = /** @class */ (function () {
    function SignupOtpPage(navCtrl, navParams, androidPermissions, formBuilder, apiService, platform, toastCtrl, alerCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.androidPermissions = androidPermissions;
        this.formBuilder = formBuilder;
        this.apiService = apiService;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.viewCtrl = viewCtrl;
        this.mobileNum = localStorage.getItem('mobnum');
        this.signupForm = formBuilder.group({
            otp1: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            otp2: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            otp3: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            otp4: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    // ionViewWillEnter() {
    //     this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(   
    //       success => console.log('Permission granted'),
    //       err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
    //     );  
    //     this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);     
    //     this.autoOtpReader();
    //   }
    // autoOtpReader() {
    //     this.platform.ready().then((readySource) => {
    //         if (SMS) SMS.startWatch(() => {
    //             console.log('watching started');
    //         }, Error => {
    //             console.log('failed to start watching');
    //         });
    //         document.addEventListener('onSMSArrive', (e: any) => {
    //             var sms = e.data;
    //             console.log(sms);
    //             this.fetchOtp = e.data.body.match(/[0-9]+/g);
    //             console.log(this.fetchOtp);
    //             this.Otp = this.fetchOtp[0];
    //             console.log(this.Otp);
    //         })
    //     })
    // }
    SignupOtpPage.prototype.getCodeBoxElement = function (index) {
        var inputValue = document.getElementById('codeBox' + index);
        return inputValue;
    };
    SignupOtpPage.prototype.onKeyUpEvent = function (index, event) {
        var eventCode = event.which || event.keyCode;
        if (this.getCodeBoxElement(index).value.length === 1) {
            if (index !== 4) {
                this.getCodeBoxElement(index + 1).focus();
            }
            else {
                this.getCodeBoxElement(index).blur();
            }
        }
        if (eventCode === 8 && index !== 1) {
            this.getCodeBoxElement(index - 1).focus();
        }
    };
    SignupOtpPage.prototype.onFocusEvent = function (index) {
        for (var item = 1; item < index; item++) {
            var currentElement = this.getCodeBoxElement(item);
            if (!currentElement.value) {
                currentElement.focus();
                break;
            }
        }
    };
    SignupOtpPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    SignupOtpPage.prototype.signupUser = function () {
        var _this = this;
        var otp = this.signupForm.value.otp1.toString() + this.signupForm.value.otp2.toString() + this.signupForm.value.otp3.toString() + this.signupForm.value.otp4.toString();
        var usersignup = {
            "phone": this.mobileNum,
            "otp": otp
        };
        this.apiService.startLoading();
        this.apiService.signupApi(usersignup)
            .subscribe(function (data) {
            _this.apiService.stopLoading();
            _this.signupverify = data;
            _this.signupverify.phoneNum = _this.mobileNum;
            var toast = _this.toastCtrl.create({
                message: 'Mobile Verified',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.navCtrl.push("LoginPage");
            });
            toast.present();
        }, function (err) {
            _this.apiService.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var toastErr = _this.toastCtrl.create({
                message: msg.message,
                position: 'top',
                duration: 2000
            });
            toastErr.present();
        });
    };
    SignupOtpPage.prototype.resendOtp = function () {
        var _this = this;
        var phoneNumber = {
            ph_num: this.mobileNum
        };
        this.apiService.resendOtp(phoneNumber)
            .subscribe(function (res) {
            if (res) {
                var toast = _this.toastCtrl.create({
                    message: "OTP sent successfully",
                    duration: 2000,
                    position: "top"
                });
                toast.present();
            }
        }, function (err) {
            console.log(err);
        });
    };
    SignupOtpPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    SignupOtpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-signup',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\signup\signup-otp\signup-otp.html"*/'<ion-content no-padding class="no-scroll">\n\n    <div width="100%" height="100%">\n\n        <div style="background: #1C3F75;padding-bottom: 10%;" height="50%">\n\n            <button ion-button (click)="back()" clear item-end style="color:white;">\n\n                <ion-icon name="md-arrow-back"></ion-icon>\n\n            </button>\n\n            <ion-card style="margin: 0px;width: 28%;border-radius: 15px; padding-top: 0%;margin-left: 37%;">\n\n                <img src="assets/imgs/icon.png" style="width: 60%;margin: 18%;">\n\n            </ion-card>\n\n            <h5\n\n                style="color: white;font-weight: 500;text-align: center;font-size: 3.6rem;padding-top: 0%;padding-bottom: 7%;">\n\n                SIGN UP</h5>\n\n        </div>\n\n        <div class="row">\n\n            <ion-card style="margin-top: -12%;border-radius: 4%;height: auto;" class="col-sm-12 col-12 col-md-12">\n\n                <p style="font-size: 1.7rem;margin: 25px;color:#605b5b">Verify Code</p>\n\n                <p style="margin: 25px;margin-bottom: 0px;color: grey;">Please type verification code send to your\n\n                    mobile no. {{mobileNum}}</p>\n\n                <form [formGroup]="signupForm">\n\n                    <div class="otpBox">\n\n                        <input id="codeBox1" type="number" maxlength="1" (keyup)="onKeyUpEvent(1, $event)"\n\n                            (onfocus)="onFocusEvent(1)" formControlName="otp1" />\n\n                        <input id="codeBox2" type="number" maxlength="1" (keyup)="onKeyUpEvent(2, $event)"\n\n                            (onfocus)="onFocusEvent(2)" formControlName="otp2" />\n\n                        <input id="codeBox3" type="number" maxlength="1" (keyup)="onKeyUpEvent(3, $event)"\n\n                            (onfocus)="onFocusEvent(3)" formControlName="otp3" />\n\n                        <input id="codeBox4" type="number" maxlength="1" (keyup)="onKeyUpEvent(4, $event)"\n\n                            (onfocus)="onFocusEvent(4)" formControlName="otp4" />\n\n                    </div>\n\n                </form>\n\n            </ion-card>\n\n            <div class="col-sm-12 col-12 col-md-12 " style="margin:auto;text-align: center">\n\n                <button ion-button round\n\n                    style="width: 18.2rem;color:white;background: #1C3F75;margin: -45px 0px 0px -12px;font-weight:400;text-transform: none;"\n\n                    [disabled]="!signupForm.valid" (click)="signupUser()">Submit</button>\n\n                <div style="padding-top: 25px;">\n\n                    <p style="color:grey;margin-left: -13px;">OTP not recieved ?\n\n                        <span style="color: black;margin-left: 5px;" (click)="resendOtp()">Resend OTP</span>\n\n                    </p>\n\n                </div>\n\n            </div>\n\n        </div>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\signup\signup-otp\signup-otp.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], SignupOtpPage);
    return SignupOtpPage;
}());

//# sourceMappingURL=signup-otp.js.map

/***/ }),

/***/ 538:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupOtpPageModule", function() { return SignupOtpPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_otp__ = __webpack_require__(1045);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SignupOtpPageModule = /** @class */ (function () {
    function SignupOtpPageModule() {
    }
    SignupOtpPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__signup_otp__["a" /* SignupOtpPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__signup_otp__["a" /* SignupOtpPage */]),
            ],
        })
    ], SignupOtpPageModule);
    return SignupOtpPageModule;
}());

//# sourceMappingURL=signup-otp.module.js.map

/***/ })

});
//# sourceMappingURL=15.js.map