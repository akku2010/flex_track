webpackJsonp([19],{

/***/ 1041:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_text_to_speech__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SettingsPage = /** @class */ (function () {
    function SettingsPage(events, tts, navCtrl, navParams, alertCtrl, toastCtrl, apiCall) {
        this.events = events;
        this.tts = tts;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.apiCall = apiCall;
        this.fuels = [];
        this.maps = [];
        this.fuels = ['LITRE', 'PERCENTAGE'];
        this.maps = ['Normal', 'Terrain', 'Hybrid', 'Satellite'];
        if (localStorage.getItem("notifValue") != null) {
            this.notif = localStorage.getItem("notifValue");
            if (this.notif == 'true') {
                this.isChecked = true;
            }
            else {
                this.isChecked = false;
            }
        }
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        if (this.islogin.fuel_unit === 'LITRE') {
            this.fuelKey = 'LITRE';
        }
        else {
            this.fuelKey = 'PERCENTAGE';
        }
        if (localStorage.getItem('MAP_KEY') != null) {
            this.selectedMapKey = localStorage.getItem('MAP_KEY');
        }
    }
    SettingsPage.prototype.ngOnInit = function () {
        if (localStorage.getItem("SCREEN") != null) {
            if (localStorage.getItem("SCREEN") == 'live') {
                this.isBooleanforLive = true;
                this.isBooleanforDash = false;
            }
            else {
                if (localStorage.getItem("SCREEN") == 'dashboard') {
                    this.isBooleanforDash = true;
                    this.isBooleanforLive = false;
                }
            }
        }
    };
    SettingsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SettingsPage');
    };
    SettingsPage.prototype.setNotif = function (notif) {
        console.log(notif);
        this.events.publish('notif:updated', notif);
        this.isChecked = notif;
        if (notif === true) {
            this.tts.speak('You have succesfully enabled voice notifications')
                .then(function () { return console.log('Success'); })
                .catch(function (reason) { return console.log(reason); });
        }
    };
    SettingsPage.prototype.rootpage = function () {
        // let alert = this.alertCtrl.create({
        //   title: 'Alert',
        //   subTitle: 'Choose default screen',
        var _this = this;
        // })
        var alert = this.alertCtrl.create();
        // alert.setTitle('Alert');
        alert.setSubTitle('Choose default screen');
        alert.addInput({
            type: 'radio',
            label: 'Dashboard',
            value: 'dashboard',
            checked: this.isBooleanforDash
        });
        alert.addInput({
            type: 'radio',
            label: 'Live Tracking',
            value: 'live',
            checked: this.isBooleanforLive
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: function (data) {
                console.log(data);
                localStorage.setItem("SCREEN", data);
                var toast = _this.toastCtrl.create({
                    message: 'Default page set to ' + data + ' page',
                    duration: 2000
                });
                toast.present();
            }
        });
        alert.present();
    };
    SettingsPage.prototype.onChangeMap = function (key) {
        console.log("map key changed: ", key);
        localStorage.setItem("MAP_KEY", key);
    };
    SettingsPage.prototype.onChangeFuel = function (key) {
        console.log("key changed: ", key);
        var newContact = {
            fname: this.islogin.fn,
            lname: this.islogin.ln,
            org: this.islogin._orgName,
            uid: this.islogin._id,
            fuel_unit: key
        };
        var _baseURl = this.apiCall.mainUrl + "users/Account_Edit";
        this.apiCall.urlpasseswithdata(_baseURl, newContact)
            .subscribe(function (data) {
            console.log("got response data: ", data);
            var logindetails = JSON.parse(JSON.stringify(data));
            var userDetails = window.atob(logindetails.token.split('.')[1]);
            var details = JSON.parse(userDetails);
            localStorage.setItem('details', JSON.stringify(details));
        });
    };
    SettingsPage.prototype.showAlert = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Immobilize Password',
            message: "Enter password for engine cut",
            inputs: [
                {
                    name: 'password',
                    placeholder: 'Password'
                },
                {
                    name: 'cpassword',
                    placeholder: 'Confirm Password'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Save',
                    handler: function (data) {
                        console.log('Saved clicked');
                        console.log("data: ", data);
                        if (data.password !== data.cpassword) {
                            _this.toastmsg("Entered password and confirm password did not match.");
                            return;
                        }
                        _this.setPassword(data);
                    }
                }
            ]
        });
        prompt.present();
    };
    SettingsPage.prototype.toastmsg = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'bottom'
        }).present();
    };
    SettingsPage.prototype.setPassword = function (data) {
        var _this = this;
        var payload = {
            uid: this.islogin._id,
            engine_cut_psd: data.password
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(this.apiCall.mainUrl + "users/set_user_setting", payload)
            .subscribe(function (resp) {
            console.log('response language code ' + resp);
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: resp.message,
                duration: 1500,
                position: 'bottom'
            });
            toast.present();
        });
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-settings',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\profile\settings\settings.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Settings</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-item>\n\n    <ion-icon *ngIf="isChecked" name="mic" item-start></ion-icon>\n\n    <ion-icon *ngIf="!isChecked" name="mic-off" item-start></ion-icon>\n\n    <ion-label>Voice Notification</ion-label>\n\n    <ion-checkbox color="dark" [(ngModel)]="notif" (click)="setNotif(notif)" item-right></ion-checkbox>\n\n  </ion-item>\n\n  <ion-item (tap)="rootpage()">\n\n    <ion-icon name="checkmark-circle-outline" item-start></ion-icon>\n\n    <ion-label>Default Page</ion-label>\n\n  </ion-item>\n\n  <ion-item>\n\n    <ion-icon name="map" item-start></ion-icon>\n\n    <ion-label color="dark">Map Type</ion-label>\n\n    <ion-select [(ngModel)]="mapKey">\n\n      <ion-option *ngFor="let mapKey of maps" [value]="mapKey" (ionSelect)="onChangeMap(mapKey)"\n\n        [selected]="selectedMapKey === mapKey">{{ mapKey }}</ion-option>\n\n    </ion-select>\n\n  </ion-item>\n\n\n\n  <ion-item>\n\n    <ion-icon name="custom-fuel" item-start></ion-icon>\n\n    <ion-label color="dark">Fuel Unit</ion-label>\n\n    <ion-select [(ngModel)]="fuelKey">\n\n      <ion-option *ngFor="let fuelKey of fuels" [value]="fuelKey" (ionSelect)="onChangeFuel(fuelKey)"\n\n        [selected]="islogin.fuel_unit === fuelKey">{{ fuelKey }}</ion-option>\n\n    </ion-select>\n\n  </ion-item>\n\n\n\n  <ion-item (click)="showAlert()">\n\n    <ion-icon name="lock" item-start></ion-icon>\n\n    <ion-label>Set Immobilize Password</ion-label>\n\n  </ion-item>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\profile\settings\settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_text_to_speech__["a" /* TextToSpeech */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 534:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPageModule", function() { return SettingsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings__ = __webpack_require__(1041);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SettingsPageModule = /** @class */ (function () {
    function SettingsPageModule() {
    }
    SettingsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__settings__["a" /* SettingsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__settings__["a" /* SettingsPage */]),
            ],
        })
    ], SettingsPageModule);
    return SettingsPageModule;
}());

//# sourceMappingURL=settings.module.js.map

/***/ })

});
//# sourceMappingURL=19.js.map