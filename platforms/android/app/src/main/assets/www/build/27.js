webpackJsonp([27],{

/***/ 1036:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceSettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DeviceSettingsPage = /** @class */ (function () {
    function DeviceSettingsPage(navCtrl, navParams, viewCtrl, apiCall, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.dData = {};
        this.fmileage = 0;
        this.dData = navParams.get("param");
        console.log("mileage: ", this.dData);
        this.vname = this.dData.Device_Name;
        this.tot_odo = this.fixDecimals(this.dData.total_odo); // for two decimals
        this.speedlimit = this.dData.SpeedLimit;
        this.ingnitionStat = this.dData.ignitionSource;
        this.fmileage = this.dData.Mileage;
    }
    DeviceSettingsPage.prototype.fixDecimals = function (value) {
        value = "" + value;
        value = value.trim();
        value = parseFloat(value).toFixed(2);
        return value;
    };
    // ionViewDidLoad() {
    //   console.log('ionViewDidLoad SettingsPage');
    // }
    DeviceSettingsPage.prototype.radioChecked = function (key) {
        console.log("ignition key=> ", key);
    };
    DeviceSettingsPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    DeviceSettingsPage.prototype.submitSettings = function () {
        var _this = this;
        if (this.speedlimit == undefined || this.ingnitionStat == undefined || this.vname == undefined || this.fmileage == undefined) {
            this.viewCtrl.dismiss();
        }
        else {
            var editData = {
                _id: this.dData._id,
                deviceid: this.dData.Device_ID,
                devicename: this.vname,
                speed: this.speedlimit,
                ignitionSource: this.ingnitionStat,
                total_odo: this.tot_odo,
                Mileage: this.fmileage
            };
            this.apiCall.startLoading().present();
            this.apiCall.deviceupdateCall(editData)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                // console.log("resp data=> " + data.message)
                var toast = _this.toastCtrl.create({
                    message: data.message + " successfully!",
                    duration: 1500,
                    position: "bottom"
                });
                toast.onDidDismiss(function () {
                    _this.viewCtrl.dismiss();
                });
                toast.present();
            }, function (err) {
                console.log(err);
                _this.apiCall.stopLoading();
            });
        }
    };
    DeviceSettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-device-settings',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\live-single-device\device-settings\device-settings.html"*/'<!-- <ion-content padding> -->\n\n  <div padding>\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <p style="text-align: center; font-size: 2rem;">\n\n          <b>Device Settings</b>\n\n         &nbsp;&nbsp;&nbsp; <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n        </p>\n\n      </ion-col>\n\n    </ion-row>\n\n    <br />\n\n    <ion-row>\n\n      <ion-col col-12><b>Vehicle Name:</b></ion-col>\n\n      <ion-col col-12>\n\n        <ion-input type="text" [(ngModel)]="vname"></ion-input>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-12><b>Total ODO:</b></ion-col>\n\n      <ion-col col-12>\n\n        <ion-input type="text" [(ngModel)]="tot_odo"></ion-input>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-12><b>Fuel Mileage:</b></ion-col>\n\n      <ion-col col-12>\n\n        <ion-input type="number" [(ngModel)]="fmileage"></ion-input>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-6><b>Speed Limit:</b></ion-col>\n\n      <ion-col col-6 style="text-align: right">\n\n        <ion-badge color="gpsc" item-end>{{speedlimit}}</ion-badge>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <ion-range min="10" max="500" step="2" color="gpsc" [(ngModel)]="speedlimit">\n\n          <ion-icon small range-left name="speedometer"></ion-icon>\n\n          <ion-icon range-right name="speedometer"></ion-icon>\n\n        </ion-range>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row padding-bottom>\n\n      <ion-col col-12><b>Ignition Detection:</b></ion-col>\n\n    </ion-row>\n\n    <ion-row radio-group [(ngModel)]="ingnitionStat">\n\n      <ion-col col-6>\n\n        <ion-label>Movement</ion-label>\n\n      </ion-col>\n\n      <ion-col col-6 style="text-align: right;">\n\n        <ion-radio color="gpsc" value="MOVEMENT" (ionSelect)="radioChecked(ingnitionStat)"></ion-radio>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row radio-group [(ngModel)]="ingnitionStat">\n\n      <ion-col col-6>\n\n        <ion-label>ACC</ion-label>\n\n      </ion-col>\n\n      <ion-col col-6 style="text-align: right;">\n\n        <ion-radio color="gpsc" value="ACC" (ionSelect)="radioChecked(ingnitionStat)"></ion-radio>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <button ion-button block (tap)="submitSettings()" color="gpsc">SUBMIT</button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </div>\n\n'/*ion-inline-end:"D:\Pro\flex_track\src\pages\live-single-device\device-settings\device-settings.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], DeviceSettingsPage);
    return DeviceSettingsPage;
}());

//# sourceMappingURL=device-settings.js.map

/***/ }),

/***/ 529:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceSettingsPageModule", function() { return DeviceSettingsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__device_settings__ = __webpack_require__(1036);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DeviceSettingsPageModule = /** @class */ (function () {
    function DeviceSettingsPageModule() {
    }
    DeviceSettingsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__device_settings__["a" /* DeviceSettingsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__device_settings__["a" /* DeviceSettingsPage */])
            ],
        })
    ], DeviceSettingsPageModule);
    return DeviceSettingsPageModule;
}());

//# sourceMappingURL=device-settings.module.js.map

/***/ })

});
//# sourceMappingURL=27.js.map