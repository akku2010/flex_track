webpackJsonp([38],{

/***/ 1021:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditDealerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EditDealerPage = /** @class */ (function () {
    function EditDealerPage(apiCall, viewCtrl, formBuilder, navPar, toastCtrl, alerCtrl) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this._dealerData = navPar.get("param");
        console.log("dealer details=> " + JSON.stringify(this._dealerData));
        if (this._dealerData.expire_date == undefined) {
            var tru = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(), 'DD/MM/YYYY').format('YYYY-MM-DD');
            var tempdate = new Date(tru);
            tempdate.setDate(tempdate.getDate() + 365);
            this.yearLater = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(tempdate), 'DD-MM-YYYY').format('YYYY-MM-DD');
        }
        else {
            this.yearLater = __WEBPACK_IMPORTED_MODULE_4_moment__(new Date(this._dealerData.expire_date), 'DD/MM/YYYY').format('YYYY-MM-DD');
        }
        this.editDealerForm = formBuilder.group({
            userid: [this._dealerData.user_id, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            first_name: [this._dealerData.first_name, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            last_name: [this._dealerData.last_name, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            email: [(this._dealerData.email ? this._dealerData.email : 'N/A'), __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            phone: [(this._dealerData.phone ? this._dealerData.phone : 'N/A'), __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            address: [(this._dealerData.address ? this._dealerData.address : 'N/A'), __WEBPACK_IMPORTED_MODULE_3__angular_forms__["Validators"].required],
            expirationdate: [this.yearLater]
        });
    }
    EditDealerPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditDealerPage');
    };
    EditDealerPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    EditDealerPage.prototype._submit = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.editDealerForm.valid) {
            var payload = {
                address: this.editDealerForm.value.address,
                contactid: this._dealerData._id,
                expire_date: new Date(this.editDealerForm.value.expirationdate).toISOString(),
                first_name: this.editDealerForm.value.first_name,
                last_name: this.editDealerForm.value.last_name,
                status: this._dealerData.status,
                user_id: this.editDealerForm.value.userid
            };
            this.apiCall.startLoading().present();
            this.apiCall.editUserDetailsCall(payload)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                console.log("dealer updated data=> ", data);
                if (data) {
                    var toast = _this.toastCtrl.create({
                        message: "Dealer updated successfully.",
                        duration: 1500,
                        position: 'middle'
                    });
                    toast.present();
                    _this.viewCtrl.dismiss();
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                console.log("error occured=> ", err);
            });
        }
    };
    EditDealerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-cust',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\dealers\edit-dealer\edit-dealer.html"*/'<ion-header>\n\n  <ion-navbar>\n\n      <ion-title>Edit Dealer Details</ion-title>\n\n      <ion-buttons end>\n\n          <button ion-button ion-only (click)="dismiss()">\n\n              <ion-icon name="close-circle"></ion-icon>\n\n          </button>\n\n      </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <form [formGroup]="editDealerForm">\n\n\n\n      <ion-item>\n\n          <ion-label fixed style="min-width: 50% !important;">User ID*</ion-label>\n\n          <ion-input formControlName="userid" type="text"></ion-input>\n\n      </ion-item>\n\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.userid.valid && (editDealerForm.controls.userid.dirty || submitAttempt)">\n\n          <p>Enter User ID</p>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n          <ion-label fixed style="min-width: 50% !important;">First Name*</ion-label>\n\n          <ion-input formControlName="first_name" type="text"></ion-input>\n\n      </ion-item>\n\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.first_name.valid && (editDealerForm.controls.first_name.dirty || submitAttempt)">\n\n          <p>Enter First Name</p>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n          <ion-label fixed style="min-width: 50% !important;">Last Name*</ion-label>\n\n          <ion-input formControlName="last_name" type="text"></ion-input>\n\n      </ion-item>\n\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.last_name.valid && (editDealerForm.controls.last_name.dirty || submitAttempt)">\n\n          <p>Enter Last Name</p>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n          <ion-label fixed style="min-width: 50% !important;">Email ID</ion-label>\n\n          <ion-input formControlName="email" type="email"></ion-input>\n\n      </ion-item>\n\n      <!-- <ion-item class="logitem1" *ngIf="!editDealerForm.controls.email.valid && (editDealerForm.controls.email.dirty || submitAttempt)">\n\n          <p>email id is required!</p>\n\n      </ion-item> -->\n\n\n\n      <ion-item>\n\n          <ion-label fixed style="min-width: 50% !important;">Mobile Number</ion-label>\n\n          <ion-input formControlName="phone" type="number" maxlength="10" minlength="10"></ion-input>\n\n      </ion-item>\n\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.phone.valid && (editDealerForm.controls.phone.dirty || submitAttempt)">\n\n          <p>mobile number is required and should be 10 digits!</p>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n        <ion-label fixed style="min-width: 50% !important;">Expire On</ion-label>\n\n        <ion-input type="date" formControlName="expirationdate" min="{{minDate}}" style="margin-left: -3px;"></ion-input>\n\n    </ion-item>\n\n\n\n      <ion-item>\n\n          <ion-label fixed style="min-width: 50% !important;">Address</ion-label>\n\n          <ion-input formControlName="address" type="text"></ion-input>\n\n      </ion-item>\n\n      <ion-item class="logitem1" *ngIf="!editDealerForm.controls.address.valid && (editDealerForm.controls.address.dirty || submitAttempt)">\n\n          <p>Enter Address</p>\n\n      </ion-item>\n\n\n\n      <!-- <ion-item>\n\n          <ion-label fixed style="min-width: 50% !important;">Creation On</ion-label>\n\n          <ion-input type="date" formControlName="creationdate" min="{{minDate}}" style="margin-left: -3px;"></ion-input>\n\n      </ion-item>\n\n\n\n      <ion-item>\n\n          <ion-label fixed style="min-width: 50% !important;">Expire On</ion-label>\n\n          <ion-input type="date" formControlName="expirationdate" min="{{minDate}}" style="margin-left: -3px;"></ion-input>\n\n      </ion-item>\n\n\n\n      <ion-item *ngIf="isSuperAdminStatus">\n\n          <ion-label>Dealers</ion-label>\n\n          <ion-select formControlName="dealer_firstname">\n\n              <ion-option *ngFor="let dealer of getAllDealersData" [value]="dealer.dealer_firstname" (ionSelect)="DealerselectData(dealer)">{{dealer.dealer_firstname}}</ion-option>\n\n          </ion-select>\n\n      </ion-item> -->\n\n  </form>\n\n  <!-- <button ion-button block (click)="updateCustomer()">UPDATE DETAILS</button> -->\n\n</ion-content>\n\n<ion-footer class="footSty">\n\n  <ion-toolbar>\n\n      <ion-row>\n\n          <ion-col style="text-align: center;">\n\n              <button ion-button clear color="light" (click)="_submit()">UPDATE DEALER DETAILS</button>\n\n          </ion-col>\n\n      </ion-row>\n\n  </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\dealers\edit-dealer\edit-dealer.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], EditDealerPage);
    return EditDealerPage;
}());

//# sourceMappingURL=edit-dealer.js.map

/***/ }),

/***/ 515:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditDealerPageModule", function() { return EditDealerPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__edit_dealer__ = __webpack_require__(1021);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EditDealerPageModule = /** @class */ (function () {
    function EditDealerPageModule() {
    }
    EditDealerPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__edit_dealer__["a" /* EditDealerPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__edit_dealer__["a" /* EditDealerPage */]),
            ],
        })
    ], EditDealerPageModule);
    return EditDealerPageModule;
}());

//# sourceMappingURL=edit-dealer.module.js.map

/***/ })

});
//# sourceMappingURL=38.js.map