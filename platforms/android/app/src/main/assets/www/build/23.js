webpackJsonp([23],{

/***/ 1038:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OverSpeedRepoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OverSpeedRepoPage = /** @class */ (function () {
    function OverSpeedRepoPage(navCtrl, navParams, apicalloverspeed, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalloverspeed = apicalloverspeed;
        this.alertCtrl = alertCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }
    OverSpeedRepoPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    OverSpeedRepoPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    OverSpeedRepoPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    OverSpeedRepoPage.prototype.getOverspeeddevice = function (selectedVehicle) {
        // console.log("selectedVehicle=> ", selectedVehicle)
        this.overSpeeddevice_id = selectedVehicle._id;
        // console.log("selected vehicle=> " + this.overSpeeddevice_id)
    };
    OverSpeedRepoPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicalloverspeed.startLoading().present();
        // this.apicalloverspeed.livedatacall(this.islogin._id, this.islogin.email)
        this.apicalloverspeed.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicalloverspeed.stopLoading();
            _this.devices1243 = [];
            _this.devices = data;
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apicalloverspeed.stopLoading();
            console.log(err);
        });
    };
    OverSpeedRepoPage.prototype.getOverSpeedReport = function (starttime, endtime) {
        var _this = this;
        var baseUrl;
        if (this.overSpeeddevice_id == undefined) {
            baseUrl = "https://www.oneqlik.in/notifs/overSpeedReport?from_date=" + new Date(starttime).toISOString() + '&to_date=' + new Date(endtime).toISOString() + '&_u=' + this.islogin._id;
        }
        else {
            baseUrl = "https://www.oneqlik.in/notifs/overSpeedReport?from_date=" + new Date(starttime).toISOString() + '&to_date=' + new Date(endtime).toISOString() + '&_u=' + this.islogin._id + '&device=' + this.overSpeeddevice_id;
        }
        this.apicalloverspeed.startLoading().present();
        this.apicalloverspeed.getOverSpeedApi(baseUrl)
            .subscribe(function (data) {
            _this.apicalloverspeed.stopLoading();
            _this.overspeedReport = data;
            console.log(_this.overspeedReport);
            if (_this.overspeedReport.length == 0) {
                var alert_1 = _this.alertCtrl.create({
                    message: "No Data Found",
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (error) {
            _this.apicalloverspeed.stopLoading();
            console.log(error);
        });
    };
    OverSpeedRepoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-over-speed-repo',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\over-speed-repo\over-speed-repo.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Over Speed Report</ion-title>\n\n  </ion-navbar>\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n\n      [canSearch]="true" (onChange)="getOverspeeddevice(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n\n\n  <ion-row style="background-color: #fafafa;" padding-left padding-right>\n\n    <ion-col width-20>\n\n\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getOverSpeedReport(datetimeStart,datetimeEnd);"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n  <ion-card *ngFor="let overdata of overspeedReport">\n\n\n\n    <ion-item style="border-bottom: 2px solid #dedede;">\n\n      <ion-avatar item-start>\n\n        <img src="assets/imgs/car_red_icon.png">\n\n        <!-- <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n                            \n\n                <img src="assets/imgs/truck_icon_red.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png"  *ngIf="(item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png"  *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-thumbnail>\n\n\n\n            <ion-thumbnail item-start *ngIf="item.device != null && item.device.iconType == null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-thumbnail>\n\n\n\n            <ion-thumbnail item-start *ngIf="item.device == null && item.device.iconType != null">\n\n                <img src="assets/imgs/car_red_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png" *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png" *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png" *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png" *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png" *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png" *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))"> -->\n\n\n\n      </ion-avatar>\n\n      <ion-row>\n\n        <ion-col col-8>\n\n          <p style="color:gray;font-size:13px;">{{overdata.vehicleName}}</p>\n\n        </ion-col>\n\n        <ion-col col-4>\n\n          <p>\n\n            <ion-icon ios="ios-speedometer" md="md-speedometer" style="color:#e14444;"></ion-icon>&nbsp;\n\n            <span style="font-size:12px;">{{overdata.overSpeed}}&nbsp;</span>\n\n          </p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row style="padding-top: 13px;">\n\n        <div class="overme" *ngIf="overdata.address">\n\n          <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:13px;"></ion-icon>&nbsp;{{overdata.address}}\n\n        </div>\n\n        <div class="overme" *ngIf="!overdata.address">\n\n          <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:13px;"></ion-icon>&nbsp;N/A\n\n        </div>\n\n      </ion-row>\n\n    </ion-item>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\over-speed-repo\over-speed-repo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], OverSpeedRepoPage);
    return OverSpeedRepoPage;
}());

//# sourceMappingURL=over-speed-repo.js.map

/***/ }),

/***/ 531:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OverSpeedRepoPageModule", function() { return OverSpeedRepoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__over_speed_repo__ = __webpack_require__(1038);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var OverSpeedRepoPageModule = /** @class */ (function () {
    function OverSpeedRepoPageModule() {
    }
    OverSpeedRepoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__over_speed_repo__["a" /* OverSpeedRepoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__over_speed_repo__["a" /* OverSpeedRepoPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], OverSpeedRepoPageModule);
    return OverSpeedRepoPageModule;
}());

//# sourceMappingURL=over-speed-repo.module.js.map

/***/ })

});
//# sourceMappingURL=23.js.map