webpackJsonp([9],{

/***/ 1051:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YourTripsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var YourTripsPage = /** @class */ (function () {
    function YourTripsPage(apicall, navCtrl, navParams, toastCtrl) {
        this.apicall = apicall;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.portstemp = [];
        this._tripList = [];
        this.skip = 1;
        this.limit = 10;
        this._tripListKey = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        this.getdevices();
    }
    YourTripsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad YourTripsPage');
    };
    YourTripsPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicall.startLoading().present();
        this.apicall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicall.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apicall.stopLoading();
            console.log(err);
        });
    };
    YourTripsPage.prototype.getID = function (val) {
        this.trip_id = val._id;
    };
    YourTripsPage.prototype.getTrips = function (infiniteScroll) {
        var _this = this;
        var url;
        var that = this;
        if (!infiniteScroll) {
            this._tripListKey = [];
            this._tripList = [];
        }
        if (this.trip_id != undefined) {
            url = "https://www.oneqlik.in/user_trip/getLastTrip?device=" + this.trip_id + "&tripInfo=all_trip&user=" + this.islogin._id + "&fromDate=" + new Date(this.datetimeStart).toISOString() + "&toDate=" + new Date(this.datetimeEnd).toISOString() + "&skip=" + that.skip + "&limit=" + that.limit;
        }
        else {
            url = "https://www.oneqlik.in/user_trip/getLastTrip?&tripInfo=all_trip&user=" + this.islogin._id + "&fromDate=" + new Date(this.datetimeStart).toISOString() + "&toDate=" + new Date(this.datetimeEnd).toISOString() + "&skip=" + that.skip + "&limit=" + that.limit;
        }
        if (!infiniteScroll) {
            this.apicall.startLoading().present();
            this.apicall.getSOSReportAPI(url)
                .subscribe(function (data) {
                _this.apicall.stopLoading();
                console.log("trip data: ", data);
                debugger;
                if (!data.message) {
                    _this.innerFunc(data, infiniteScroll);
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: "Trip(s) not found..!",
                        duration: 1500,
                        position: "bottom"
                    });
                    toast.present();
                }
            }, function (err) {
                _this.apicall.stopLoading();
            });
        }
        else {
            this.apicall.getSOSReportAPI(url)
                .subscribe(function (data) {
                console.log("trip data: ", data);
                debugger;
                if (!data.message) {
                    _this.innerFunc(data, infiniteScroll);
                }
            });
        }
    };
    YourTripsPage.prototype.innerFunc = function (data, infiniteScroll) {
        var outerthis = this;
        outerthis.startAdd = undefined;
        outerthis.endAdd = undefined;
        var i = 0, howManyTimes = data.length;
        function f() {
            var date1 = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(data[i].start_time), 'DD/MM/YYYY').format("LLLL");
            var str = date1.split(', ');
            var date2 = str[0];
            var date3 = str[1].split(' ');
            var date4 = str[2].split(' ');
            var date5 = date4[0];
            var date6 = date3[0];
            var date7 = date3[1];
            var date8 = date6 + ' ' + date5;
            var date9 = date4[1];
            outerthis._tripList.push({
                "date1": date2,
                "date2": date8,
                "date3": date7,
                "dtime": date9,
                "trip_name": data[i].trip_name,
                "trip_status": data[i].trip_status
            });
            if (data[i].end_lat != null && data[i].start_lat != null) {
                var latEnd = data[i].end_lat;
                var lngEnd = data[i].end_long;
                var latlng = new google.maps.LatLng(latEnd, lngEnd);
                var latStart = data[i].start_lat;
                var lngStart = data[i].start_long;
                var latlng1 = new google.maps.LatLng(latStart, lngStart);
                var geocoder = new google.maps.Geocoder();
                var geocoder1 = new google.maps.Geocoder();
                var request = {
                    latLng: latlng
                };
                var request1 = {
                    latLng: latlng1
                };
                var city1;
                geocoder.geocode(request, function (data, status) {
                    debugger;
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (data[1] != null) {
                            var component = data[1].address_components[0];
                            outerthis.endAdd = data[1].formatted_address;
                            // var drvar = data[1].address_components[2].short_name;
                            // var add1 = drvar.split(' ');
                            // outerthis.addkey1 = add1[0];
                            if (component.types[0] === 'locality') {
                                city1 = component.long_name;
                                outerthis.addkey1 = city1;
                            }
                            else {
                                city1 = "N/A";
                                outerthis.addkey1 = city1;
                            }
                        }
                    }
                    outerthis._tripList[outerthis._tripList.length - 1].eaddress = outerthis.endAdd;
                    outerthis._tripList[outerthis._tripList.length - 1].eaddkey = outerthis.addkey1;
                });
                var city;
                geocoder1.geocode(request1, function (data, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (data[1] != null) {
                            debugger;
                            var component = data[1].address_components[0];
                            outerthis.startAdd = data[1].formatted_address;
                            // var drvar1 = data[1].address_components[2].short_name;
                            // var add2 = drvar1.split(' ');
                            // outerthis.addkey2 = add2[0];
                            if (component.types[0] === 'locality') {
                                city = component.long_name;
                                outerthis.addkey2 = city;
                            }
                            else {
                                city = "N/A";
                                outerthis.addkey2 = city;
                            }
                        }
                    }
                    outerthis._tripList[outerthis._tripList.length - 1].saddress = outerthis.startAdd;
                    outerthis._tripList[outerthis._tripList.length - 1].saddkey = outerthis.addkey2;
                });
            }
            else {
                outerthis._tripList[outerthis._tripList.length - 1].eaddress = 'N/A';
                outerthis._tripList[outerthis._tripList.length - 1].saddress = 'N/A';
                outerthis._tripList[outerthis._tripList.length - 1].saddkey = 'N/A';
                outerthis._tripList[outerthis._tripList.length - 1].eaddkey = 'N/A';
            }
            debugger;
            if (infiniteScroll) {
                outerthis._tripListKey.push(outerthis._tripList);
                // infiniteScroll.complete();
            }
            else {
                // infiniteScroll.complete();
                outerthis._tripListKey = outerthis._tripList;
            }
            console.log(outerthis._tripListKey);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    YourTripsPage.prototype.doInfinite = function (infiniteScroll) {
        var that = this;
        that.skip = that.skip + 1;
        setTimeout(function () {
            that.getTrips(infiniteScroll);
            infiniteScroll.complete();
        }, 200);
    };
    YourTripsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-your-trips',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\your-trips\your-trips.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Your Trips</ion-title>\n\n  </ion-navbar>\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getID(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n  <ion-row style="background-color: #fafafa;" padding-left padding-right>\n\n    <ion-col width-20>\n\n\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getTrips();"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-card *ngFor="let t of _tripListKey">\n\n    <ion-row>\n\n      <ion-col col-4 style="background-color: #878888;">\n\n        <ion-row>\n\n          <ion-col col-12 style="text-align: center;">\n\n            <ion-icon name="car" color="light" style="font-size: 1.8em;"></ion-icon>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row style="text-align: center;">\n\n          <ion-col col-12 style="color:white; font-size: 1.8em; font-weight: 300">\n\n            {{t.date3}}\n\n          </ion-col>\n\n          <ion-col col-12 style="color: #f2f2f2; font-size: 1.1em;">\n\n            {{t.date1}}\n\n          </ion-col>\n\n          <ion-col col-12 style="color:white;font-size: 1.1em;">\n\n            <b>{{t.date2}}</b>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row style="border-top: 2px solid white; text-align: center; color:white;">\n\n          <ion-col col-12>\n\n            {{t.dtime}}\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-col>\n\n      <ion-col col-8>\n\n        <ion-row>\n\n          <ion-col col-12 style="font-size: 1.2em">\n\n            {{t.trip_name}}\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-12 style="font-size: 1.5em">\n\n            {{t.saddkey}} - {{t.eaddkey}}\n\n          </ion-col>\n\n          <ion-col col-12>\n\n            <ion-row class="card11">\n\n              <ion-col col-1>\n\n                <ion-icon name="pin" color="secondary"></ion-icon>\n\n              </ion-col>\n\n              <ion-col col-11 class="item11">{{t.saddress}}</ion-col>\n\n            </ion-row>\n\n          </ion-col>\n\n          <ion-col col-12>\n\n            <ion-row class="card11">\n\n              <ion-col col-1>\n\n                <ion-icon name="pin" color="danger"></ion-icon>\n\n              </ion-col>\n\n              <ion-col col-11 class="item11">{{t.eaddress}}</ion-col>\n\n            </ion-row>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-12 style="text-align: right;">\n\n            <button ion-button small clear color="secondary" *ngIf="t.trip_status == \'Started\'">Ongoing</button>\n\n            <button ion-button small clear color="gpsc" *ngIf="t.trip_status != \'Started\'">{{t.trip_status}}</button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-card>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n\n    </ion-infinite-scroll-content>\n\n</ion-infinite-scroll>\n\n  <!-- <ion-card>\n\n    <ion-row>\n\n      <ion-col col-4 style="background-color: #d80622;;">\n\n        <ion-row>\n\n          <ion-col col-12 style="text-align: center;">\n\n            <ion-icon name="car" color="light" style="font-size: 1.8em;"></ion-icon>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row style="text-align: center;">\n\n          <ion-col col-12 style="font-size: 1.5em; font-weight: 300">\n\n            25\n\n          </ion-col>\n\n          <ion-col col-12 style="font-size: 1.1em;">\n\n            Tuesday\n\n          </ion-col>\n\n          <ion-col col-12 style="font-size: 1.1em;">\n\n            <b>Jun 2019</b>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row style="border-top: 2px solid white; text-align: center; color:white;">\n\n          <ion-col col-12>\n\n            22:56\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-col>\n\n      <ion-col col-8>\n\n        <ion-row>\n\n          <ion-col col-12 style="font-size: 1.2em">\n\n            Bus Ticket\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-12 style="font-size: 1.5em">\n\n            Delhi - Dehradun\n\n          </ion-col>\n\n          <ion-col col-12 style="font-size: 1.2em">\n\n            Laxmi holidays\n\n          </ion-col>\n\n          <ion-col col-12 style="overflow: hidden;">\n\n            Boarding at I S B T kashmiri gate\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-12 style="text-align: right;">\n\n            <button ion-button small clear color="gpsc">Share Bus Images</button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-card> -->\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\your-trips\your-trips.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], YourTripsPage);
    return YourTripsPage;
}());

//# sourceMappingURL=your-trips.js.map

/***/ }),

/***/ 544:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YourTripsPageModule", function() { return YourTripsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__your_trips__ = __webpack_require__(1051);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var YourTripsPageModule = /** @class */ (function () {
    function YourTripsPageModule() {
    }
    YourTripsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__your_trips__["a" /* YourTripsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__your_trips__["a" /* YourTripsPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], YourTripsPageModule);
    return YourTripsPageModule;
}());

//# sourceMappingURL=your-trips.module.js.map

/***/ })

});
//# sourceMappingURL=9.js.map