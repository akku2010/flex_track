webpackJsonp([35],{

/***/ 1030:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeofenceReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GeofenceReportPage = /** @class */ (function () {
    function GeofenceReportPage(navCtrl, navParams, apicallGeofenceReport, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallGeofenceReport = apicallGeofenceReport;
        this.toastCtrl = toastCtrl;
        this.geofenceRepoert = [];
        this.geofenceReportdata = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
    }
    GeofenceReportPage.prototype.ngOnInit = function () {
        this.getgeofence1();
    };
    GeofenceReportPage.prototype.getgeofence1 = function () {
        var _this = this;
        this.apicallGeofenceReport.startLoading().present();
        this.apicallGeofenceReport.getallgeofenceCall(this.islogin._id)
            .subscribe(function (data) {
            _this.apicallGeofenceReport.stopLoading();
            _this.geofencelist = data;
            console.log("geofencelist=> ", _this.geofencelist);
        }, function (err) {
            _this.apicallGeofenceReport.stopLoading();
            console.log(err);
        });
    };
    GeofenceReportPage.prototype.getGeofencedata = function (geofence) {
        console.log("selectedVehicle=> ", geofence);
        this.Ignitiondevice_id = geofence._id;
        this.getGeofenceReport();
    };
    GeofenceReportPage.prototype.getGeofenceReport = function () {
        var _this = this;
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        var that = this;
        this.apicallGeofenceReport.startLoading().present();
        this.apicallGeofenceReport.getGeogenceReportApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.Ignitiondevice_id, this.islogin._id)
            .subscribe(function (data) {
            _this.apicallGeofenceReport.stopLoading();
            _this.geofenceRepoert = data;
            if (_this.geofenceRepoert.length > 0) {
                _this.innerFunc(_this.geofenceRepoert);
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Report(s) not fond for selected Dates/Vehicle.',
                    duration: 1500,
                    position: 'bottom'
                });
                toast.present();
            }
        }, function (error) {
            _this.apicallGeofenceReport.stopLoading();
            console.log(error);
        });
    };
    GeofenceReportPage.prototype.innerFunc = function (geofenceRepoert) {
        var outerthis = this;
        outerthis.geofenceReportdata = [];
        var i = 0, howManyTimes = geofenceRepoert.length;
        function f() {
            outerthis.locationEndAddress = undefined;
            outerthis.geofenceReportdata.push({
                'device': outerthis.geofenceRepoert[i].device,
                'vehicleName': outerthis.geofenceRepoert[i].vehicleName,
                'direction': outerthis.geofenceRepoert[i].direction,
                'timestamp': outerthis.geofenceRepoert[i].timestamp,
                '_id': outerthis.geofenceRepoert[i]._id,
            });
            if (outerthis.geofenceRepoert[i].lat != null && outerthis.geofenceRepoert[i].long != null) {
                var latEnd = outerthis.geofenceRepoert[i].lat;
                var lngEnd = outerthis.geofenceRepoert[i].long;
                var latlng = new google.maps.LatLng(latEnd, lngEnd);
                var geocoder = new google.maps.Geocoder();
                var request = {
                    latLng: latlng
                };
                geocoder.geocode(request, function (data, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (data[1] != null) {
                            outerthis.locationEndAddress = data[1].formatted_address;
                        }
                    }
                    outerthis.geofenceReportdata[outerthis.geofenceReportdata.length - 1].address = outerthis.locationEndAddress;
                });
            }
            else {
                outerthis.geofenceReportdata[outerthis.geofenceReportdata.length - 1].address = 'N/A';
            }
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    GeofenceReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-geofence-report',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\geofence-report\geofence-report.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>Geofence Report</ion-title>\n\n    </ion-navbar>\n\n    <ion-item style="background-color: #fafafa;">\n\n        <ion-label style="margin-top: 15px;">Select Geofence</ion-label>\n\n        <select-searchable item-content [(ngModel)]="selectedGeofence" [items]="geofencelist" itemValueField="geoname"\n\n            itemTextField="geoname" [canSearch]="true" (onChange)="getGeofencedata(selectedGeofence)">\n\n        </select-searchable>\n\n    </ion-item>\n\n\n\n    <ion-row style="background-color: #fafafa;" padding-left padding-right>\n\n        <ion-col width-20>\n\n\n\n            <ion-label>\n\n                <span style="font-size: 13px">From Date</span>\n\n                <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a"\n\n                    [(ngModel)]="datetimeStart" style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n\n                </ion-datetime>\n\n            </ion-label>\n\n        </ion-col>\n\n\n\n        <ion-col width-20>\n\n            <ion-label>\n\n                <span style="font-size: 13px">To Date</span>\n\n                <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a"\n\n                    [(ngModel)]="datetimeEnd" style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n\n                </ion-datetime>\n\n            </ion-label>\n\n        </ion-col>\n\n\n\n        <ion-col width-20>\n\n            <div style="margin-top: 9px; float: right">\n\n                <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getGeofenceReport();">\n\n                </ion-icon>\n\n            </div>\n\n        </ion-col>\n\n    </ion-row>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <ion-card *ngFor="let item of geofenceReportdata;">\n\n        <ion-item style="border-bottom: 2px solid #dedede;">\n\n            <ion-avatar item-start>\n\n                <img src="assets/imgs/car_red_icon.png"\n\n                    *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/car_green_icon.png"\n\n                    *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/car_grey_icon.png"\n\n                    *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/truck_icon_red.png"\n\n                    *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/truck_icon_green.png"\n\n                    *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/truck_icon_grey.png"\n\n                    *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bike_red_icon.png"\n\n                    *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bike_green_icon.png"\n\n                    *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bike_grey_icon.png"\n\n                    *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/jcb_red.png"\n\n                    *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/jcb_green.png"\n\n                    *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/jcb_gray.png"\n\n                    *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n\n\n                <img src="assets/imgs/bus_red.png"\n\n                    *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n\n                <img src="assets/imgs/bus_green.png"\n\n                    *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n\n                <img src="assets/imgs/bus_gray.png"\n\n                    *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n\n\n            </ion-avatar>\n\n            <ion-label>{{item.vehicleName}}</ion-label>\n\n            <ion-badge item-end color="gpsc" *ngIf="item.direction==\'Out\'">{{item.direction}}&nbsp; <ion-icon\n\n                    ios="ios-arrow-round-up" md="md-arrow-round-up"></ion-icon>\n\n            </ion-badge>\n\n            <ion-badge item-end color="secondary" *ngIf="item.direction==\'In\'">{{item.direction}}&nbsp;<ion-icon\n\n                    ios="ios-arrow-round-down" md="md-arrow-round-down"></ion-icon>\n\n            </ion-badge>\n\n        </ion-item>\n\n        <ion-card-content>\n\n            <ion-row style="padding-top:5px;">\n\n                <ion-col col-1>\n\n                    <ion-icon ios="ios-time" md="md-time" style="font-size:15px;"></ion-icon>\n\n                </ion-col>\n\n                <ion-col col-11 style="color:gray;font-size:11px;font-weight: 400;">\n\n                    {{item.timestamp | date: \'medium\'}}\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row>\n\n                <ion-col col-1>\n\n                    <ion-icon ios="ios-pin" md="md-pin" color="gpsc" style="font-size:15px;"></ion-icon>\n\n                </ion-col>\n\n                <ion-col col-11 style="color:gray;font-size:11px;font-weight: 400;">\n\n                    {{item.address}}\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-card-content>\n\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\geofence-report\geofence-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], GeofenceReportPage);
    return GeofenceReportPage;
}());

//# sourceMappingURL=geofence-report.js.map

/***/ }),

/***/ 523:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeofenceReportPageModule", function() { return GeofenceReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__geofence_report__ = __webpack_require__(1030);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var GeofenceReportPageModule = /** @class */ (function () {
    function GeofenceReportPageModule() {
    }
    GeofenceReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__geofence_report__["a" /* GeofenceReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__geofence_report__["a" /* GeofenceReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], GeofenceReportPageModule);
    return GeofenceReportPageModule;
}());

//# sourceMappingURL=geofence-report.module.js.map

/***/ })

});
//# sourceMappingURL=35.js.map