webpackJsonp([49],{

/***/ 1053:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AllNotificationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_socket_io_client__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__filter_filter__ = __webpack_require__(412);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



 // with ES6 import

var AllNotificationsPage = /** @class */ (function () {
    function AllNotificationsPage(navCtrl, navParams, apiCall, events, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.events = events;
        this.popoverCtrl = popoverCtrl;
        this.NotifyData = [];
        this.MassArray1 = [];
        this.page = 1;
        this.items = [];
        this.limit = 8;
        this.portstemp = [];
        this.keyData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.events.publish('cart:updated', 0);
        for (var i = 0; i < 30; i++) {
            this.items.push(this.items.length);
        }
        if (navParams.get('param') != null) {
            this.vehicleData = navParams.get('param');
        }
    }
    AllNotificationsPage.prototype.ngOnInit = function () {
        var _this = this;
        localStorage.removeItem("filterByType");
        localStorage.removeItem("filterByDate");
        console.log(this.vehicleData);
        if (this.vehicleData == undefined) {
            console.log("vehicle undefined");
            this.getUsersOnScroll();
            this.getVehicleList();
        }
        else {
            console.log("vehicle defined");
            this.temp(this.vehicleData);
        }
        this.socket = __WEBPACK_IMPORTED_MODULE_3_socket_io_client__('https://www.oneqlik.in/sbNotifIO', {
            transports: ['websocket', 'polling']
        });
        this.socket.on('connect', function () {
            console.log('IO Connected page');
            console.log("socket connected page ", _this.socket.connected);
        });
        this.socket.on(this.islogin._id, function (msg) {
            _this.NotifyData.push(msg);
        });
    };
    AllNotificationsPage.prototype.doRefresh = function (refresher) {
        console.log('Begin async operation', refresher);
        this.getUsersOnScroll();
        refresher.complete();
    };
    AllNotificationsPage.prototype.filterby = function (ev) {
        var _this = this;
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_4__filter_filter__["a" /* FilterPage */], {
            cssClass: 'iosPop-popover'
        });
        popover.present({
            ev: ev
        });
        popover.onDidDismiss(function (data) {
            if (localStorage.getItem("types") != null) {
                var typeArr = [];
                if (data != null) {
                    if (data.length > 0) {
                        for (var i = 0; i < data.length; i++) {
                            typeArr.push(data[i].filterValue);
                        }
                    }
                    _this.keyData = typeArr;
                    console.log("keyData=> ", _this.keyData);
                    _this.filterByType();
                }
            }
            else {
                if (localStorage.getItem("dates") != null) {
                    _this.dates = data;
                    _this.filterByDate();
                }
            }
        });
    };
    AllNotificationsPage.prototype.filterByType = function () {
        var that = this;
        localStorage.setItem("filterByType", "filterByType");
        var baseUrl;
        if (that.vehicleData != undefined) {
            baseUrl = 'https://www.oneqlik.in/notifs/getNotifiLimit?user=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&type=' + that.keyData + '&device=' + that.vehicleData.Device_ID;
        }
        else {
            baseUrl = 'https://www.oneqlik.in/notifs/getNotifiLimit?user=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&type=' + that.keyData;
        }
        that.apiCall.startLoading().present();
        that.apiCall.filterByType(baseUrl)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            that.ndata = data;
            that.MassArray1 = that.ndata;
            localStorage.removeItem("types");
        }, function (err) {
            that.apiCall.stopLoading();
            console.log(err);
        });
    };
    AllNotificationsPage.prototype.filterByDate = function () {
        var that = this;
        localStorage.setItem("filterByDate", "filterByDate");
        that.apiCall.startLoading().present();
        that.apiCall.filterByDateCall(that.islogin._id, that.page, that.limit, that.dates)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            that.ndata = data;
            that.MassArray1 = that.ndata;
            localStorage.removeItem("dates");
        }, function (err) {
            that.apiCall.stopLoading();
            console.log(err);
        });
    };
    AllNotificationsPage.prototype.temp = function (key) {
        var _this = this;
        this.page = 1;
        this.apiCall.startLoading().present();
        this.apiCall.getFilteredcall(this.islogin._id, this.page, this.limit, key.Device_ID)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.ndata = data;
            _this.MassArray1 = _this.ndata;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    AllNotificationsPage.prototype.getUsersOnScroll = function () {
        var _this = this;
        this.apiCall.getDataOnScroll(this.islogin._id, this.page, this.limit)
            .subscribe(function (res) {
            _this.ndata = res;
            _this.MassArray1 = _this.ndata;
        }, function (error) {
            console.log(error);
        });
    };
    AllNotificationsPage.prototype.getVehicleList = function () {
        var that = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email;
        if (that.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + that.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + that.islogin._id;
            }
        }
        // that.apiCall.getVehicleListCall(that.islogin._id, that.islogin.email)
        that.apiCall.getVehicleListCall(baseURLp)
            .subscribe(function (data) {
            that.portstemp = data.devices;
        }, function (err) {
            console.log(err);
        });
    };
    // getVehicleList() {
    //   let that = this;
    //   that.apiCall.getVehicleListCall(that.islogin._id, that.islogin.email)
    //     .subscribe(data => {
    //       that.portstemp = data.devices;
    //     },
    //       err => {
    //         console.log(err);
    //       });
    // }
    AllNotificationsPage.prototype.doInfinite = function (infiniteScroll) {
        var that = this;
        that.page = that.page + 1;
        setTimeout(function () {
            if (localStorage.getItem("filterByType") != null) {
                // when filter by type is selected
                localStorage.setItem("filterByType", "filterByType");
                var baseUrl1;
                if (that.vehicleData != undefined) {
                    baseUrl1 = 'https://www.oneqlik.in/notifs/getNotifiLimit?user=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&type=' + that.keyData + '&device=' + that.vehicleData.Device_ID;
                }
                else {
                    baseUrl1 = 'https://www.oneqlik.in/notifs/getNotifiLimit?user=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit + '&type=' + that.keyData;
                }
                that.apiCall.filterByType(baseUrl1)
                    .subscribe(function (data) {
                    that.ndata = data;
                    for (var i = 0; i < that.ndata.length; i++) {
                        that.MassArray1.push(that.ndata[i]);
                    }
                    localStorage.removeItem("types");
                }, function (err) {
                    console.log(err);
                });
            }
            else {
                if (localStorage.getItem("filterByDate") != null) {
                    // when filter by date is applied
                    that.apiCall.filterByDateCall(that.islogin._id, that.page, that.limit, that.dates)
                        .subscribe(function (data) {
                        that.ndata = data;
                        for (var i = 0; i < that.ndata.length; i++) {
                            that.MassArray1.push(that.ndata[i]);
                        }
                        localStorage.removeItem("dates");
                    }, function (err) {
                        console.log(err);
                    });
                }
                else {
                    if (that.selectedVehicle != undefined || that.vehicleData != undefined) {
                        var keyTemp;
                        if (that.selectedVehicle == undefined) {
                            keyTemp = that.vehicleData;
                        }
                        else {
                            keyTemp = that.selectedVehicle;
                        }
                        // when applying sorting by selected vehicle
                        that.apiCall.getFilteredcall(that.islogin._id, that.page, that.limit, keyTemp.Device_ID)
                            .subscribe(function (data) {
                            that.ndata = data;
                            for (var i = 0; i < that.ndata.length; i++) {
                                that.MassArray1.push(that.ndata[i]);
                            }
                        }, function (err) {
                            console.log(err);
                        });
                    }
                    else {
                        // when no filter applied
                        that.apiCall.getDataOnScroll(that.islogin._id, that.page, that.limit)
                            .subscribe(function (res) {
                            that.ndata = res;
                            for (var i = 0; i < that.ndata.length; i++) {
                                that.MassArray1.push(that.ndata[i]);
                            }
                        }, function (error) {
                            console.log(error);
                        });
                    }
                }
            }
            console.log('Async operation has ended');
            infiniteScroll.complete();
        }, 100);
    };
    AllNotificationsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-all-notifications',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\all-notifications\all-notifications.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Notifications</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="filterby($event)">\n\n        <ion-icon ios="ios-funnel" md="md-funnel"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n  <ion-item *ngIf="portstemp.length != 0" class="itemStyle">\n\n    <ion-label>Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n\n      [canSearch]="true" (onChange)="temp(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n</ion-header>\n\n\n\n<ion-content [ngClass]="{ \'masters\': portstemp.length != 0,\'masters1\': portstemp.length == 0 }">\n\n  <!-- <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles" refreshingText="Refreshing...">\n\n    </ion-refresher-content>\n\n  </ion-refresher> -->\n\n  <div *ngIf="MassArray1.length == 0">\n\n    <p padding-left>Oops.. No data found for selected vehicle..</p>\n\n  </div>\n\n  <div *ngIf="MassArray1.length != 0">\n\n    <ion-list>\n\n      <ion-item *ngFor="let notif of MassArray1; let i= index;">\n\n\n\n        <ion-avatar item-start>\n\n          <img src="assets/imgs/sos.webp" *ngIf="notif.type == \'SOS\' ">\n\n          <img src="assets/imgs/speedlimit.webp" *ngIf="notif.type == \'overspeed\' ">\n\n          <img src="assets/imgs/geofence.jpg" *ngIf="notif.type == \'Geo-Fence\' ">\n\n          <img src="assets/imgs/ignition-switch-icon.jpg" *ngIf="notif.type == \'IGN\' ">\n\n          <img src="assets/imgs/car2.webp" *ngIf="notif.type == \'route-poi\' ">\n\n          <img src="assets/imgs/petrolpump4.jpg" *ngIf="notif.type == \'Fuel\' ">\n\n          <img src="assets/imgs/system_status-noun_63767_cc.webp" *ngIf="notif.type == \'status\'">\n\n          <img src="assets/imgs/AccurateMapping.jpg" *ngIf="notif.type == \'Route\'">\n\n          <img src="assets/imgs/397px-Snow_flake.svg.png" *ngIf="notif.type == \'AC\' ">\n\n          <img src="assets/imgs/power-png-icon-6.webp" *ngIf="notif.type == \'power\'">\n\n          <img src="assets/imgs/key_orange.jpg" *ngIf="notif.type == \'immo\'">\n\n          <img src="assets/imgs/342e07b3a6f1628afd14c9b4e6c93afe_icon.webp" *ngIf="notif.type == \'theft\'">\n\n          \n\n          \n\n        </ion-avatar>\n\n        <h2>{{notif.type}}</h2>\n\n        <p ion-text text-wrap>{{notif.item[\'sentence\']}}</p>\n\n        <ion-row item-end>\n\n          <ion-col width-40>\n\n            <div style="margin-top:19%;">\n\n              <p style="text-align: right;font-size: 12px;">{{notif.timestamp | date: \'mediumDate\'}}</p>\n\n            </div>\n\n            <div style="margin-top:19%;">\n\n              <p style="text-align: right;font-size: 12px;">{{notif.timestamp | date:\'shortTime\'}}</p>\n\n            </div>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-item>\n\n    </ion-list>\n\n  </div>\n\n\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n\n    </ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\all-notifications\all-notifications.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"]])
    ], AllNotificationsPage);
    return AllNotificationsPage;
}());

//# sourceMappingURL=all-notifications.js.map

/***/ }),

/***/ 546:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllNotificationsPageModule", function() { return AllNotificationsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__all_notifications__ = __webpack_require__(1053);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AllNotificationsPageModule = /** @class */ (function () {
    function AllNotificationsPageModule() {
    }
    AllNotificationsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__all_notifications__["a" /* AllNotificationsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__all_notifications__["a" /* AllNotificationsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], AllNotificationsPageModule);
    return AllNotificationsPageModule;
}());

//# sourceMappingURL=all-notifications.module.js.map

/***/ })

});
//# sourceMappingURL=49.js.map