webpackJsonp([39],{

/***/ 1061:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DealerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DealerPage = /** @class */ (function () {
    function DealerPage(navCtrl, navParams, apiCall, modalCtrl, toastCtrl, alerCtrl, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.events = events;
        this.islogin = {};
        this.page = 1;
        this.limit = 5;
        this.DealerArraySearch = [];
        this.DealerArray = [];
        this.ndata = [];
        this.DealerData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    DealerPage.prototype.ionViewDidLoad = function () {
        this.getDealersListCall();
    };
    DealerPage.prototype.getDealersListCall = function () {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.getDealersCall(this.islogin._id, this.page, this.limit, this.searchKey)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.DealerArray = data;
            _this.DealerArraySearch = data;
            console.log("Dealer data=> ", data);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("getting error from server=> ", err);
            var s = JSON.parse(err._body);
            var p = s.message;
            var toast = _this.toastCtrl.create({
                message: "No Dealer(s) found",
                duration: 2000,
                position: "bottom"
            });
            toast.present();
            toast.onDidDismiss(function () {
                _this.navCtrl.setRoot("DashboardPage");
            });
        });
    };
    DealerPage.prototype.addDealersModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create('AddDealerPage');
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.getDealersListCall();
        });
        modal.present();
    };
    DealerPage.prototype._editDealer = function (item) {
        var _this = this;
        var modal = this.modalCtrl.create('EditDealerPage', {
            param: item
        });
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.getDealersListCall();
        });
        modal.present();
    };
    DealerPage.prototype.DeleteDealer = function (_id) {
        var _this = this;
        var alert = this.alerCtrl.create({
            message: 'Do you want to delete this Dealer?',
            buttons: [{
                    text: 'No'
                },
                {
                    text: 'YES',
                    handler: function () {
                        _this.deleteDeal(_id);
                    }
                }]
        });
        alert.present();
    };
    DealerPage.prototype.deleteDeal = function (_id) {
        var _this = this;
        var deletePayload = {
            'userId': _id,
            'deleteuser': true
        };
        this.apiCall.startLoading().present();
        this.apiCall.deleteDealerCall(deletePayload).
            subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("deleted dealer data=> " + data);
            var toast = _this.toastCtrl.create({
                message: 'Deleted dealer successfully.',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.getDealersListCall();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    DealerPage.prototype.callSearch = function (ev) {
        var _this = this;
        console.log(ev.target.value);
        this.searchKey = ev.target.value;
        // this.apiCall.DealerSearchService(this.islogin._id, this.page, this.limit, this.searchKey)
        this.apiCall.getDealersCall(this.islogin._id, this.page, this.limit, this.searchKey)
            .subscribe(function (data) {
            _this.DealerArraySearch = data;
            _this.DealerArray = data;
        }, function (err) {
            var a = JSON.parse(err._body);
            var b = a.message;
            _this.DealerArraySearch = [];
            _this.DealerArray = [];
            var toast = _this.toastCtrl.create({
                message: "No Dealer found for search key '" + ev.target.value + "' ..",
                duration: 3000,
                position: "bottom"
            });
            toast.present();
            toast.onDidDismiss(function () { });
        });
    };
    DealerPage.prototype.onClear = function (ev) {
        this.getDealersListCall();
        ev.target.value = '';
    };
    DealerPage.prototype.switchDealer = function (_id) {
        var _this = this;
        //debugger;
        console.log(_id);
        // localStorage.setItem('isDealervalue', 'true');
        // localStorage.setItem('isSuperAdminValue', 'false');
        // $rootScope.dealer = $rootScope.islogin;
        localStorage.setItem('superAdminData', JSON.stringify(this.islogin));
        localStorage.setItem('custumer_status', 'OFF');
        localStorage.setItem('dealer_status', 'ON');
        this.apiCall.getcustToken(_id)
            .subscribe(function (res) {
            console.log('UserChangeObj=>', res);
            var custToken = res;
            var logindata = JSON.stringify(custToken);
            var logindetails = JSON.parse(logindata);
            var userDetails = window.atob(logindetails.custumer_token.split('.')[1]);
            // console.log('token=>', logindata);
            var details = JSON.parse(userDetails);
            // console.log(details.isDealer);
            localStorage.setItem("loginflag", "loginflag");
            localStorage.setItem('details', JSON.stringify(details));
            var dealerSwitchObj = {
                "logindata": logindata,
                "details": userDetails,
                'condition_chk': details.isDealer
            };
            var temp = localStorage.getItem('isDealervalue');
            console.log("temp=> ", temp);
            _this.events.publish("event_sidemenu", JSON.stringify(dealerSwitchObj));
            _this.events.publish("sidemenu:event", temp);
            _this.navCtrl.setRoot('DashboardPage');
        }, function (err) {
            console.log(err);
        });
    };
    DealerPage.prototype.dealerStatus = function (data) {
        var _this = this;
        var msg;
        if (data.status) {
            msg = 'Do you want to Deactivate this Dealer?';
        }
        else {
            msg = 'Do you want to Activate this Dealer?';
        }
        var alert = this.alerCtrl.create({
            message: msg,
            buttons: [{
                    text: 'YES',
                    handler: function () {
                        _this.user_status(data);
                    }
                },
                {
                    text: 'NO',
                    handler: function () {
                        _this.getDealersListCall();
                    }
                }]
        });
        alert.present();
    };
    DealerPage.prototype.user_status = function (data) {
        var _this = this;
        var stat;
        if (data.status) {
            stat = false;
        }
        else {
            stat = true;
        }
        var ddata = {
            "uId": data._id,
            "loggedIn_id": this.islogin._id,
            "status": stat
        };
        this.apiCall.startLoading().present();
        this.apiCall.user_statusCall(ddata)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            // this.DeletedDevice = data;
            console.log("Dealer data=> ", data);
            // console.log("DeletedDevice=> " + this.DeletedDevice)
            var toast = _this.toastCtrl.create({
                message: 'Dealer status updated successfully!',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.getDealersListCall();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error => ", err);
            // var body = err._body;
            // var msg = JSON.parse(body);
            // let alert = this.alerCtrl.create({
            //   title: 'Oops!',
            //   message: msg.message,
            //   buttons: ['OK']
            // });
            // alert.present();
        });
    };
    DealerPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var that = this;
        that.page = that.page + 1;
        setTimeout(function () {
            that.ndata = [];
            _this.apiCall.getDealersCall(that.islogin._id, that.page, that.limit, that.searchKey)
                .subscribe(function (data) {
                that.ndata = data;
                for (var i = 0; i < that.ndata.length; i++) {
                    that.DealerData.push(that.ndata[i]);
                }
                that.DealerArraySearch = that.DealerData;
                infiniteScroll.complete();
            }, function (err) {
                _this.apiCall.stopLoading();
                console.log("error found=> " + err);
            });
        }, 500);
    };
    DealerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-customers',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\dealers\dealers.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>Dealers</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="addDealersModal()">\n\n                <ion-icon name="add"></ion-icon>\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n    <ion-searchbar (ionInput)="callSearch($event)" (ionClear)="onClear($event)"></ion-searchbar>\n\n    <!-- <ion-searchbar placeholder="Search..." (ionInput)="getItems($event)"></ion-searchbar> -->\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <!-- <ion-refresher (ionRefresh)="doRefresh($event)">\n\n        <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles" refreshingText="Refreshing...">\n\n        </ion-refresher-content>\n\n    </ion-refresher> -->\n\n    <ion-list>\n\n        <div *ngFor="let item of DealerArraySearch">\n\n\n\n            <ion-item>\n\n                <ion-thumbnail item-start>\n\n                    <img src="assets/imgs/user.png" alt="item.Device_Name">\n\n                    <ion-icon name="trash" style="margin-left: 41%;margin-top: 10%;font-size: 30px;color: #b9002f;" (tap)="DeleteDealer(item._id)"></ion-icon>\n\n                </ion-thumbnail>\n\n                <div (tap)="switchDealer(item._id)">\n\n                    <p>\n\n                        <span ion-text color="dark">Name: </span>\n\n                        {{item.first_name}}&nbsp;{{item.last_name}}\n\n                    </p>\n\n                    <p>\n\n                        <span ion-text color="dark">Email: </span>\n\n                        {{item.email}}\n\n                    </p>\n\n                    <p *ngIf="item.pass">\n\n                        <span ion-text color="dark">Password: </span>\n\n                        {{item.pass}}\n\n                    </p>\n\n                    <p *ngIf="!item.pass">\n\n                        <span ion-text color="dark">Password: </span>\n\n                        Not Saved\n\n                    </p>\n\n                    <p>\n\n                        <span ion-text color="dark">Phone: </span>\n\n                        <a href="tel:{{item.phone}}">{{item.phone}}</a>\n\n                    </p>\n\n                    <p>\n\n                        <span ion-text color="dark">Created On: </span>\n\n                        {{item.created_on | date:\'shortDate\'}}\n\n                    </p>\n\n                    <p>\n\n                        <span ion-text color="dark">Total Vehicles: </span>\n\n                        <span ion-text color="danger">{{item.total_vehicle}}</span>\n\n                    </p>\n\n                </div>\n\n\n\n                <p>\n\n                    <button ion-button small (click)="_editDealer(item)">Edit</button>\n\n                    <button ion-button small (click)="dealerStatus(item)" *ngIf="item.status == true">Active</button>\n\n                    <button ion-button small color="danger" (click)="dealerStatus(item)" *ngIf="item.status != true">InActive</button>\n\n                </p>\n\n            </ion-item>\n\n\n\n        </div>\n\n    </ion-list>\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n        <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n\n        </ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\dealers\dealers.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], DealerPage);
    return DealerPage;
}());

//# sourceMappingURL=dealers.js.map

/***/ }),

/***/ 553:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DealerPageModule", function() { return DealerPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dealers__ = __webpack_require__(1061);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DealerPageModule = /** @class */ (function () {
    function DealerPageModule() {
    }
    DealerPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__dealers__["a" /* DealerPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__dealers__["a" /* DealerPage */])
            ]
        })
    ], DealerPageModule);
    return DealerPageModule;
}());

//# sourceMappingURL=dealers.module.js.map

/***/ })

});
//# sourceMappingURL=39.js.map