webpackJsonp([21],{

/***/ 1039:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PoiListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__ = __webpack_require__(120);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PoiListPage = /** @class */ (function () {
    function PoiListPage(navCtrl, navParams, apiCall, modalCtrl, toastCtrl, alertCtrl, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.poilist = [];
        this.allData = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.events.subscribe('reloadpoilist', function () {
            _this.poilist = [];
            _this.getpois();
        });
    }
    PoiListPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PoiListPage');
    };
    PoiListPage.prototype.ngOnInit = function () {
        this.getpois();
    };
    PoiListPage.prototype.getpois = function () {
        var _this = this;
        this.apiCall.getPoisAPI(this.islogin._id)
            .subscribe(function (data) {
            _this.poilist = data;
            _this.allData._poiListData = [];
            var i = 0, howManyTimes = data.length;
            var that = _this;
            function f() {
                that.allData._poiListData.push({
                    "poiname": data[i].poi.poiname,
                    "radius": data[i].radius ? data[i].radius : 'N/A',
                    "address": data[i].poi.address ? data[i].poi.address : 'N/A'
                });
                that.allData._poiListData[that.allData._poiListData.length - 1].mapid = "a" + i;
                that.allData.map = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["b" /* GoogleMaps */].create(that.allData._poiListData[that.allData._poiListData.length - 1].mapid, {
                    camera: {
                        target: {
                            lat: data[i].poi.location.coordinates[1],
                            lng: data[i].poi.location.coordinates[0]
                        },
                        zoom: 12
                    }
                });
                that.allData.map.addMarker({
                    position: {
                        lat: data[i].poi.location.coordinates[1],
                        lng: data[i].poi.location.coordinates[0]
                    }
                });
                i++;
                if (i < howManyTimes) {
                    setTimeout(f, 200);
                }
            }
            f();
        }, function (err) {
            console.log("error in pois: ", err);
        });
    };
    PoiListPage.prototype.editPOI = function (data) {
        console.log("edited data: " + JSON.stringify(data));
        this.navCtrl.push('AddPoiPage', {
            "param": data
        });
    };
    PoiListPage.prototype.deletePOI = function (data) {
        var _this = this;
        console.log("delete data: ", data);
        var alert = this.alertCtrl.create({
            message: "Do you want to delete this POI?",
            buttons: [{
                    text: 'BACK'
                }, {
                    text: 'YES PROCEED',
                    handler: function () {
                        _this.apiCall.startLoading().present();
                        _this.apiCall.deletePOIAPI(data._id)
                            .subscribe(function (data) {
                            _this.apiCall.stopLoading();
                            var toast = _this.toastCtrl.create({
                                message: "POI deleted successfully!",
                                position: "top",
                                duration: 2000
                            });
                            toast.present();
                            _this.getpois();
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            console.log("error occured while deleting POI: ", err);
                            var toast = _this.toastCtrl.create({
                                message: "POI deleted successfully!",
                                position: "top",
                                duration: 2000
                            });
                            toast.present();
                            _this.getpois();
                        });
                    }
                }]
        });
        alert.present();
    };
    PoiListPage.prototype.addPOI = function () {
        this.navCtrl.push('AddPoiPage');
    };
    PoiListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-poi-list',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\poi-list\poi-list.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>POI List</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only clear (tap)="addPOI()">\n\n        <ion-icon color="light" name="add"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-card *ngFor="let list of allData._poiListData;">\n\n    <ion-item>\n\n      <ion-thumbnail item-start>\n\n        <div id="{{list.mapid}}" style="height: 100px; width: 100px;"></div>\n\n      </ion-thumbnail>\n\n      <h1>{{list.poiname}}</h1>\n\n      <p style="color: #687DCA">Radius: {{list.radius}}</p>\n\n      <p>\n\n        <ion-icon name="pin"></ion-icon>&nbsp;&nbsp;{{list.address}}\n\n      </p>\n\n      <p>\n\n        <button ion-button round small (click)="editPOI(list)">Edit</button>\n\n        <button ion-button round small style="background: #878888; color: white"\n\n          (click)="deletePOI(list)">Delete</button>\n\n      </p>\n\n    </ion-item>\n\n  </ion-card>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Pro\flex_track\src\pages\poi-list\poi-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], PoiListPage);
    return PoiListPage;
}());

//# sourceMappingURL=poi-list.js.map

/***/ }),

/***/ 532:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PoiListPageModule", function() { return PoiListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__poi_list__ = __webpack_require__(1039);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PoiListPageModule = /** @class */ (function () {
    function PoiListPageModule() {
    }
    PoiListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__poi_list__["a" /* PoiListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__poi_list__["a" /* PoiListPage */]),
            ],
        })
    ], PoiListPageModule);
    return PoiListPageModule;
}());

//# sourceMappingURL=poi-list.module.js.map

/***/ })

});
//# sourceMappingURL=21.js.map