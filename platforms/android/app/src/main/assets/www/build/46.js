webpackJsonp([46],{

/***/ 1055:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CustomersPage = /** @class */ (function () {
    function CustomersPage(navCtrl, navParams, apiCall, modalCtrl, toastCtrl, alerCtrl, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.alerCtrl = alerCtrl;
        this.events = events;
        this.CustomerArraySearch = [];
        this.page = 1;
        this.limit = 5;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("_id=> " + this.islogin._id);
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.setsmsforotp = localStorage.getItem('setsms');
        this.isSuperAdminStatus = this.islogin.isSuperAdmin;
        console.log("isSuperAdminStatus=> " + this.isSuperAdminStatus);
        this.isDealer = this.islogin.isDealer;
        console.log("isDealer=> " + this.isDealer);
    }
    CustomersPage.prototype.ngOnInit = function () {
        this.getcustomer();
    };
    CustomersPage.prototype.dialNumber = function (number) {
        window.open('tel:' + number, '_system');
    };
    CustomersPage.prototype.doRefresh = function (refresher) {
        console.log('Begin async operation', refresher);
        this.getcustomer();
        refresher.complete();
    };
    CustomersPage.prototype.getItems = function (ev) {
        console.log(ev.target.value, this.CustomerArray);
        var val = ev.target.value.trim();
        this.CustomerArraySearch = this.CustomerArray.filter(function (item) {
            return (item.first_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
        console.log("search====", this.CustomerArraySearch);
    };
    CustomersPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var that = this;
        that.page = that.page + 1;
        setTimeout(function () {
            var baseURLp;
            baseURLp = 'https://www.oneqlik.in/users/getCust?uid=' + that.islogin._id + '&pageNo=' + that.page + '&size=' + that.limit;
            that.ndata = [];
            _this.apiCall.getCustomersCall(baseURLp)
                .subscribe(function (data) {
                that.ndata = data;
                for (var i = 0; i < that.ndata.length; i++) {
                    that.CustomerData.push(that.ndata[i]);
                }
                that.CustomerArraySearch = that.CustomerData;
                // this.CustomerArray = [];
                // for (var i = 0; i < this.CustomerData.length; i++) {
                //   this.CratedeOn = JSON.stringify(this.CustomerData[i].created_on).split('"')[1].split('T')[0];
                //   var gmtDateTime = moment.utc(JSON.stringify(this.CustomerData[i].created_on).split('T')[1].split('.')[0], "HH:mm:ss");
                //   var gmtDate = moment.utc(JSON.stringify(this.CustomerData[i].created_on).slice(0, -1).split('T'), "YYYY-MM-DD");
                //   if (this.CustomerData[i].expiration_date != null) {
                //     // var expirationDate = JSON.stringify(this.CustomerData[i].expiration_date).split('"')[1].split('T')[0];
                //     // var gmtDateTime1 = moment.utc(JSON.stringify(this.CustomerData[i].expiration_date).split('T')[1].split('.')[0], "HH:mm:ss");
                //     var gmtDate2 = moment.utc(JSON.stringify(this.CustomerData[i].expiration_date).slice(0, -1).split('T'), "YYYY-MM-DD");
                //     this.expirydate = gmtDate2.format('DD/MM/YYYY');
                //   } else {
                //     this.expirydate = null;
                //   }
                //   this.time = gmtDateTime.local().format(' h:mm:ss a');
                //   this.date = gmtDate.format('DD/MM/YYYY');
                //   this.CustomerArray.push({ '_id': this.CustomerData[i]._id, 'first_name': this.CustomerData[i].first_name, 'last_name': this.CustomerData[i].last_name, 'email': this.CustomerData[i].email, 'phone': this.CustomerData[i].phone, 'date': this.date, 'status': this.CustomerData[i].status, 'pass': this.CustomerData[i].pass, 'total_vehicle': this.CustomerData[i].total_vehicle, 'userid': this.CustomerData[i].userid, 'address': this.CustomerData[i].address, 'dealer_firstname': this.CustomerData[i].dealer_firstname, 'expiration_date': this.expirydate });
                // }
                // this.CustomerArraySearch = this.CustomerArray;
            }, function (err) {
                _this.apiCall.stopLoading();
                console.log("error found=> " + err);
            });
            console.log('Async operation has ended');
            infiniteScroll.complete();
        }, 500);
    };
    CustomersPage.prototype.callSearch = function (ev) {
        var _this = this;
        console.log(ev.target.value);
        var searchKey = ev.target.value;
        this.page = 1;
        // this.apiCall.startLoading().present();
        this.apiCall.callcustomerSearchService(this.islogin._id, this.page, this.limit, searchKey)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            // console.log("search result=> " + data)
            _this.CustomerArraySearch = data;
            // this.CustomerArray = data;
            // this.CustomerData = data;
        }, function (err) {
            // console.log("error=> ", err)
            // console.log("_body=> ", err._body)
            var msg = JSON.parse(err._body);
            // console.log("msg=> ", msg.message)
            var toast = _this.toastCtrl.create({
                message: msg.message,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        });
    };
    CustomersPage.prototype.onClear = function (ev) {
        // debugger;
        this.getcustomer();
        ev.target.value = '';
    };
    CustomersPage.prototype.getcustomer = function () {
        var _this = this;
        console.log("getcustomer");
        var baseURLp = 'https://www.oneqlik.in/users/getCust?uid=' + this.islogin._id + '&pageNo=' + this.page + '&size=' + this.limit;
        this.apiCall.startLoading().present();
        this.apiCall.getCustomersCall(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.CustomerData = data;
            // console.log("customer data => ", this.CustomerData)
            // this.CustomerArray = [];
            // for (var i = 0; i < this.CustomerData.length; i++) {
            //   this.CratedeOn = JSON.stringify(this.CustomerData[i].created_on).split('"')[1].split('T')[0];
            //   var gmtDateTime = moment.utc(JSON.stringify(this.CustomerData[i].created_on).split('T')[1].split('.')[0], "HH:mm:ss");
            //   var gmtDate = moment.utc(JSON.stringify(this.CustomerData[i].created_on).slice(0, -1).split('T'), "YYYY-MM-DD");
            //   if (this.CustomerData[i].expiration_date != null) {
            //     // var expirationDate = JSON.stringify(this.CustomerData[i].expiration_date).split('"')[1].split('T')[0];
            //     // var gmtDateTime1 = moment.utc(JSON.stringify(this.CustomerData[i].expiration_date).split('T')[1].split('.')[0], "HH:mm:ss");
            //     var gmtDate2 = moment.utc(JSON.stringify(this.CustomerData[i].expiration_date).slice(0, -1).split('T'), "YYYY-MM-DD");
            //     this.expirydate = gmtDate2.format('DD/MM/YYYY');
            //   } else {
            //     this.expirydate = null;
            //   }
            //   this.time = gmtDateTime.local().format(' h:mm:ss a');
            //   this.date = gmtDate.format('DD/MM/YYYY');
            //   this.CustomerArray.push({ '_id': this.CustomerData[i]._id, 'first_name': this.CustomerData[i].first_name, 'last_name': this.CustomerData[i].last_name, 'email': this.CustomerData[i].email, 'phone': this.CustomerData[i].phone, 'date': this.date, 'status': this.CustomerData[i].status, 'pass': this.CustomerData[i].pass, 'total_vehicle': this.CustomerData[i].total_vehicle, 'userid': this.CustomerData[i].userid, 'address': this.CustomerData[i].address, 'dealer_firstname': this.CustomerData[i].dealer_firstname, 'expiration_date': this.expirydate });
            // }
            // this.CustomerArraySearch = this.CustomerArray;
            _this.CustomerArraySearch = _this.CustomerData;
        }, function (err) {
            _this.apiCall.stopLoading();
            var a = JSON.parse(err._body);
            var b = a.message;
            var toast = _this.toastCtrl.create({
                message: b,
                duration: 2000,
                position: "bottom"
            });
            toast.present();
            toast.onDidDismiss(function () {
                _this.navCtrl.setRoot('DashboardPage');
            });
        });
    };
    CustomersPage.prototype.CustomerStatus = function (Customersdeta) {
        var _this = this;
        // console.log("status=> " + Customersdeta.status)
        // console.log(Customersdeta._id)
        // console.log(this.islogin._id);
        var msg;
        if (Customersdeta.status) {
            msg = 'Do you want to Deactivate this customer?';
        }
        else {
            msg = 'Do you want to Activate this customer?';
        }
        var alert = this.alerCtrl.create({
            message: msg,
            buttons: [{
                    text: 'YES',
                    handler: function () {
                        _this.user_status(Customersdeta);
                    }
                },
                {
                    text: 'NO',
                    handler: function () {
                        _this.getcustomer();
                    }
                }]
        });
        alert.present();
    };
    CustomersPage.prototype.user_status = function (Customersdeta) {
        var _this = this;
        var stat;
        if (Customersdeta.status) {
            stat = false;
        }
        else {
            stat = true;
        }
        var data = {
            "uId": Customersdeta._id,
            "loggedIn_id": this.islogin._id,
            "status": stat
        };
        this.apiCall.startLoading().present();
        this.apiCall.user_statusCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.DeletedDevice = data;
            console.log("DeletedDevice=> " + _this.DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: 'Customer was updated successfully',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.getcustomer();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error => ", err);
            // var body = err._body;
            // var msg = JSON.parse(body);
            // let alert = this.alerCtrl.create({
            //   title: 'Oops!',
            //   message: msg.message,
            //   buttons: ['OK']
            // });
            // alert.present();
        });
    };
    CustomersPage.prototype.openupdateCustomersModal = function (Customersdetails) {
        var _this = this;
        console.log('Opening Modal openAdddeviceModal');
        this.customer = Customersdetails;
        // console.log("search array data=> " + this.CustomerArraySearch)
        var modal = this.modalCtrl.create('UpdateCustModalPage', {
            param: this.customer
        });
        modal.onDidDismiss(function (data) {
            console.log(data);
            // this.getcustomer();
            _this.navCtrl.setRoot("CustomersPage");
        });
        modal.present();
    };
    CustomersPage.prototype.openAdddeviceModal = function (Customersdetails) {
        var _this = this;
        console.log('Opening Modal openAdddeviceModal');
        this.customer = Customersdetails;
        console.log("customers=> ", this.customer);
        console.log(this.customer._id);
        console.log(this.customer.email);
        var profileModal = this.modalCtrl.create('AddDeviceModalPage', { custDet: this.customer });
        profileModal.onDidDismiss(function (data) {
            console.log("vehData=> " + JSON.stringify(data));
            _this.getcustomer();
        });
        profileModal.present();
    };
    CustomersPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    CustomersPage.prototype.openAddCustomerModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create('AddCustomerModal');
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.getcustomer();
        });
        modal.present();
    };
    CustomersPage.prototype.switchUser = function (cust_id) {
        var _this = this;
        //debugger;
        console.log(cust_id);
        localStorage.setItem('isDealervalue', 'true');
        // $rootScope.dealer = $rootScope.islogin;
        localStorage.setItem('dealer', JSON.stringify(this.islogin));
        localStorage.setItem('custumer_status', 'ON');
        localStorage.setItem('dealer_status', 'OFF');
        this.apiCall.getcustToken(cust_id)
            .subscribe(function (res) {
            console.log('UserChangeObj=>', res);
            var custToken = res;
            var logindata = JSON.stringify(custToken);
            var logindetails = JSON.parse(logindata);
            var userDetails = window.atob(logindetails.custumer_token.split('.')[1]);
            // console.log('token=>', logindata);
            var details = JSON.parse(userDetails);
            // console.log(details.isDealer);
            localStorage.setItem("loginflag", "loginflag");
            localStorage.setItem('details', JSON.stringify(details));
            var dealerSwitchObj = {
                "logindata": logindata,
                "details": userDetails,
                'condition_chk': details.isDealer
            };
            var temp = localStorage.getItem('isDealervalue');
            console.log("temp=> ", temp);
            _this.events.publish("event_sidemenu", JSON.stringify(dealerSwitchObj));
            _this.events.publish("sidemenu:event", temp);
            _this.navCtrl.setRoot('DashboardPage');
        }, function (err) {
            console.log(err);
        });
    };
    CustomersPage.prototype.DelateCustomer = function (_id) {
        var _this = this;
        var alert = this.alerCtrl.create({
            message: 'Do you want to delete this Customer?',
            buttons: [{
                    text: 'No'
                },
                {
                    text: 'YES',
                    handler: function () {
                        _this.deleteCus(_id);
                    }
                }]
        });
        alert.present();
    };
    CustomersPage.prototype.deleteCus = function (_id) {
        var _this = this;
        console.log('user id bole to customer', _id);
        // this.apiCall.startLoading().present();
        var data = {
            "userId": _id,
            'deleteuser': true
        };
        this.apiCall.deleteCustomerCall(data).
            subscribe(function (data) {
            // this.apiCall.stopLoading();
            _this.DeletedCustomer = data;
            var toast = _this.toastCtrl.create({
                message: 'Deleted customer successfully.',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.getcustomer();
            });
            toast.present();
        }, function (err) {
            console.log(err);
            // this.apiCall.stopLoading();
            // var body = err._body;
            // var msg = JSON.parse(body);
            // let alert = this.alerCtrl.create({
            //   title: 'Oops!',
            //   message: msg.message,
            //   buttons: ['OK']
            // });
            // alert.present();
        });
    };
    CustomersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-customers',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\customers\customers.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Customers</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="openAddCustomerModal()">\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n  <ion-searchbar (ionInput)="callSearch($event)" (ionClear)="onClear($event)"></ion-searchbar>\n\n  <!-- <ion-searchbar placeholder="Search..." (ionInput)="getItems($event)"></ion-searchbar> -->\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles" refreshingText="Refreshing...">\n\n    </ion-refresher-content>\n\n  </ion-refresher>\n\n  <ion-list>\n\n    <div *ngFor="let item of CustomerArraySearch">\n\n      <ion-item>\n\n        <ion-thumbnail item-start>\n\n          <img src="assets/imgs/user.png" alt="item.Device_Name">\n\n          <ion-icon name="trash" style="margin-left: 41%;margin-top: 37%;font-size: 30px;color: #b9002f;" (tap)="DelateCustomer(item._id)"></ion-icon>\n\n        </ion-thumbnail>\n\n        <div (tap)="switchUser(item._id)">\n\n          <p>\n\n            <span ion-text color="dark">Name: </span> {{item.first_name}}&nbsp;{{item.last_name}}</p>\n\n          <p>\n\n            <span ion-text color="dark">Email: </span> {{item.email}}</p>\n\n          <p>\n\n            <span ion-text color="dark">Phone: </span>\n\n            <a href="tel:{{item.phone}}">{{item.phone}}</a>\n\n            <!-- <button ion-button color="light" (tap)="dialNumber(item.phone)"><ion-icon name="call"></ion-icon>&nbsp;{{item.phone}}</button> -->\n\n          </p>\n\n          <p>\n\n            <span ion-text color="dark">Password: </span>{{item.pass}}</p>\n\n          <p>\n\n            <span ion-text color="dark">Created On: </span>{{item.created_on | date:\'short\'}}</p>\n\n          <p>\n\n            <span ion-text color="dark">Total Vehicles: </span>\n\n            <span ion-text color="danger">{{item.total_vehicle}}</span>\n\n          </p>\n\n        </div>\n\n        <p>\n\n          <button ion-button small (click)="openupdateCustomersModal(item)">Edit</button>\n\n          <button ion-button small (click)="openAdddeviceModal(item)">Add Vehicle</button>\n\n          <button ion-button small (click)="CustomerStatus(item)" *ngIf="item.status == true">Active</button>\n\n          <button ion-button small color="danger" (click)="CustomerStatus(item)" *ngIf="item.status != true">InActive</button>\n\n        </p>\n\n      </ion-item>\n\n    </div>\n\n    <!-- <div *ngFor="let item of CustomerArraySearch">\n\n      <ion-item>\n\n        <ion-thumbnail item-start>\n\n          <img src="assets/imgs/user.png" alt="item.Device_Name">\n\n          <ion-icon name="trash" style="margin-left: 41%;margin-top: 37%;font-size: 30px;color: #b9002f;" (tap)="DelateCustomer(item._id)"></ion-icon>\n\n        </ion-thumbnail>\n\n        <div (tap)="switchUser(item._id)">\n\n          <p>\n\n            <span ion-text color="dark">Name: </span> {{item.first_name}}</p>\n\n          <p>\n\n            <span ion-text color="dark">Email: </span> {{item.email}}</p>\n\n          <p>\n\n            <span ion-text color="dark">Phone: </span>\n\n            <a href="tel:{{item.phone}}">{{item.phone}}</a>\n\n           \n\n    </p>\n\n    <p>\n\n      <span ion-text color="dark">Password: </span>{{item.pass}}</p>\n\n    <p>\n\n      <span ion-text color="dark">Created On: </span>{{item.date}}</p>\n\n    <p>\n\n      <span ion-text color="dark">Total Vehicles: </span>\n\n      <span ion-text color="danger">{{item.total_vehicle}}</span>\n\n    </p>\n\n    </div>\n\n    <p>\n\n      <button ion-button small (click)="openupdateCustomersModal(item)">Edit</button>\n\n      <button ion-button small (click)="openAdddeviceModal(item)">Add Vehicle</button>\n\n      <button ion-button small (click)="CustomerStatus(item)" *ngIf="item.status == true">Active</button>\n\n      <button ion-button small color="danger" (click)="CustomerStatus(item)" *ngIf="item.status != true">InActive</button>\n\n    </p>\n\n    </ion-item>\n\n    </div> -->\n\n  </ion-list>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n\n    </ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\customers\customers.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], CustomersPage);
    return CustomersPage;
}());

//# sourceMappingURL=customers.js.map

/***/ }),

/***/ 548:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomersPageModule", function() { return CustomersPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__customers__ = __webpack_require__(1055);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { AddDeviceModalPage } from './modals/add-device-modal';
var CustomersPageModule = /** @class */ (function () {
    function CustomersPageModule() {
    }
    CustomersPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__customers__["a" /* CustomersPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__customers__["a" /* CustomersPage */]),
            ],
            exports: []
        })
    ], CustomersPageModule);
    return CustomersPageModule;
}());

//# sourceMappingURL=customers.module.js.map

/***/ })

});
//# sourceMappingURL=46.js.map