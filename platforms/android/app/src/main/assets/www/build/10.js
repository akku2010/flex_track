webpackJsonp([10],{

/***/ 1064:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TripReviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__ = __webpack_require__(120);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TripReviewPage = /** @class */ (function () {
    function TripReviewPage(navParams, apiCall, alertCtrl) {
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.target = 0;
        this.allData = {};
        console.log("pramas=> " + JSON.stringify(navParams.get("params")));
        this.tripData = navParams.get("params");
        this.Device_Id = navParams.get("device_id");
        console.log(this.Device_Id);
    }
    TripReviewPage.prototype.ngOnInit = function () {
        localStorage.removeItem("tripTarget");
        this.triphistory();
    };
    TripReviewPage.prototype.onShare = function () {
        var tempalert = this.alertCtrl.create({
            title: 'Share Trip',
            message: 'Enter your Mobile Number so that we can share you a trip',
            inputs: [
                {
                    name: 'mobno',
                    placeholder: 'Mobile Number',
                    type: 'number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Share',
                    handler: function (data) {
                        var sNumber = data.mobno.toString();
                        if (sNumber.length == 10) {
                            console.log(data.mobno);
                        }
                        else {
                            alert("Plaese provide valid mobile number");
                        }
                    }
                }
            ]
        });
        tempalert.present();
        console.log("shareTrip");
    };
    TripReviewPage.prototype.triphistory = function () {
        var that = this;
        that.apiCall.startLoading().present();
        that.apiCall.tripReviewCall(that.Device_Id, that.tripData.start_time, that.tripData.end_time)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            // console.log("resp data=> ", data)
            that.points = data;
            that.mapData = [];
            that.mapData = that.points.map(function (d) {
                return { lat: d.latDecimal, lng: d.longDecimal };
            });
            that.points.reverse();
            // console.log('data of latlanf', that.mapData);
            var latLngBounds = new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["g" /* LatLngBounds */](that.mapData);
            that.dataArrayCoords = [];
            // for (var i = that.points.length - 1; i > 0; i--) {
            for (var i = that.points.length - 1; i > 0; i--) {
                if (that.points[i].latDecimal && that.points[i].longDecimal) {
                    var arr = [];
                    var startdatetime = new Date(that.points[i].insertionTime);
                    arr.push(that.points[i].latDecimal);
                    arr.push(that.points[i].longDecimal);
                    arr.push({ "time": startdatetime.toLocaleString() });
                    arr.push({ "speed": that.points[i].speed });
                    that.dataArrayCoords.push(arr);
                }
            }
            setInterval(function () {
                that.dataArrayCoords;
            }, 0);
            // console.log(that.dataArrayCoords);
            that.playbackData = [];
            for (var j = 0; j < that.points.length; j++) {
                var startdatetime1 = new Date(that.points[j].insertionTime);
                that.playbackData.push({ "lat": that.points[j].latDecimal, "lng": that.points[j].longDecimal, 'speed': that.points[j].speed + "Km/hr", 'isettiontime': startdatetime1.toLocaleString() });
                that.speed1 = that.playbackData[j].speed;
                setInterval(function () {
                    that.speed1;
                }, 0);
            }
            if (that.allData.map != undefined) {
                that.allData.map.remove();
            }
            that.allData.map = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["b" /* GoogleMaps */].create('trip_map_canvas');
            that.allData.map.moveCamera({
                'target': latLngBounds
            });
            that.allData.map.addMarker({
                title: 'S',
                position: that.mapData[0],
                icon: 'green',
                styles: {
                    'text-align': 'center',
                    'font-style': 'italic',
                    'font-weight': 'bold',
                    'color': 'red'
                },
            }).then(function (marker) {
                marker.showInfoWindow();
                that.allData.map.addMarker({
                    title: 'D',
                    position: that.mapData[that.mapData.length - 1],
                    icon: 'red',
                    styles: {
                        'text-align': 'center',
                        'font-style': 'italic',
                        'font-weight': 'bold',
                        'color': 'green'
                    },
                }).then(function (marker) {
                    marker.showInfoWindow();
                    // animateMarker(marker, dataArrayCoords, speed, trackerType)
                });
            });
            // console.log("latlang.............", that.mapData)
            that.allData.map.addPolyline({
                points: that.mapData,
                color: 'blue',
                width: 4,
                geodesic: true
            });
        }, function (err) {
            console.log(err);
            that.apiCall.stopLoading();
        });
    };
    ;
    TripReviewPage.prototype.Playback = function () {
        var that = this;
        if (localStorage.getItem("tripTarget") != null) {
            that.target = JSON.parse(localStorage.getItem("tripTarget"));
        }
        console.log("target=> " + that.target);
        that.playing = !that.playing; // This would alternate the state each time
        console.log("playing=> " + that.playing);
        // that.dataArrayCoords.reverse();
        var coord = that.dataArrayCoords[that.target];
        // console.log("coords=> " + coord);
        console.log("datacorrd=> ", that.dataArrayCoords[that.target]);
        that.coordreplaydata = coord;
        var lat = coord[0];
        var lng = coord[1];
        that.startPos = [lat, lng];
        that.speed = 200; // km/h
        // var delay = 100;
        if (that.playing) {
            console.log("if initialize=> ", that.playing);
            that.allData.map.setCameraTarget({ lat: lat, lng: lng });
            if (that.allData.mark == undefined) {
                that.allData.map.addMarker({
                    icon: './assets/imgs/vehicles/runningcar.png',
                    styles: {
                        'text-align': 'center',
                        'font-style': 'italic',
                        'font-weight': 'bold',
                        'color': 'green'
                    },
                    position: new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["f" /* LatLng */](that.startPos[0], that.startPos[1]),
                }).then(function (marker) {
                    that.allData.mark = marker;
                    // marker.showInfoWindow();
                    that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
                });
            }
            else {
                that.allData.mark.setPosition(new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["f" /* LatLng */](that.startPos[0], that.startPos[1]));
                that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
            }
        }
        else {
            that.allData.mark.setPosition(new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["f" /* LatLng */](that.startPos[0], that.startPos[1]));
        }
    };
    TripReviewPage.prototype.liveTrack = function (map, mark, coords, target, startPos, speed, delay) {
        var that = this;
        var target = target;
        if (!startPos.length)
            coords.push([startPos[0], startPos[1]]);
        function _gotoPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000;
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["f" /* LatLng */](coords[target][0], coords[target][1]);
            var distance = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); //in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target][0] - lat) / numStep;
            var deltaLng = (coords[target][1] - lng) / numStep;
            function changeMarker(mark, deg) {
                // console.log("deg=> ", deg)
                // mark.setIcon($("img[src='" + imgPath + "']").css("transform", "rotate(" + deg + "deg)"));
                mark.setRotation(deg);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                if (i < distance) {
                    // var head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
                    if ((head != 0) || (head == NaN)) {
                        // icons.rotation = head;
                        changeMarker(mark, head);
                    }
                    // mark.setIcon(icons);
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    // map.setCameraTarget(new LatLng(lat, lng))
                    // map.setCenter({ lat: lat, lng: lng });
                    setTimeout(_moveMarker, delay);
                }
                else {
                    // var head = Spherical.computeHeading(mark.getPosition(), new LatLng(lat, lng));
                    if ((head != 0) || (head == NaN)) {
                        // icons.rotation = head;
                        changeMarker(mark, head);
                    }
                    // mark.setIcon(icons);
                    mark.setPosition(dest);
                    // map.setCameraTarget(dest);
                    // map.setCenter({ lat: lat, lng: lng });
                    target++;
                    setTimeout(_gotoPoint, delay);
                }
            }
            a++;
            if (a > coords.length) {
            }
            else {
                that.speedMarker = coords[target][3].speed;
                that.updatetimedate = coords[target][2].time;
                if (that.playing) {
                    // console.log("if playing=> "+ playing)
                    _moveMarker();
                    target = target;
                    localStorage.setItem("tripTarget", target);
                }
                else {
                    // marker.setIcon(icons[trackerType]);
                    // target = target;
                    // console.log("target102=> ", target)
                }
                // km_h = km_h;
            }
        }
        var a = 0;
        _gotoPoint();
    };
    TripReviewPage.prototype.inter = function (fastforwad) {
        var that = this;
        console.log("fastforwad=> " + fastforwad);
        if (fastforwad == 'fast') {
            that.speed = 2 * that.speed;
            console.log("speed=> " + that.speed);
        }
        else if (fastforwad == 'slow') {
            if (that.speed > 50) {
                that.speed = that.speed / 2;
                console.log("speed=> " + that.speed);
            }
            else {
            }
        }
        else {
            that.speed = 200;
        }
    };
    TripReviewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-trip-review',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\trip-report\trip-review\trip-review.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Trip Review</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <div id="trip_map_canvas">\n\n        <ion-fab top right>\n\n            <button ion-fab mini (click)="onShare()" color="gpsc">\n\n                <ion-icon name="share" color="black"></ion-icon>\n\n            </button>\n\n        </ion-fab>\n\n    </div>\n\n</ion-content>\n\n\n\n<ion-footer class="footSty">\n\n\n\n    <ion-row style="background-color: #dfdfdf; padding: 0px !important;">\n\n        <ion-col width-50 style="padding: 0px">\n\n            <p style="color:black;font-size:14px; text-align:center;">\n\n                <ion-icon name="time" style="color:#33cd5f;font-size:15px;"></ion-icon>&nbsp;\n\n                <span *ngIf="updatetimedate">{{updatetimedate}}&nbsp;</span>\n\n                <span *ngIf="!updatetimedate">0:0&nbsp;</span>\n\n            </p>\n\n        </ion-col>\n\n\n\n        <ion-col width-50 style="padding: 0px">\n\n            <p style="color:black;font-size:14px;text-align:center;">\n\n                <ion-icon name="speedometer" style="color:#cd4343"></ion-icon>&nbsp;\n\n                <span *ngIf="speedMarker">{{speedMarker}}&nbsp;Km/hr</span>\n\n                <span *ngIf="!speedMarker">0&nbsp;Km/hr</span>\n\n            </p>\n\n        </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-toolbar>\n\n        <div style="text-align: center;">\n\n            <button ion-button clear>\n\n                <ion-icon color="light" name="rewind" style="font-size:26px;margin-right: 17px" (click)="inter(\'slow\')"></ion-icon>\n\n                <ion-icon color="light" name="arrow-dropright-circle" style="font-size:26px;margin-right: 15px" class="play" *ngIf="!playing"\n\n                    (click)="Playback()"></ion-icon>\n\n                <ion-icon color="light" name="pause" style="font-size:26px;margin-right: 15px" class="pause" *ngIf="playing" (click)="Playback()"></ion-icon>\n\n                <ion-icon color="light" name="fastforward" style="font-size:26px;margin-right: 17px" (click)="inter(\'fast\')"></ion-icon>\n\n            </button>\n\n        </div>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\trip-report\trip-review\trip-review.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], TripReviewPage);
    return TripReviewPage;
}());

//# sourceMappingURL=trip-review.js.map

/***/ }),

/***/ 557:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TripReviewPageModule", function() { return TripReviewPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__trip_review__ = __webpack_require__(1064);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var TripReviewPageModule = /** @class */ (function () {
    function TripReviewPageModule() {
    }
    TripReviewPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__trip_review__["a" /* TripReviewPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_3__trip_review__["a" /* TripReviewPage */]),
                __WEBPACK_IMPORTED_MODULE_2_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], TripReviewPageModule);
    return TripReviewPageModule;
}());

//# sourceMappingURL=trip-review.module.js.map

/***/ })

});
//# sourceMappingURL=10.js.map