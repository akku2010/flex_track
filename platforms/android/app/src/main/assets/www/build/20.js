webpackJsonp([20],{

/***/ 1040:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return POIReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var POIReportPage = /** @class */ (function () {
    function POIReportPage(navCtrl, navParam, apicall) {
        this.navCtrl = navCtrl;
        this.navParam = navParam;
        this.apicall = apicall;
        this.portstemp = [];
        this.poilist = [];
        this.pageNo = 0;
        this.poireportData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }
    POIReportPage.prototype.ngOnInit = function () {
        this.getdevices();
        this.getpois();
    };
    POIReportPage.prototype.getpois = function () {
        var _this = this;
        this.apicall.getPoisAPI(this.islogin._id)
            .subscribe(function (data) {
            for (var i = 0; i < data.length; i++) {
                _this.poilist.push({
                    poiname: data[i].poi.poiname,
                    _id: data[i]._id
                });
            }
            console.log("poi list: ", _this.poilist);
        }, function (err) {
            console.log("error in pois: ", err);
        });
    };
    POIReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicall.startLoading().present();
        // this.apicall.livedatacall(this.islogin._id, this.islogin.email)
        this.apicall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicall.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apicall.stopLoading();
            console.log(err);
        });
    };
    POIReportPage.prototype.getpoiid = function (selectedPOI) {
        console.log("selectedPOI: ", selectedPOI);
        this.poiId = selectedPOI._id;
    };
    POIReportPage.prototype.getvehicleid = function (selectedVehicle) {
        console.log("selectedVehicle: ", selectedVehicle);
        this.deviceid = selectedVehicle._id;
    };
    POIReportPage.prototype.getReport = function () {
        var _this = this;
        this.apicall.startLoading().present();
        this.apicall.getpoireportAPI(this.islogin._id, this.pageNo, new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), this.poiId, this.deviceid)
            .subscribe(function (data) {
            _this.apicall.stopLoading();
            var i = 0, howManyTimes = data.length;
            var that = _this;
            that.poireportData = [];
            that.locadd = undefined;
            function f() {
                that.poireportData.push({
                    'poiname': data[i].poi.poi.poiname,
                    'Device_Name': data[i].device.Device_Name,
                    'Device_ID': data[i].device.Device_ID,
                    'arrivalTime': data[i].arrivalTime,
                    'departureTime': data[i].departureTime
                });
                if (data[i].lat != null && data[i].long != null) {
                    var latEnd = data[i].lat;
                    var lngEnd = data[i].long;
                    var latlng = new google.maps.LatLng(latEnd, lngEnd);
                    var geocoder = new google.maps.Geocoder();
                    var request = {
                        latLng: latlng
                    };
                    geocoder.geocode(request, function (data, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (data[1] != null) {
                                that.locadd = data[1].formatted_address;
                            }
                        }
                        that.poireportData[that.poireportData.length - 1].address = that.locadd;
                    });
                }
                i++;
                if (i < howManyTimes) {
                    setTimeout(f, 100);
                }
            }
            f();
            console.log("poireport data: ", that.poireportData);
        }, function (err) {
            _this.apicall.stopLoading();
            console.log("error occured: ", err);
        });
    };
    POIReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-report-poi',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\poi-report\poi-report.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>POI Report</ion-title>\n\n\n\n    </ion-navbar>\n\n\n\n    <ion-item style="background-color: #fafafa;" *ngIf="poilist.length != 0">\n\n        <ion-label style="margin-top: 15px;">Select POI</ion-label>\n\n        <select-searchable item-content [(ngModel)]="selectedPOI" [items]="poilist" itemValueField="poiname"\n\n            itemTextField="poiname" [canSearch]="true" (onChange)="getpoiid(selectedPOI)">\n\n        </select-searchable>\n\n    </ion-item>\n\n    <ion-item style="background-color: #fafafa;" *ngIf="portstemp.length != 0">\n\n        <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n\n        <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n            itemTextField="Device_Name" [canSearch]="true" (onChange)="getvehicleid(selectedVehicle)">\n\n        </select-searchable>\n\n    </ion-item>\n\n    <ion-toolbar style="padding:0px;">\n\n            <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n                    <ion-col width-20>\n\n                        <ion-label>\n\n                            <span style="font-size: 13px">From Date</span>\n\n                            <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a"\n\n                                [(ngModel)]="datetimeStart" style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n\n                            </ion-datetime>\n\n                        </ion-label>\n\n                    </ion-col>\n\n                    <ion-col width-20>\n\n                        <ion-label>\n\n                            <span style="font-size: 13px">To Date</span>\n\n                            <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a"\n\n                                [(ngModel)]="datetimeEnd" style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n\n                            </ion-datetime>\n\n                        </ion-label>\n\n                    </ion-col>\n\n                    <ion-col width-20>\n\n                        <div style="margin-top: 9px; float: right">\n\n                            <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getReport();">\n\n                            </ion-icon>\n\n                        </div>\n\n                    </ion-col>\n\n                </ion-row>\n\n    </ion-toolbar>\n\n    \n\n\n\n</ion-header>\n\n\n\n<ion-content style="margin: 5%;">\n\n    <ion-card *ngFor="let rep of poireportData" style="border-radius: 5px;">\n\n        <ion-row style="background-color:#f2f2f2; padding-top:5px; padding-bottom: 5px" padding-left padding-right>\n\n            <p style="font-size:1.2em; color:gray; font-weight: bold">POI Name: {{rep.poiname}}</p>\n\n        </ion-row>\n\n        <ion-row>\n\n            <ion-col col-12>\n\n                <p style="font-size: 1em;">{{rep.Device_Name}}</p>\n\n            </ion-col>\n\n            <!-- <ion-col col-6>{{rep.Device_ID}}</ion-col> -->\n\n        </ion-row>\n\n        <ion-row>\n\n            <ion-col col-1 style="text-align: center;">\n\n                <ion-icon style="font-size: 1.3em;" color="gpsc" name="pin"></ion-icon>\n\n            </ion-col>\n\n            <ion-col col-11>\n\n                <p *ngIf="rep.address != undefined">{{rep.address}}</p>\n\n                <p *ngIf="rep.address == undefined">N/A</p>\n\n            </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n            <ion-col col-6 style="font-size:0.8em; color: gray">Arr -\n\n                <span *ngIf="rep.arrivalTime != undefined">{{rep.arrivalTime | date:\'short\'}}</span>\n\n                <span *ngIf="rep.arrivalTime == undefined">N/A</span>\n\n            </ion-col>\n\n            <ion-col col-6 style="font-size:0.8em; color: gray">Dep -\n\n                <span *ngIf="rep.departureTime != undefined">{{rep.departureTime | date:\'short\'}}</span>\n\n                <span *ngIf="rep.departureTime == undefined">N/A</span>\n\n            </ion-col>\n\n        </ion-row>\n\n        <!-- <ion-row>\n\n            <ion-col col-6>Dep Time</ion-col>\n\n            <ion-col col-6>{{rep.departureTime | date:\'short\'}}</ion-col>\n\n        </ion-row> -->\n\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\poi-report\poi-report.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], POIReportPage);
    return POIReportPage;
}());

//# sourceMappingURL=poi-report.js.map

/***/ }),

/***/ 533:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "POIReportPageModule", function() { return POIReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__poi_report__ = __webpack_require__(1040);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var POIReportPageModule = /** @class */ (function () {
    function POIReportPageModule() {
    }
    POIReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__poi_report__["a" /* POIReportPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__poi_report__["a" /* POIReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], POIReportPageModule);
    return POIReportPageModule;
}());

//# sourceMappingURL=poi-report.module.js.map

/***/ })

});
//# sourceMappingURL=20.js.map