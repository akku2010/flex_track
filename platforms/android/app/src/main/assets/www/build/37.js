webpackJsonp([37],{

/***/ 1022:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceSummaryRepoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DeviceSummaryRepoPage = /** @class */ (function () {
    function DeviceSummaryRepoPage(navCtrl, navParams, apicallsummary, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallsummary = apicallsummary;
        this.toastCtrl = toastCtrl;
        this.summaryReport = [];
        this.summaryReportData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        // var yestDay = moment().subtract(1, 'days');
        // console.log("yesterdays date: ", yestDay); 
        console.log("yest time: ", __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).subtract(1, 'days').format());
        // this.datetimeStart = moment({ hours: 0 }).format();
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).subtract(1, 'days').format(); // yesterday date with 12:00 am
        console.log("today time: ", this.datetimeStart);
        // this.datetimeEnd = moment().format();//new Date(a).toISOString();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format(); // today date and time with 12:00am
    }
    DeviceSummaryRepoPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    DeviceSummaryRepoPage.prototype.getSummaarydevice = function (selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.device_id = selectedVehicle.Device_ID;
    };
    DeviceSummaryRepoPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicallsummary.startLoading().present();
        this.apicallsummary.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicallsummary.stopLoading();
            _this.devices = data;
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apicallsummary.stopLoading();
            console.log(err);
        });
    };
    DeviceSummaryRepoPage.prototype.getSummaryReport = function () {
        var _this = this;
        var that = this;
        this.summaryReport = [];
        this.summaryReportData = [];
        if (this.device_id == undefined) {
            this.device_id = "";
        }
        this.apicallsummary.startLoading().present();
        this.apicallsummary.getSummaryReportApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.islogin._id, this.device_id)
            .subscribe(function (data) {
            _this.apicallsummary.stopLoading();
            _this.summaryReport = data;
            if (_this.summaryReport.length > 0) {
                _this.innerFunc(_this.summaryReport);
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Report(s) not found for selected dates/vehicle.',
                    duration: 1500,
                    position: 'bottom'
                });
                toast.present();
            }
        }, function (error) {
            _this.apicallsummary.stopLoading();
            console.log(error);
        });
    };
    // millisToMinutesAndSeconds(millis) {
    //   debugger
    //   var minutes = Math.floor(millis / 60000);
    //   var seconds = Number(((millis % 60000) / 1000).toFixed(0));
    //   return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    // }
    DeviceSummaryRepoPage.prototype.innerFunc = function (summaryReport) {
        var outerthis = this;
        var i = 0, howManyTimes = summaryReport.length;
        function f() {
            var hourconversion = 2.7777778 / 10000000;
            outerthis.summaryReportData.push({
                'Device_Name': outerthis.summaryReport[i].devObj[0].Device_Name,
                'routeViolations': outerthis.summaryReport[i].today_routeViolations,
                'overspeeds': (outerthis.summaryReport[i].today_overspeeds * hourconversion).toFixed(2),
                'ignOn': (outerthis.summaryReport[i].today_running * hourconversion).toFixed(2),
                'ignOff': (outerthis.summaryReport[i].today_stopped * hourconversion).toFixed(2),
                'distance': outerthis.summaryReport[i].today_odo,
                'tripCount': outerthis.summaryReport[i].today_trips
            });
            if (outerthis.summaryReport[i].end_location != null && outerthis.summaryReport[i].start_location != null) {
                var latEnd = outerthis.summaryReport[i].end_location.lat;
                var lngEnd = outerthis.summaryReport[i].end_location.long;
                var latlng = new google.maps.LatLng(latEnd, lngEnd);
                var latStart = outerthis.summaryReport[i].start_location.lat;
                var lngStart = outerthis.summaryReport[i].start_location.long;
                var lngStart1 = new google.maps.LatLng(latStart, lngStart);
                var geocoder = new google.maps.Geocoder();
                var request = {
                    latLng: latlng
                };
                var request1 = {
                    latLng: lngStart1
                };
                geocoder.geocode(request, function (data, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (data[1] != null) {
                            outerthis.locationEndAddress = data[1].formatted_address;
                        }
                    }
                    outerthis.summaryReportData[outerthis.summaryReportData.length - 1].EndLocation = outerthis.locationEndAddress;
                });
                geocoder.geocode(request1, function (data, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (data[1] != null) {
                            outerthis.locationAddress = data[1].formatted_address;
                        }
                    }
                    outerthis.summaryReportData[outerthis.summaryReportData.length - 1].StartLocation = outerthis.locationAddress;
                });
            }
            else {
                outerthis.summaryReportData[outerthis.summaryReportData.length - 1].EndLocation = 'N/A';
                outerthis.summaryReportData[outerthis.summaryReportData.length - 1].StartLocation = 'N/A';
            }
            console.log(outerthis.summaryReportData);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 5000);
            }
        }
        f();
    };
    DeviceSummaryRepoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-device-summary-repo',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\device-summary-repo\device-summary-repo.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>Summary Report</ion-title>\n\n    </ion-navbar>\n\n\n\n    <ion-item style="background-color: #fafafa;">\n\n        <ion-label>Select Vehicle</ion-label>\n\n        <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n            itemTextField="Device_Name" [canSearch]="true" (onChange)="getSummaarydevice(selectedVehicle)">\n\n        </select-searchable>\n\n    </ion-item>\n\n\n\n    <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n        <ion-col width-20>\n\n            <ion-label>\n\n                <span style="font-size: 13px">From Date</span>\n\n                <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a"\n\n                    [(ngModel)]="datetimeStart" style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n\n                </ion-datetime>\n\n            </ion-label>\n\n        </ion-col>\n\n\n\n        <ion-col width-20>\n\n            <ion-label>\n\n                <span style="font-size: 13px">To Date</span>\n\n                <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a"\n\n                    [(ngModel)]="datetimeEnd" style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n\n                </ion-datetime>\n\n            </ion-label>\n\n        </ion-col>\n\n\n\n        <ion-col width-20>\n\n            <div style="margin-top: 9px; float: right">\n\n                <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getSummaryReport();">\n\n                </ion-icon>\n\n            </div>\n\n        </ion-col>\n\n    </ion-row>\n\n</ion-header>\n\n<ion-content>\n\n\n\n    <ion-card *ngFor="let item of summaryReportData">\n\n        <ion-item style="border-bottom: 2px solid #dedede;">\n\n            <ion-avatar item-start>\n\n                <img src="assets/imgs/car_red_icon.webp">\n\n            </ion-avatar>\n\n            <ion-label>{{item.Device_Name }}</ion-label>\n\n            <ion-badge item-end color="gpsc">Trips - {{item.tripCount}}</ion-badge>\n\n        </ion-item>\n\n        <ion-card-content>\n\n            <ion-row style="padding-top: 12px;">\n\n                <ion-col>\n\n                    <p class="para">\n\n                        <span *ngIf="item.ignOn">{{item.ignOn}}</span>\n\n                        <span *ngIf="!item.ignOn">00.00</span>&nbsp;</p>\n\n                    <p style="color:#53ab53;font-size:11px;font-weight: 350;"> Running </p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p class="para">\n\n                        <span *ngIf="item.ignOff">{{item.ignOff}}</span>\n\n                        <span *ngIf="!item.ignOff">00.00</span>&nbsp;</p>\n\n                    <p style="text-align:left;font-size: 11px;color:red;font-weight:350;">Stop</p>\n\n                </ion-col>\n\n                <ion-col center text-center>\n\n                    <p class="para">\n\n                        <span *ngIf="item.distance">{{item.distance | number : \'1.0-2\'}}</span>\n\n                        <span *ngIf="!item.distance">00.00</span>&nbsp;</p>\n\n                    <p style="text-align:left;font-size: 11px;color:#11c1f3;font-weight:350;">Total KM</p>\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row style="padding-top: 5px;">\n\n                <ion-col>\n\n                    <p class="para">\n\n                        <span *ngIf="item.overspeeds">{{item.overspeeds}}</span>\n\n                        <span *ngIf="!item.overspeeds">00.00</span>&nbsp;</p>\n\n                    <p class="para1">Overspeeding</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p class="para">\n\n                        <span *ngIf="item.routeViolations">{{item.routeViolations}}&nbsp;Km/h</span>\n\n                        <span *ngIf="!item.routeViolations">0 Km/h</span>&nbsp;</p>\n\n                    <p class="para1">Route Voilation</p>\n\n                </ion-col>\n\n                <ion-col></ion-col>\n\n            </ion-row>\n\n\n\n            <ion-row style="padding-top: 5px;">\n\n                <ion-col col-1>\n\n                    <ion-icon name="pin" style="color:#33c45c; font-size:15px;"></ion-icon>\n\n                </ion-col>\n\n                <ion-col col-11 style="color:gray;font-size:11px;font-weight: 400;">\n\n                    {{item.StartLocation}}\n\n                </ion-col>\n\n            </ion-row>\n\n            <ion-row>\n\n                <ion-col col-1>\n\n                    <ion-icon name="pin" style="color:#e14444;font-size:15px;"></ion-icon>\n\n                </ion-col>\n\n                <ion-col col-11 style="color:gray;font-size:11px;font-weight: 400;">\n\n                    {{item.EndLocation}}\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-card-content>\n\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\device-summary-repo\device-summary-repo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], DeviceSummaryRepoPage);
    return DeviceSummaryRepoPage;
}());

//# sourceMappingURL=device-summary-repo.js.map

/***/ }),

/***/ 516:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceSummaryRepoPageModule", function() { return DeviceSummaryRepoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__device_summary_repo__ = __webpack_require__(1022);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DeviceSummaryRepoPageModule = /** @class */ (function () {
    function DeviceSummaryRepoPageModule() {
    }
    DeviceSummaryRepoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__device_summary_repo__["a" /* DeviceSummaryRepoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__device_summary_repo__["a" /* DeviceSummaryRepoPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], DeviceSummaryRepoPageModule);
    return DeviceSummaryRepoPageModule;
}());

//# sourceMappingURL=device-summary-repo.module.js.map

/***/ })

});
//# sourceMappingURL=37.js.map