webpackJsonp([7],{

/***/ 1028:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FuelConsumptionReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FuelConsumptionReportPage = /** @class */ (function () {
    function FuelConsumptionReportPage(navCtrl, navParams, apiCall, toastCtrl, geocoderApi, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.geocoderApi = geocoderApi;
        this.modalCtrl = modalCtrl;
        this.portstemp = [];
        this._listData = [];
        this.fuelDataArr = [];
        this.reportType = [];
        this.repkey = "Time";
        // this.reportType = ["Time", "Daily", "Trip"];
        this.reportType = ["Time", "Daily", "Trip"];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
    }
    FuelConsumptionReportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FuelConsumptionReportPage');
    };
    FuelConsumptionReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    FuelConsumptionReportPage.prototype.getFuelNotifs = function (fuelData, key) {
        this.getData(fuelData, key);
    };
    FuelConsumptionReportPage.prototype.getData = function (fuelData, key) {
        var _this = this;
        // this.eventType = key;
        // const paramData = this.navParams.get('paramMaps');
        var _url = "https://www.oneqlik.in/gps/getFuelNotifs";
        var payload = {
            "start_time": fuelData.start_time,
            "end_time": fuelData.end_time,
            "imei": fuelData.imei,
            "event": key
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(_url, payload)
            .subscribe(function (resp) {
            _this.apiCall.stopLoading();
            console.log("response: ", resp);
            // this.fuelData = resp;
            _this.modalCtrl.create('FuelEventsComponent', {
                paramMaps: resp,
            }).present();
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    FuelConsumptionReportPage.prototype.reportWise = function (repId) {
        console.log("repId: ", repId);
    };
    FuelConsumptionReportPage.prototype.getId = function (veh) {
        this.vehId = veh;
        // this.vehId = veh.Device_ID;
    };
    FuelConsumptionReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    FuelConsumptionReportPage.prototype.getReport = function () {
        var _this = this;
        var payload = {};
        var _basU = "https://www.oneqlik.in/gps/updatedFuelReport";
        debugger;
        if (this.repkey === "Daily" || this.repkey === "Trip") {
            if (this.vehId == undefined) {
                var toast = this.toastCtrl.create({
                    message: 'Please Select Vehicle..',
                    duration: 1500,
                    position: "bottom"
                });
                toast.present();
            }
            else {
                if (this.repkey === "Daily") {
                    payload = {
                        "from": new Date(this.datetimeStart).toISOString(),
                        "to": new Date(this.datetimeEnd).toISOString(),
                        "user": this.islogin._id,
                        "daywise": true,
                        "imei": this.vehId.Device_ID,
                        "vehicle": this.vehId._id
                    };
                }
                else if (this.repkey === "Trip") {
                    payload = {
                        "from": new Date(this.datetimeStart).toISOString(),
                        "to": new Date(this.datetimeEnd).toISOString(),
                        "user": this.islogin._id,
                        "trip": true,
                        "imei": this.vehId.Device_ID,
                        "vehicle": this.vehId._id
                    };
                }
                this.apiCall.startLoading().present();
                this.apiCall.urlpasseswithdata(_basU, payload)
                    .subscribe(function (res) {
                    _this.apiCall.stopLoading();
                    console.log("report data: ", res);
                    _this.fuelDataArr = [];
                    if (res.message === undefined) {
                        _this.fuelDataArr = res;
                    }
                    else {
                        var toast = _this.toastCtrl.create({
                            message: "No data found..",
                            duration: 1500,
                            position: 'bottom'
                        });
                        toast.present();
                    }
                });
            }
        }
        else if (this.repkey === "Time") {
            if (this.vehId != undefined) {
                payload = {
                    "from": new Date(this.datetimeStart).toISOString(),
                    "to": new Date(this.datetimeEnd).toISOString(),
                    "imei": this.vehId.Device_ID,
                    "vehicle": this.vehId._id
                };
            }
            else {
                payload = {
                    "from": new Date(this.datetimeStart).toISOString(),
                    "to": new Date(this.datetimeEnd).toISOString(),
                    "user": this.islogin._id
                };
            }
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(_basU, payload)
                .subscribe(function (res) {
                _this.apiCall.stopLoading();
                console.log("report data: ", res);
                _this.fuelDataArr = [];
                if (res.message === undefined) {
                    _this.fuelDataArr = res;
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: "No data found..",
                        duration: 1500,
                        position: 'bottom'
                    });
                    toast.present();
                }
            });
        }
    };
    FuelConsumptionReportPage.prototype.device_address = function (device, index) {
        var that = this;
        that.fuelDataArr[index].saddress = "N/A";
        if (!device.start_location) {
            that.fuelDataArr[index].saddress = "N/A";
        }
        else if (device.start_location) {
            this.geocoderApi.reverseGeocode(Number(device.start_location.lat), Number(device.start_location.lng))
                .then(function (res) {
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.fuelDataArr[index].saddress = str;
            });
        }
    };
    FuelConsumptionReportPage.prototype.device_eaddress = function (device, index) {
        debugger;
        var that = this;
        that.fuelDataArr[index].eaddress = "N/A";
        if (!device.end_location) {
            that.fuelDataArr[index].eaddress = "N/A";
        }
        else if (device.end_location) {
            this.geocoderApi.reverseGeocode(Number(device.end_location.lat), Number(device.end_location.lng))
                .then(function (res) {
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.fuelDataArr[index].eaddress = str;
            });
        }
    };
    FuelConsumptionReportPage.prototype.secondsToHms = function (d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);
        var hDisplay = h > 0 ? h + (h == 1 ? " hr, " : " hrs, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? " min, " : " mins, ") : "";
        var sDisplay = s > 0 ? s + (s == 1 ? " sec" : " sec") : "";
        return hDisplay + mDisplay + sDisplay;
    };
    FuelConsumptionReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fuel-consumption-report',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\fuel\fuel-consumption-report\fuel-consumption-report.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Fuel Consumption Report</ion-title>\n  </ion-navbar>\n  <ion-item style="background-color: #fafafa;">\n    <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n    <select-searchable\n      item-content\n      [(ngModel)]="selectedVehicle"\n      [items]="portstemp"\n      itemValueField="Device_Name"\n      itemTextField="Device_Name"\n      [canSearch]="true"\n      (onChange)="getId(selectedVehicle)"\n    >\n    </select-searchable>\n  </ion-item>\n  <ion-item style="background-color: #fafafa;">\n    <ion-label>Report Type</ion-label>\n    <ion-select [(ngModel)]="repkey">\n      <ion-option\n        *ngFor="let rep of reportType"\n        [value]="rep"\n        (ionSelect)="reportWise(rep)"\n        [selected]="repkey === rep"\n        >{{ rep }}</ion-option\n      >\n    </ion-select>\n  </ion-item>\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">From Date</span>\n        <ion-datetime\n          displayFormat="DD-MM-YYYY hh:mm a"\n          pickerFormat="DD/MM/YY hh:mm a"\n          [(ngModel)]="datetimeStart"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"\n        >\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">To Date</span>\n        <ion-datetime\n          displayFormat="DD-MM-YYYY hh:mm a"\n          pickerFormat="DD/MM/YY hh:mm a"\n          [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"\n        >\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right;">\n        <ion-icon\n          ios="ios-search"\n          md="md-search"\n          style="font-size:2.3em;"\n          (click)="getReport()"\n        ></ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n<ion-content>\n  <ion-card *ngFor="let r of fuelDataArr; let i = index">\n    <ion-card-header no-padding>\n      <ion-item *ngIf="repkey !== \'Trip\'">\n        <ion-avatar item-start>\n          <img src="assets/imgs/car_red_icon.png" />\n        </ion-avatar>\n        <ion-label>{{ r.vehicle_name }}</ion-label>\n      </ion-item>\n    </ion-card-header>\n    <ion-card-content\n      [ngClass]="{ paddClass: repkey === \'Trip\' }"\n    >\n      <ion-row\n        style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;"\n      >\n        <ion-col col-4 ion-text text-wrap>Start Time</ion-col>\n        <ion-col col-4 ion-text text-wrap>End Time</ion-col>\n        <ion-col col-4 ion-text text-wrap>Distance(Kms)</ion-col>\n      </ion-row>\n      <ion-row\n        style="color:gray;font-size:11px; text-align:left;font-weight: 400;"\n      >\n        <ion-col col-4 ion-text text-wrap\n          >{{ r.start_time | date: "mediumDate" }},{{\n            r.start_time | date: "shortTime"\n          }}</ion-col\n        >\n        <ion-col col-4 ion-text text-wrap>{{\n          r.end_time | date: "mediumDate"\n        }},{{\n          r.end_time | date: "shortTime"\n        }}</ion-col>\n        <ion-col col-4 ion-text text-wrap>{{\n          r.distance | number: "1.0-2"\n        }}</ion-col>\n      </ion-row>\n\n      <ion-row\n        style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;"\n      >\n        <ion-col col-4 ion-text text-wrap>Fuel Con(Litre)</ion-col>\n        <ion-col col-4 ion-text text-wrap>In event count</ion-col>\n        <ion-col col-4 ion-text text-wrap>Out event count</ion-col>\n      </ion-row>\n      <ion-row\n        style="color:gray;font-size:11px; text-align:left;font-weight: 400;"\n      >\n        <ion-col col-4>{{ r.fuel_consumed | number: "1.0-2" }}</ion-col>\n        <ion-col\n          col-4\n          *ngIf="r.in_event_count > 0"\n          tappable\n          (click)="getFuelNotifs(r, \'IN\')"\n          >{{ r.in_event_count }}</ion-col\n        >\n        <ion-col col-4 *ngIf="r.in_event_count === 0">{{\n          r.in_event_count\n        }}</ion-col>\n        <ion-col\n          col-4\n          *ngIf="r.out_event_count > 0"\n          tappable\n          (click)="getFuelNotifs(r, \'OUT\')"\n          >{{ r.out_event_count }}</ion-col\n        >\n        <ion-col col-4 *ngIf="r.out_event_count === 0">{{\n          r.out_event_count\n        }}</ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-1\n          ><ion-icon name="pin" style="color:#33c45c;font-size:15px;"></ion-icon\n        ></ion-col>\n        <ion-col\n          col-11\n          *ngIf="r.start_location === \'NA\'"\n          style="color:gray;font-size:11px;font-weight: 400;"\n          >N/A</ion-col\n        >\n        <ion-col\n          col-11\n          *ngIf="r.start_location !== \'NA\'"\n          (onCreate2)="device_address(r, i)"\n          style="color:gray;font-size:11px;font-weight: 400;"\n          >{{ r.saddress }}</ion-col\n        >\n      </ion-row>\n      <ion-row>\n        <ion-col col-1\n          ><ion-icon name="pin" style="color:#e14444;font-size:15px;"></ion-icon\n        ></ion-col>\n        <ion-col\n          col-11\n          *ngIf="r.end_location === \'NA\'"\n          style="color:gray;font-size:11px;font-weight: 400;"\n          >N/A</ion-col\n        >\n        <ion-col\n          col-11\n          *ngIf="r.end_location !== \'NA\'"\n          (onCreate2)="device_eaddress(r, i)"\n          style="color:gray;font-size:11px;font-weight: 400;"\n          >{{ r.eaddress }}</ion-col\n        >\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"D:\Pro\flex_track\src\pages\fuel\fuel-consumption-report\fuel-consumption-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"]])
    ], FuelConsumptionReportPage);
    return FuelConsumptionReportPage;
}());

//# sourceMappingURL=fuel-consumption-report.js.map

/***/ }),

/***/ 520:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FuelConsumptionReportPageModule", function() { return FuelConsumptionReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fuel_consumption_report__ = __webpack_require__(1028);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dummy2_directive_module__ = __webpack_require__(644);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var FuelConsumptionReportPageModule = /** @class */ (function () {
    function FuelConsumptionReportPageModule() {
    }
    FuelConsumptionReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fuel_consumption_report__["a" /* FuelConsumptionReportPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__fuel_consumption_report__["a" /* FuelConsumptionReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__dummy2_directive_module__["a" /* OnCreate2Module */]
            ]
        })
    ], FuelConsumptionReportPageModule);
    return FuelConsumptionReportPageModule;
}());

//# sourceMappingURL=fuel-consumption-report.module.js.map

/***/ }),

/***/ 644:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnCreate2Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__dummy2_directive__ = __webpack_require__(645);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var OnCreate2Module = /** @class */ (function () {
    function OnCreate2Module() {
    }
    OnCreate2Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__dummy2_directive__["a" /* OnCreate2 */]],
            exports: [__WEBPACK_IMPORTED_MODULE_1__dummy2_directive__["a" /* OnCreate2 */]]
        })
    ], OnCreate2Module);
    return OnCreate2Module;
}());

//# sourceMappingURL=dummy2-directive.module.js.map

/***/ }),

/***/ 645:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnCreate2; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OnCreate2 = /** @class */ (function () {
    function OnCreate2() {
        this.onCreate2 = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    OnCreate2.prototype.ngOnInit = function () {
        this.onCreate2.emit('dummy');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], OnCreate2.prototype, "onCreate2", void 0);
    OnCreate2 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[onCreate2]'
        }),
        __metadata("design:paramtypes", [])
    ], OnCreate2);
    return OnCreate2;
}());

//# sourceMappingURL=dummy2-directive.js.map

/***/ })

});
//# sourceMappingURL=7.js.map