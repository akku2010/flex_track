webpackJsonp([48],{

/***/ 1017:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactUsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number__ = __webpack_require__(409);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ContactUsPage = /** @class */ (function () {
    function ContactUsPage(navCtrl, navParams, formBuilder, api, toastCtrl, callNumber) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.callNumber = callNumber;
        this.contact_data = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.contactusForm = formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            mail: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].email],
            mobno: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            note: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]
        });
    }
    ContactUsPage_1 = ContactUsPage;
    ContactUsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactUsPage');
    };
    ContactUsPage.prototype.call = function (num) {
        this.callNumber.callNumber(num, true)
            .then(function (res) { return console.log('Launched dialer!', res); })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    ContactUsPage.prototype.contactUs = function () {
        var _this = this;
        this.submitAttempt = true;
        if (this.contactusForm.valid) {
            this.contact_data = {
                "user": this.islogin._id,
                "email": this.contactusForm.value.mail,
                "msg": this.contactusForm.value.note,
                "phone": (this.contactusForm.value.mobno).toString(),
            };
            this.api.startLoading().present();
            this.api.contactusApi(this.contact_data)
                .subscribe(function (data) {
                _this.api.stopLoading();
                console.log(data.message);
                if (data.message == 'email sent') {
                    var toast = _this.toastCtrl.create({
                        message: 'Your request has been submitted successfully. We will get back to you soon.',
                        position: 'bottom',
                        duration: 3000
                    });
                    // toast.onDidDismiss(() => {
                    //   console.log('Dismissed toast');
                    //   // this.contactusForm.reset();
                    //   this.navCtrl.setRoot(ContactUsPage);
                    // });
                    toast.present();
                    _this.navCtrl.setRoot(ContactUsPage_1);
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: 'Something went wrong. Please try after some time.',
                        position: 'bottom',
                        duration: 3000
                    });
                    toast.onDidDismiss(function () {
                        console.log('Dismissed toast');
                        _this.navCtrl.setRoot(ContactUsPage_1);
                    });
                    toast.present();
                }
            }, function (error) {
                _this.api.stopLoading();
                console.log(error);
            });
        }
    };
    ContactUsPage = ContactUsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-contact-us',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\contact-us\contact-us.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Contact Us</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <form class="form" [formGroup]="contactusForm">\n\n    <p type="Name:">\n\n      <input formControlName="name" type="text" placeholder="Write your name here.." />\n\n    </p>\n\n    <span class="span"\n\n      *ngIf="!contactusForm.controls.name.valid && (contactusForm.controls.name.dirty || submitAttempt)">Name is\n\n      required and should be in valid format!</span>\n\n    <p type="Email:">\n\n      <input formControlName="mail" type="email" placeholder="Let us know how to contact you back.." />\n\n    </p>\n\n    <span class="span"\n\n      *ngIf="!contactusForm.controls.mail.valid && (contactusForm.controls.mail.dirty || submitAttempt)">Email id is\n\n      required and should be in valid format!</span>\n\n    <p type="Mobile Num.:">\n\n      <input formControlName="mobno" type="number" maxlength="10" minlength="10"\n\n        placeholder="Let us know how to contact you back via mobile number.." />\n\n    </p>\n\n    <span class="span"\n\n      *ngIf="!contactusForm.controls.mobno.valid && (contactusForm.controls.mobno.dirty || submitAttempt)">Mobile number\n\n      is required and should be in 10 digits format!</span>\n\n    <p type="Message:">\n\n      <textarea rows="2" cols="50" formControlName="note" placeholder="What would you like to tell us.."></textarea>\n\n    </p>\n\n    <span class="span"\n\n      *ngIf="!contactusForm.controls.note.valid && (contactusForm.controls.note.dirty || submitAttempt)">please write\n\n      your message!</span>\n\n    <ion-row style="padding-bottom: 20px;">\n\n      <ion-col col-12><button (tap)="contactUs()" class="buttonclass">Send Message</button></ion-col>\n\n    </ion-row>\n\n\n\n    <!-- <ion-row style="text-align: center;">\n\n      <ion-col col-12 (click)="call(8956628472)" style="color: rgb(131, 128, 128)">\n\n        <ion-icon name="call" color="gpsc"></ion-icon>&nbsp; +91 8956628472\n\n      </ion-col>\n\n      \n\n    </ion-row>\n\n    <ion-row style="text-align: center;">\n\n      \n\n      <ion-col col-12 style="color: rgb(131, 128, 128)">\n\n        <ion-icon name="mail" color="primary"></ion-icon>&nbsp;support@oneqlik.in\n\n      </ion-col>\n\n    </ion-row> -->\n\n  </form>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\contact-us\contact-us.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number__["a" /* CallNumber */]])
    ], ContactUsPage);
    return ContactUsPage;
    var ContactUsPage_1;
}());

//# sourceMappingURL=contact-us.js.map

/***/ }),

/***/ 511:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactUsPageModule", function() { return ContactUsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_us__ = __webpack_require__(1017);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ContactUsPageModule = /** @class */ (function () {
    function ContactUsPageModule() {
    }
    ContactUsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__contact_us__["a" /* ContactUsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__contact_us__["a" /* ContactUsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["ReactiveFormsModule"]
            ],
        })
    ], ContactUsPageModule);
    return ContactUsPageModule;
}());

//# sourceMappingURL=contact-us.module.js.map

/***/ })

});
//# sourceMappingURL=48.js.map