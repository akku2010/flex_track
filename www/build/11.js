webpackJsonp([11],{

/***/ 1050:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TripReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TripReportPage = /** @class */ (function () {
    function TripReportPage(navCtrl, navParams, apicalligi, alertCtrl, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalligi = apicalligi;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.portstemp = [];
        this.allData = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        if (navParams.get('param') != null) {
            this.vehicleData = navParams.get('param');
        }
    }
    TripReportPage.prototype.ngOnInit = function () {
        if (this.vehicleData == undefined) {
            this.getdevices();
        }
        else {
            this.device_id = this.vehicleData._id;
            this.getTripReport();
        }
    };
    TripReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicalligi.startLoading().present();
        this.apicalligi.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicalligi.stopLoading();
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.isdevice = localStorage.getItem('devices1243');
        }, function (err) {
            _this.apicalligi.stopLoading();
            console.log(err);
        });
    };
    TripReportPage.prototype.getTripdevice = function (item) {
        this.device_id = item._id;
        console.log("device id=> " + this.device_id);
        this.did = item.Device_ID;
        localStorage.setItem('devices_id', item);
        this.isdeviceTripreport = localStorage.getItem('devices_id');
    };
    TripReportPage.prototype.getTripReport = function () {
        var _this = this;
        this.TripReportData = [];
        if (this.datetimeEnd <= this.datetimeStart && this.device_id) {
            var alert_1 = this.alertCtrl.create({
                message: 'To time is always greater than From Time',
                buttons: ['OK']
            });
            alert_1.present();
        }
        else {
            this.apicalligi.startLoading().present();
            this.apicalligi.trip_detailCall(this.islogin._id, new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), this.device_id)
                .subscribe(function (data) {
                _this.apicalligi.stopLoading();
                _this.TripsdataAddress = [];
                if (data.length > 0) {
                    _this.tripFunction(data);
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: "Report(s) not found for selected dates/Vehicles.",
                        duration: 1500,
                        position: "bottom"
                    });
                    toast.present();
                }
            }, function (err) {
                _this.apicalligi.stopLoading();
                console.log(err);
            });
        }
    };
    TripReportPage.prototype.tripFunction = function (data) {
        var that = this;
        var i = 0, howManyTimes = data.length;
        function f() {
            var deviceId = data[i]._id;
            var distanceBt = data[i].distance / 1000;
            var gmtDateTime = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(data[i].start_time).split('T')[1].split('.')[0], "HH:mm:ss");
            var gmtDate = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(data[i].start_time).slice(0, -1).split('T'), "YYYY-MM-DD");
            var Startetime = gmtDateTime.local().format(' h:mm a');
            var Startdate = gmtDate.format('ll');
            var gmtDateTime1 = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(data[i].end_time).split('T')[1].split('.')[0], "HH:mm:ss");
            var gmtDate1 = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(data[i].end_time).slice(0, -1).split('T'), "YYYY-MM-DD");
            var Endtime = gmtDateTime1.local().format(' h:mm a');
            var Enddate = gmtDate1.format('ll');
            var startDate = new Date(data[i].start_time).toLocaleString();
            var endDate = new Date(data[i].end_time).toLocaleString();
            var fd = new Date(startDate).getTime();
            var td = new Date(endDate).getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60;
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            var Durations = rhours + 'hrs' + ' ' + rminutes + 'mins';
            that.TripReportData.push({ 'Device_Name': data[i].device.Device_Name, 'Startetime': Startetime, 'Startdate': Startdate, 'Endtime': Endtime, 'Enddate': Enddate, 'distance': distanceBt, '_id': deviceId, 'start_time': data[i].start_time, 'end_time': data[i].end_time, 'duration': Durations });
            if (data[i].end_lat != null && data[i].start_lat != null) {
                var latEnd = data[i].end_lat;
                var lngEnd = data[i].end_long;
                var latlng = new google.maps.LatLng(latEnd, lngEnd);
                var latStart = data[i].start_lat;
                var lngStart = data[i].start_long;
                var lngStart1 = new google.maps.LatLng(latStart, lngStart);
                var geocoder = new google.maps.Geocoder();
                var request = {
                    latLng: latlng
                };
                var request1 = {
                    latLng: lngStart1
                };
                geocoder.geocode(request, function (data, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (data[1] != null) {
                            that.locationEndAddress = data[1].formatted_address;
                        }
                    }
                    if (that.locationEndAddress) {
                        that.TripReportData[that.TripReportData.length - 1].endAddress = that.locationEndAddress;
                    }
                    else {
                        that.TripReportData[that.TripReportData.length - 1].endAddress = 'N/A';
                    }
                });
                geocoder.geocode(request1, function (data, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (data[1] != null) {
                            that.locationAddress = data[1].formatted_address;
                        }
                    }
                    if (that.locationAddress) {
                        that.TripReportData[that.TripReportData.length - 1].startAddress = that.locationAddress;
                    }
                    else {
                        that.TripReportData[that.TripReportData.length - 1].startAddress = 'N/A';
                    }
                });
                i++;
                if (i < howManyTimes) {
                    setTimeout(f, 200);
                }
            }
        }
        f();
    };
    TripReportPage.prototype.tripReview = function (tripData) {
        this.navCtrl.push('TripReviewPage', {
            params: tripData,
            device_id: this.did
        });
    };
    TripReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-trip-report',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\trip-report\trip-report.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Trip Report</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="background-color: #fafafa;" *ngIf="portstemp.length != 0">\n\n    <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getTripdevice(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getTripReport();"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n<ion-content [ngClass]="{ \'masters\': portstemp.length != 0,\'masters1\': portstemp.length == 0 }">\n\n\n\n  <ion-card *ngFor="let tripdata of TripReportData" style="border-radius: 5px;">\n\n    <ion-item>\n\n      <ion-label item-start>{{tripdata.Device_Name}}</ion-label>\n\n      <ion-badge item-end color="gpsc">{{tripdata.distance | number : \'1.0-2\'}}KM</ion-badge>\n\n    </ion-item>\n\n    <ion-card-content>\n\n      <ion-row>\n\n        <ion-col col-8 padding-right>\n\n          <ion-row style="height: 33px; overflow: hidden;text-overflow: ellipsis;">\n\n            <ion-col col-1>\n\n              <ion-icon name="pin" color="secondary" style="font-size: 15px;"></ion-icon>\n\n            </ion-col>\n\n            <ion-col col-11 style="font-size: 11px">\n\n              {{tripdata.startAddress}}\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row style="height: 33px; overflow: hidden;text-overflow: ellipsis;">\n\n            <ion-col col-1>\n\n              <ion-icon name="pin" color="danger" style="font-size: 15px;"></ion-icon>\n\n            </ion-col>\n\n            <ion-col col-11 style="font-size: 11px">\n\n              {{tripdata.endAddress}}\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n        </ion-col>\n\n        <ion-col col-4>\n\n          <ion-thumbnail item-end>\n\n            <img src="assets/imgs/5Jzwb.webp" height="70" width="70">\n\n          </ion-thumbnail>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n      <!-- <ion-row>\n\n        <ion-col col-8 padding-right>\n\n          <ion-row>\n\n              <ion-col style="font-size:1.2em;"><b>{{tripdata.Device_Name}}</b></ion-col>\n\n          </ion-row>\n\n          <ion-row style="height: 45px; overflow: hidden;text-overflow: ellipsis;">\n\n            <ion-col col-2>\n\n              <ion-icon name="pin" color="secondary" style="font-size: 25px;"></ion-icon>\n\n            </ion-col>\n\n            <ion-col col-10>\n\n              <p>{{tripdata.startAddress}}</p>\n\n            </ion-col>\n\n          </ion-row>\n\n          <ion-row style="height: 45px; overflow: hidden;text-overflow: ellipsis;">\n\n            <ion-col col-2>\n\n              <ion-icon name="pin" color="danger" style="font-size: 25px;"></ion-icon>\n\n            </ion-col>\n\n            <ion-col col-10>\n\n              <p>{{tripdata.endAddress}}</p>\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n        </ion-col>\n\n        <ion-col col-4>\n\n          <ion-thumbnail item-end>\n\n            <img src="assets/imgs/5Jzwb.png">\n\n          </ion-thumbnail>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row style="padding-top: 10px;">\n\n        <ion-col col-6 class="divS">\n\n          <p style="padding: 0px; margin: 0px;" *ngIf="tripdata.distance">\n\n            <span>DISTNACE </span>\n\n            <b>{{tripdata.distance | number : \'1.0-2\'}}KM</b>\n\n          </p>\n\n          <p style="padding: 0px; margin: 0px;" *ngIf="!tripdata.distance">\n\n            <span>DISTNACE </span><b>0KM</b></p>\n\n        </ion-col>\n\n        <ion-col col-6 class="divS">\n\n          <p *ngIf="tripdata.duration">\n\n            <span>DURATION </span>\n\n            <b>{{tripdata.duration}}</b>\n\n          </p>\n\n          <p *ngIf="!tripdata.duration">\n\n            <span>DURATION </span><b>0hrs 0mins</b></p>\n\n        </ion-col>\n\n      </ion-row> -->\n\n    </ion-card-content>\n\n    <!-- <ion-row style="background-color:#f2f2f2; padding-top:5px; padding-bottom: 5px" padding-left padding-right>\n\n      <p style="font-size:0.85em; color:gray; font-weight: bold">{{tripdata.Startdate}}</p>\n\n    </ion-row> -->\n\n    <ion-row style="background-color:#f2f2f2; padding-top:5px; padding-bottom: 5px" padding-left padding-right>\n\n      <ion-col col-6 style=" color:gray; font-weight: bold; text-align: left;">{{tripdata.Startdate}}</ion-col>\n\n      <ion-col col-6 style=" color:gray; font-weight: bold; text-align: right;" *ngIf="tripdata.duration">\n\n        DURATION&nbsp;{{tripdata.duration}}</ion-col>\n\n      <ion-col col-6 style=" color:gray; font-weight: bold; text-align: right;" *ngIf="!tripdata.duration">\n\n        DURATION&nbsp;0hrs 0mins</ion-col>\n\n    </ion-row>\n\n  </ion-card>\n\n \n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\trip-report\trip-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], TripReportPage);
    return TripReportPage;
}());

//# sourceMappingURL=trip-report.js.map

/***/ }),

/***/ 543:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TripReportPageModule", function() { return TripReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__trip_report__ = __webpack_require__(1050);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var TripReportPageModule = /** @class */ (function () {
    function TripReportPageModule() {
    }
    TripReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__trip_report__["a" /* TripReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__trip_report__["a" /* TripReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], TripReportPageModule);
    return TripReportPageModule;
}());

//# sourceMappingURL=trip-report.module.js.map

/***/ })

});
//# sourceMappingURL=11.js.map