webpackJsonp([28],{

/***/ 1035:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IgnitionReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var IgnitionReportPage = /** @class */ (function () {
    function IgnitionReportPage(navCtrl, navParams, apicalligi, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalligi = apicalligi;
        this.toastCtrl = toastCtrl;
        this.igiReport = [];
        this.igiReportData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
    }
    IgnitionReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    IgnitionReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicalligi.startLoading().present();
        this.apicalligi.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicalligi.stopLoading();
            _this.devices = data;
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apicalligi.stopLoading();
            console.log(err);
        });
    };
    IgnitionReportPage.prototype.getIgnitiondevice = function (selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.Ignitiondevice_id = selectedVehicle.Device_Name;
    };
    IgnitionReportPage.prototype.getIgnitiondeviceReport = function () {
        var _this = this;
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        var that = this;
        this.apicalligi.startLoading().present();
        this.apicalligi.getIgiApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.Ignitiondevice_id, this.islogin._id)
            .subscribe(function (data) {
            _this.apicalligi.stopLoading();
            _this.igiReport = data;
            if (_this.igiReport.length > 0) {
                _this.innerFunc(_this.igiReport);
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Report(s) not found for selected Dates/Vehicle.',
                    duration: 1500,
                    position: 'bottom'
                });
                toast.present();
            }
        }, function (error) {
            _this.apicalligi.stopLoading();
            console.log(error);
        });
    };
    IgnitionReportPage.prototype.innerFunc = function (igiReport) {
        var outerthis = this;
        outerthis.igiReportData = [];
        outerthis.locationEndAddress = undefined;
        var i = 0, howManyTimes = igiReport.length;
        function f() {
            outerthis.igiReportData.push({
                'vehicleName': outerthis.igiReport[i].vehicleName,
                'switch': outerthis.igiReport[i].switch,
                'timestamp': outerthis.igiReport[i].timestamp
            });
            if (outerthis.igiReport[i].lat != null && outerthis.igiReport[i].long != null) {
                var latEnd = outerthis.igiReport[i].lat;
                var lngEnd = outerthis.igiReport[i].long;
                var latlng = new google.maps.LatLng(latEnd, lngEnd);
                var geocoder = new google.maps.Geocoder();
                var request = {
                    latLng: latlng
                };
                geocoder.geocode(request, function (data, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (data[1] != null) {
                            outerthis.locationEndAddress = data[1].formatted_address;
                        }
                    }
                    outerthis.igiReportData[outerthis.igiReportData.length - 1].address = outerthis.locationEndAddress;
                });
            }
            else {
                outerthis.igiReportData[outerthis.igiReportData.length - 1].address = 'N/A';
            }
            console.log(outerthis.igiReportData);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    IgnitionReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-ignition-report',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\ignition-report\ignition-report.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Ignition Report</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getIgnitiondevice(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD-MM-YYYY hh:mm a" [(ngModel)]="datetimeStart"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD-MM-YYYY hh:mm a" [(ngModel)]="datetimeEnd"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getIgnitiondeviceReport();">\n\n        </ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list>\n\n    <ion-card *ngFor="let ignitationdata of igiReportData">\n\n\n\n      <ion-item style="border-bottom: 2px solid #dedede;">\n\n        <ion-avatar item-start>\n\n          <img src="assets/imgs/car_red_icon.png">\n\n        </ion-avatar>\n\n        <ion-label>{{ignitationdata.vehicleName}}</ion-label>\n\n        <ion-badge item-end color="gpsc" *ngIf="ignitationdata.switch==\'OFF\'">\n\n          <ion-icon ios="ios-switch" md="md-switch"></ion-icon>\n\n        </ion-badge>\n\n        <ion-badge item-end color="secondary" *ngIf="ignitationdata.switch==\'ON\'">\n\n          <ion-icon ios="ios-switch" md="md-switch"></ion-icon>\n\n        </ion-badge>\n\n      </ion-item>\n\n      <ion-card-content>\n\n        <ion-row style="padding-top:5px;">\n\n          <ion-col col-1>\n\n            <ion-icon ios="ios-time" md="md-time" style="font-size:15px;"></ion-icon>\n\n          </ion-col>\n\n          <ion-col col-11 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">\n\n            {{ignitationdata.timestamp | date: \'medium\'}}</ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-1>\n\n            <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;font-size:15px;"></ion-icon>\n\n          </ion-col>\n\n          <ion-col col-11 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">\n\n            {{ignitationdata.address}}</ion-col>\n\n\n\n        </ion-row>\n\n      </ion-card-content>\n\n    </ion-card>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\ignition-report\ignition-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], IgnitionReportPage);
    return IgnitionReportPage;
}());

//# sourceMappingURL=ignition-report.js.map

/***/ }),

/***/ 528:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IgnitionReportPageModule", function() { return IgnitionReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ignition_report__ = __webpack_require__(1035);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var IgnitionReportPageModule = /** @class */ (function () {
    function IgnitionReportPageModule() {
    }
    IgnitionReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__ignition_report__["a" /* IgnitionReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__ignition_report__["a" /* IgnitionReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], IgnitionReportPageModule);
    return IgnitionReportPageModule;
}());

//# sourceMappingURL=ignition-report.module.js.map

/***/ })

});
//# sourceMappingURL=28.js.map