webpackJsonp([61],{

/***/ 138:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 138;

/***/ }),

/***/ 179:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/ac-report/ac-report.module": [
		503,
		60
	],
	"../pages/add-devices/add-devices.module": [
		561,
		55
	],
	"../pages/add-devices/immobilize/immobilize.module": [
		504,
		54
	],
	"../pages/add-devices/payment-greeting/payment-greeting.module": [
		505,
		53
	],
	"../pages/add-devices/payment-secure/payment-secure.module": [
		506,
		8
	],
	"../pages/add-devices/paytmwalletlogin/paytmwalletlogin.module": [
		507,
		52
	],
	"../pages/add-devices/update-device/update-device.module": [
		545,
		51
	],
	"../pages/add-devices/upload-doc/add-doc/add-doc.module": [
		508,
		50
	],
	"../pages/add-devices/upload-doc/upload-doc.module": [
		509,
		59
	],
	"../pages/add-devices/vehicle-details/vehicle-details.module": [
		562,
		4
	],
	"../pages/add-devices/wallet/wallet.module": [
		510,
		0
	],
	"../pages/all-notifications/all-notifications.module": [
		546,
		49
	],
	"../pages/contact-us/contact-us.module": [
		511,
		48
	],
	"../pages/create-trip/create-trip.module": [
		547,
		47
	],
	"../pages/customers/customers.module": [
		548,
		46
	],
	"../pages/customers/modals/add-customer-modal/add-customer-modal.module": [
		550,
		45
	],
	"../pages/customers/modals/add-device-modal.module": [
		549,
		44
	],
	"../pages/customers/modals/group-modal/group-modal.module": [
		512,
		43
	],
	"../pages/customers/modals/update-cust/update-cust.module": [
		513,
		42
	],
	"../pages/daily-report/daily-report.module": [
		551,
		41
	],
	"../pages/dashboard/dashboard.module": [
		552,
		1
	],
	"../pages/dealers/add-dealer/add-dealer.module": [
		514,
		40
	],
	"../pages/dealers/dealers.module": [
		553,
		39
	],
	"../pages/dealers/edit-dealer/edit-dealer.module": [
		515,
		38
	],
	"../pages/device-summary-repo/device-summary-repo.module": [
		516,
		37
	],
	"../pages/distance-report/distance-report.module": [
		518,
		36
	],
	"../pages/feedback/feedback.module": [
		517,
		5
	],
	"../pages/fuel/fuel-chart/fuel-chart.module": [
		519,
		2
	],
	"../pages/fuel/fuel-consumption-report/fuel-consumption-report.module": [
		520,
		7
	],
	"../pages/fuel/fuel-consumption/fuel-consumption.module": [
		521,
		58
	],
	"../pages/fuel/fuel-events/fuel-events.module": [
		522,
		6
	],
	"../pages/geofence-report/geofence-report.module": [
		523,
		35
	],
	"../pages/geofence/add-geofence/add-geofence.module": [
		554,
		34
	],
	"../pages/geofence/geofence-show/geofence-show.module": [
		524,
		33
	],
	"../pages/geofence/geofence.module": [
		525,
		32
	],
	"../pages/groups/groups.module": [
		526,
		31
	],
	"../pages/history-device/history-device.module": [
		559,
		30
	],
	"../pages/idle-report/idle-report.module": [
		527,
		29
	],
	"../pages/ignition-report/ignition-report.module": [
		528,
		28
	],
	"../pages/live-single-device/device-settings/device-settings.module": [
		529,
		27
	],
	"../pages/live-single-device/live-single-device.module": [
		563,
		57
	],
	"../pages/live/expired/expired.module": [
		530,
		26
	],
	"../pages/live/live.module": [
		560,
		25
	],
	"../pages/login/login.module": [
		555,
		24
	],
	"../pages/over-speed-repo/over-speed-repo.module": [
		531,
		23
	],
	"../pages/poi-list/add-poi/add-poi.module": [
		558,
		22
	],
	"../pages/poi-list/poi-list.module": [
		532,
		21
	],
	"../pages/poi-report/poi-report.module": [
		533,
		20
	],
	"../pages/profile/profile.module": [
		556,
		56
	],
	"../pages/profile/settings/settings.module": [
		534,
		19
	],
	"../pages/route-map-show/route-map-show.module": [
		535,
		18
	],
	"../pages/route-voilations/route-voilations.module": [
		536,
		17
	],
	"../pages/route/route.module": [
		537,
		16
	],
	"../pages/signup/signup-otp/signup-otp.module": [
		538,
		15
	],
	"../pages/signup/signup.module": [
		539,
		14
	],
	"../pages/sos-report/sos-report.module": [
		540,
		13
	],
	"../pages/speed-repo/speed-repo.module": [
		541,
		3
	],
	"../pages/stoppages-repo/stoppages-repo.module": [
		542,
		12
	],
	"../pages/trip-report/trip-report.module": [
		543,
		11
	],
	"../pages/trip-report/trip-review/trip-review.module": [
		557,
		10
	],
	"../pages/your-trips/your-trips.module": [
		544,
		9
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 179;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 20:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_timeout__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ApiServiceProvider = /** @class */ (function () {
    function ApiServiceProvider(http, loadingCtrl) {
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json; charset=utf-8' });
        this.mainUrl = 'https://www.oneqlik.in/';
        this.usersURL = "https://www.oneqlik.in/users/";
        this.devicesURL = "https://www.oneqlik.in/devices";
        this.gpsURL = "https://www.oneqlik.in/gps";
        this.geofencingURL = "https://www.oneqlik.in/geofencing";
        this.trackRouteURL = "https://www.oneqlik.in/trackRoute";
        this.groupURL = "https://www.oneqlik.in/groups/";
        this.notifsURL = "https://www.oneqlik.in/notifs";
        this.stoppageURL = "https://www.oneqlik.in/stoppage";
        this.summaryURL = "https://www.oneqlik.in/summary";
        this.shareURL = "https://www.oneqlik.in/share";
        this.appId = "OneQlikVTS";
        console.log('Hello ApiServiceProvider Provider');
    }
    ////////////////// LOADING SERVICE /////////////////
    ApiServiceProvider.prototype.startLoadingnew = function (key) {
        var str;
        if (key == 1) {
            str = 'unlocking';
        }
        else {
            str = 'locking';
        }
        return this.loading1 = this.loadingCtrl.create({
            content: "Please wait for some time, as we are " + str + " your vehicle...",
            spinner: "bubbles"
        });
    };
    ApiServiceProvider.prototype.stopLoadingnw = function () {
        return this.loading1.dismiss();
    };
    ApiServiceProvider.prototype.startLoading = function () {
        return this.loading = this.loadingCtrl.create({
            content: "Please wait...",
            spinner: "bubbles"
        });
    };
    ApiServiceProvider.prototype.stopLoading = function () {
        return this.loading.dismiss();
    };
    ////////////////// END LOADING SERVICE /////////////
    ApiServiceProvider.prototype.getAddressApi = function (data) {
        return this.http.post("https://www.oneqlik.in/gps/getaddress", data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updatePOIAPI = function (pay) {
        return this.http.post("https://www.oneqlik.in/vehtra/poi/updatePOI", pay, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addPOIAPI = function (payload) {
        return this.http.post("https://www.oneqlik.in/poi/addpoi", payload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deletePOIAPI = function (_id) {
        return this.http.get("https://www.oneqlik.in/poi/deletePoi?_id=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getpoireportAPI = function (_id, pageNo, fromT, toT, poiId, devid) {
        return this.http.get("https://www.oneqlik.in/poi/poiReport?user=" + _id + "&s=" + pageNo + "&l=9&from=" + fromT + "&to=" + toT + "&poi=" + poiId + "&device=" + devid, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getPoisAPI = function (id) {
        return this.http.get("https://www.oneqlik.in/poi/getPois?user=" + id)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateDL = function (updateDL) {
        return this.http.post('https://www.oneqlik.in/users' + "/zogoUserUpdate", updateDL, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getsingledevice = function (id) {
        return this.http.get(this.devicesURL + "/getDevicebyId?deviceId=" + id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSOSReportAPI = function (url) {
        // return this.http.get("http://192.168.1.20:3000/notifs/SOSReport?from_date=" + starttime + '&to_date=' + endtime + '&dev_id=' + sos_id + '&_u=' + _id, { headers: this.headers })
        return this.http.get(url, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.urlpasseswithdata = function (url, data) {
        return this.http.post(url, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.DealerSearchService = function (_id, pageno, limit, key) {
        return this.http.get(this.usersURL + 'getAllDealerVehicles?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit + '&search=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteDealerCall = function (deletePayload) {
        return this.http.post(this.usersURL + 'deleteUser', deletePayload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDealersCall = function (_id, pageno, limit, searchKey) {
        if (searchKey != undefined)
            return this.http.get(this.usersURL + 'getDealers?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit + '&search=' + searchKey, { headers: this.headers })
                .map(function (res) { return res.json(); });
        else
            return this.http.get(this.usersURL + 'getDealers?supAdmin=' + _id + '&pageNo=' + pageno + '&size=' + limit, { headers: this.headers })
                .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.releaseAmount = function (uid) {
        return this.http.post('https://www.oneqlik.in/paytm/releaseAmount', uid)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateTripStatus = function (uId) {
        return this.http.get('https://www.oneqlik.in/zogo/updateZogoPaymentStatus?user=' + uId)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.proceedSecurely = function (withdrawObj) {
        return this.http.post('https://www.oneqlik.in/paytm/Withdraw', withdrawObj)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getblockamt = function () {
        return this.http.get('https://www.oneqlik.in/zogo/getInnitialBlockAmount')
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.paytmbalance = function (userCred) {
        return this.http.get('https://www.oneqlik.in/paytm/checkBalance?CUST_ID=' + userCred + "&app_id=" + this.appId)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addMoneyPaytm = function (amtObj) {
        return this.http.post('https://www.oneqlik.in/paytm/addMoney', amtObj)
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.paytmLoginWallet = function (walletcredentials) {
        return this.http.post('https://www.oneqlik.in/paytm/sendOTP', walletcredentials)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.paytmOTPvalidation = function (otpObj) {
        return this.http.post('https://www.oneqlik.in/paytm/validateOTP', otpObj)
            .map(function (res) { return res.json(); });
    };
    // callSearchService(email, id, key) {
    //   return this.http.get(this.devicesURL + "/getDeviceByUser?email=" + email + "&id=" + id + "&skip=0&limit=10&search=" + key, { headers: this.headers })
    //     .map(resp => resp.json());
    // }
    ApiServiceProvider.prototype.callSearchService = function (baseURL) {
        return this.http.get(baseURL, { headers: this.headers })
            .map(function (resp) { return resp.json(); });
    };
    ApiServiceProvider.prototype.callResponse = function (_id) {
        return this.http.get("https://www.oneqlik.in/trackRouteMap/getRideStatusApp?_id=" + _id, { headers: this.headers })
            .map(function (resp) { return resp.json(); });
    };
    ApiServiceProvider.prototype.serverLevelonoff = function (data) {
        return this.http.post(this.devicesURL + "/addCommandQueue", data, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updatePassword = function (data) {
        return this.http.post(this.usersURL + "updatePassword", data, { headers: this.headers })
            .timeout(5000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.updateprofile = function (data) {
        return this.http.post(this.usersURL + 'Account_Edit', data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGeofenceCall = function (_id) {
        return this.http.get(this.geofencingURL + '/getgeofence?uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.get7daysData = function (a, t) {
        return this.http.get(this.gpsURL + '/getDashGraph?imei=' + a + '&t=' + t, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.dataRemoveFuncCall = function (_id, did) {
        return this.http.get(this.devicesURL + '/RemoveShareDevice?did=' + did + '&uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.tripReviewCall = function (device_id, stime, etime) {
        return this.http.get(this.gpsURL + '?id=' + device_id + '&from=' + stime + '&to=' + etime, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.sendTokenCall = function (payLoad) {
        return this.http.post(this.shareURL + "/propagate", payLoad, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.shareLivetrackCall = function (data) {
        return this.http.post(this.shareURL, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDriverList = function (_id) {
        return this.http.get("https://www.oneqlik.in/driver/getDrivers?userid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByDateCall = function (_id, skip, limit, dates) {
        // console.log("from date => "+ dates.fromDate.toISOString())
        // console.log("new date "+ new Date(dates.fromDate).toISOString())
        var from = new Date(dates.fromDate).toISOString();
        var to = new Date(dates.toDate).toISOString();
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&f=' + from + '&t=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.filterByType = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFilteredcall = function (_id, skip, limit, key) {
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit + '&device=' + key, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDataOnScroll = function (_id, skip, limit) {
        // https://www.oneqlik.in/notifs/getNotifByFilters?from_date=2019-06-13T18:30:00.769Z&to_date=2019-06-14T09:40:16.769Z&user=5cde59324e4d600905f4e690&sortOrder=-1
        // return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
        return this.http.get(this.notifsURL + '/getNotifiLimit?user=' + _id + '&pageNo=' + skip + '&size=' + limit, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getACReportAPI = function (fdate, tdate, userid, imei) {
        return this.http.get(this.notifsURL + '/ACSwitchReport?from_date=' + fdate + '&to_date=' + tdate + '&user=' + userid + '&device=' + imei, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDetailACReportAPI = function (fdate, tdate, userid, imei) {
        return this.http.get(this.notifsURL + "/acReport?from_date=" + fdate + "&to_date=" + tdate + "&_u=" + userid + "&vname=" + imei, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getVehicleListCall = function (_url) {
        return this.http.get(_url, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trip_detailCall = function (_id, starttime, endtime, did) {
        if (did == undefined) {
            return this.http.get('https://www.oneqlik.in/user_trip/trip_detail?uId=' + _id + '&from_date=' + starttime + '&to_date=' + endtime, { headers: this.headers })
                .map(function (res) { return res.json(); });
        }
        else {
            return this.http.get('https://www.oneqlik.in/user_trip/trip_detail?uId=' + _id + '&from_date=' + starttime + '&to_date=' + endtime + '&device=' + did, { headers: this.headers })
                .map(function (res) { return res.json(); });
        }
    };
    ApiServiceProvider.prototype.trackRouteDataCall = function (data) {
        return this.http.post(this.trackRouteURL, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gettrackRouteCall = function (_id, data) {
        return this.http.post(this.trackRouteURL + '/' + _id, data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.trackRouteCall = function (_id) {
        return this.http.delete(this.trackRouteURL + '/' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getRoutesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getStoppageApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.stoppageURL + "/stoppageReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getIgiApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.notifsURL + "/ignitionReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getOverSpeedApi = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGeogenceReportApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.notifsURL + "/GeoFencingReport?from_date=" + starttime + '&to_date=' + endtime + '&geoid=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getFuelApi = function (starttime, endtime, Ignitiondevice_id, _id) {
        return this.http.get(this.notifsURL + "/fuelReport?from_date=" + starttime + '&to_date=' + endtime + '&vname=' + Ignitiondevice_id + '&_u=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDistanceReportApi = function (starttime, endtime, _id, Ignitiondevice_id) {
        return this.http.get(this.summaryURL + "/distance?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + Ignitiondevice_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDailyReport = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDailyReport1 = function (url, payload) {
        return this.http.post(url, payload, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.contactusApi = function (contactdata) {
        console.log("payload: ", contactdata);
        return this.http.post(this.usersURL + "contactous", contactdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllNotificationCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addgeofenceCall = function (data) {
        return this.http.post(this.geofencingURL + '/addgeofence', data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.getdevicegeofenceCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.geofencestatusCall = function (_id, status, entering, exiting) {
        return this.http.get(this.geofencingURL + '/geofencestatus?gid=' + _id + '&status=' + status + '&entering=' + entering + '&exiting=' + exiting, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteGeoCall = function (_id) {
        return this.http.get(this.geofencingURL + '/deletegeofence?id=' + _id, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.getallgeofenceCall = function (_id) {
        return this.http.get(this.geofencingURL + '/getallgeofence?uid=' + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.user_statusCall = function (data) {
        return this.http.post(this.usersURL + 'user_status', data, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.editUserDetailsCall = function (devicedetails) {
        return this.http.post(this.usersURL + 'editUserDetails', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllDealerVehiclesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addGroupCall = function (devicedetails) {
        return this.http.post(this.groupURL + 'addGroup', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getVehicleTypesCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllUsersCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDeviceModelCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.groupsCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.addDeviceCall = function (devicedetails) {
        return this.http.post(this.devicesURL + '/addDevice', devicedetails, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getCustomersCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.geofenceCall = function (_id) {
        return this.http.get(this.geofencingURL + "/getgeofence?uid=" + _id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.forgotPassApi = function (mobno) {
        return this.http.get(this.usersURL + "forgotpwd?cred=" + mobno.cred, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.forgotPassMobApi = function (Passwordset) {
        return this.http.get(this.usersURL + "forgotpwd?phone=" + Passwordset.otpMess + "&otp=" + Passwordset.otp + "&newpwd=" + Passwordset.newpwd + "&cred=" + Passwordset.otpMess, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.loginApi = function (userdata) {
        return this.http.post(this.usersURL + "LoginWithOtp", userdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.signupApi = function (usersignupdata) {
        return this.http.post(this.usersURL + "signUp", usersignupdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.siginupverifyCall = function (usersignup) {
        return this.http.post(this.usersURL + "/signUpZogo", usersignup, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.resendOtp = function (phnum) {
        return this.http.post(this.usersURL + "/sendOtpZRides", phnum)
            .map(function (res) { return res.json(); });
    };
    // dashboardcall(email, from, to, _id) {
    //   return this.http.get(this.gpsURL + '/getDashboard?email=' + email + '&from=' + from + '&to=' + to + '&id=' + _id, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.dashboardcall = function (_baseUrl) {
        return this.http.get(_baseUrl, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.stoppedDevices = function (_id, email, off_ids) {
        return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email + '&dev=' + off_ids, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.livedatacall = function (_id, email) {
        return this.http.get(this.devicesURL + "/getDeviceByUser?id=" + _id + "&email=" + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getdevicesApi = function (_id, email) {
        return this.http.get(this.devicesURL + '/getDeviceByUser?id=' + _id + '&email=' + email, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getdevicesForAllVehiclesApi = function (link) {
        return this.http.get(link, { headers: this.headers })
            .timeout(500000000)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.ignitionoffCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deviceupdateCall = function (devicedetail) {
        return this.http.post(this.devicesURL + "/deviceupdate", devicedetail, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getDistanceSpeedCall = function (device_id, from, to) {
        return this.http.get(this.gpsURL + '/getDistanceSpeed?imei=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.stoppage_detail = function (_id, from, to, device_id) {
        return this.http.get(this.stoppageURL + '/stoppage_detail?uId=' + _id + '&from_date=' + from + '&to_date=' + to + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.gpsCall = function (device_id, from, to) {
        return this.http.get(this.gpsURL + '/v2?id=' + device_id + '&from=' + from + '&to=' + to, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getcustToken = function (id) {
        return this.http.get(this.usersURL + "getCustumerDetail?uid=" + id)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSummaryReportApi = function (starttime, endtime, _id, device_id) {
        return this.http.get(this.summaryURL + "/summaryReport?from=" + starttime + '&to=' + endtime + '&user=' + _id + '&device=' + device_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getallrouteCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getSpeedReport = function (_id, time) {
        return this.http.get(this.gpsURL + '/getGpsSpeedReport?imei=' + _id + '&time=' + time, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    // deviceupdateInCall(devicedetail) {
    //   return this.http.post(this.devicesURL + "/deviceupdate", devicedetail, { headers: this.headers })
    //     .map(res => res.json());
    // }
    ApiServiceProvider.prototype.deleteDeviceCall = function (d_id) {
        return this.http.get(this.devicesURL + "/deleteDevice?did=" + d_id, { headers: this.headers })
            .map(function (res) { return res; });
    };
    ApiServiceProvider.prototype.deviceShareCall = function (data) {
        return this.http.get(this.devicesURL + "/deviceShare?email=" + data.email + "&uid=" + data.uid + "&did=" + data.did, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pushnotifyCall = function (pushdata) {
        return this.http.post(this.usersURL + "PushNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.pullnotifyCall = function (pushdata) {
        return this.http.post(this.usersURL + "PullNotification", pushdata, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getGroupCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteGroupCall = function (d_id) {
        return this.http.get(this.groupURL + "deleteGroup?_id=" + d_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.deleteCustomerCall = function (data) {
        return this.http.post(this.usersURL + 'deleteUser', data, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.getAllDealerCall = function (link) {
        return this.http.get(link, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.route_details = function (_id, user_id) {
        return this.http.get(this.trackRouteURL + '/routepath/getRoutePathWithPoi?id=' + _id + '&user=' + user_id, { headers: this.headers })
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider.prototype.callcustomerSearchService = function (uid, pageno, limit, seachKey) {
        return this.http.get(this.usersURL + 'getCust?uid=' + uid + '&pageNo=' + pageno + '&size=' + limit + '&search=' + seachKey)
            .map(function (res) { return res.json(); });
    };
    ApiServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["LoadingController"]])
    ], ApiServiceProvider);
    return ApiServiceProvider;
}());

//# sourceMappingURL=api-service.js.map

/***/ }),

/***/ 305:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewDoc; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ViewDoc = /** @class */ (function () {
    function ViewDoc(navparams, viewCtrl) {
        this.navparams = navparams;
        this.viewCtrl = viewCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("param1: ", this.navparams.get("param1"));
        this._instData = this.navparams.get("param1");
        var str = this._instData.imageURL;
        var str1 = str.split('public/');
        this.imgUrl = "https://www.oneqlik.in/" + str1[1];
        console.log("img url: ", this._instData);
    }
    ViewDoc.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ViewDoc = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'page-view-doc',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\add-devices\upload-doc\view-doc\view-doc.html"*/'<div>\n\n    <ion-row>\n\n        <ion-col col-11>\n\n\n\n        </ion-col>\n\n        <ion-col col-1>\n\n            <ion-icon style="font-size:1.5em" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col style="text-align: center;">\n\n            <img src="{{imgUrl}}" width="300" height="450" />\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row padding-left>\n\n        <ion-col col-3>\n\n            <b>Expiry Date:</b>\n\n        </ion-col>\n\n        <ion-col col-9>{{_instData.expDate | date: \'medium\'}}</ion-col>\n\n    </ion-row>\n\n</div>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\add-devices\upload-doc\view-doc\view-doc.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["ViewController"]])
    ], ViewDoc);
    return ViewDoc;
}());

//# sourceMappingURL=view-doc.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ConnectionStatusEnum */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NetworkProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConnectionStatusEnum;
(function (ConnectionStatusEnum) {
    ConnectionStatusEnum[ConnectionStatusEnum["Online"] = 0] = "Online";
    ConnectionStatusEnum[ConnectionStatusEnum["Offline"] = 1] = "Offline";
})(ConnectionStatusEnum || (ConnectionStatusEnum = {}));
var NetworkProvider = /** @class */ (function () {
    function NetworkProvider(alertCtrl, network, eventCtrl) {
        this.alertCtrl = alertCtrl;
        this.network = network;
        this.eventCtrl = eventCtrl;
        this._status = new __WEBPACK_IMPORTED_MODULE_3_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        console.log('Hello NetworkProvider Provider');
        this.previousStatus = ConnectionStatusEnum.Online;
    }
    NetworkProvider.prototype.initializeNetworkEvents = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function () {
            if (_this.status === ConnectionStatusEnum.Online) {
                _this.setStatus(ConnectionStatusEnum.Offline);
                _this.eventCtrl.publish('network:offline');
            }
            // if (this.previousStatus === ConnectionStatusEnum.Online) {
            //   this.eventCtrl.publish('network:offline');
            // }
            // this.previousStatus = ConnectionStatusEnum.Offline;
        });
        this.network.onConnect().subscribe(function () {
            if (_this.status === ConnectionStatusEnum.Offline) {
                _this.setStatus(ConnectionStatusEnum.Online);
                _this.eventCtrl.publish('network:online');
            }
            // setTimeout(() => {
            //   if (this.previousStatus === ConnectionStatusEnum.Offline) {
            //     this.eventCtrl.publish('network:online');
            //   }
            //   this.previousStatus = ConnectionStatusEnum.Online;
            // }, 3000);
        });
    };
    NetworkProvider.prototype.getNetworkType = function () {
        return this.network.type;
    };
    NetworkProvider.prototype.getNetworkStatus = function () {
        return this._status.asObservable();
    };
    NetworkProvider.prototype.setStatus = function (status) {
        this.status = status;
        this._status.next(this.status);
    };
    NetworkProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], NetworkProvider);
    return NetworkProvider;
}());

//# sourceMappingURL=network.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuProvider = /** @class */ (function () {
    function MenuProvider(http) {
        this.http = http;
    }
    MenuProvider.prototype.getSideMenus = function () {
        return [{
                title: 'Home', component: 'DashboardPage', icon: 'home'
            }, {
                title: 'Groups', component: 'GroupsPage', icon: 'people'
            }, {
                title: 'Customers', component: 'CustomersPage', icon: 'contacts'
            }, {
                title: 'Notifications', component: 'AllNotificationsPage', icon: 'notifications'
            },
            {
                title: 'Reports',
                subPages: [{
                        title: 'Daily Report',
                        component: 'DailyReportPage',
                    }, {
                        title: 'Speed Variation Report',
                        component: 'SpeedRepoPage',
                    }, {
                        title: 'Summary Report',
                        component: 'DeviceSummaryRepoPage',
                    }, {
                        title: 'Geofenceing Report',
                        component: 'GeofenceReportPage',
                    },
                    {
                        title: 'Overspeed Report',
                        component: 'OverSpeedRepoPage',
                    }, {
                        title: 'Ignition Report',
                        component: 'IgnitionReportPage',
                    },
                    {
                        title: 'Route Violation Report',
                        component: 'RouteVoilationsPage',
                    }, {
                        title: 'Stoppage Report',
                        component: 'StoppagesRepoPage',
                    },
                    {
                        title: 'Fuel Report',
                        component: 'FuelReportPage',
                    }, {
                        title: 'Distnace Report',
                        component: 'DistanceReportPage',
                    },
                    {
                        title: 'Trip Report',
                        component: 'TripReportPage',
                    }],
                icon: 'clipboard'
            },
            {
                title: 'Routes',
                subPages: [{
                        title: 'Routes',
                        component: 'RoutePage',
                    }, {
                        title: 'Route Maping',
                        component: 'RouteMapPage',
                    }],
                icon: 'map'
            }, {
                title: 'Feedback', component: 'FeedbackPage', icon: 'paper'
            }, {
                title: 'Contact Us', component: 'ContactUsPage', icon: 'mail'
            }, {
                title: 'Support', component: 'SupportPage', icon: 'call'
            }, {
                title: 'About Us', component: 'AboutUsPage', icon: 'alert'
            },];
    };
    MenuProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]])
    ], MenuProvider);
    return MenuProvider;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuContentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__ = __webpack_require__(500);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Angular
 // tslint:disable-line
// Ionic


// This class is defined in this file because
// we don't want to make it exportable
var InnerMenuOptionModel = /** @class */ (function () {
    function InnerMenuOptionModel() {
    }
    InnerMenuOptionModel.fromMenuOptionModel = function (option, parent) {
        var innerMenuOptionModel = new InnerMenuOptionModel();
        innerMenuOptionModel.id = this.counter++;
        innerMenuOptionModel.iconName = option.iconName;
        innerMenuOptionModel.displayText = option.displayText;
        innerMenuOptionModel.badge = option.badge;
        innerMenuOptionModel.targetOption = option;
        innerMenuOptionModel.parent = parent || null;
        innerMenuOptionModel.selected = option.selected;
        if (option.suboptions) {
            innerMenuOptionModel.expanded = false;
            innerMenuOptionModel.suboptionsCount = option.suboptions.length;
            innerMenuOptionModel.subOptions = [];
            option.suboptions.forEach(function (subItem) {
                var innerSubItem = InnerMenuOptionModel.fromMenuOptionModel(subItem, innerMenuOptionModel);
                innerMenuOptionModel.subOptions.push(innerSubItem);
                // Select the parent if any
                // child option is selected
                if (subItem.selected) {
                    innerSubItem.parent.selected = true;
                    innerSubItem.parent.expanded = true;
                }
            });
        }
        return innerMenuOptionModel;
    };
    InnerMenuOptionModel.counter = 1;
    return InnerMenuOptionModel;
}());
var SideMenuContentComponent = /** @class */ (function () {
    function SideMenuContentComponent(platform, eventsCtrl, cdRef) {
        var _this = this;
        this.platform = platform;
        this.eventsCtrl = eventsCtrl;
        this.cdRef = cdRef;
        this.collapsableItems = [];
        // Outputs: return the selected option to the caller
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        // Handle the redirect event
        this.eventsCtrl.subscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */], function (data) {
            _this.updateSelectedOption(data);
        });
    }
    Object.defineProperty(SideMenuContentComponent.prototype, "options", {
        set: function (value) {
            var _this = this;
            if (value) {
                // Keep a reference to the options
                // sent to this component
                this.menuOptions = value;
                this.collapsableItems = new Array();
                // Map the options to our internal models
                this.menuOptions.forEach(function (option) {
                    var innerMenuOption = InnerMenuOptionModel.fromMenuOptionModel(option);
                    _this.collapsableItems.push(innerMenuOption);
                    // Check if there's any option marked as selected
                    if (option.selected) {
                        _this.selectedOption = innerMenuOption;
                    }
                    else if (innerMenuOption.suboptionsCount) {
                        innerMenuOption.subOptions.forEach(function (subItem) {
                            if (subItem.selected) {
                                _this.selectedOption = subItem;
                            }
                        });
                    }
                });
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "settings", {
        set: function (value) {
            if (value) {
                this.menuSettings = value;
                this.mergeSettings();
            }
        },
        enumerable: true,
        configurable: true
    });
    SideMenuContentComponent.prototype.ngOnDestroy = function () {
        this.eventsCtrl.unsubscribe(__WEBPACK_IMPORTED_MODULE_2__models_side_menu_option_select_event__["a" /* SideMenuOptionSelect */]);
    };
    // ---------------------------------------------------
    // PUBLIC methods
    // ---------------------------------------------------
    // Send the selected option to the caller component
    SideMenuContentComponent.prototype.select = function (option) {
        if (this.menuSettings.showSelectedOption) {
            this.setSelectedOption(option);
        }
        // Return the selected option (not our inner option)
        this.change.emit(option.targetOption);
    };
    // Toggle the sub options of the selected item
    SideMenuContentComponent.prototype.toggleItemOptions = function (targetOption) {
        if (!targetOption)
            return;
        // If the accordion mode is set to true, we need
        // to collapse all the other menu options
        if (this.menuSettings.accordionMode) {
            this.collapsableItems.forEach(function (option) {
                if (option.id !== targetOption.id) {
                    option.expanded = false;
                }
            });
        }
        // Toggle the selected option
        targetOption.expanded = !targetOption.expanded;
    };
    // Reset the entire menu
    SideMenuContentComponent.prototype.collapseAllOptions = function () {
        this.collapsableItems.forEach(function (option) {
            if (!option.selected) {
                option.expanded = false;
            }
            if (option.suboptionsCount) {
                option.subOptions.forEach(function (subItem) {
                    if (subItem.selected) {
                        // Expand the parent if any of
                        // its childs is selected
                        subItem.parent.expanded = true;
                    }
                });
            }
        });
        // Update the view since there wasn't
        // any user interaction with it
        this.cdRef.detectChanges();
    };
    Object.defineProperty(SideMenuContentComponent.prototype, "subOptionIndentation", {
        // Get the proper indentation of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.subOptionIndentation.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.subOptionIndentation.wp;
            return this.menuSettings.subOptionIndentation.md;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SideMenuContentComponent.prototype, "optionHeight", {
        // Get the proper height of each option
        get: function () {
            if (this.platform.is('ios'))
                return this.menuSettings.optionHeight.ios;
            if (this.platform.is('windows'))
                return this.menuSettings.optionHeight.wp;
            return this.menuSettings.optionHeight.md;
        },
        enumerable: true,
        configurable: true
    });
    // ---------------------------------------------------
    // PRIVATE methods
    // ---------------------------------------------------
    // Method that set the selected option and its parent
    SideMenuContentComponent.prototype.setSelectedOption = function (option) {
        if (!option.targetOption.component)
            return;
        // Clean the current selected option if any
        if (this.selectedOption) {
            this.selectedOption.selected = false;
            this.selectedOption.targetOption.selected = false;
            if (this.selectedOption.parent) {
                this.selectedOption.parent.selected = false;
                this.selectedOption.parent.expanded = false;
            }
            this.selectedOption = null;
        }
        // Set this option to be the selected
        option.selected = true;
        option.targetOption.selected = true;
        if (option.parent) {
            option.parent.selected = true;
            option.parent.expanded = true;
        }
        // Keep a reference to the selected option
        this.selectedOption = option;
        // Update the view if needed since we may have
        // expanded or collapsed some options
        this.cdRef.detectChanges();
    };
    // Update the selected option
    SideMenuContentComponent.prototype.updateSelectedOption = function (data) {
        if (!data.displayText)
            return;
        var targetOption;
        this.collapsableItems.forEach(function (option) {
            if (option.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                targetOption = option;
            }
            else if (option.suboptionsCount) {
                option.subOptions.forEach(function (subOption) {
                    if (subOption.displayText.toLowerCase() === data.displayText.toLowerCase()) {
                        targetOption = subOption;
                    }
                });
            }
        });
        if (targetOption) {
            this.setSelectedOption(targetOption);
        }
    };
    // Merge the settings received with the default settings
    SideMenuContentComponent.prototype.mergeSettings = function () {
        var defaultSettings = {
            accordionMode: false,
            optionHeight: {
                ios: 50,
                md: 50,
                wp: 50
            },
            arrowIcon: 'ios-arrow-down',
            showSelectedOption: false,
            selectedOptionClass: 'selected-option',
            indentSubOptionsWithoutIcons: false,
            subOptionIndentation: {
                ios: 16,
                md: 16,
                wp: 16
            }
        };
        if (!this.menuSettings) {
            // Use the default values
            this.menuSettings = defaultSettings;
            return;
        }
        if (!this.menuSettings.optionHeight) {
            this.menuSettings.optionHeight = defaultSettings.optionHeight;
        }
        else {
            this.menuSettings.optionHeight.ios = this.isDefinedAndPositive(this.menuSettings.optionHeight.ios) ? this.menuSettings.optionHeight.ios : defaultSettings.optionHeight.ios;
            this.menuSettings.optionHeight.md = this.isDefinedAndPositive(this.menuSettings.optionHeight.md) ? this.menuSettings.optionHeight.md : defaultSettings.optionHeight.md;
            this.menuSettings.optionHeight.wp = this.isDefinedAndPositive(this.menuSettings.optionHeight.wp) ? this.menuSettings.optionHeight.wp : defaultSettings.optionHeight.wp;
        }
        this.menuSettings.showSelectedOption = this.isDefined(this.menuSettings.showSelectedOption) ? this.menuSettings.showSelectedOption : defaultSettings.showSelectedOption;
        this.menuSettings.accordionMode = this.isDefined(this.menuSettings.accordionMode) ? this.menuSettings.accordionMode : defaultSettings.accordionMode;
        this.menuSettings.arrowIcon = this.isDefined(this.menuSettings.arrowIcon) ? this.menuSettings.arrowIcon : defaultSettings.arrowIcon;
        this.menuSettings.selectedOptionClass = this.isDefined(this.menuSettings.selectedOptionClass) ? this.menuSettings.selectedOptionClass : defaultSettings.selectedOptionClass;
        this.menuSettings.indentSubOptionsWithoutIcons = this.isDefined(this.menuSettings.indentSubOptionsWithoutIcons) ? this.menuSettings.indentSubOptionsWithoutIcons : defaultSettings.indentSubOptionsWithoutIcons;
        if (!this.menuSettings.subOptionIndentation) {
            this.menuSettings.subOptionIndentation = defaultSettings.subOptionIndentation;
        }
        else {
            this.menuSettings.subOptionIndentation.ios = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.ios) ? this.menuSettings.subOptionIndentation.ios : defaultSettings.subOptionIndentation.ios;
            this.menuSettings.subOptionIndentation.md = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.md) ? this.menuSettings.subOptionIndentation.md : defaultSettings.subOptionIndentation.md;
            this.menuSettings.subOptionIndentation.wp = this.isDefinedAndPositive(this.menuSettings.subOptionIndentation.wp) ? this.menuSettings.subOptionIndentation.wp : defaultSettings.subOptionIndentation.wp;
        }
    };
    SideMenuContentComponent.prototype.isDefined = function (property) {
        return property !== null && property !== undefined;
    };
    SideMenuContentComponent.prototype.isDefinedAndPositive = function (property) {
        return this.isDefined(property) && !isNaN(property) && property > 0;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('options'),
        __metadata("design:type", Array),
        __metadata("design:paramtypes", [Array])
    ], SideMenuContentComponent.prototype, "options", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('settings'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], SideMenuContentComponent.prototype, "settings", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], SideMenuContentComponent.prototype, "change", void 0);
    SideMenuContentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'side-menu-content',template:/*ion-inline-start:"D:\Pro\flex_track\shared\side-menu-content\side-menu-content.component.html"*/'<ion-list no-margin no-lines>\n\n    <ng-template ngFor let-option [ngForOf]="collapsableItems" let-i="index">\n\n\n\n        <!-- It is a simple option -->\n\n        <ng-template [ngIf]="!option.suboptionsCount">\n\n            <ion-item class="option" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n\n                (tap)="select(option)" tappable>\n\n                <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon>\n\n                {{ option.displayText }}\n\n                <ion-badge item-right *ngIf="option.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n\n            </ion-item>\n\n        </ng-template>\n\n\n\n        <!-- It has nested options -->\n\n        <ng-template [ngIf]="option.suboptionsCount">\n\n\n\n            <ion-list no-margin class="accordion-menu">\n\n\n\n                <!-- Header -->\n\n                <ion-item class="header" [ngClass]="menuSettings?.showSelectedOption && option.selected ? menuSettings.selectedOptionClass : null"\n\n                    (tap)="toggleItemOptions(option)" tappable>\n\n                    <ion-icon [class.rotate]="option.expanded" class="header-icon" [name]="option.iconName || menuSettings.arrowIcon"\n\n                        item-left></ion-icon>\n\n                    <!-- <ion-icon *ngIf="option.iconName" [name]="option.iconName" item-left></ion-icon> -->\n\n                    {{ option.displayText }}\n\n                </ion-item>\n\n\n\n                <!-- Sub items -->\n\n                <div [style.height]="option.expanded ? ((optionHeight + 1) * option.suboptionsCount) + \'px\' : \'0px\'" class="options">\n\n                    <ng-template ngFor let-item [ngForOf]="option.subOptions">\n\n                        <ion-item class="sub-option" [style.padding-left]="subOptionIndentation + \'px\'" [class.no-icon]="menuSettings?.indentSubOptionsWithoutIcons && !item.iconName"\n\n                            [ngClass]="menuSettings?.showSelectedOption && item.selected ? menuSettings.selectedOptionClass : null"\n\n                            tappable (tap)="select(item)">\n\n                            <ion-icon *ngIf="item.iconName" [name]="item.iconName" item-left></ion-icon>\n\n                            {{ item.displayText }}\n\n                            <ion-badge item-right *ngIf="item.badge | async as badgeNo">{{ badgeNo }}</ion-badge>\n\n                        </ion-item>\n\n                    </ng-template>\n\n                </div>\n\n            </ion-list>\n\n\n\n        </ng-template>\n\n\n\n    </ng-template>\n\n</ion-list>'/*ion-inline-end:"D:\Pro\flex_track\shared\side-menu-content\side-menu-content.component.html"*/,
            changeDetection: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectionStrategy"].OnPush
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]])
    ], SideMenuContentComponent);
    return SideMenuContentComponent;
}());

//# sourceMappingURL=side-menu-content.component.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LiveSingleDevice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PoiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash__ = __webpack_require__(416);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_native_page_transitions__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_geocoder_geocoder__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ion_bottom_drawer__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_Subscription__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_rxjs_Subscription___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_rxjs_Subscription__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__live_map_style_model__ = __webpack_require__(481);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var LiveSingleDevice = /** @class */ (function () {
    function LiveSingleDevice(navCtrl, navParams, apiCall, actionSheetCtrl, elementRef, platform, event, modalCtrl, socialSharing, alertCtrl, toastCtrl, storage, plt, nativePageTransitions, viewCtrl, geocoderApi) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.elementRef = elementRef;
        this.platform = platform;
        this.event = event;
        this.modalCtrl = modalCtrl;
        this.socialSharing = socialSharing;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.plt = plt;
        this.nativePageTransitions = nativePageTransitions;
        this.viewCtrl = viewCtrl;
        this.geocoderApi = geocoderApi;
        this.shouldBounce = true;
        this.dockedHeight = 80;
        this.distanceTop = 200;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_11_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.states = __WEBPACK_IMPORTED_MODULE_11_ion_bottom_drawer__["a" /* DrawerState */];
        this.minimumHeight = 50;
        this.transition = '0.85s ease-in-out';
        this.drawerState1 = __WEBPACK_IMPORTED_MODULE_11_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.dockedHeight1 = 150;
        this.distanceTop1 = 378;
        this.minimumHeight1 = 0;
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.service = new google.maps.DistanceMatrixService();
        this.data = {};
        this.socketSwitch = {};
        this.socketChnl = [];
        this.socketData = {};
        this.selectedFlag = { _id: '', flag: false };
        this.allData = {};
        this.isEnabled = false;
        this.showShareBtn = false;
        this.mapHideTraffic = false;
        this.locateme = false;
        this.deviceDeatils = {};
        this.tempArray = [];
        this.mapTrail = false;
        this.condition = 'gpsc';
        this.condition1 = 'light';
        this.condition2 = 'light';
        this.showaddpoibtn = false;
        this.btnString = "Create Trip";
        this.showIcon = false;
        this.expectation = {};
        this.polyLines = [];
        this.marksArray = [];
        this.shwBckBtn = false;
        this.car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
        this.bike = "M419 1754 l3 -44 -37 0 c-21 0 -34 4 -30 10 3 6 -1 10 -9 10 -19 0 -66 -21 -66 -30 0 -18 44 -44 71 -42 16 1 29 0 29 -2 0 -12 -24 -56 -30 -56 -4 0 -7 -6 -7 -12 0 -7 -8 -29 -16 -48 l-15 -35 -7 30 c-4 17 -8 36 -11 43 -6 19 -40 14 -69 -11 l-27 -23 16 -134 c12 -99 23 -149 41 -186 28 -57 25 -84 -10 -84 -34 0 -51 -25 -31 -45 9 -9 16 -33 16 -53 0 -30 -9 -46 -44 -84 -24 -27 -42 -48 -40 -48 2 0 -10 -21 -26 -47 -17 -27 -30 -61 -30 -79 0 -35 35 -189 52 -232 6 -15 10 -41 9 -59 -3 -34 19 -53 58 -53 12 0 21 -6 21 -14 0 -12 -7 -13 -32 -5 -44 12 -70 11 -66 -2 2 -6 21 -12 41 -14 21 -1 43 -7 49 -13 17 -17 23 -119 8 -137 -10 -12 -9 -19 3 -38 l15 -23 -46 23 c-29 15 -63 23 -94 23 -56 0 -61 -14 -17 -48 24 -20 42 -24 97 -24 63 -1 69 -3 109 -39 22 -21 38 -41 35 -44 -4 -4 -2 -5 3 -4 6 1 30 -3 55 -10 54 -16 142 -9 176 14 13 8 24 13 24 10 0 -3 16 13 36 34 35 39 36 40 103 39 59 -1 73 2 99 23 45 35 41 49 -17 49 -32 0 -64 -8 -97 -25 -27 -14 -42 -20 -33 -13 10 8 14 23 11 38 -10 54 -7 139 6 152 7 7 30 13 52 13 27 0 40 4 40 13 0 11 -9 13 -36 8 -19 -4 -42 -10 -50 -13 -10 -3 -12 0 -8 10 3 8 18 17 33 20 23 5 30 13 35 41 4 18 13 47 20 62 8 16 26 77 41 136 l26 106 -45 90 c-24 49 -56 102 -70 119 -14 17 -23 33 -19 37 3 3 1 6 -5 6 -15 0 -16 57 -1 62 22 7 -3 38 -31 38 -19 0 -31 6 -34 18 -7 20 38 114 49 104 5 -4 5 -2 2 4 -4 7 0 67 9 135 8 68 13 134 10 145 -8 29 -60 51 -81 34 -8 -7 -15 -22 -15 -35 0 -43 -13 -33 -46 36 -19 39 -34 73 -34 76 0 3 13 3 30 1 22 -3 35 2 50 17 27 29 17 49 -26 53 -22 1 -33 -2 -29 -8 4 -6 -8 -10 -29 -10 -34 0 -35 1 -38 43 -3 42 -3 42 -43 43 l-40 1 4 -43z m-140 -187 c13 -16 5 -27 -21 -27 -14 0 -18 -5 -14 -17 8 -29 26 -196 24 -220 -5 -48 -19 -2 -32 106 -16 127 -15 133 3 155 16 20 25 20 40 3z m402 -3 c7 -9 14 -22 15 -30 4 -26 -25 -237 -34 -246 -12 -12 -12 12 0 127 6 55 16 103 22 107 6 4 -2 12 -22 19 -26 9 -30 14 -21 25 15 18 24 18 40 -2z m-123 -226 c48 -23 54 -30 46 -51 -5 -13 -10 -14 -39 -1 -50 21 -163 18 -207 -5 -35 -17 -37 -17 -41 0 -7 24 23 49 85 70 58 20 89 18 156 -13z m28 -469 c39 -36 56 -76 56 -134 2 -169 -205 -230 -284 -85 -26 47 -21 155 7 176 10 8 15 14 10 14 -13 0 27 41 50 51 11 5 46 7 77 6 45 -3 64 -9 84 -28z m131 -37 c7 -8 10 -19 7 -23 -3 -5 -1 -9 4 -9 17 0 21 -41 10 -103 -10 -59 -28 -100 -28 -64 0 14 -44 137 -75 209 -6 15 -2 16 32 11 21 -3 43 -12 50 -21z m-455 -49 c-15 -38 -30 -71 -35 -74 -5 -3 -8 -9 -8 -13 3 -21 0 -36 -8 -36 -5 0 -11 30 -13 68 -5 61 -3 69 20 95 14 15 36 27 48 27 l22 0 -26 -67z m377 -317 c19 14 25 14 32 3 13 -21 12 -23 -21 -42 -16 -10 -30 -23 -30 -28 0 -6 -4 -8 -8 -5 -4 2 -9 -9 -10 -25 -2 -22 2 -29 16 -29 14 0 21 -12 29 -44 10 -43 9 -45 -13 -43 -13 1 -29 10 -37 21 -13 17 -15 14 -37 -40 -13 -33 -30 -65 -38 -72 -9 -6 -12 -12 -7 -12 4 0 0 -9 -10 -20 -35 -39 -107 -11 -117 45 -1 11 -9 26 -15 33 -12 12 -17 33 -15 54 1 3 -5 11 -13 18 -11 9 -16 7 -25 -9 -12 -23 -50 -29 -50 -7 0 44 14 76 32 76 17 0 19 4 13 23 -4 12 -7 30 -8 39 -1 9 -4 15 -8 12 -4 -2 -19 6 -33 18 -19 16 -23 27 -17 37 8 12 13 12 34 -2 l25 -16 7 39 c4 22 8 40 9 40 1 0 18 -9 39 -19 l37 -19 -28 -8 c-15 -4 -30 -7 -33 -6 -3 0 -6 -11 -5 -26 l1 -27 125 0 c120 0 125 1 128 21 2 16 -5 25 -27 34 l-31 12 33 22 32 22 10 -43 c10 -43 10 -44 34 -27z m-161 11 c2 -10 -3 -17 -12 -17 -18 0 -29 16 -21 31 9 14 29 6 33 -14z m-58 -11 c0 -16 -18 -31 -27 -22 -8 8 5 36 17 36 5 0 10 -6 10 -14z m108 -10 c3 -12 -1 -17 -10 -14 -7 3 -15 13 -16 22 -3 12 1 17 10 14 7 -3 15 -13 16 -22z m-148 -5 c0 -14 -18 -23 -31 -15 -8 4 -7 9 2 15 18 11 29 11 29 0z m198 -3 c-3 -7 -11 -13 -18 -13 -7 0 -15 6 -17 13 -3 7 4 12 17 12 13 0 20 -5 18 -12z m-430 -224 c37 -10 25 -23 -14 -14 -19 5 -38 11 -41 14 -8 8 26 7 55 0z m672 -3 c-8 -5 -35 -11 -60 -15 l-45 -6 35 14 c39 16 96 21 70 7z m-454 -113 l29 -30 -30 15 c-17 8 -38 24 -48 35 -18 20 -18 20 1 15 11 -2 33 -18 48 -35z m239 15 c-11 -8 -33 -21 -50 -30 l-30 -15 30 31 c16 17 35 29 42 27 7 -3 13 0 13 6 0 6 3 8 7 4 5 -4 -1 -14 -12 -23z";
        this.truck = 'M180 1725 c0 -8 -21 -25 -47 -37 -27 -12 -58 -31 -70 -42 l-23 -20 0 -495 0 -496 23 -18 c12 -10 31 -24 42 -32 20 -13 20 -14 -2 -37 -21 -22 -23 -34 -23 -131 0 -65 -4 -107 -10 -107 -5 0 -10 -7 -10 -15 0 -8 5 -15 10 -15 6 0 10 -31 10 -74 0 -44 5 -78 11 -82 8 -4 7 -9 -2 -15 -38 -23 96 -89 181 -89 85 0 219 66 181 89 -9 6 -10 11 -2 15 6 4 11 38 11 82 0 43 4 74 10 74 6 0 10 7 10 15 0 8 -4 15 -10 15 -6 0 -10 43 -10 108 0 96 -2 108 -20 120 -30 18 -24 38 20 69 l40 28 0 491 0 491 -22 24 c-22 24 -86 59 -106 59 -5 0 -12 9 -15 20 -5 18 -14 20 -91 20 -70 0 -86 -3 -86 -15z m115 -101 c21 -8 35 -45 28 -72 -7 -29 -47 -44 -78 -30 -29 13 -38 36 -29 72 7 30 43 44 79 30z m-148 -726 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m250 0 c-1 -134 -2 -25 -2 242 0 267 1 376 2 243 2 -134 2 -352 0 -485z m-102 326 c21 -9 35 -46 28 -74 -12 -44 -93 -46 -108 -1 -17 54 27 95 80 75z m20 -436 c2 -2 6 -15 10 -30 10 -38 -26 -72 -68 -64 -37 8 -35 6 -43 43 -8 42 18 67 64 59 19 -3 36 -6 37 -8z m18 -245 c-36 -16 -102 -16 -130 0 -18 10 -7 12 68 12 80 0 87 -1 62 -12z m-203 -69 c0 -21 -4 -33 -10 -29 -5 3 -10 19 -10 36 0 16 5 29 10 29 6 0 10 -16 10 -36z m300 1 c0 -24 -5 -35 -15 -35 -15 0 -22 50 -8 63 15 15 23 5 23 -28z m-300 -87 c0 -24 -5 -50 -10 -58 -7 -11 -10 2 -10 43 0 31 4 57 10 57 6 0 10 -19 10 -42z m300 -20 c0 -43 -3 -58 -10 -48 -13 20 -23 87 -15 100 15 25 25 5 25 -52z m-201 -44 c31 -4 80 -2 108 4 59 10 70 5 75 -43 3 -29 2 -30 -57 -38 -58 -8 -199 -2 -229 9 -12 5 -12 11 2 45 11 28 20 38 30 34 9 -3 40 -8 71 -11z m147 -222 c-14 -15 -15 -13 -3 17 8 23 13 28 15 16 2 -10 -3 -25 -12 -33z m-213 18 c3 -11 1 -20 -4 -20 -5 0 -9 9 -9 20 0 11 2 20 4 20 2 0 6 -9 9 -20z m267 -25 c0 -2 -10 -10 -22 -16 -21 -11 -22 -11 -9 4 13 16 31 23 31 12z';
        this.carIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [10, 20],
        };
        this.busIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [12.5, 12.5],
        };
        this.bikeIcon = {
            path: this.bike,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [12.5, 12.5],
        };
        this.truckIcon = {
            path: this.truck,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [10, 20],
        };
        this.userIcon = {
            path: this.truck,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            anchor: [12.5, 12.5],
        };
        this.icons = {
            "car": this.carIcon,
            "bike": this.bikeIcon,
            "truck": this.truckIcon,
            "bus": this.busIcon,
            "user": this.userIcon,
            "jcb": this.carIcon,
            "tractor": this.carIcon
        };
        this.resumeListener = new __WEBPACK_IMPORTED_MODULE_12_rxjs_Subscription__["Subscription"]();
        // Environment.setBackgroundColor("black");
        var selectedMapKey;
        if (localStorage.getItem('MAP_KEY') != null) {
            selectedMapKey = localStorage.getItem('MAP_KEY');
            if (selectedMapKey == 'Hybrid') {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
            else if (selectedMapKey == 'Normal') {
                this.mapKey = 'MAP_TYPE_NORMAL';
            }
            else if (selectedMapKey == 'Terrain') {
                this.mapKey = 'MAP_TYPE_TERRAIN';
            }
            else if (selectedMapKey == 'Satellite') {
                this.mapKey = 'MAP_TYPE_SATELLITE';
            }
        }
        else {
            this.mapKey = 'MAP_TYPE_NORMAL';
        }
        this.event.subscribe("tripstatUpdated", function (data) {
            if (data) {
                _this.checktripstat();
            }
        });
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> " + JSON.stringify(this.userdetails));
        if (navParams.get("device") != null) {
            this.deviceDeatils = navParams.get("device");
        }
        this.motionActivity = 'Activity';
        this.menuActive = false;
    }
    LiveSingleDevice.prototype.ngOnInit = function () {
        var that = this;
        that.tempArray = [];
        this.checktripstat();
    };
    LiveSingleDevice.prototype.setDocHeight = function () {
        var that = this;
        that.distanceTop = 200;
        that.drawerState = __WEBPACK_IMPORTED_MODULE_11_ion_bottom_drawer__["a" /* DrawerState */].Top;
    };
    LiveSingleDevice.prototype.setDocHeightAtFirst = function () {
        var that = this;
        that.distanceTop = 80;
        that.drawerState = __WEBPACK_IMPORTED_MODULE_11_ion_bottom_drawer__["a" /* DrawerState */].Top;
    };
    LiveSingleDevice.prototype.goBack = function () {
        if (this.navCtrl.canGoBack()) {
            var options = {
                direction: 'left',
                duration: 500,
                slowdownfactor: 3,
                slidePixels: 20,
                iosdelay: 100,
                androiddelay: 150,
                fixedPixelsTop: 0,
                fixedPixelsBottom: 60
            };
            this.nativePageTransitions.slide(options);
            this.navCtrl.pop();
        }
        else {
            var options = {
                duration: 700
            };
            this.nativePageTransitions.fade(options);
            this.navCtrl.setRoot('DashboardPage');
        }
    };
    LiveSingleDevice.prototype.refreshMe = function () {
        this.ngOnDestroy();
        this.ngOnInit();
        this.ionViewDidLoad();
    };
    LiveSingleDevice.prototype.reCenterMe = function () {
        // console.log("getzoom level: " + this.allData.map.getCameraZoom());
        this.allData.map.moveCamera({
            target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
            zoom: this.allData.map.getCameraZoom()
        }).then(function () {
        });
    };
    LiveSingleDevice.prototype.newMap = function () {
        var mapOptions = {
            camera: { zoom: 10 },
            gestures: {
                rotate: false,
                tilt: false
            }
        };
        var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
        // Environment.setBackgroundColor("black");
        return map;
    };
    LiveSingleDevice.prototype.isNight = function () {
        //Returns true if the time is between
        //7pm to 5am
        var time = new Date().getHours();
        return (time > 5 && time < 19) ? false : true;
    };
    LiveSingleDevice.prototype.checktripstat = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + "user_trip/getLastTrip?device=" + this.deviceDeatils._id + "&status=Started&tripInfo=last_trip";
        this.apiCall.getSOSReportAPI(url)
            .subscribe(function (data) {
            // debugger
            if (!data.message) {
                _this.allData.tripStat = data[0].trip_status;
                if (_this.allData.tripStat == 'Started') {
                    var sources = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](parseFloat(data[0].start_lat), parseFloat(data[0].start_long));
                    var dest = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](parseFloat(data[0].end_lat), parseFloat(data[0].end_long));
                    _this.calcRoute(sources, dest);
                }
            }
        });
    };
    LiveSingleDevice.prototype.calcRoute = function (start, end) {
        this.allData.AIR_PORTS = [];
        var directionsService = new google.maps.DirectionsService();
        var that = this;
        var request = {
            origin: start,
            destination: end,
            // waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING
        };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                var path = new google.maps.MVCArray();
                for (var i = 0, len = response.routes[0].overview_path.length; i < len; i++) {
                    path.push(response.routes[0].overview_path[i]);
                    that.allData.AIR_PORTS.push({
                        lat: path.j[i].lat(), lng: path.j[i].lng()
                    });
                    // debugger
                    if (that.allData.AIR_PORTS.length > 1) {
                        that.allData.map.addMarker({
                            title: 'Start',
                            position: start,
                            icon: 'green'
                        }).then(function (mark) {
                            that.marksArray.push(mark);
                        });
                        that.allData.map.addMarker({
                            title: 'Destination',
                            position: end,
                            icon: 'red'
                        }).then(function (mark) {
                            that.marksArray.push(mark);
                        });
                        that.allData.map.addPolyline({
                            'points': that.allData.AIR_PORTS,
                            'color': '#4aa9d5',
                            'width': 4,
                            'geodesic': true,
                        }).then(function (poly) {
                            that.polyLines.push(poly);
                            that.getTravelDetails(start, end);
                            //   that.showBtn = true;
                        });
                    }
                }
                that.apiCall.stopLoading();
            }
        });
    };
    LiveSingleDevice.prototype.getTravelDetails = function (source, dest) {
        var _this = this;
        var that = this;
        this._id = setInterval(function () {
            if (localStorage.getItem("livepagetravelDetailsObject") != null) {
                if (that.expectation.distance == undefined && that.expectation.duration == undefined) {
                    that.expectation = JSON.parse(localStorage.getItem("livepagetravelDetailsObject"));
                    console.log("expectation: ", that.expectation);
                }
                else {
                    clearInterval(_this._id);
                }
            }
        }, 3000);
        that.service.getDistanceMatrix({
            origins: [source],
            destinations: [dest],
            travelMode: 'DRIVING'
        }, that.callback);
    };
    LiveSingleDevice.prototype.callback = function (response, status) {
        var travelDetailsObject;
        if (status == 'OK') {
            var origins = response.originAddresses;
            for (var i = 0; i < origins.length; i++) {
                var results = response.rows[i].elements;
                for (var j = 0; j < results.length; j++) {
                    var element = results[j];
                    var distance = element.distance.text;
                    var duration = element.duration.text;
                    travelDetailsObject = {
                        distance: distance,
                        duration: duration
                    };
                }
            }
            localStorage.setItem("livepagetravelDetailsObject", JSON.stringify(travelDetailsObject));
        }
    };
    LiveSingleDevice.prototype.diff = function (start, end) {
        start = start.split(":");
        end = end.split(":");
        var startDate = new Date(0, 0, 0, start[0], start[1], 0);
        var endDate = new Date(0, 0, 0, end[0], end[1], 0);
        var diff = endDate.getTime() - startDate.getTime();
        var hours = Math.floor(diff / 1000 / 60 / 60);
        diff -= hours * 1000 * 60 * 60;
        var minutes = Math.floor(diff / 1000 / 60);
        // If using time pickers with 24 hours format, add the below line get exact hours
        if (hours < 0)
            hours = hours + 24;
        return (hours <= 9 ? "0" : "") + hours + " hours " + (minutes <= 9 ? "0" : "") + minutes + " mins";
    };
    LiveSingleDevice.prototype.callendtripfunc = function (res) {
        var _this = this;
        var endtime = __WEBPACK_IMPORTED_MODULE_5_moment__(new Date(), 'DD/MM/YYY').format('hh:mm');
        var starttime = __WEBPACK_IMPORTED_MODULE_5_moment__(new Date(res.doc.start_time), 'DD/MM/YYYY').format('hh:mm');
        var duration = this.diff(starttime, endtime);
        var dest = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](res.doc.start_lat, res.doc.start_long);
        var dest2 = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](res.doc.end_lat, res.doc.end_long);
        var distance = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, dest2); // in meters
        var bUrl = this.apiCall.mainUrl + "user_trip/updatePlantrip";
        var payload = {
            "user": res.doc.user,
            "device": res.doc.device,
            "start_loc": {
                "lat": res.doc.start_lat,
                "long": res.doc.start_long
            },
            "end_loc": {
                "lat": res.doc.end_lat,
                "long": res.doc.end_long,
            },
            "duration": duration,
            "distance": distance,
            "trip_status": "completed",
            "trip_id": res.doc._id
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(bUrl, payload)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("tri end data: ", data);
            // alert("trip ended succesfully");
            var toast = _this.toastCtrl.create({
                message: 'Trip has ended succesfully.',
                duration: 1500,
                position: 'bottom'
            });
            toast.present();
            var that = _this;
            // debugger
            that.allData.tripStat = 'completed';
            if (that.polyLines.length > 0) {
                for (var t = 0; t < that.polyLines.length; t++) {
                    that.polyLines[t].remove();
                }
            }
            if (that.marksArray.length > 0) {
                for (var e = 0; e < that.marksArray.length; e++) {
                    that.marksArray[e].remove();
                }
            }
            if (localStorage.getItem("livepagetravelDetailsObject") != null) {
                localStorage.removeItem("livepagetravelDetailsObject");
            }
            _this.checktripstat();
        });
    };
    LiveSingleDevice.prototype.endTrip = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Confirm',
            message: 'Do you want to end the trip?',
            buttons: [
                {
                    text: 'YES PROCEED',
                    handler: function () {
                        _this.storage.get("TRIPDATA").then(function (res) {
                            if (res) {
                                _this.callendtripfunc(res);
                            }
                        });
                    }
                },
                {
                    text: 'Back'
                }
            ]
        });
        alert.present();
    };
    LiveSingleDevice.prototype.ionViewDidLoad = function () {
        // this.setDocHeightAtFirst();
        this._io = __WEBPACK_IMPORTED_MODULE_4_socket_io_client__["connect"](this.apiCall.mainUrl + 'gps', { secure: true, rejectUnauthorized: false, transports: ["websocket", "polling"], upgrade: false });
        this._io.on('connect', function (data) { console.log('Ignition IO Connected', data); });
        this.showShareBtn = true;
        var paramData = this.navParams.get("device");
        this.titleText = paramData.Device_Name;
        this.temp(paramData);
        this.showActionSheet = true;
    };
    LiveSingleDevice.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.plt.is('ios')) {
            this.shwBckBtn = true;
            this.viewCtrl.showBackButton(false);
        }
        this.platform.ready().then(function () {
            _this.resumeListener = _this.platform.resume.subscribe(function () {
                var today, Christmas;
                today = new Date();
                Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
                var diffMs = (today - Christmas); // milliseconds between now & Christmas
                var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
                if (diffMins >= 5) {
                    localStorage.removeItem("backgroundModeTime");
                    _this.alertCtrl.create({
                        message: 'Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?',
                        buttons: [
                            {
                                text: 'YES PROCEED',
                                handler: function () {
                                    _this.refreshMe();
                                }
                            },
                            {
                                text: 'Back',
                                handler: function () {
                                    _this.navCtrl.setRoot('DashboardPage');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    };
    LiveSingleDevice.prototype.ionViewWillLeave = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.resumeListener.unsubscribe();
        });
    };
    LiveSingleDevice.prototype.ngOnDestroy = function () {
        var that = this;
        for (var i = 0; i < that.socketChnl.length; i++)
            that._io.removeAllListeners(that.socketChnl[i]);
        that._io.on('disconnect', function () {
            that._io.open();
        });
        if (localStorage.getItem("livepagetravelDetailsObject") != null) {
            localStorage.removeItem("livepagetravelDetailsObject");
        }
    };
    LiveSingleDevice.prototype.onClickMap = function (maptype) {
        var that = this;
        if (maptype == 'SATELLITE') {
            that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].SATELLITE);
        }
        else {
            if (maptype == 'TERRAIN') {
                that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].TERRAIN);
            }
            else {
                if (maptype == 'NORMAL') {
                    that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].NORMAL);
                }
                else {
                    if (maptype == 'HYBRID') {
                        that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].HYBRID);
                    }
                }
            }
        }
    };
    LiveSingleDevice.prototype.createTrip = function () {
        console.log("create trip data: ", this.deviceDeatils);
        this.navCtrl.push('CreateTripPage', {
            'paramData': this.deviceDeatils,
        });
    };
    LiveSingleDevice.prototype.trafficFunc = function () {
        var that = this;
        that.isEnabled = !that.isEnabled;
        if (that.isEnabled == true) {
            that.allData.map.setTrafficEnabled(true);
        }
        else {
            that.allData.map.setTrafficEnabled(false);
        }
    };
    LiveSingleDevice.prototype.shareLive = function () {
        var that = this;
        that.showActionSheet = false;
        // that.drawerHidden = false;
        that.drawerState1 = __WEBPACK_IMPORTED_MODULE_11_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        that.showFooter = true;
    };
    LiveSingleDevice.prototype.sharedevices = function (param) {
        var that = this;
        if (param == '15mins') {
            that.condition = 'gpsc';
            that.condition1 = 'light';
            that.condition2 = 'light';
            that.tttime = 15;
            // that.tttime  = (15 * 60000); //for miliseconds
        }
        else {
            if (param == '1hour') {
                that.condition1 = 'gpsc';
                that.condition = 'light';
                that.condition2 = 'light';
                that.tttime = 60;
                // that.tttime  = (1 * 3600000); //for miliseconds
            }
            else {
                if (param == '8hours') {
                    that.condition2 = 'gpsc';
                    that.condition = 'light';
                    that.condition1 = 'light';
                    that.tttime = (8 * 60);
                    // that.tttime  = (8 * 3600000);
                }
            }
        }
    };
    LiveSingleDevice.prototype.shareLivetemp = function () {
        var _this = this;
        var that = this;
        if (that.tttime == undefined) {
            that.tttime = 15;
        }
        var data = {
            id: that.liveDataShare._id,
            imei: that.liveDataShare.Device_ID,
            sh: this.userdetails._id,
            ttl: that.tttime // set to 1 hour by default
        };
        this.apiCall.startLoading().present();
        this.apiCall.shareLivetrackCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.resToken = data.t;
            _this.liveShare();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LiveSingleDevice.prototype.liveShare = function () {
        var that = this;
        var link = this.apiCall.mainUrl + "share/liveShare?t=" + that.resToken;
        that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
        that.showActionSheet = true;
        that.showFooter = false;
        that.tttime = undefined;
    };
    LiveSingleDevice.prototype.parseMillisecondsIntoReadableTime = function (milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;
        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
        // return h + ':' + m;
        return h + ':' + m + ':' + s;
    };
    LiveSingleDevice.prototype.settings = function () {
        var _this = this;
        var that = this;
        var profileModal = this.modalCtrl.create('DeviceSettingsPage', {
            param: that.deviceDeatils
        });
        profileModal.present();
        profileModal.onDidDismiss(function () {
            for (var i = 0; i < that.socketChnl.length; i++)
                that._io.removeAllListeners(that.socketChnl[i]);
            var paramData = _this.navParams.get("device");
            _this.temp(paramData);
        });
    };
    LiveSingleDevice.prototype.getAddress = function (coordinates) {
        // debugger
        var that = this;
        if (!coordinates) {
            that.address = 'N/A';
            return;
        }
        // this.geocoderApi.geocoderResult(coordinates.lat, coordinates.long)
        // .then(res => {
        //   that.address = res;
        // })
        this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
            .then(function (res) {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            that.address = str;
        });
    };
    LiveSingleDevice.prototype.socketInit = function (pdata, center) {
        if (center === void 0) { center = false; }
        this._io.emit('acc', pdata.Device_ID);
        this.socketChnl.push(pdata.Device_ID + 'acc');
        var that = this;
        this._io.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            if (d4 != undefined)
                console.log("ping data: => ", d4);
            (function (data) {
                if (data == undefined) {
                    return;
                }
                localStorage.setItem("pdata", JSON.stringify(data));
                if (data._id != undefined && data.last_location != undefined) {
                    var key = data._id;
                    that.impkey = data._id;
                    that.deviceDeatils = data;
                    var ic_1 = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](that.icons[data.iconType]);
                    if (!ic_1) {
                        return;
                    }
                    ic_1.path = null;
                    if (data.status.toLowerCase() === 'running' && data.last_speed > 0) {
                        if (that.plt.is('ios')) {
                            ic_1.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
                        }
                        else if (that.plt.is('android')) {
                            ic_1.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
                        }
                    }
                    else {
                        if (data.status.toLowerCase() === 'running' && data.last_speed === 0) {
                            if (that.plt.is('ios')) {
                                ic_1.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                            }
                            else if (that.plt.is('android')) {
                                ic_1.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                            }
                        }
                        else {
                            if (that.plt.is('ios')) {
                                ic_1.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                            }
                            else if (that.plt.is('android')) {
                                ic_1.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                            }
                        }
                    }
                    that.vehicle_speed = data.last_speed;
                    that.todays_odo = data.today_odo;
                    that.total_odo = data.total_odo;
                    if (that.userdetails.fuel_unit == 'LITRE') {
                        that.fuel = data.currentFuel;
                    }
                    else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
                        that.fuel = data.fuel_percent;
                    }
                    else {
                        that.fuel = data.currentFuel;
                    }
                    that.last_ping_on = data.last_ping_on;
                    var tempvar = new Date(data.lastStoppedAt);
                    if (data.lastStoppedAt != null) {
                        var fd = tempvar.getTime();
                        var td = new Date().getTime();
                        var time_difference = td - fd;
                        var total_min = time_difference / 60000;
                        var hours = total_min / 60;
                        var rhours = Math.floor(hours);
                        var minutes = (hours - rhours) * 60;
                        var rminutes = Math.round(minutes);
                        that.lastStoppedAt = rhours + ':' + rminutes;
                    }
                    else {
                        that.lastStoppedAt = '00' + ':' + '00';
                    }
                    that.distFromLastStop = data.distFromLastStop;
                    if (!isNaN(data.timeAtLastStop)) {
                        that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
                    }
                    else {
                        that.timeAtLastStop = '00:00:00';
                    }
                    that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
                    that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
                    that.last_ACC = data.last_ACC;
                    that.acModel = data.ac;
                    that.currentFuel = data.currentFuel;
                    that.power = data.power;
                    that.gpsTracking = data.gpsTracking;
                    that.recenterMeLat = data.last_location.lat;
                    that.recenterMeLng = data.last_location.long;
                    if (that.allData[key]) {
                        that.socketSwitch[key] = data;
                        console.log("check mark is avalible or not? ", that.allData[key].mark);
                        if (that.allData[key].mark !== undefined) {
                            that.allData[key].mark.setIcon(ic_1);
                            that.allData[key].mark.setPosition(that.allData[key].poly[1]);
                            var temp = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](that.allData[key].poly[1]);
                            that.allData[key].poly[0] = __WEBPACK_IMPORTED_MODULE_6_lodash__["cloneDeep"](temp);
                            that.allData[key].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                            var speed = data.status == "RUNNING" ? data.last_speed : 0;
                            that.getAddress(data.last_location);
                            // debugger
                            that.liveTrack(that.allData.map, that.allData[key].mark, ic_1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                        }
                        else {
                            return;
                        }
                    }
                    else {
                        that.allData[key] = {};
                        that.socketSwitch[key] = data;
                        that.allData[key].poly = [];
                        if (data.sec_last_location) {
                            that.allData[key].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
                        }
                        else {
                            that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                        }
                        that.allData[key].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                        if (data.last_location != undefined) {
                            that.allData.map.addMarker({
                                title: data.Device_Name,
                                position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                                icon: ic_1.url,
                            }).then(function (marker) {
                                that.allData[key].mark = marker;
                                marker.addEventListener(__WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                                    .subscribe(function (e) {
                                    that.liveVehicleName = data.Device_Name;
                                    that.showActionSheet = true;
                                    that.showaddpoibtn = true;
                                    that.getAddressTitle(marker);
                                });
                                that.getAddress(data.last_location);
                                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                that.liveTrack(that.allData.map, that.allData[key].mark, ic_1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that.elementRef, data.iconType, data.status, that);
                            });
                        }
                    }
                }
            })(d4);
        });
    };
    LiveSingleDevice.prototype.getAddressTitle = function (marker) {
        this.geocoderApi.reverseGeocode(marker.getPosition().lat, marker.getPosition().lng)
            .then(function (res) {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            marker.setSnippet(str);
        });
    };
    LiveSingleDevice.prototype.addPOI = function () {
        var _this = this;
        var that = this;
        var modal = this.modalCtrl.create(PoiPage, {
            param1: that.allData[that.impkey]
        });
        modal.onDidDismiss(function (data) {
            console.log(data);
            var that = _this;
            that.showaddpoibtn = false;
        });
        modal.present();
    };
    LiveSingleDevice.prototype.liveTrack = function (map, mark, icons, coords, speed, delay, center, id, elementRef, iconType, status, that) {
        var target = 0;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        if (center) {
            map.setCameraTarget(coords[0]);
        }
        function _goToPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](coords[target].lat, coords[target].lng);
            var distance = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
                // mark.setIcon(icons);
            }
            function _moveMarker() {
                // debugger
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.mapTrail) {
                        that.showTrail(lat, lng, map);
                    }
                    else {
                        if (that.allData.polylineID) {
                            that.tempArray = [];
                            that.allData.polylineID.remove();
                        }
                    }
                    map.setCameraTarget(new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    that.latlngCenter = new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](lat, lng);
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    head = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.mapTrail) {
                        that.showTrail(dest.lat, dest.lng, map);
                    }
                    else {
                        if (that.allData.polylineID) {
                            that.tempArray = [];
                            that.allData.polylineID.remove();
                        }
                    }
                    map.setCameraTarget(dest);
                    that.latlngCenter = dest;
                    mark.setPosition(dest);
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    };
    LiveSingleDevice.prototype.zoomin = function () {
        var that = this;
        that.allData.map.moveCameraZoomIn();
    };
    LiveSingleDevice.prototype.zoomout = function () {
        var that = this;
        that.allData.map.animateCameraZoomOut();
    };
    LiveSingleDevice.prototype.temp = function (data) {
        var that = this;
        that.showShareBtn = true;
        that.liveDataShare = data;
        that.onClickShow = false;
        if (that.allData.map != undefined) {
            that.allData.map.remove();
        }
        for (var i = 0; i < that.socketChnl.length; i++)
            that._io.removeAllListeners(that.socketChnl[i]);
        that.allData = {};
        that.socketChnl = [];
        that.socketSwitch = {};
        var style = [];
        //Change Style to night between 7pm to 5am
        if (this.isNight()) {
            style = __WEBPACK_IMPORTED_MODULE_13__live_map_style_model__["a" /* mapStyle */];
        }
        if (data) {
            if (data.last_location) {
                if (that.plt.is('android')) {
                    var mapOptions = {
                        backgroundColor: 'white',
                        controls: {
                            compass: true,
                            zoom: false,
                        },
                        gestures: {
                            rotate: false,
                            tilt: false
                        },
                        mapType: that.mapKey,
                        styles: style
                    };
                    var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
                    // Environment.setBackgroundColor("black");
                    map.animateCamera({
                        target: { lat: 20.5937, lng: 78.9629 },
                        zoom: 18,
                        duration: 3000,
                        padding: 0 // default = 20px
                    });
                    map.setCameraTarget({ lat: data.last_location['lat'], lng: data.last_location['long'] });
                    that.allData.map = map;
                }
                else {
                    if (that.plt.is('ios')) {
                        var mapOptions = {
                            backgroundColor: 'white',
                            controls: {
                                compass: true,
                                zoom: false,
                            },
                            gestures: {
                                rotate: false,
                                tilt: false
                            },
                            camera: {
                                target: { lat: 20.5937, lng: 78.9629 },
                                zoom: 18,
                            },
                            mapType: that.mapKey,
                            styles: style
                        };
                        var map = __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas_single_device', mapOptions);
                        // Environment.setBackgroundColor("black");
                        that.allData.map = map;
                    }
                }
                that.socketInit(data);
            }
            else {
                that.allData.map = that.newMap();
                that.socketInit(data);
            }
        }
    };
    LiveSingleDevice.prototype.showTrail = function (lat, lng, map) {
        var that = this;
        that.tempArray.push({ lat: lat, lng: lng });
        if (that.tempArray.length === 2) {
            map.addPolyline({
                points: that.tempArray,
                color: '#0260f7',
                width: 5,
                geodesic: true
            }).then(function (polyline) {
                that.allData.polylineID = polyline;
                // that.polylineID.push(polyline);
            });
        }
        else if (that.tempArray.length > 2) {
            that.tempArray.shift();
            map.addPolyline({
                points: that.tempArray,
                color: '#0260f7',
                width: 5,
                geodesic: true
            }).then(function (polyline) {
                that.allData.polylineID = polyline;
                // that.allData.polylineID.push(polyline);
            });
        }
    };
    LiveSingleDevice.prototype.onClickMainMenu = function (item) {
        this.menuActive = !this.menuActive;
    };
    LiveSingleDevice.prototype.onSelectMapOption = function (type) {
        var that = this;
        if (type == 'locateme') {
            that.allData.map.setCameraTarget(that.latlngCenter);
        }
        else {
            if (type == 'mapHideTraffic') {
                this.mapHideTraffic = !this.mapHideTraffic;
                if (this.mapHideTraffic) {
                    that.allData.map.setTrafficEnabled(true);
                }
                else {
                    that.allData.map.setTrafficEnabled(false);
                }
            }
            else {
                if (type == 'mapTrail') {
                    this.mapTrail = !this.mapTrail;
                }
            }
        }
    };
    LiveSingleDevice = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-live',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\live-single-device\live-single-device.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-buttons start *ngIf="shwBckBtn">\n\n      <button ion-button small (click)="goBack()">\n\n        <ion-icon name="arrow-back"></ion-icon>Back\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>Live for {{ titleText }}</ion-title>\n\n    <ion-buttons end>\n\n      <button *ngIf="allData.tripStat != \'Started\'" ion-button icon-only (click)="createTrip()">\n\n        <ion-icon name="play"></ion-icon>\n\n      </button>\n\n      <button *ngIf="allData.tripStat == \'Started\'" ion-button icon-only (click)="endTrip()">\n\n        <ion-icon name="radio-button-on"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <div id="map_canvas_single_device">\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-fab top left>\n\n          <button ion-fab color="light" mini (click)="onClickMainMenu()">\n\n            <ion-icon color="gpsc" name="map"></ion-icon>\n\n          </button>\n\n          <ion-fab-list side="bottom">\n\n            <button style="margin: 0 2px;" ion-fab mini (click)="onClickMap(\'SATELLITE\')" color="gpsc">\n\n              S\n\n            </button>\n\n            <button style="margin: 1px 0px 1px 0px;" ion-fab mini (click)="onClickMap(\'HYBRID\')" color="gpsc">\n\n              H\n\n            </button>\n\n            <button style="margin: 1px 0px 1px 0px;" ion-fab mini (click)="onClickMap(\'TERRAIN\')" color="gpsc">\n\n              T\n\n            </button>\n\n            <button style="margin: 1px 0px 1px 0px;" ion-fab mini (click)="onClickMap(\'NORMAL\')" color="gpsc">\n\n              N\n\n            </button>\n\n          </ion-fab-list>\n\n        </ion-fab>\n\n      </ion-col>\n\n      <ion-col>\n\n        <p class="blink" style="text-align: center;font-size:24px;color:cornflowerblue;font-weight: 600;"\n\n          *ngIf="!vehicle_speed">\n\n          0\n\n          <span style="font-size:16px;">Km/hr</span>\n\n        </p>\n\n        <p class="blink" style="text-align: center;font-size:24px;color: green;font-weight: 600;"\n\n          *ngIf="vehicle_speed <= 60">\n\n          {{ vehicle_speed }}\n\n          <span style="font-size:16px;">Km/hr</span>\n\n        </p>\n\n        <p class="blink" style="text-align: center;font-size:24px;color: red;font-weight: 600;"\n\n          *ngIf="vehicle_speed > 60">\n\n          {{ vehicle_speed }}\n\n          <span style="font-size:16px;">Km/hr</span>\n\n        </p>\n\n      </ion-col>\n\n      <ion-col>\n\n        <ion-fab top right>\n\n          <button ion-fab color="gpsc" mini>\n\n            <ion-icon name="arrow-dropdown"></ion-icon>\n\n          </button>\n\n          <ion-fab-list side="bottom">\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab *ngIf="showShareBtn"\n\n              (click)="shareLive($event)">\n\n              <ion-icon name="share"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="onSelectMapOption(\'mapTrail\')">\n\n              <ion-icon name="eye"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="onSelectMapOption(\'mapHideTraffic\')">\n\n              <ion-icon name="walk"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="settings()">\n\n              <ion-icon name="cog"></ion-icon>\n\n            </button>\n\n            <button color="gpsc" style="margin: 1px 0px 1px 0px;" ion-fab (click)="addPOI()">\n\n              <ion-icon name="flag"></ion-icon>\n\n            </button>\n\n          </ion-fab-list>\n\n        </ion-fab>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-fab right style="margin-top:59%">\n\n      <button ion-fab color="gpsc" mini (click)="reCenterMe()">\n\n        <ion-icon name="locate"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab right style="margin-top:72%">\n\n      <button color="gpsc" ion-fab (click)="zoomin()" mini>\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab right style="margin-top:85%">\n\n      <button color="gpsc" ion-fab (click)="zoomout()" mini>\n\n        <ion-icon name="remove"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n    <ion-fab left style="margin-top:85%">\n\n      <button ion-fab color="gpsc" mini (click)="refreshMe()">\n\n        <ion-icon name="refresh"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n    <ion-row *ngIf="expectation.distance"\n\n      style="margin-top:72%; background-color: rgb(0, 0, 0, 0.5); font-size: 0.8em; color: white;border-radius: 25px;width: 70%;margin: auto; padding:5px;">\n\n      <ion-col style="background-color: transparent; text-align: center;" col-6>\n\n        Distance {{ expectation.distance }}\n\n      </ion-col>\n\n      <ion-col style="background-color: transparent; text-align: center;" col-6>\n\n        Time {{ expectation.duration }}\n\n      </ion-col>\n\n    </ion-row>\n\n  </div>\n\n</ion-content>\n\n<div *ngIf="showActionSheet" class="divPlan">\n\n  <ion-bottom-drawer [(state)]="drawerState" [dockedHeight]="dockedHeight" [shouldBounce]="shouldBounce"\n\n    [distanceTop]="distanceTop" [minimumHeight]="minimumHeight" [transition]="transition" (click)="setDocHeight()">\n\n    <div class="drawer-content">\n\n      <p padding-left style="font-size: 13px; padding-bottom: 3px; color:cornflowerblue;">\n\n        Last Updated On &mdash;\n\n        {{ last_ping_on | date: "medium" }}\n\n      </p>\n\n      <p padding-left style="font-size: 13px" *ngIf="!address">N/A</p>\n\n      <p padding-left style="font-size: 13px" *ngIf="address">{{ address }}</p>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/ign_red.png" *ngIf="last_ACC == \'0\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ign_green.png" *ngIf="last_ACC == \'1\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ign_idle.png" *ngIf="last_ACC == null" width="20" height="20" />\n\n          <p>IGN</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/ac_na.png" *ngIf="acModel == null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ac_present.png" *ngIf="acModel == \'1\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ac_empty.png" *ngIf="acModel == \'0\'" width="20" height="20" />\n\n          <p>AC</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/fuel_available.png" *ngIf="currentFuel != null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/fuel_empty.png" *ngIf="currentFuel == null" width="20" height="20" />\n\n          <p>Fuel</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/power_na.png" *ngIf="power == null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/pwer_disconnected.png" *ngIf="power == \'0\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/power_connected.png" *ngIf="power == \'1\'" width="20" height="20" />\n\n          <p>Power</p>\n\n        </ion-col>\n\n        <ion-col width-10 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/gps_na.png" *ngIf="gpsTracking == null" width="30" height="20" />\n\n          <img src="assets/imgs/statusIcons/gps_notPresent.png" *ngIf="gpsTracking == \'0\'" width="30" height="20" />\n\n          <img src="assets/imgs/statusIcons/gps_present.png" *ngIf="gpsTracking == \'1\'" width="30" height="20" />\n\n          <p>GPS</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!total_odo">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="total_odo">\n\n            {{ total_odo | number: "1.0-2" }}\n\n          </p>\n\n          <p style="font-size: 13px">Odometer</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!vehicle_speed">\n\n            0 Km/hr\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="vehicle_speed">\n\n            {{ vehicle_speed }} Km/hr\n\n          </p>\n\n          <p style="font-size: 13px">\n\n            Speed\n\n          </p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="fuel">\n\n            {{ fuel }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!fuel">N/A</p>\n\n          <p style="font-size: 13px">Fuel</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!distFromLastStop">\n\n            0 Kms\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="distFromLastStop">\n\n            {{ distFromLastStop | number: "1.0-2" }} Kms\n\n          </p>\n\n          <p style="font-size: 13px">From Last Stop</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/milestone-png-1.png" width="30" height="35" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!todays_odo">\n\n            0 Kms\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="todays_odo">\n\n            {{ todays_odo | number: "1.0-2" }} Kms\n\n          </p>\n\n          <p style="font-size: 13px">Total</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!timeAtLastStop">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="timeAtLastStop">\n\n            {{ timeAtLastStop }}\n\n          </p>\n\n          <p style="font-size: 13px">At Last Stop</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/parkinglogo.png" width="30" height="30" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!today_stopped">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="today_stopped">\n\n            {{ today_stopped }}\n\n          </p>\n\n          <p style="font-size: 13px">Total</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!lastStoppedAt">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="lastStoppedAt">\n\n            {{ lastStoppedAt }}\n\n          </p>\n\n          <p style="font-size: 13px">From Last Stop</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/2530791.png" width="30" height="35" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!today_running">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="today_running">\n\n            {{ today_running }}\n\n          </p>\n\n          <p style="font-size: 13px">Total</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    </div>\n\n  </ion-bottom-drawer>\n\n</div>\n\n\n\n<div *ngIf="showFooter">\n\n  <ion-bottom-drawer [(state)]="drawerState1" [dockedHeight]="dockedHeight1" [shouldBounce]="shouldBounce"\n\n    [distanceTop]="distanceTop1" [minimumHeight]="minimumHeight1">\n\n    <div class="drawer-content">\n\n      <p style="font-size:1.2em; color:black;">\n\n        Share Live Vehicle\n\n      </p>\n\n      <ion-row>\n\n        <ion-col col-4 style="padding: 5px">\n\n          <button ion-button block color="{{ condition }}" (click)="sharedevices(\'15mins\')">\n\n            15 mins\n\n          </button>\n\n        </ion-col>\n\n        <ion-col col-4 style="padding: 5px">\n\n          <button ion-button block color="{{ condition1 }}" (click)="sharedevices(\'1hour\')">\n\n            1 hour\n\n          </button>\n\n        </ion-col>\n\n        <ion-col col-4 style="padding: 5px">\n\n          <button ion-button block color="{{ condition2 }}" (click)="sharedevices(\'8hours\')">\n\n            8 hours\n\n          </button>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col col-4></ion-col>\n\n        <ion-col col-4></ion-col>\n\n        <ion-col col-4 style="text-align: right;">\n\n          <ion-fab style="right: calc(10px + env(safe-area-inset-right));">\n\n            <button ion-fab mini (click)="shareLivetemp()" color="gpsc">\n\n              <ion-icon name="send" color="black"></ion-icon>\n\n            </button>\n\n          </ion-fab>\n\n        </ion-col>\n\n      </ion-row>\n\n    </div>\n\n  </ion-bottom-drawer>\n\n</div>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\live-single-device\live-single-device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_native_page_transitions__["a" /* NativePageTransitions */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_10__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], LiveSingleDevice);
    return LiveSingleDevice;
}());

var PoiPage = /** @class */ (function () {
    function PoiPage(apicall, toastCtrl, navparam, viewCtrl) {
        this.apicall = apicall;
        this.toastCtrl = toastCtrl;
        this.navparam = navparam;
        this.viewCtrl = viewCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.params = this.navparam.get("param1");
        this.lat = this.params.mark.getPosition().lat;
        this.lng = this.params.mark.getPosition().lng;
    }
    PoiPage.prototype.ngOnInit = function () {
        var that = this;
        that.address = undefined;
        var geocoder = new google.maps.Geocoder;
        var latlng = new google.maps.LatLng(this.lat, this.lng);
        var request = { "latLng": latlng };
        geocoder.geocode(request, function (resp, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (resp[0] != null) {
                    that.address = resp[0].formatted_address;
                }
                else {
                    console.log("No address available");
                }
            }
            else {
                that.address = 'N/A';
            }
        });
    };
    PoiPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    PoiPage.prototype.save = function () {
        var _this = this;
        if (this.poi_name == undefined || this.address == undefined) {
            var toast = this.toastCtrl.create({
                message: "POI name is required!",
                duration: 1500,
                position: "bottom"
            });
            toast.present();
        }
        else {
            var payload = {
                "poi": [{
                        "location": {
                            "type": "Point",
                            "coordinates": [
                                this.lng,
                                this.lat
                            ]
                        },
                        "poiname": this.poi_name,
                        "status": "Active",
                        "address": this.address,
                        "user": this.islogin._id
                    }]
            };
            this.apicall.startLoading().present();
            this.apicall.addPOIAPI(payload)
                .subscribe(function (data) {
                _this.apicall.stopLoading();
                var toast = _this.toastCtrl.create({
                    message: "POI added successfully!",
                    duration: 2000,
                    position: 'top'
                });
                toast.present();
                _this.viewCtrl.dismiss();
            }, function (err) {
                _this.apicall.stopLoading();
            });
        }
    };
    PoiPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\live-single-device\poi.html"*/'<!-- <ion-content padding> -->\n\n<div>\n\n    <ion-row>\n\n        <ion-col col-12 style="text-align: center; font-size: 2rem;">\n\n            <b>Add POI</b>&nbsp;&nbsp;&nbsp;\n\n            <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n        </ion-col>\n\n        <!-- <ion-col col-2 style="text-align: right">\n\n                <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n\n            </ion-col> -->\n\n    </ion-row><br />\n\n    <!-- <p style="text-align: center; font-size: 2rem;">\n\n            <b>Add POI&nbsp;&nbsp;<ion-icon class="close-button" id="close-button" name="close-circle"\n\n                (tap)="dismiss()"></ion-icon></b>\n\n        </p><br /> -->\n\n    <ion-row>\n\n        <ion-col col-12><b>POI Name:</b></ion-col>\n\n        <ion-col col-12>\n\n            <ion-input type="text" [(ngModel)]="poi_name"></ion-input>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col col-12><b>POI Address:</b></ion-col>\n\n        <ion-col col-12>\n\n            <ion-input type="text" [(ngModel)]="address"></ion-input>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col col-6>\n\n            <button ion-button block (tap)="save()" color="gpsc">SAVE</button>\n\n        </ion-col>\n\n        <ion-col col-6>\n\n            <button ion-button block (tap)="dismiss()" color="gpsc">CANCEL</button>\n\n        </ion-col>\n\n    </ion-row>\n\n</div>\n\n<!-- </ion-content> -->'/*ion-inline-end:"D:\Pro\flex_track\src\pages\live-single-device\poi.html"*/,
            selector: 'page-device-settings'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], PoiPage);
    return PoiPage;
}());

//# sourceMappingURL=live-single-device.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return AcReportPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ACDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AcReportPage = /** @class */ (function () {
    function AcReportPage(navCtrl, navParams, apiCAll) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCAll = apiCAll;
        this.portstemp = [];
        this.reportData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
    }
    AcReportPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AcReportPage');
    };
    AcReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    AcReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCAll.startLoading().present();
        this.apiCAll.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCAll.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apiCAll.stopLoading();
            console.log(err);
        });
    };
    AcReportPage.prototype.getAcReportData = function () {
        var _this = this;
        var that = this;
        this.reportData = [];
        if (that.dev_id == undefined) {
            that.dev_id = "";
        }
        this.apiCAll.startLoading().present();
        ;
        this.apiCAll.getACReportAPI(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), that.islogin._id, that.dev_id)
            .subscribe(function (data) {
            _this.apiCAll.stopLoading();
            console.log("ac report data: ", data);
            _this.reportData = data;
            console.log("1stAction: ", data[0]['1stAction']);
        });
    };
    AcReportPage.prototype.getAcReporID = function (key) {
        console.log("key: ", key.Device_ID);
        this.dev_id = key.Device_ID;
    };
    AcReportPage.prototype.showDetail = function (pdata) {
        console.log("pdata: ", pdata);
        var that = this;
        this.navCtrl.push(ACDetailPage, {
            'param': pdata,
            'fdate': that.datetimeStart,
            'tdate': that.datetimeEnd,
            'uid': that.islogin._id
        });
    };
    AcReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-ac-report',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\ac-report\ac-report.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>AC Report</ion-title>\n\n  </ion-navbar>\n\n  <ion-item style="background-color: #fafafa;">\n\n    <ion-label>Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getAcReporID(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n\n        </ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getAcReportData();">\n\n        </ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list>\n\n    <ion-card *ngFor="let rep of reportData;" (click)="showDetail(rep)">\n\n      <ion-item style="border-bottom: 2px solid #dedede;">\n\n        <ion-avatar item-start>\n\n          <img src="assets/imgs/car_red_icon.png">\n\n        </ion-avatar>\n\n        <ion-label>{{rep.VehicleName}}</ion-label>\n\n      </ion-item>\n\n\n\n      <ion-card-content>\n\n        <ion-row style="padding-top:5px;">\n\n          <ion-col col-6>{{rep[\'1stAction\']}}</ion-col>\n\n          <ion-col col-6>{{rep[\'1stTime\']}}</ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">Action</ion-col>\n\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">ON Time(Min.)</ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-6>{{rep[\'2stAction\']}}</ion-col>\n\n          <ion-col col-6>{{rep[\'2stTime\']}}</ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">Action</ion-col>\n\n          <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">OFF Time(Min.)</ion-col>\n\n        </ion-row>\n\n      </ion-card-content>\n\n    </ion-card>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\ac-report\ac-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], AcReportPage);
    return AcReportPage;
}());

var ACDetailPage = /** @class */ (function () {
    function ACDetailPage(apiCall, navParam) {
        this.apiCall = apiCall;
        this.navParam = navParam;
        this.paramData = {};
        this.ddata = [];
        console.log("param parameters: ", this.navParam.get('param'));
        this.paramData = this.navParam.get('param');
        this.fdate = this.navParam.get('fdate');
        this.tdate = this.navParam.get('tdate');
        this.uid = this.navParam.get('uid');
    }
    ACDetailPage.prototype.ngOnInit = function () {
        this.getDetails();
    };
    ACDetailPage.prototype.getDetails = function () {
        var _this = this;
        debugger;
        this.apiCall.startLoading().present();
        this.apiCall.getDetailACReportAPI(new Date(this.fdate).toISOString(), new Date(this.tdate).toISOString(), this.uid, this.paramData._id.imei)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("detailed ac report data: ", data[0].s);
            _this.ddata = data[0].s;
        });
    };
    ACDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\ac-report\ac-detail.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Deatils for {{paramData.VehicleName}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <ion-card *ngFor="let d of ddata;">\n\n        <ion-card-content>\n\n            <ion-row>\n\n                <ion-col col-6 style="font-size:13px;text-align:justify;">{{d.switch}}</ion-col>\n\n                <ion-col col-6 style="font-size:13px;text-align:justify;">\n\n                    {{d.timestamp | date:\'medium\'}}</ion-col>\n\n            </ion-row>\n\n            <ion-row>\n\n                <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">On/Off</ion-col>\n\n                <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;text-align:justify;">Time</ion-col>\n\n            </ion-row>\n\n        </ion-card-content>\n\n    </ion-card>\n\n\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\ac-report\ac-detail.html"*/,
            styles: ["\n  .col {\n    padding: 0px;\n}"]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ACDetailPage);
    return ACDetailPage;
}());

//# sourceMappingURL=ac-report.js.map

/***/ }),

/***/ 405:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return UploadDocPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocPopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__view_doc_view_doc__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__ = __webpack_require__(63);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UploadDocPage = /** @class */ (function () {
    function UploadDocPage(navCtrl, navParams, viewCtrl, platform, toastCtrl, loadingCtrl, apiCall, modalCtrl, popoverCtrl, event) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.apiCall = apiCall;
        this.modalCtrl = modalCtrl;
        this.popoverCtrl = popoverCtrl;
        this.event = event;
        this._docTypeList = [];
        this.imgData = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.vehData = this.navParams.get("vehData");
        this.event.subscribe("reloaddoclist", function () {
            _this._docTypeList = [];
            _this.checkDevice();
        });
    }
    UploadDocPage.prototype.ngOnInit = function () {
        this.checkDevice();
    };
    UploadDocPage.prototype._addDocument = function () {
        this.navCtrl.push("AddDocPage", { vehData: this.vehData });
    };
    UploadDocPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UploadDocPage');
    };
    UploadDocPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UploadDocPage.prototype.onSelectChange = function (selectedValue) {
        console.log('Selected', selectedValue);
        this.docType = selectedValue;
    };
    UploadDocPage.prototype.brows = function () {
        this.docImge = true;
    };
    UploadDocPage.prototype.showToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'bottom'
        });
        toast.present();
    };
    UploadDocPage.prototype.viewDocFunc = function (imgData) {
        // let that = this;
        console.log("imgData: ", imgData);
        if (imgData == undefined) {
            this.showToast('Data not found');
        }
        else {
            var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__view_doc_view_doc__["a" /* ViewDoc */], {
                param1: imgData
            });
            modal.present();
        }
    };
    UploadDocPage.prototype.checkDevice = function () {
        var _this = this;
        this.apiCall.startLoading();
        this.apiCall.getsingledevice(this.vehData.Device_ID)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            if (data.imageDoc.length > 0) {
                // console.log("imagedoc data: " + JSON.stringify(data.imageDoc))
                for (var i = 0; i < data.imageDoc.length; i++) {
                    _this._docTypeList.push({
                        docname: data.imageDoc[i].doctype,
                        expDate: data.imageDoc[i].docdate ? data.imageDoc[i].docdate : 'N/A',
                        imageURL: data.imageDoc[i].image
                    });
                }
            }
        }, function (err) {
            console.log(err);
            _this.apiCall.stopLoading();
        });
    };
    UploadDocPage.prototype.presentPopover = function (ev, data) {
        var _this = this;
        console.log("populated=> " + JSON.stringify(data));
        var that = this;
        var popover = this.popoverCtrl.create(DocPopoverPage, {
            vehData: that.vehData,
            imgData: data
        }, {
            cssClass: 'contact-popover'
        });
        popover.onDidDismiss(function () {
            _this._docTypeList = [];
            _this.checkDevice();
        });
        popover.present({
            ev: ev
        });
    };
    UploadDocPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-upload-doc',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\add-devices\upload-doc\upload-doc.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Documents</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="_addDocument()">\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-list *ngIf="_docTypeList.length > 0">\n\n    <ion-item *ngFor="let doc of _docTypeList" (click)="viewDocFunc(doc)">\n\n      <ion-avatar item-start>\n\n        <img *ngIf="doc.docname == \'RC\'" src="assets/imgs/rc.webp">\n\n        <img *ngIf="doc.docname == \'PUC\'" src="assets/imgs/puc.webp">\n\n        <img *ngIf="doc.docname == \'Insurance\'" src="assets/imgs/insurence.webp">\n\n        <img *ngIf="doc.docname == \'Licence\'" src="assets/imgs/dl.webp">\n\n        <img *ngIf="doc.docname == \'Fitment\'" src="assets/imgs/rc.webp">\n\n        <img *ngIf="doc.docname == \'Permit\'" src="assets/imgs/rc.webp">\n\n      </ion-avatar>\n\n      <h1>{{doc.docname}}</h1>\n\n      <p>Expire On &nbsp;\n\n        <span *ngIf="doc.expDate != \'N/A\'">{{doc.expDate | date:\'mediumDate\'}}</span>\n\n        <span *ngIf="doc.expDate == \'N/A\'">{{doc.expDate}}</span>\n\n      </p>\n\n      <button ion-button item-end small icon-only color="gpsc" (click)="presentPopover($event, doc); $event.stopPropagation()">\n\n        <ion-icon name="more"></ion-icon>\n\n      </button>\n\n    </ion-item>\n\n  </ion-list>\n\n  <ion-card *ngIf="_docTypeList.length == 0">\n\n    <ion-card-content>\n\n      No doucments uploaded yet... Upload documents of your vehicle by pressing above <b>+</b> button.\n\n    </ion-card-content>\n\n  </ion-card>\n\n\n\n</ion-content>\n\n<!-- <ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Upload Document</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (click)="dismiss()">\n\n        <ion-icon name="close-circle"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n        <ion-row>\n\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'RC\' && !rcPresent)}" style="padding: 5px;"\n\n            (click)="firstFunc(\'RC\')" tappable>\n\n            <div style="border: 5px solid #f2f2f2;">\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-row>\n\n                    <ion-col class="docCol">RC</ion-col>\n\n                  </ion-row>\n\n                  <ion-row>\n\n                    <ion-col style="text-align: center;">\n\n                      <ion-icon *ngIf="!rcPresent" name="cloud-upload" color="gpsc" style="font-size: 4em"></ion-icon>\n\n                      <ion-icon *ngIf="rcPresent" name="cloud-done" color="secondary" style="font-size: 4em"></ion-icon>\n\n                    </ion-col>\n\n                  </ion-row>\n\n                </ion-col>\n\n              </ion-row>\n\n            </div>\n\n          </ion-col>\n\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'Insurance\' && !insurencePresent)}" style="padding: 5px;"\n\n            (click)="firstFunc(\'Insurance\')" tappable>\n\n            <div style="border: 5px solid #f2f2f2;">\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-row>\n\n                    <ion-col class="docCol">Insurance</ion-col>\n\n                  </ion-row>\n\n                  <ion-row>\n\n                    <ion-col style="text-align: center;">\n\n                      <ion-icon *ngIf="!insurencePresent" name="cloud-upload" color="gpsc" style="font-size: 4em">\n\n                      </ion-icon>\n\n                      <ion-icon *ngIf="insurencePresent" name="cloud-done" color="secondary" style="font-size: 4em">\n\n                      </ion-icon>\n\n                    </ion-col>\n\n                  </ion-row>\n\n                </ion-col>\n\n              </ion-row>\n\n            </div>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'Licence\' && !licencePresent)}" style="padding: 5px;"\n\n            (click)="firstFunc(\'Licence\')" tappable>\n\n            <div style="border: 5px solid #f2f2f2;">\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-row>\n\n                    <ion-col class="docCol">Licence</ion-col>\n\n                  </ion-row>\n\n                  <ion-row>\n\n                    <ion-col style="text-align: center;">\n\n                      <ion-icon *ngIf="!licencePresent" name="cloud-upload" color="gpsc" style="font-size: 4em">\n\n                      </ion-icon>\n\n                      <ion-icon *ngIf="licencePresent" name="cloud-done" color="secondary" style="font-size: 4em">\n\n                      </ion-icon>\n\n                    </ion-col>\n\n                  </ion-row>\n\n                </ion-col>\n\n              </ion-row>\n\n            </div>\n\n          </ion-col>\n\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'PUC\' && !licencePresent)}" style="padding: 5px;"\n\n            (click)="firstFunc(\'PUC\')" tappable>\n\n            <div style="border: 5px solid #f2f2f2;">\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-row>\n\n                    <ion-col class="docCol">PUC</ion-col>\n\n                  </ion-row>\n\n                  <ion-row>\n\n                    <ion-col style="text-align: center;">\n\n                      <ion-icon *ngIf="!pucPresent" name="cloud-upload" color="gpsc" style="font-size: 4em"></ion-icon>\n\n                      <ion-icon *ngIf="pucPresent" name="cloud-done" color="secondary" style="font-size: 4em">\n\n                      </ion-icon>\n\n                    </ion-col>\n\n                  </ion-row>\n\n                </ion-col>\n\n              </ion-row>\n\n            </div>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'Fitment\' && !fitmentPresent)}" style="padding: 5px;"\n\n            (click)="firstFunc(\'Fitment\')" tappable>\n\n            <div style="border: 5px solid #f2f2f2;" id="step2">\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-row>\n\n                    <ion-col class="docCol">Fitment Certificate</ion-col>\n\n                  </ion-row>\n\n                  <ion-row>\n\n                    <ion-col style="text-align: center;">\n\n                      <ion-icon *ngIf="!fitmentPresent" name="cloud-upload" color="gpsc" style="font-size: 4em">\n\n                      </ion-icon>\n\n                      <ion-icon *ngIf="fitmentPresent" name="cloud-done" color="secondary" style="font-size: 4em">\n\n                      </ion-icon>\n\n                    </ion-col>\n\n                  </ion-row>\n\n                </ion-col>\n\n              </ion-row>\n\n            </div>\n\n          </ion-col>\n\n          <ion-col col-6 [ngClass]="{largeWidth: (myVar == \'Permitt\' && !permittPresent)}" style="padding: 5px;"\n\n            (click)="firstFunc(\'Permit\')" tappable>\n\n            <div style="border: 5px solid #f2f2f2;" id="step1">\n\n              <ion-row>\n\n                <ion-col>\n\n                  <ion-row>\n\n                    <ion-col class="docCol">Permit Certificate</ion-col>\n\n                  </ion-row>\n\n                  <ion-row>\n\n                    <ion-col style="text-align: center;">\n\n                      <ion-icon *ngIf="!permittPresent" name="cloud-upload" color="gpsc" style="font-size: 4em"></ion-icon>\n\n                      <ion-icon *ngIf="permittPresent" name="cloud-done" color="secondary" style="font-size: 4em">\n\n                      </ion-icon>\n\n                    </ion-col>\n\n                  </ion-row>\n\n                </ion-col>\n\n              </ion-row>\n\n            </div>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n  <form #docForm="ngForm" *ngIf="docString != undefined" padding>\n\n    <ion-item>\n\n      <ion-label floating>{{docString}} Number </ion-label>\n\n      <ion-input type="text" [(ngModel)]="docname" name="docname"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Expiry Date </ion-label>\n\n      <ion-input type="text" onfocus="(this.type=\'date\')" onfocusout="(this.type=\'text\')" [(ngModel)]="docexp_date"\n\n        name="docexp_date">\n\n      </ion-input>\n\n    </ion-item>\n\n    <button ion-button block color="gpsc" [hidden]="(docname == undefined) || (docexp_date == undefined) || (lastImage != null)"\n\n      (click)="presentActionSheet()">\n\n      Select Document\n\n    </button>\n\n    <ion-row *ngIf="lastImage !== null" padding-left padding-right>\n\n      <ion-col col-10>\n\n        Selected {{docString}} Doc <span style="color: blue;">{{lastImage}}</span>\n\n      </ion-col>\n\n      <ion-col col-2 (click)="removeImg()">\n\n          <ion-icon name="close-circle"></ion-icon>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row *ngIf="lastImage !== null">\n\n      <ion-col>\n\n        <button ion-button block (click)="uploadImage()">Upload Doc</button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </form>\n\n\n\n</ion-content> -->'/*ion-inline-end:"D:\Pro\flex_track\src\pages\add-devices\upload-doc\upload-doc.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["LoadingController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], UploadDocPage);
    return UploadDocPage;
}());

var DocPopoverPage = /** @class */ (function () {
    function DocPopoverPage(navParams, modalCtrl, alertCtrl, apiCall, toastCtrl, navCtrl, viewCtrl, socialSharing) {
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.socialSharing = socialSharing;
        this.vehData = {};
        this.imgData = {};
        this.vehData = navParams.get("vehData");
        this.imgData = navParams.get("imgData");
        console.log("popover data=> ", this.imgData);
        var str = this.imgData.imageURL;
        var str1 = str.split('public/');
        this.link = "https://www.oneqlik.in/" + str1[1];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
    }
    DocPopoverPage.prototype.ngOnInit = function () { };
    DocPopoverPage.prototype.editItem = function () {
        console.log("edit");
    };
    DocPopoverPage.prototype.deleteDoc = function () {
        var that = this;
        var alert = this.alertCtrl.create({
            message: 'Do you want to delete this Document?',
            buttons: [{
                    text: 'YES PROCEED',
                    handler: function () {
                        console.log(that.vehData.Device_ID);
                        that.deleteDevice(that.vehData.Device_ID);
                    }
                },
                {
                    text: 'NO'
                }]
        });
        alert.present();
    };
    DocPopoverPage.prototype.deleteDevice = function (d_id) {
        var _this = this;
        var that = this;
        var payload = {
            devId: d_id,
            doctype: that.imgData.docname
        };
        this.apiCall.startLoading().present();
        var baseUrl = "https://www.oneqlik.in/devices/deleteDocuments";
        this.apiCall.urlpasseswithdata(baseUrl, payload)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            var DeletedDevice = data;
            console.log(DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: 'Document deleted successfully!',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                _this.viewCtrl.dismiss();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    DocPopoverPage.prototype.shareItem = function () {
        this.socialSharing.share(this.islogin.fn + " " + this.islogin.ln + " has shared the document with you.", "OneQlik- Vehicle Document", this.link, "");
    };
    DocPopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n    <ion-list>\n      <ion-item class=\"text-palatino\" (click)=\"editItem()\">\n        <ion-icon name=\"create\"></ion-icon>&nbsp;&nbsp;Edit\n      </ion-item>\n      <ion-item class=\"text-san-francisco\" (click)=\"deleteDoc()\">\n        <ion-icon name=\"trash\"></ion-icon>&nbsp;&nbsp;Delete\n      </ion-item>\n      <ion-item class=\"text-seravek\" (click)=\"shareItem()\">\n        <ion-icon name=\"share\"></ion-icon>&nbsp;&nbsp;Share\n      </ion-item>\n      \n    </ion-list>\n  "
        })
        // <ion-item id="step1" class="text-seravek" (click)="upload()">
        //        <ion-icon name="cloud-upload"></ion-icon>&nbsp;&nbsp;Docs
        //        </ion-item>
        ,
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing__["a" /* SocialSharing */]])
    ], DocPopoverPage);
    return DocPopoverPage;
}());

//# sourceMappingURL=upload-doc.js.map

/***/ }),

/***/ 410:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FuelConsumptionPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return FuelEntryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FuelConsumptionPage = /** @class */ (function () {
    function FuelConsumptionPage(apiCall, navCtrl, navParams, toastCtrl, event) {
        var _this = this;
        this.apiCall = apiCall;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.event = event;
        this.portstemp = [];
        this.fuelList = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_4_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_4_moment__().format();
        this.event.subscribe('reloadFuellist', function () {
            if (_this._vehId != undefined) {
                _this.getFuelList();
            }
        });
    }
    FuelConsumptionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FuelConsumptionPage');
    };
    FuelConsumptionPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    FuelConsumptionPage.prototype.toggleFooter = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_2_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    };
    FuelConsumptionPage.prototype.addFuelEntry = function () {
        this.navCtrl.push(FuelEntryPage, {
            portstemp: this.portstemp
        });
    };
    FuelConsumptionPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    FuelConsumptionPage.prototype.getFuelList = function () {
        var _this = this;
        this.fuelList = [];
        if (this._vehId == undefined) {
            var toast = this.toastCtrl.create({
                message: 'Please select the vehicle first..',
                position: 'bottom',
                duration: 1500
            });
            toast.present();
        }
        else {
            var _baseURL = "https://www.oneqlik.in/fuel/getFuels?vehicle=" + this._vehId + "&user=" + this.islogin._id;
            this.apiCall.startLoading().present();
            this.apiCall.getSOSReportAPI(_baseURL)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                console.log("fule entry list: ", data);
                if (!data.message) {
                    _this.fuelList = data;
                }
            }, function (err) {
                _this.apiCall.stopLoading();
                var toast = _this.toastCtrl.create({
                    message: "Report not found!",
                    duration: 1500,
                    position: "bottom"
                });
                toast.present();
            });
        }
    };
    FuelConsumptionPage.prototype.getid = function (veh) {
        this._vehId = veh._id;
    };
    FuelConsumptionPage.prototype.footerExpanded = function () {
        console.log('Footer expanded!');
    };
    FuelConsumptionPage.prototype.footerCollapsed = function () {
        console.log('Footer collapsed!');
    };
    FuelConsumptionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fuel-consumption',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\fuel\fuel-consumption\fuel-consumption.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Fuel Entry</ion-title>\n  </ion-navbar>\n  <ion-item style="background-color: #fafafa;">\n    <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n    <select-searchable\n      item-content\n      [(ngModel)]="selectedVehicle"\n      [items]="portstemp"\n      itemValueField="Device_Name"\n      itemTextField="Device_Name"\n      [canSearch]="true"\n      (onChange)="getid(selectedVehicle)"\n    >\n    </select-searchable>\n  </ion-item>\n\n  <ion-row style="background-color: #fafafa;" padding-left padding-right>\n    <ion-col width-20 class="cust">\n      <ion-label>\n        <span style="font-size: 13px">From Date</span>\n        <ion-datetime\n          displayFormat="DD-MM-YYYY hh:mm a"\n          pickerFormat="DD/MM/YY hh:mm a"\n          [(ngModel)]="datetimeStart"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"\n        >\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20 class="cust">\n      <ion-label>\n        <span style="font-size: 13px">To Date</span>\n        <ion-datetime\n          displayFormat="DD-MM-YYYY hh:mm a"\n          pickerFormat="DD/MM/YY hh:mm a"\n          [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"\n        >\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right">\n        <ion-icon\n          ios="ios-search"\n          md="md-search"\n          style="font-size:2.3em;"\n          (click)="getFuelList()"\n        >\n        </ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n\n<ion-content>\n  <ion-card *ngFor="let f of fuelList">\n    <ion-card-content>\n      <ion-row>\n        <ion-col col-6 style="font-size: 1.2em;" no-padding>\n          <ion-row>\n            <ion-col col-2>\n              <ion-icon name="car" color="gpsc"></ion-icon>\n            </ion-col>\n            <ion-col col-10>\n              {{ f.vehicle.Device_Name }}\n            </ion-col>\n          </ion-row>\n        </ion-col>\n        <ion-col col-6 style="text-align: right;" no-padding>\n          <ion-row>\n            <ion-col col-12>\n              <ion-badge color="gpsc" style="font-size: 1.2em;"\n                >{{ f.quantity }} liters</ion-badge\n              >\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-6 no-padding>\n          <ion-row>\n            <ion-col col-2>\n              <ion-icon style="color:gray;" name="calendar"></ion-icon>\n            </ion-col>\n            <ion-col\n              col-10\n              style="color:gray;font-size:14px;font-weight: 400;"\n              >{{ f.Date | date: "mediumDate" }}</ion-col\n            >\n          </ion-row>\n        </ion-col>\n        <ion-col col-6 no-padding>\n          <ion-row style="text-align: right;">\n            <ion-col col-12 style="color:gray;font-size:14px;font-weight: 400;">\n              <ion-icon style="color:gray;" name="cash"></ion-icon\n              >&nbsp;&nbsp;{{ f.price }} INR\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n<ion-fab right bottom>\n  <button ion-fab color="gpsc" (click)="addFuelEntry()">\n    <ion-icon name="add"></ion-icon>\n  </button>\n</ion-fab>\n'/*ion-inline-end:"D:\Pro\flex_track\src\pages\fuel\fuel-consumption\fuel-consumption.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], FuelConsumptionPage);
    return FuelConsumptionPage;
}());

var FuelEntryPage = /** @class */ (function () {
    function FuelEntryPage(navParams, apiCall, toastCtrl, event) {
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.event = event;
        this.portstemp = [];
        this.fuelType = 'CNG';
        this.liter = 0;
        this.amt = 0;
        this.ddate = new Date().toISOString();
        this.portstemp = this.navParams.get("portstemp");
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
    }
    FuelEntryPage.prototype.onChnageEvent = function (veh) {
        console.log("vehicle info:", veh);
        var that = this;
        that.veh_id = veh._id;
        var sb = veh.total_odo;
        that.tot_odo = sb.toFixed(2);
    };
    FuelEntryPage.prototype.submit = function () {
        var _this = this;
        var that = this;
        debugger;
        if (this.liter == undefined || this.amt == undefined || this.tot_odo == undefined || this.fuelType == undefined || this.ddate == undefined || this.vehName == undefined) {
            var toast = this.toastCtrl.create({
                message: "Fields should not be empty.",
                position: 'bottom',
                duration: 2000
            });
            toast.present();
        }
        else {
            var payload = {};
            var _baseURL = "https://www.oneqlik.in/fuel/addFuel";
            payload = {
                "vehicle": that.veh_id,
                "user": that.islogin._id,
                "date": new Date(that.ddate).toISOString(),
                "quantity": that.liter,
                "odometer": that.tot_odo,
                "price": that.amt,
                "fuel_type": that.fuelType,
                "comment": "First fill"
            };
            this.apiCall.startLoading().present();
            this.apiCall.urlpasseswithdata(_baseURL, payload)
                .subscribe(function (data) {
                _this.apiCall.stopLoading();
                console.log("fuel entry added: " + data);
                _this.liter = null;
                _this.amt = null;
                _this.tot_odo = null;
                _this.fuelType = null;
                _this.ddate = null;
                _this.vehName = null;
                _this.veh_id = null;
                var toast = _this.toastCtrl.create({
                    message: "Fuel entry added successfully! Add another entry..",
                    position: 'bottom',
                    duration: 2000
                });
                toast.present();
                _this.event.publish('reloadFuellist');
            });
        }
    };
    FuelEntryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\fuel\fuel-consumption\fuel-entry.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Add Fuel Entry</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-item>\n        <ion-label>Select Vehicle</ion-label>\n        <ion-select [(ngModel)]="vehName">\n            <ion-option *ngFor="let veh of portstemp" [value]="veh.Device_Name" (ionSelect)="onChnageEvent(veh)">\n                {{veh.Device_Name}}</ion-option>\n        </ion-select>\n    </ion-item>\n    <ion-item *ngIf="tot_odo != undefined">\n        <ion-label>Total Odo</ion-label>\n        <ion-input type="number" [(ngModel)]="tot_odo" readonly></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label>Fuel Type</ion-label>\n        <ion-select [(ngModel)]="fuelType">\n            <ion-option [value]="CNG" selected>CNG</ion-option>\n            <ion-option [value]="Petrol">PETROL</ion-option>\n            <ion-option [value]="Diesel">DIESEL</ion-option>\n            <ion-option [value]="Other">OTHER</ion-option>\n        </ion-select>\n    </ion-item>\n\n    <ion-item>\n        <ion-label>Liter(s)</ion-label>\n        <ion-input type="number" [(ngModel)]="liter"></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label>Amount</ion-label>\n        <ion-input type="number" [(ngModel)]="amt"></ion-input>\n    </ion-item>\n    <ion-item>\n        <ion-label>Date</ion-label>\n        <ion-datetime displayFormat="DD/MM/YYYY" pickerFormat="DD/MM/YYYY" [(ngModel)]="ddate" name="ddate">\n        </ion-datetime>\n    </ion-item>\n  </ion-list>\n</ion-content>\n\n<ion-footer class="footSty">\n    <ion-toolbar>\n        <ion-row no-padding>\n            <ion-col width-50 style="text-align: center;">\n                <button ion-button clear color="light" (click)="submit()">SUBMIT</button>\n            </ion-col>\n        </ion-row>\n    </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"D:\Pro\flex_track\src\pages\fuel\fuel-consumption\fuel-entry.html"*/,
            selector: 'page-fuel-consumption'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"]])
    ], FuelEntryPage);
    return FuelEntryPage;
}());

//# sourceMappingURL=fuel-consumption.js.map

/***/ }),

/***/ 412:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { DatePicker } from '@ionic-native/date-picker';
// import * as moment from 'moment';
var FilterPage = /** @class */ (function () {
    function FilterPage(apiCall, viewCtrl) {
        this.apiCall = apiCall;
        this.viewCtrl = viewCtrl;
        this.modal = {};
        this.filterList = [
            { filterID: 1, filterName: "Overspeed", filterValue: 'overspeed', checked: false },
            { filterID: 2, filterName: "Ignition", filterValue: 'IGN', checked: false },
            { filterID: 3, filterName: "Route", filterValue: 'Route', checked: false },
            { filterID: 4, filterName: "Geofence", filterValue: 'Geo-Fence', checked: false },
            { filterID: 5, filterName: "Stoppage", filterValue: 'MAXSTOPPAGE', checked: false },
            { filterID: 6, filterName: "Fuel", filterValue: 'Fuel', checked: false },
            { filterID: 7, filterName: "AC", filterValue: 'AC', checked: false },
            { filterID: 8, filterName: "Route POI", filterValue: 'route-poi', checked: false },
            { filterID: 9, filterName: "Power", filterValue: 'power', checked: false },
            { filterID: 10, filterName: "Immo", filterValue: 'immo', checked: false },
            { filterID: 11, filterName: "Theft", filterValue: 'theft', checked: false }
        ];
        this.selectedArray = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
        if (localStorage.getItem("checkedList") != null) {
            this.checkedData = JSON.parse(localStorage.getItem("checkedList"));
            console.log("checked data=> " + this.checkedData);
        }
    }
    FilterPage.prototype.ngOnInit = function () {
        // this.getNotifTypes()
    };
    FilterPage.prototype.getNotifTypes = function () {
        var _this = this;
        this.apiCall.startLoading();
        this.apiCall.getVehicleListCall("https://www.oneqlik.in/notifs/getTypes")
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("notif type list: ", data);
            _this.filterList = data;
        });
    };
    // dateOpen() {
    //     this.datePicker.show({
    //         date: new Date(),
    //         mode: 'date',
    //         androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    //     }).then(
    //         date => console.log('Got date: ', date),
    //         err => console.log('Error occurred while getting date: ', err)
    //     );
    // }
    // changeDTformat(fromDT) {
    //     let that = this;
    //     that.modal.fromDate = moment(new Date(fromDT), "YYYY-MM-DD").format('dd/mm/yyyy');
    //     console.log("moment=> ", that.modal.fromDate);
    // }
    FilterPage.prototype.applyFilter = function () {
        console.log(this.modal);
        console.log(this.selectedArray);
        localStorage.setItem("checkedList", JSON.stringify(this.selectedArray));
        if (this.selectedArray.length == 0) {
            localStorage.setItem("dates", "dates");
            this.viewCtrl.dismiss(this.modal);
        }
        else {
            localStorage.setItem("types", "types");
            this.viewCtrl.dismiss(this.selectedArray);
        }
    };
    FilterPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    FilterPage.prototype.selectMember = function (data) {
        console.log("selected=> " + JSON.stringify(data));
        if (data.checked == true) {
            this.selectedArray.push(data);
        }
        else {
            var newArray = this.selectedArray.filter(function (el) {
                return el.filterID !== data.filterID;
            });
            this.selectedArray = newArray;
        }
    };
    FilterPage.prototype.cancel = function () {
        console.log("do nothing");
        this.viewCtrl.dismiss();
    };
    FilterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-filter',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\all-notifications\filter\filter.html"*/'<ion-list>\n\n    <!-- <button ion-item (click)="close()">Close</button> -->\n\n    <!-- <ion-list-header style="text-align: center;">\n\n        Filter By\n\n    </ion-list-header>\n\n    <hr> -->\n\n    <ion-item-group>\n\n        <ion-item-divider color="light">Filter Type</ion-item-divider>\n\n        <div *ngIf="filterList.length > 0">\n\n            <ion-item *ngFor="let member of filterList">\n\n                <ion-label>{{member?.filterName || \'N/A\'}}</ion-label>\n\n                <ion-checkbox color="danger" (click)="selectMember(member)" [(ngModel)]="member.checked"></ion-checkbox>\n\n            </ion-item>\n\n        </div>\n\n    </ion-item-group>\n\n    <!-- <ion-item-group>\n\n        <ion-item-divider color="light">Time</ion-item-divider>\n\n        <div class="input-w">\n\n            <label for="#your-input" class="leb2">From</label>\n\n            <input type="date" id="your-input" [(ngModel)] ="modal.fromDate"/>\n\n        </div>\n\n        <div class="input-w">\n\n            <label for="#your-input" class="leb1">To</label>\n\n            <input type="date" id="your-input" [(ngModel)] ="modal.toDate"/>\n\n        </div>\n\n    </ion-item-group> -->\n\n\n\n\n\n\n\n</ion-list>\n\n<ion-row style="background-color: darkgray">\n\n    <ion-col width-50 style="text-align: center; background-color: #f53d3d;">\n\n        <button ion-button clear small color="light" (click)="cancel()">Cancel</button>\n\n    </ion-col>\n\n    <ion-col width-50 style="text-align: center; background-color: #11b700;">\n\n        <button ion-button clear small color="light" (click)="applyFilter()">Apply Filter</button>\n\n    </ion-col>\n\n</ion-row>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\all-notifications\filter\filter.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["ViewController"]])
    ], FilterPage);
    return FilterPage;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 414:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ServiceProviderPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return UpdatePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(68);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// import { TextToSpeech } from '@ionic-native/text-to-speech';
var ProfilePage = /** @class */ (function () {
    function ProfilePage(appVersion, apiCall, alertCtrl, navCtrl, navParams, events, formBuilder, storage, platform) {
        var _this = this;
        this.appVersion = appVersion;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.formBuilder = formBuilder;
        this.storage = storage;
        this.platform = platform;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> ", JSON.stringify(this.islogin));
        this.appVersion.getVersionNumber().then(function (version) {
            _this.aVer = version;
            console.log("app version=> " + _this.aVer);
        });
        this.isDealer = this.islogin.isDealer;
        console.log("isDealer=> " + this.isDealer);
        this.footerState = __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
        this.credentialsForm = this.formBuilder.group({
            fname: [this.islogin.fn],
            lname: [this.islogin.ln],
            email: [this.islogin.email],
            phonenum: [this.islogin.phn],
            org: [this.islogin._orgName],
        });
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.footerExpanded = function () {
        console.log('Footer expanded!');
    };
    ProfilePage.prototype.footerCollapsed = function () {
        console.log('Footer collapsed!');
    };
    ProfilePage.prototype.toggleFooter = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_3_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    };
    ProfilePage.prototype.settings = function () {
        this.navCtrl.push('SettingsPage');
    };
    ProfilePage.prototype.service = function () {
        this.navCtrl.push(ServiceProviderPage, {
            param: this.islogin
        });
    };
    ProfilePage.prototype.password = function () {
        this.navCtrl.push(UpdatePasswordPage, {
            param: this.islogin
        });
    };
    ProfilePage.prototype.onSignIn = function () {
        var _this = this;
        // this.logger.info('SignInPage: onSignIn()');
        console.log(this.credentialsForm.value);
        var data = {
            "fname": this.credentialsForm.value.fname,
            "lname": this.credentialsForm.value.lname,
            "org": this.credentialsForm.value.org,
            "noti": true,
            "uid": this.islogin._id
        };
        this.apiCall.startLoading().present();
        this.apiCall.updateprofile(data)
            .subscribe(function (resdata) {
            _this.apiCall.stopLoading();
            console.log("response from server=> ", resdata);
            if (resdata.token) {
                var alert_1 = _this.alertCtrl.create({
                    message: 'Profile updated succesfully!',
                    buttons: [{
                            text: 'OK',
                            handler: function () {
                                var logindata = JSON.stringify(resdata);
                                var logindetails = JSON.parse(logindata);
                                var userDetails = window.atob(logindetails.token.split('.')[1]);
                                var details = JSON.parse(userDetails);
                                console.log(details.email);
                                localStorage.setItem("loginflag", "loginflag");
                                localStorage.setItem('details', JSON.stringify(details));
                                localStorage.setItem('condition_chk', details.isDealer);
                                _this.islogin = JSON.parse(localStorage.getItem('details')) || {};
                                _this.footerState = 1;
                                _this.toggleFooter();
                            }
                        }]
                });
                alert_1.present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error from service=> ", err);
        });
    };
    ProfilePage.prototype.logout = function () {
        var _this = this;
        this.token = localStorage.getItem("DEVICE_TOKEN");
        if (this.platform.is('android')) {
            var pushdata = {
                "uid": this.islogin._id,
                "token": this.token,
                "os": "android"
            };
        }
        else {
            var pushdata = {
                "uid": this.islogin._id,
                "token": this.token,
                "os": "ios"
            };
        }
        var alert = this.alertCtrl.create({
            message: 'Do you want to logout from the application?',
            buttons: [{
                    text: 'Yes',
                    handler: function () {
                        _this.apiCall.startLoading().present();
                        _this.apiCall.pullnotifyCall(pushdata)
                            .subscribe(function (data) {
                            _this.apiCall.stopLoading();
                            console.log("push notifications updated " + data.message);
                            localStorage.clear();
                            localStorage.setItem('count', null);
                            _this.storage.clear().then(function () {
                                console.log("ionic storage cleared!");
                            });
                            _this.navCtrl.setRoot('LoginPage');
                        }, function (err) {
                            _this.apiCall.stopLoading();
                            console.log(err);
                        });
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                        // this.menuCtrl.close();
                    }
                }]
        });
        alert.present();
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\profile\profile.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Me</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button icon-only (tap)="toggleFooter()">\n\n        <ion-icon color="light" name="create" *ngIf="footerState == 0"></ion-icon>\n\n        <ion-icon color="light" name="done-all" *ngIf="footerState == 1"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-item-group>\n\n    <ion-item-divider color="light"></ion-item-divider>\n\n  </ion-item-group>\n\n  <ion-item-group>\n\n    <ion-item>\n\n      <ion-avatar item-start>\n\n        <img src="assets/imgs/dummy-user-img.png">\n\n      </ion-avatar>\n\n      <h2>{{islogin.fn}}&nbsp;{{islogin.ln}}</h2>\n\n      <p>Account:{{islogin.account}}</p>\n\n    </ion-item>\n\n  </ion-item-group>\n\n  <ion-item-group>\n\n    <ion-item-divider color="light"></ion-item-divider>\n\n  </ion-item-group>\n\n  <ion-item-group>\n\n    <!-- <ion-item (tap)="service()" *ngIf="!isDealer"> -->\n\n    <ion-item (tap)="service()">\n\n      <ion-icon name="person" item-start></ion-icon>\n\n      Service Provider\n\n    </ion-item>\n\n    <ion-item (tap)="password()">\n\n      <ion-icon name="lock" item-start></ion-icon>\n\n      Password\n\n    </ion-item>\n\n  </ion-item-group>\n\n  <ion-item-group>\n\n    <ion-item-divider color="light"></ion-item-divider>\n\n  </ion-item-group>\n\n  <ion-item-group>\n\n    <ion-item (tap)="settings()">\n\n      <ion-icon name="cog" item-start></ion-icon>\n\n      Settings\n\n    </ion-item>\n\n  </ion-item-group>\n\n  <ion-item-group>\n\n    <ion-item-divider color="light"></ion-item-divider>\n\n  </ion-item-group>\n\n  <ion-item-group (tap)="logout()">\n\n    <ion-item ion-text text-center>\n\n      Logout\n\n    </ion-item>\n\n  </ion-item-group>\n\n</ion-content>\n\n<ion-footer padding-bottom padding-top>\n\n  <div style="text-align: center;">Version {{aVer}}</div>\n\n  <br/>\n\n  <div id="photo" style="text-align: center">\n\n    <span style="vertical-align:middle">Powered by </span>&nbsp;\n\n    <a href="#">\n\n      <img style="vertical-align:middle" src="assets/imgs/sign.png" width="55" height="19">\n\n    </a>\n\n  </div>\n\n</ion-footer>\n\n\n\n<ion-pullup #pullup (onExpand)="footerExpanded()" (onCollapse)="footerCollapsed()" [(state)]="footerState">\n\n  <!-- <ion-pullup-tab [footer]="pullup" (tap)="toggleFooter()">\n\n    <ion-icon name="arrow-up" *ngIf="footerState == 0"></ion-icon><ion-icon name="arrow-down" *ngIf="footerState == 1"></ion-icon>\n\n  </ion-pullup-tab>\n\n  <ion-toolbar color="primary" (tap)="toggleFooter()">\n\n    <ion-title>Footer</ion-title>\n\n  </ion-toolbar> -->\n\n  <ion-content>\n\n    <ion-row padding>\n\n      <ion-col style="text-align: center; font-size: 15px">Update Profile</ion-col>\n\n    </ion-row>\n\n    <form [formGroup]="credentialsForm">\n\n      <ion-item>\n\n        <ion-label floating>First Name</ion-label>\n\n        <ion-input [formControl]="credentialsForm.controls[\'fname\']" type="text"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label floating>Last Name</ion-label>\n\n        <ion-input [formControl]="credentialsForm.controls[\'lname\']" type="text"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label floating>Email Id</ion-label>\n\n        <ion-input [formControl]="credentialsForm.controls[\'email\']" type="email"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label floating>Phone Number</ion-label>\n\n        <ion-input [formControl]="credentialsForm.controls[\'phonenum\']" type="number"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label floating>Organisation</ion-label>\n\n        <ion-input [formControl]="credentialsForm.controls[\'org\']" type="text"></ion-input>\n\n      </ion-item>\n\n      <!-- <ion-item>\n\n        <ion-label>Timezone</ion-label>\n\n        <ion-select interface="popover">\n\n          <ion-option value="f">Female</ion-option>\n\n          <ion-option value="m">Male</ion-option>\n\n        </ion-select>\n\n      </ion-item> -->\n\n      <!-- <ion-item no-lines>\n\n        <ion-label>Dealer</ion-label>\n\n        <ion-checkbox item-right color="green" checked="true" *ngIf="islogin.isDealer"></ion-checkbox>\n\n        <ion-checkbox item-right color="gpsc" checked="true" *ngIf="!islogin.isDealer"></ion-checkbox>\n\n      </ion-item> -->\n\n\n\n      <ion-row>\n\n        <ion-col text-center>\n\n          <button ion-button block color="gpsc" (click)="onSignIn()">\n\n            Update Account\n\n          </button>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n    </form>\n\n  </ion-content>\n\n</ion-pullup>\n\n'/*ion-inline-end:"D:\Pro\flex_track\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_app_version__["a" /* AppVersion */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"]])
    ], ProfilePage);
    return ProfilePage;
}());

var ServiceProviderPage = /** @class */ (function () {
    function ServiceProviderPage(navParam) {
        this.navParam = navParam;
        this.uData = {};
        this.sorted = [];
        this.uData = this.navParam.get("param");
        if (this.uData.Dealer_ID != undefined) {
            this.sorted = this.uData.Dealer_ID;
        }
        else {
            this.sorted.first_name = this.uData.fn;
            this.sorted.last_name = this.uData.ln;
            this.sorted.phone = this.uData.phn;
            this.sorted.email = this.uData.email;
        }
        console.log("udata=> ", this.uData);
        console.log("udata=> ", JSON.stringify(this.uData));
    }
    ServiceProviderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\profile\service-provider.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Provider Info</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n<ion-content>\n\n    <div class="div">\n\n        <img src="assets/imgs/dummy_user.jpg">\n\n    </div>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Name</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.first_name}}&nbsp;{{sorted.last_name}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Contacts</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.phone}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Mobile</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.phone}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>E-mail</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">{{sorted.email}}</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n    <ion-row padding-left padding-right style="padding-bottom: 6px; padding-top: 6px">\n\n        <ion-col>Address</ion-col>\n\n        <ion-col>\n\n            <p style="float: right; margin:0px; padding:0px">N/A</p>\n\n        </ion-col>\n\n    </ion-row>\n\n    <hr>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\profile\service-provider.html"*/,
            selector: 'page-profile'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ServiceProviderPage);
    return ServiceProviderPage;
}());

var UpdatePasswordPage = /** @class */ (function () {
    function UpdatePasswordPage(navParam, alertCtrl, toastCtrl, apiSrv, navCtrl) {
        this.navParam = navParam;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.apiSrv = apiSrv;
        this.navCtrl = navCtrl;
        this.passData = this.navParam.get("param");
        console.log("passData=> " + JSON.stringify(this.passData));
    }
    UpdatePasswordPage.prototype.savePass = function () {
        var _this = this;
        if (this.oldP == undefined || this.newP == undefined || this.cnewP == undefined) {
            var alert_2 = this.alertCtrl.create({
                message: 'Fields should not be empty!',
                buttons: ['OK']
            });
            alert_2.present();
        }
        else {
            if (this.newP != this.cnewP) {
                var alert_3 = this.alertCtrl.create({
                    message: 'Password Missmatched!!',
                    buttons: ['Try Again']
                });
                alert_3.present();
            }
            else {
                var data = {
                    "ID": this.passData._id,
                    "OLD_PASS": this.oldP,
                    "NEW_PASS": this.newP
                };
                this.apiSrv.startLoading().present();
                this.apiSrv.updatePassword(data)
                    .subscribe(function (respData) {
                    _this.apiSrv.stopLoading();
                    console.log("respData=> ", respData);
                    var toast = _this.toastCtrl.create({
                        message: 'Password Updated successfully',
                        position: "bottom",
                        duration: 2000
                    });
                    toast.onDidDismiss(function () {
                        _this.oldP = "";
                        _this.newP = "";
                        _this.cnewP = "";
                    });
                    toast.present();
                }, function (err) {
                    _this.apiSrv.stopLoading();
                    console.log("error in update password=> ", err);
                    // debugger
                    if (err.message == "Timeout has occurred") {
                        // alert("the server is taking much time to respond. Please try in some time.")
                        var alerttemp = _this.alertCtrl.create({
                            message: "the server is taking much time to respond. Please try in some time.",
                            buttons: [{
                                    text: 'Okay',
                                    handler: function () {
                                        _this.navCtrl.setRoot("DashboardPage");
                                    }
                                }]
                        });
                        alerttemp.present();
                    }
                    else {
                        var toast = _this.toastCtrl.create({
                            message: err._body.message,
                            position: "bottom",
                            duration: 2000
                        });
                        toast.onDidDismiss(function () {
                            _this.oldP = "";
                            _this.newP = "";
                            _this.cnewP = "";
                            _this.navCtrl.setRoot("DashboardPage");
                        });
                        toast.present();
                    }
                });
            }
        }
    };
    UpdatePasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-profile',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\profile\update-password.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>Change Password</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button icon-only (click)="savePass()">\n\n                Save\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <ion-list>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="Old Password" [(ngModel)]="oldP"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="New Password" [(ngModel)]="newP"></ion-input>\n\n        </ion-item>\n\n        <ion-item>\n\n            <ion-icon class="ic" name="lock" item-start></ion-icon>\n\n            <ion-input placeholder="Confirm New Password" [(ngModel)]="cnewP"></ion-input>\n\n        </ion-item>\n\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\profile\update-password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], UpdatePasswordPage);
    return UpdatePasswordPage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 415:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalPage = /** @class */ (function () {
    function ModalPage(
        // private navParams: NavParams,
        viewCtrl) {
        this.viewCtrl = viewCtrl;
        this.min_time = "10";
    }
    ModalPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(this.min_time);
    };
    ModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            // selector: 'page-modal',
            template: "\n        <div class=\"mainDiv\">\n            <div class=\"secondDiv\">\n                <ion-list radio-group [(ngModel)]=\"min_time\">\n                    <ion-item>\n                        <ion-label>10 Mins</ion-label>\n                        <ion-radio value=\"10\" checked></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>20 Mins</ion-label>\n                        <ion-radio value=\"20\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>30 Mins</ion-label>\n                        <ion-radio value=\"30\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>40 Mins</ion-label>\n                        <ion-radio value=\"40\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>50 Mins</ion-label>\n                        <ion-radio value=\"50\"></ion-radio>\n                    </ion-item>\n                    <ion-item>\n                        <ion-label>60 Mins</ion-label>\n                        <ion-radio value=\"60\"></ion-radio>\n                    </ion-item>\n                    \n                </ion-list>\n                <div style=\"padding: 5px;\"><button ion-button (click)=\"dismiss()\" color=\"gpsc\" block>Submit</button></div>\n            </div>\n        </div>\n    ",
            styles: ["\n      \n            .mainDiv {\n                padding: 50px;\n                height: 100%;\n                background-color: rgba(0, 0, 0, 0.7);\n            }\n\n            .secondDiv {\n                padding-top: 20px;\n                border-radius: 18px;\n                border: 2px solid black;\n                background: white;\n            }\n        \n    "]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], ModalPage);
    return ModalPage;
}());

//# sourceMappingURL=modal.js.map

/***/ }),

/***/ 417:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDevicesPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_sms__ = __webpack_require__(418);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// declare var google;
var AddDevicesPage = /** @class */ (function () {
    function AddDevicesPage(navCtrl, navParams, apiCall, alertCtrl, toastCtrl, sms, modalCtrl, popoverCtrl, geocoderApi) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.sms = sms;
        this.modalCtrl = modalCtrl;
        this.popoverCtrl = popoverCtrl;
        this.geocoderApi = geocoderApi;
        this.allDevices = [];
        this.allDevicesSearch = [];
        this.option_switch = false;
        this.searchCountryString = ''; // initialize your searchCountryString string empty
        this.page = 0;
        this.limit = 5;
        this.searchQuery = '';
        this.clicked = false;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.isDealer = this.islogin.isDealer;
        console.log("isDealer=> " + this.isDealer);
        this.isSuperAdmin = this.islogin.isSuperAdmin;
        this.islogindealer = localStorage.getItem('isDealervalue');
        console.log("islogindealer=> " + this.islogindealer);
        if (navParams.get("label") && navParams.get("value")) {
            this.stausdevice = localStorage.getItem('status');
        }
        else {
            // localStorage.removeItem("status");
            this.stausdevice = undefined;
        }
    }
    AddDevicesPage.prototype.fonctionTest = function (d) {
        var _this = this;
        console.log("clicked: " + JSON.stringify(d));
        var theft;
        theft = !(d.theftAlert);
        if (theft) {
            var alert_1 = this.alertCtrl.create({
                title: "Confirm",
                message: "Are you sure you want to activate anti theft alarm? On activating this alert you will get receive notification if vehicle moves.",
                buttons: [
                    {
                        text: 'YES PROCEED',
                        handler: function () {
                            // theft = !(d.theftAlert);
                            // theft = d.theftAlert;
                            var payload = {
                                "_id": d._id,
                                "theftAlert": theft
                            };
                            _this.apiCall.startLoading();
                            _this.apiCall.deviceupdateCall(payload)
                                .subscribe(function (data) {
                                _this.apiCall.stopLoading();
                                console.log("resp: ", data);
                                var toast = _this.toastCtrl.create({
                                    message: "Anti theft alarm Activated!",
                                    position: "bottom",
                                    duration: 1000
                                });
                                toast.present();
                                _this.getdevices();
                            });
                        }
                    },
                    {
                        text: 'BACK',
                        handler: function () {
                            d.theftAlert = !(d.theftAlert);
                        }
                    }
                ]
            });
            alert_1.present();
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: "Confirm",
                message: "Are you sure you want to deactivate anti theft alarm?",
                buttons: [
                    {
                        text: 'YES PROCEED',
                        handler: function () {
                            // theft = d.theftAlert;
                            var payload = {
                                "_id": d._id,
                                "theftAlert": theft
                            };
                            _this.apiCall.startLoading();
                            _this.apiCall.deviceupdateCall(payload)
                                .subscribe(function (data) {
                                _this.apiCall.stopLoading();
                                console.log("resp: ", data);
                                var toast = _this.toastCtrl.create({
                                    message: "Anti theft alarm Deactivated!",
                                    position: "bottom",
                                    duration: 1000
                                });
                                toast.present();
                                _this.getdevices();
                            });
                        }
                    },
                    {
                        text: 'BACK',
                        handler: function () {
                            d.theftAlert = !(d.theftAlert);
                        }
                    }
                ]
            });
            alert_2.present();
        }
    };
    AddDevicesPage.prototype.ngOnInit = function () {
        this.now = new Date().toISOString();
    };
    AddDevicesPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        if (localStorage.getItem("SCREEN") != null) {
            this.navBar.backButtonClick = function (e) {
                if (localStorage.getItem("SCREEN") != null) {
                    if (localStorage.getItem("SCREEN") === 'live') {
                        _this.navCtrl.setRoot('LivePage');
                    }
                    else {
                        if (localStorage.getItem("SCREEN") === 'dashboard') {
                            _this.navCtrl.setRoot('DashboardPage');
                        }
                    }
                }
            };
        }
        this.getdevices();
        console.log(this.isDealer);
    };
    AddDevicesPage.prototype.activateVehicle = function (data) {
        this.navCtrl.push("PaytmwalletloginPage", {
            "param": data
        });
    };
    AddDevicesPage.prototype.timeoutAlert = function () {
        var _this = this;
        var alerttemp = this.alertCtrl.create({
            message: "the server is taking much time to respond. Please try in some time.",
            buttons: [{
                    text: 'Okay',
                    handler: function () {
                        _this.navCtrl.setRoot("DashboardPage");
                    }
                }]
        });
        alerttemp.present();
    };
    AddDevicesPage.prototype.showVehicleDetails = function (vdata) {
        this.navCtrl.push('VehicleDetailsPage', {
            param: vdata,
            option_switch: this.option_switch
        });
    };
    AddDevicesPage.prototype.doRefresh = function (refresher) {
        var that = this;
        that.page = 0;
        that.limit = 5;
        console.log('Begin async operation', refresher);
        this.getdevices();
        refresher.complete();
    };
    AddDevicesPage.prototype.shareVehicle = function (d_data) {
        var that = this;
        var prompt = this.alertCtrl.create({
            title: 'Share Vehicle',
            inputs: [
                {
                    name: 'device_name',
                    value: d_data.Device_Name
                },
                {
                    name: 'shareId',
                    placeholder: 'Enter Email Id/Mobile Number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Share',
                    handler: function (data) {
                        that.sharedevices(data, d_data);
                    }
                }
            ]
        });
        prompt.present();
    };
    AddDevicesPage.prototype.sharedevices = function (data, d_data) {
        var _this = this;
        var that = this;
        var devicedetails = {
            "did": d_data._id,
            "email": data.shareId,
            "uid": that.islogin._id
        };
        that.apiCall.startLoading().present();
        that.apiCall.deviceShareCall(devicedetails)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            var toast = that.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 2000
            });
            toast.present();
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    AddDevicesPage.prototype.showDeleteBtn = function (b) {
        // debugger;
        var that = this;
        if (localStorage.getItem('isDealervalue') == 'true') {
            return false;
        }
        else {
            if (b) {
                var u = b.split(",");
                for (var p = 0; p < u.length; p++) {
                    if (that.islogin._id == u[p]) {
                        return true;
                    }
                }
            }
            else {
                return false;
            }
        }
    };
    AddDevicesPage.prototype.sharedVehicleDelete = function (device) {
        var that = this;
        that.deivceId = device;
        var alert = that.alertCtrl.create({
            message: 'Do you want to delete this share vehicle ?',
            buttons: [{
                    text: 'YES PROCEED',
                    handler: function () {
                        that.removeDevice(that.deivceId._id);
                    }
                },
                {
                    text: 'NO'
                }]
        });
        alert.present();
    };
    AddDevicesPage.prototype.removeDevice = function (did) {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.dataRemoveFuncCall(this.islogin._id, did)
            .subscribe(function (data) {
            console.log(data);
            _this.apiCall.stopLoading();
            var toast = _this.toastCtrl.create({
                message: 'Shared Device was deleted successfully!',
                duration: 1500
            });
            toast.onDidDismiss(function () {
                _this.getdevices();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    AddDevicesPage.prototype.showSharedBtn = function (a, b) {
        // debugger
        if (b) {
            return !(b.split(",").indexOf(a) + 1);
        }
        else {
            return true;
        }
    };
    AddDevicesPage.prototype.presentPopover = function (ev, data) {
        var _this = this;
        console.log("populated=> " + JSON.stringify(data));
        var popover = this.popoverCtrl.create(PopoverPage, {
            vehData: data
        }, {
            cssClass: 'contact-popover'
        });
        popover.onDidDismiss(function () {
            _this.getdevices();
        });
        popover.present({
            ev: ev
        });
    };
    AddDevicesPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var that = this;
        that.page = that.page + 1;
        setTimeout(function () {
            var baseURLp;
            if (that.stausdevice) {
                baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevice + '&skip=' + that.page + '&limit=' + that.limit;
            }
            else {
                baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit;
            }
            if (_this.islogin.isSuperAdmin == true) {
                baseURLp += '&supAdmin=' + _this.islogin._id;
            }
            else {
                if (_this.islogin.isDealer == true) {
                    baseURLp += '&dealer=' + _this.islogin._id;
                }
            }
            that.ndata = [];
            that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
                .subscribe(function (res) {
                that.ndata = res.devices;
                for (var i = 0; i < that.ndata.length; i++) {
                    that.allDevices.push(that.ndata[i]);
                }
                that.allDevicesSearch = that.allDevices;
            }, function (error) {
                console.log(error);
            });
            infiniteScroll.complete();
        }, 500);
    };
    AddDevicesPage.prototype.ionViewWillEnter = function () {
        this.getdevicesTemp();
    };
    AddDevicesPage.prototype.getdevicesTemp = function () {
        var _this = this;
        var baseURLp;
        if (this.stausdevice) {
            baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
        }
        else {
            baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
        }
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        // this.apiCall.toastMsgStarted();
        // this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            // this.apiCall.toastMsgDismised();
            _this.ndata = data.devices;
            _this.allDevices = _this.ndata;
            _this.allDevicesSearch = _this.ndata;
            var that = _this;
            that.userPermission = JSON.parse(localStorage.getItem('details'));
            if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
                that.option_switch = true;
            }
            else {
                if (localStorage.getItem('isDealervalue') == 'true') {
                    that.option_switch = true;
                }
                else {
                    if (that.userPermission.isDealer == false) {
                        that.option_switch = false;
                    }
                }
            }
        }, function (err) {
            console.log("error=> ", err);
            // this.apiCall.stopLoading();
            // this.apiCall.toastMsgDismised();
        });
    };
    AddDevicesPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp;
        if (this.stausdevice) {
            baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice + '&skip=' + this.page + '&limit=' + this.limit;
        }
        else {
            baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&skip=' + this.page + '&limit=' + this.limit;
        }
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.ndata = data.devices;
            _this.allDevices = _this.ndata;
            _this.allDevicesSearch = _this.ndata;
            var that = _this;
            that.userPermission = JSON.parse(localStorage.getItem('details'));
            if (that.userPermission.isDealer == true || that.userPermission.isSuperAdmin == true) {
                that.option_switch = true;
            }
            else {
                if (localStorage.getItem('isDealervalue') == 'true') {
                    that.option_switch = true;
                }
                else {
                    if (that.userPermission.isDealer == false) {
                        that.option_switch = false;
                    }
                }
            }
            // if (localStorage.getItem("INTRO") != null)
            // this.intro();
            ///////////////////////////////
        }, function (err) {
            console.log("error=> ", err);
            _this.apiCall.stopLoading();
            // if (err.message == "Timeout has occurred") {
            //   this.timeoutAlert();
            // }
        });
    };
    AddDevicesPage.prototype.livetrack = function (device) {
        localStorage.setItem("LiveDevice", "LiveDevice");
        this.navCtrl.push('LiveSingleDevice', {
            device: device
        });
    };
    AddDevicesPage.prototype.showHistoryDetail = function (device) {
        this.navCtrl.push('HistoryDevicePage', {
            device: device
        });
    };
    AddDevicesPage.prototype.device_address = function (device, index) {
        var that = this;
        that.allDevices[index].address = "N/A";
        if (!device.last_location) {
            that.allDevices[index].address = "N/A";
        }
        else if (device.last_location) {
            this.geocoderApi.reverseGeocode(Number(device.last_location.lat), Number(device.last_location.long))
                .then(function (res) {
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.allDevices[index].address = str;
            });
        }
    };
    AddDevicesPage.prototype.callSearch = function (ev) {
        var _this = this;
        console.log(ev.target.value);
        var searchKey = ev.target.value;
        var _baseURL;
        if (this.islogin.isDealer == true) {
            _baseURL = "https://www.oneqlik.in/devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&dealer=" + this.islogin._id;
        }
        else {
            if (this.islogin.isSuperAdmin == true) {
                _baseURL = "https://www.oneqlik.in/devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&supAdmin=" + this.islogin._id;
            }
            else {
                _baseURL = "https://www.oneqlik.in/devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey;
            }
        }
        // this.apiCall.startLoading().present();
        // this.apiCall.callSearchService(this.islogin.email, this.islogin._id, searchKey)
        this.apiCall.callSearchService(_baseURL)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            console.log("search result=> " + JSON.stringify(data));
            _this.allDevicesSearch = data.devices;
            _this.allDevices = data.devices;
        }, function (err) {
            console.log(err);
            // this.apiCall.stopLoading();
        });
    };
    AddDevicesPage.prototype.checkImmobilizePassword = function () {
        var _this = this;
        this.checkedPass = undefined;
        var rurl = this.apiCall.mainUrl + 'users/get_user_setting';
        var Var = { uid: this.islogin._id };
        this.apiCall.urlpasseswithdata(rurl, Var)
            .subscribe(function (data) {
            if (!data.engine_cut_psd) {
                _this.checkedPass = 'PASSWORD_NOT_SET';
            }
            else {
                _this.checkedPass = 'PASSWORD_SET';
            }
        });
    };
    AddDevicesPage.prototype.IgnitionOnOff = function (d) {
        var _this = this;
        var that = this;
        if (d.last_ACC != null || d.last_ACC != undefined) {
            if (that.clicked) {
                var alert_3 = this.alertCtrl.create({
                    message: 'Process ongoing..',
                    buttons: ['Okay']
                });
                alert_3.present();
            }
            else {
                this.checkImmobilizePassword();
                this.messages = undefined;
                this.dataEngine = d;
                var baseURLp = this.apiCall.mainUrl + 'deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
                this.apiCall.startLoading().present();
                this.apiCall.ignitionoffCall(baseURLp)
                    .subscribe(function (data) {
                    _this.apiCall.stopLoading();
                    _this.DeviceConfigStatus = data;
                    _this.immobType = data[0].imobliser_type;
                    if (_this.dataEngine.ignitionLock == '1') {
                        _this.messages = 'Do you want to unlock the engine?';
                    }
                    else {
                        if (_this.dataEngine.ignitionLock == '0') {
                            _this.messages = 'Do you want to lock the engine?';
                        }
                    }
                    var alert = _this.alertCtrl.create({
                        message: _this.messages,
                        buttons: [{
                                text: 'YES',
                                handler: function () {
                                    if (_this.immobType == 0 || _this.immobType == undefined) {
                                        that.clicked = true;
                                        var devicedetail = {
                                            "_id": _this.dataEngine._id,
                                            "engine_status": !_this.dataEngine.engine_status
                                        };
                                        // this.apiCall.startLoading().present();
                                        _this.apiCall.deviceupdateCall(devicedetail)
                                            .subscribe(function (response) {
                                            // this.apiCall.stopLoading();
                                            _this.editdata = response;
                                            var toast = _this.toastCtrl.create({
                                                message: response.message,
                                                duration: 2000,
                                                position: 'top'
                                            });
                                            toast.present();
                                            // this.responseMessage = "Edit successfully";
                                            _this.getdevices();
                                            var msg;
                                            if (!_this.dataEngine.engine_status) {
                                                msg = _this.DeviceConfigStatus[0].resume_command;
                                            }
                                            else {
                                                msg = _this.DeviceConfigStatus[0].immoblizer_command;
                                            }
                                            _this.sms.send(d.sim_number, msg);
                                            var toast1 = _this.toastCtrl.create({
                                                message: 'SMS sent successfully',
                                                duration: 2000,
                                                position: 'bottom'
                                            });
                                            toast1.present();
                                            that.clicked = false;
                                        }, function (error) {
                                            that.clicked = false;
                                            // this.apiCall.stopLoading();
                                            console.log(error);
                                        });
                                    }
                                    else {
                                        console.log("Call server code here!!");
                                        if (that.checkedPass === 'PASSWORD_SET') {
                                            _this.askForPassword(d);
                                            return;
                                        }
                                        that.serverLevelOnOff(d);
                                    }
                                }
                            },
                            {
                                text: 'NO'
                            }]
                    });
                    alert.present();
                }, function (error) {
                    _this.apiCall.stopLoading();
                    console.log("some error: ", error._body.message);
                });
            }
        }
    };
    ;
    AddDevicesPage.prototype.askForPassword = function (d) {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Enter Password',
            message: "Enter password for engine cut",
            inputs: [
                {
                    name: 'password',
                    placeholder: 'Password'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Proceed',
                    handler: function (data) {
                        console.log('Saved clicked');
                        console.log("data: ", data);
                        // if (data.password !== data.cpassword) {
                        //   this.toastmsg("Entered password and confirm password did not match.")
                        //   return;
                        // } 
                        _this.verifyPassword(data, d);
                    }
                }
            ]
        });
        prompt.present();
    };
    AddDevicesPage.prototype.verifyPassword = function (pass, d) {
        var _this = this;
        var ryurl = this.apiCall.mainUrl + "users/verify_EngineCut_Password";
        var payLd = {
            "uid": this.islogin._id,
            "psd": pass.password
        };
        this.apiCall.startLoading().present();
        this.apiCall.urlpasseswithdata(ryurl, payLd)
            .subscribe(function (resp) {
            _this.apiCall.stopLoading();
            console.log(resp);
            if (resp.message === 'password not matched') {
                _this.toastmsg(resp.message);
                return;
            }
            _this.serverLevelOnOff(d);
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    AddDevicesPage.prototype.toastmsg = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 1500,
            position: 'bottom'
        }).present();
    };
    AddDevicesPage.prototype.serverLevelOnOff = function (d) {
        var _this = this;
        var that = this;
        that.clicked = true;
        var data = {
            "imei": d.Device_ID,
            "_id": this.dataEngine._id,
            "engine_status": d.ignitionLock,
            "protocol_type": d.device_model.device_type
        };
        // this.apiCall.startLoading().present();
        this.apiCall.serverLevelonoff(data)
            .subscribe(function (resp) {
            // this.apiCall.stopLoading();
            console.log("ignition on off=> ", resp);
            _this.respMsg = resp;
            // this.apiCall.startLoadingnew(this.dataEngine.ignitionLock).present();
            _this.intervalID = setInterval(function () {
                _this.apiCall.callResponse(_this.respMsg._id)
                    .subscribe(function (data) {
                    console.log("interval=> " + data);
                    _this.commandStatus = data.status;
                    if (_this.commandStatus == 'SUCCESS') {
                        clearInterval(_this.intervalID);
                        that.clicked = false;
                        // this.apiCall.stopLoadingnw();
                        var toast1 = _this.toastCtrl.create({
                            message: 'process has been completed successfully!',
                            duration: 1500,
                            position: 'bottom'
                        });
                        toast1.present();
                        _this.getdevices();
                    }
                });
            }, 5000);
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log("error in onoff=>", err);
            that.clicked = false;
        });
    };
    // IgnitionOnOff(d) {
    //   let that = this;
    //   debugger
    //   if (d.last_ACC != null || d.last_ACC != undefined) {
    //     if (that.clicked) {
    //       let alert = this.alertCtrl.create({
    //         message: 'Process ongoing..',
    //         buttons: ['Okay']
    //       });
    //       alert.present();
    //     } else {
    //       this.messages = undefined;
    //       this.dataEngine = d;
    //       var baseURLp = 'https://www.oneqlik.in/deviceModel/getDevModelByName?type=' + this.dataEngine.device_model.device_type;
    //       this.apiCall.startLoading().present();
    //       this.apiCall.ignitionoffCall(baseURLp)
    //         .subscribe(data => {
    //           this.apiCall.stopLoading();
    //           this.DeviceConfigStatus = data;
    //           this.immobType = data[0].imobliser_type;
    //           if (this.dataEngine.ignitionLock == '1') {
    //             this.messages = 'Do you want to unlock the engine?'
    //           } else {
    //             if (this.dataEngine.ignitionLock == '0') {
    //               this.messages = 'Do you want to lock the engine?'
    //             }
    //           }
    //           let alert = this.alertCtrl.create({
    //             message: this.messages,
    //             buttons: [{
    //               text: 'YES',
    //               handler: () => {
    //                 if (this.immobType == 0 || this.immobType == undefined) {
    //                   that.clicked = true;
    //                   var devicedetail = {
    //                     "_id": this.dataEngine._id,
    //                     "engine_status": !this.dataEngine.engine_status
    //                   }
    //                   // this.apiCall.startLoading().present();
    //                   this.apiCall.deviceupdateCall(devicedetail)
    //                     .subscribe(response => {
    //                       // this.apiCall.stopLoading();
    //                       this.editdata = response;
    //                       const toast = this.toastCtrl.create({
    //                         message: response.message,
    //                         duration: 2000,
    //                         position: 'top'
    //                       });
    //                       toast.present();
    //                       this.responseMessage = "Edit successfully";
    //                       this.getdevices();
    //                       var msg;
    //                       if (!this.dataEngine.engine_status) {
    //                         msg = this.DeviceConfigStatus[0].resume_command;
    //                       }
    //                       else {
    //                         msg = this.DeviceConfigStatus[0].immoblizer_command;
    //                       }
    //                       this.sms.send(d.sim_number, msg);
    //                       const toast1 = this.toastCtrl.create({
    //                         message: 'SMS sent successfully',
    //                         duration: 2000,
    //                         position: 'bottom'
    //                       });
    //                       toast1.present();
    //                       that.clicked = false;
    //                     },
    //                       error => {
    //                         that.clicked = false;
    //                         // this.apiCall.stopLoading();
    //                         console.log(error);
    //                       });
    //                 } else {
    //                   console.log("Call server code here!!")
    //                   that.clicked = true;
    //                   var data = {
    //                     "imei": d.Device_ID,
    //                     "_id": this.dataEngine._id,
    //                     "engine_status": d.ignitionLock,
    //                     "protocol_type": d.device_model.device_type
    //                   }
    //                   // this.apiCall.startLoading().present();
    //                   this.apiCall.serverLevelonoff(data)
    //                     .subscribe(resp => {
    //                       // this.apiCall.stopLoading();
    //                       console.log("ignition on off=> ", resp)
    //                       this.respMsg = resp;
    //                       // this.apiCall.startLoadingnew(this.dataEngine.ignitionLock).present();
    //                       this.intervalID = setInterval(() => {
    //                         this.apiCall.callResponse(this.respMsg._id)
    //                           .subscribe(data => {
    //                             console.log("interval=> " + data)
    //                             this.commandStatus = data.status;
    //                             if (this.commandStatus == 'SUCCESS') {
    //                               clearInterval(this.intervalID);
    //                               that.clicked = false;
    //                               // this.apiCall.stopLoadingnw();
    //                               const toast1 = this.toastCtrl.create({
    //                                 message: 'process has completed successfully!',
    //                                 duration: 1500,
    //                                 position: 'bottom'
    //                               });
    //                               toast1.present();
    //                               this.getdevices();
    //                             }
    //                           })
    //                       }, 5000);
    //                     },
    //                       err => {
    //                         this.apiCall.stopLoading();
    //                         console.log("error in onoff=>", err);
    //                         that.clicked = false;
    //                       });
    //                 }
    //               }
    //             },
    //             {
    //               text: 'No'
    //             }]
    //           });
    //           alert.present();
    //         },
    //           error => {
    //             this.apiCall.stopLoading();
    //             console.log("some error: ", error._body.message);
    //           });
    //     }
    //   }
    // };
    AddDevicesPage.prototype.dialNumber = function (number) {
        window.open('tel:' + number, '_system');
    };
    AddDevicesPage.prototype.getItems = function (ev) {
        console.log(ev.target.value, this.allDevices);
        var val = ev.target.value.trim();
        this.allDevicesSearch = this.allDevices.filter(function (item) {
            return (item.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
        console.log("search====", this.allDevicesSearch);
    };
    AddDevicesPage.prototype.onClear = function (ev) {
        // debugger;
        this.getdevices();
        ev.target.value = '';
    };
    AddDevicesPage.prototype.openAdddeviceModal = function () {
        var _this = this;
        var profileModal = this.modalCtrl.create('AddDeviceModalPage');
        profileModal.onDidDismiss(function (data) {
            console.log(data);
            _this.getdevices();
        });
        profileModal.present();
    };
    AddDevicesPage.prototype.upload = function (vehData) {
        // let mod = this.modalCtrl.create('UploadDocPage',
        //   { vehData: vehData });
        // mod.present();
        // mod.onDidDismiss(() => {
        //   // this.viewCtrl.dismiss();
        // })
        this.navCtrl.push('UploadDocPage', { vehData: vehData });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChildren"])("step"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["QueryList"])
    ], AddDevicesPage.prototype, "steps", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverContent', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AddDevicesPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverText', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], AddDevicesPage.prototype, "text", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], AddDevicesPage.prototype, "navBar", void 0);
    AddDevicesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-devices',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\add-devices\add-devices.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Vehicle List</ion-title>\n\n    <ion-buttons end *ngIf="isDealer || isSuperAdmin">\n\n      <button ion-button icon-only (click)="openAdddeviceModal()">\n\n        <ion-icon name="add"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n  <ion-searchbar (ionInput)="callSearch($event)" (ionClear)="onClear($event)"></ion-searchbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles"\n\n      refreshingText="Refreshing...">\n\n    </ion-refresher-content>\n\n  </ion-refresher>\n\n\n\n  <ion-card *ngFor="let d of allDevicesSearch; let i=index;">\n\n    <div *ngIf="d.expiration_date < now " style="position: relative">\n\n      <div>\n\n        <ion-item style="background-color: rgba(0, 0, 0, 0.9)">\n\n          <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType != null">\n\n            <img src="assets/imgs/car_grey_icon.webp" title="{{d.Device_Name}}"\n\n              *ngIf="(d.vehicleType.iconType == \'car\')">\n\n            <img src="assets/imgs/truck_icon_grey.webp" title="{{d.Device_Name}}"\n\n              *ngIf="(d.vehicleType.iconType == \'truck\')">\n\n            <img src="assets/imgs/bike_grey_icon.webp" title="{{d.Device_Name}}"\n\n              *ngIf="(d.vehicleType.iconType == \'bike\')">\n\n            <img src="assets/imgs/jcb_gray.webp" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'jcb\')">\n\n            <img src="assets/imgs/bus_gray.webp" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'bus\')">\n\n            <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'user\')">\n\n            <img src="assets/imgs/tractor_gray.webp" title="{{d.Device_Name}}"\n\n              *ngIf="(d.vehicleType.iconType == \'tractor\')">\n\n          </ion-avatar>\n\n\n\n          <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType == null">\n\n            <img src="assets/imgs/car_grey_icon.webp" title="{{d.Device_Name}}"\n\n              *ngIf="(d.vehicleType.iconType == \'car\')">\n\n            <img src="assets/imgs/truck_icon_grey.webp" title="{{d.Device_Name}}"\n\n              *ngIf="(d.vehicleType.iconType == \'truck\')">\n\n            <img src="assets/imgs/bike_grey_icon.webp" title="{{d.Device_Name}}"\n\n              *ngIf="(d.vehicleType.iconType == \'bike\')">\n\n            <img src="assets/imgs/jcb_gray.webp" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'jcb\')">\n\n            <img src="assets/imgs/bus_gray.webp" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'bus\')">\n\n            <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}" *ngIf="(d.vehicleType.iconType == \'user\')">\n\n            <img src="assets/imgs/tractor_gray.webp" title="{{d.Device_Name}}"\n\n              *ngIf="(d.vehicleType.iconType == \'tractor\')">\n\n          </ion-avatar>\n\n\n\n          <ion-avatar item-start *ngIf="d.vehicleType == null && d.iconType != null">\n\n            <img src="assets/imgs/car_grey_icon.webp" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'car\')">\n\n            <img src="assets/imgs/truck_icon_grey.webp" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'truck\')">\n\n            <img src="assets/imgs/bike_grey_icon.webp" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'bike\')">\n\n            <img src="assets/imgs/jcb_gray.webp" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'jcb\')">\n\n            <img src="assets/imgs/bus_gray.webp" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'bus\')">\n\n            <img src="assets/imgs/user-dummy.png" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'user\')">\n\n            <img src="assets/imgs/tractor_gray.webp" title="{{d.Device_Name}}" *ngIf="(d.iconType == \'tractor\')">\n\n          </ion-avatar>\n\n\n\n          <div>\n\n            <h2 style="color: gray">{{d.Device_Name}}</h2>\n\n            <p style="color:gray;font-size: 10px;font-weight: 400;margin:0px;" ion-text text-wrap>\n\n              <span style="text-transform: uppercase; color: red;">{{d.status}} </span>\n\n              <span *ngIf="d.status_updated_at ">since&nbsp;{{d.status_updated_at | date:\'medium\'}} </span>\n\n            </p>\n\n          </div>\n\n\n\n          <button ion-button item-end clear *ngIf="!showDeleteBtn(d.SharedWith)">\n\n            <ion-icon ios="ios-more" md="md-more" color="grey" *ngIf="option_switch"></ion-icon>\n\n          </button>\n\n\n\n          <button ion-button item-end clear *ngIf="!option_switch">\n\n            <ion-icon ios="ios-share" md="md-share" *ngIf="showSharedBtn(islogin._id,d.SharedWith)"></ion-icon>\n\n          </button>\n\n\n\n          <button ion-button item-end clear *ngIf="showDeleteBtn(d.SharedWith)">\n\n            <ion-icon ios="ios-remove-circle" md="md-remove-circle" color="danger"></ion-icon>\n\n          </button>\n\n\n\n          <div item-end *ngIf="d.currentFuel">\n\n            <ion-avatar class="ava">\n\n              <img src="assets/imgs/fuel.png">\n\n            </ion-avatar>\n\n            <p style="color: gray; font-size: 10px;font-weight: 400;margin:0px;">{{d.currentFuel}}L</p>\n\n          </div>\n\n        </ion-item>\n\n\n\n        <ion-row style="background-color: rgba(0, 0, 0, 0.9)" padding-right>\n\n          <ion-col width-20 style="text-align:center">\n\n            <ion-icon name="pin" style="color:gray;font-size: 12px;"></ion-icon>\n\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Live</p>\n\n          </ion-col>\n\n          <ion-col width-20 style="text-align:center">\n\n            <ion-icon name="clock" style="color:gray;font-size: 12px;"></ion-icon>\n\n            <p style="color:gray;font-size: 10px;font-weight: 400;">History</p>\n\n          </ion-col>\n\n          <ion-col width-20 style="text-align:center">\n\n            <ion-icon name="power" style="color:gray;font-size: 12px;"></ion-icon>\n\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Ignition</p>\n\n          </ion-col>\n\n          <ion-col width-20 style="text-align:center">\n\n            <ion-icon name="lock" style="color:gray;font-size: 12px;" *ngIf="d.ignitionLock==\'1\'"></ion-icon>\n\n            <ion-icon name="unlock" style="color:gray;font-size: 12px;" *ngIf="d.ignitionLock==\'0\'"></ion-icon>\n\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Immobilize</p>\n\n          </ion-col>\n\n          <ion-col width-20 style="text-align:center">\n\n            <ion-icon name="battery-charging" style="color:gray;font-size: 12px;"></ion-icon>\n\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Power</p>\n\n          </ion-col>\n\n          <ion-col width-20 style="text-align:center">\n\n            <ion-icon name="call" style="color:gray;font-size: 12px;"></ion-icon>\n\n            <p style="color:gray;font-size: 10px;font-weight: 400;">Driver</p>\n\n          </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-item item-start class="itemStyle" style="background-color: rgba(0, 0, 0, 0.9)">\n\n          <ion-row>\n\n            <ion-col (onCreate)="device_address(d,i)" class="colSt2">\n\n              <div class="overme" style="color: gray">\n\n                {{d.address}}\n\n              </div>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-item>\n\n      </div>\n\n      <div>\n\n        <button class="activeBtn" ion-button outline color="secondary" (tap)="activateVehicle(d)">Activate</button>\n\n      </div>\n\n    </div>\n\n\n\n    <div *ngIf="d.expiration_date == null || d.expiration_date > now">\n\n      <ion-item>\n\n        <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType != null"\n\n          (click)="showVehicleDetails(d); $event.stopPropagation()">\n\n          <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/imgs/car_blue_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/car_yellow_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/imgs/truck_icon_blue.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/truck_icon_yellow.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/imgs/bike_blue_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/bike_yellow_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/imgs/jcb_blue.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/jcb_yellow.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.last_ACC==null ))"> -->\n\n          <img src="assets/imgs/bus_blue.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/bus_yellow.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/tractor_red.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/tractor_green.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/tractor_gray.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.last_ACC==null ))"> -->\n\n          <img src="assets/imgs/tractor_blue.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/tractor_yellow.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.vehicleType.iconType == \'user\')&&(d.last_ACC==null ))"> -->\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'IDLING\'))">\n\n\n\n        </ion-avatar>\n\n\n\n        <ion-avatar item-start *ngIf="d.vehicleType != null && d.iconType == null"\n\n          (click)="showVehicleDetails(d); $event.stopPropagation()">\n\n\n\n          <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.vehicleType.iconType == \'car\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/imgs/car_blue_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/car_yellow_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'car\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/imgs/truck_icon_blue.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/truck_icon_yellow.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'truck\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/imgs/bike_blue_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/bike_yellow_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bike\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/imgs/jcb_blue.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/jcb_yellow.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'jcb\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/img/bus_blue.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/bus_yellow.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'bus\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/tractor_red.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/tractor_green.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/img/tractor_blue.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/tractor_yellow.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'tractor\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.vehicleType.iconType == \'user\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/img/user.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.vehicleType.iconType == \'user\')&&(d.status==\'IDLING\'))">\n\n\n\n        </ion-avatar>\n\n\n\n        <ion-avatar item-start *ngIf="d.vehicleType == null && d.iconType != null"\n\n          (click)="showVehicleDetails(d); $event.stopPropagation()">\n\n\n\n          <img src="assets/imgs/car_red_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'car\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/car_green_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'car\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/car_grey_icon.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.iconType == \'car\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/imgs/car_blue_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'car\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/car_yellow_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'car\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/truck_icon_red.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'truck\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/truck_icon_green.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'truck\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/truck_icon_grey.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.iconType == \'truck\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/imgs/truck_icon_blue.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'truck\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/truck_icon_yellow.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'truck\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/bike_red_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'bike\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/bike_green_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'bike\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/bike_grey_icon.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.iconType == \'bike\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/imgs/bike_blue_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'bike\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/bike_yellow_icon.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'bike\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/jcb_red.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'jcb\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/jcb_green.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'jcb\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/jcb_gray.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.iconType == \'jcb\')&&(d.last_ACC==null))"> -->\n\n          <img src="assets/imgs/jcb_blue.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'jcb\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/jcb_yellow.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'jcb\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/bus_red.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'bus\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/bus_green.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'bus\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/bus_gray.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.iconType == \'bus\')&&(d.last_ACC ==null))"> -->\n\n          <img src="assets/imgs/bus_blue.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'bus\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/bus_yellow.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'bus\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/tractor_red.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'tractor\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/tractor_green.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'tractor\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/tractor_gray.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.iconType == \'bus\')&&(d.last_ACC ==null))"> -->\n\n          <img src="assets/imgs/tractor_blue.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'tractor\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/tractor_yellow.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'tractor\')&&(d.status==\'IDLING\'))">\n\n\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'user\')&&(d.status==\'STOPPED\'))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'user\')&&(d.status==\'RUNNING\'))">\n\n          <!-- <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n\n                *ngIf="((d.iconType == \'user\')&&(d.last_ACC ==null))"> -->\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'user\')&&(d.status==\'OUT OF REACH\'))">\n\n          <img src="assets/imgs/user.png" title="{{d.Device_Name}}"\n\n            *ngIf="((d.iconType == \'user\')&&(d.status==\'IDLING\'))">\n\n\n\n        </ion-avatar>\n\n        <div>\n\n          <p style="color: black; font-size: 4vw; padding-right: 8px;">\n\n            {{ d.Device_Name }} &nbsp;&nbsp;\n\n            <!-- <label class="switch">\n\n              <input type="checkbox" [(ngModel)]="d.theftAlert" (click)="fonctionTest(d)">\n\n              <span class="slider round"></span>\n\n            </label> -->\n\n          </p>\n\n\n\n          <p style="color:#cf1c1c;font-size: 3vw;font-weight: 400;margin:0px;" ion-text text-wrap>\n\n            <span style="text-transform: uppercase;">{{ d.status }} </span>\n\n            <span *ngIf="d.status_updated_at">Since&nbsp;{{\n\n                d.status_updated_at | date: "medium"\n\n              }}\n\n            </span>\n\n          </p>\n\n        </div>\n\n\n\n        <div item-end *ngIf="islogin.fuel_unit === \'PERCENTAGE\'">\n\n          <div *ngIf="d.fuel_percent">\n\n            <ion-avatar class="ava">\n\n              <img src="assets/imgs/fuel.png" />\n\n            </ion-avatar>\n\n            <p style="color: green; font-size: 10px;font-weight: 400;margin:0px;">\n\n              <b>{{ d.fuel_percent }}%</b>\n\n            </p>\n\n          </div>\n\n        </div>\n\n\n\n        <div item-end *ngIf="islogin.fuel_unit === \'LITRE\'">\n\n          <div *ngIf="d.currentFuel">\n\n            <ion-avatar class="ava">\n\n              <img src="assets/imgs/fuel.png" />\n\n            </ion-avatar>\n\n            <p style="color: green; font-size: 10px;font-weight: 400;margin:0px;">\n\n              <b>{{ d.currentFuel }}L</b>\n\n            </p>\n\n          </div>\n\n        </div>\n\n\n\n        <div item-end *ngIf="d.last_speed">\n\n          <ion-badge color="gpsc">{{ d.last_speed }} km/h</ion-badge>\n\n        </div>\n\n        <button ion-button item-end clear color="gpsc" (click)="presentPopover($event, d); $event.stopPropagation()"\n\n          *ngIf="!showDeleteBtn(d.SharedWith)" style="font-size: 4vw; margin: 0px;">\n\n          <ion-icon ios="ios-more" md="md-more" *ngIf="option_switch"></ion-icon>\n\n        </button>\n\n\n\n        <button ion-button item-end clear color="gpsc" (click)="shareVehicle(d)" *ngIf="!option_switch"\n\n          style="font-size: 4vw; margin: 0px;">\n\n          <ion-icon ios="ios-share" md="md-share" *ngIf="showSharedBtn(islogin._id, d.SharedWith)"></ion-icon>\n\n        </button>\n\n\n\n        <button ion-button item-end clear color="danger" (click)="sharedVehicleDelete(d)"\n\n          *ngIf="showDeleteBtn(d.SharedWith)" style="font-size: 4vw; margin: 0px;">\n\n          <ion-icon ios="ios-remove-circle" md="md-remove-circle"></ion-icon>\n\n        </button>\n\n      </ion-item>\n\n\n\n      <ion-row style="background-color: #fafafa;">\n\n        <ion-col col-2 style="text-align:center" (click)="livetrack(d)">\n\n          <ion-icon name="pin" style="color:#ffc900; font-size: 3vw;"></ion-icon>\n\n          <p style="color:#131212; font-size: 2vw; font-weight: 400;">\n\n            Live\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-2 style="text-align:center" (click)="showHistoryDetail(d)">\n\n          <ion-icon name="clock" style="color:#d675ea;font-size: 3vw;"></ion-icon>\n\n          <p style="color:#131212;font-size: 2vw;font-weight: 400;">\n\n            History\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-2 style="text-align:center">\n\n          <ion-icon name="power" style="color:#ef473a;font-size: 3vw;" *ngIf="d.last_ACC == \'0\'"></ion-icon>\n\n          <ion-icon name="power" style="color:#1de21d;font-size: 3vw;" *ngIf="d.last_ACC == \'1\'"></ion-icon>\n\n          <ion-icon name="power" style="color:gray;font-size: 3vw;" *ngIf="d.last_ACC == null"></ion-icon>\n\n          <p style="color:#131212;font-size: 2vw;font-weight: 400;">\n\n            Ignition\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-2 style="text-align:center" (click)="IgnitionOnOff(d)">\n\n          <ion-icon name="lock" style="color:#ef473a;font-size: 3vw;" *ngIf="d.ignitionLock == \'1\'"></ion-icon>\n\n          <ion-icon name="unlock" style="color:#1de21d;font-size: 3vw;" *ngIf="d.ignitionLock == \'0\'"></ion-icon>\n\n          <p style="color:#131212;font-size: 2vw;font-weight: 400;">\n\n            Immobilize\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-2 style="text-align:center">\n\n          <ion-icon name="battery-charging" style="color:#ef473a;font-size: 3vw;" *ngIf="d.power != \'1\'"></ion-icon>\n\n          <ion-icon name="battery-charging" style="color:#1de21d;font-size: 3vw;" *ngIf="d.power == \'1\'"></ion-icon>\n\n          <p style="color:#131212;font-size: 2vw;font-weight: 400;">\n\n            Power\n\n          </p>\n\n        </ion-col>\n\n        <ion-col col-2 style="text-align:center" (click)="dialNumber(d.contact_number)">\n\n          <ion-icon name="call" style="color:#0000FF;font-size: 3vw;"></ion-icon>\n\n          <p style="color:#131212;font-size: 2vw;font-weight: 400;">\n\n            Driver\n\n          </p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-item item-start class="itemStyle" style="background:#696D74;">\n\n        <ion-row>\n\n          <ion-col (onCreate)="device_address(d, i)" class="colSt2">\n\n            <div class="overme">\n\n              {{ d.address }}\n\n            </div>\n\n          </ion-col>\n\n          <ion-col class="colSt1">\n\n            <p style="color:#fff;font-size: 12px;font-weight: 400;margin-top: 6px;">\n\n              Today\'s Distance\n\n            </p>\n\n            <p style="color:#fff;font-size: 12px;font-weight: 400;margin-top: 6px;">\n\n              {{ d.today_odo | number: "1.0-2" }} km\n\n            </p>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-item>\n\n    </div>\n\n  </ion-card>\n\n\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n\n    </ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\add-devices\add-devices.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_sms__["a" /* SMS */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["PopoverController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], AddDevicesPage);
    return AddDevicesPage;
}());

var PopoverPage = /** @class */ (function () {
    function PopoverPage(navParams, modalCtrl, alertCtrl, apiCall, toastCtrl, navCtrl, viewCtrl) {
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.vehData = navParams.get("vehData");
        console.log("popover data=> ", this.vehData);
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + this.islogin);
    }
    PopoverPage.prototype.ngOnInit = function () { };
    PopoverPage.prototype.editItem = function () {
        var _this = this;
        console.log("edit");
        var modal = this.modalCtrl.create('UpdateDevicePage', {
            vehData: this.vehData
        });
        modal.onDidDismiss(function () {
            console.log("modal dismissed!");
            _this.viewCtrl.dismiss();
        });
        modal.present();
    };
    PopoverPage.prototype.deleteItem = function () {
        var that = this;
        console.log("delete");
        var alert = this.alertCtrl.create({
            message: 'Do you want to delete this vehicle ?',
            buttons: [{
                    text: 'YES PROCEED',
                    handler: function () {
                        console.log(that.vehData.Device_ID);
                        that.deleteDevice(that.vehData.Device_ID);
                    }
                },
                {
                    text: 'NO'
                }]
        });
        alert.present();
    };
    PopoverPage.prototype.deleteDevice = function (d_id) {
        var _this = this;
        this.apiCall.startLoading().present();
        this.apiCall.deleteDeviceCall(d_id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            var DeletedDevice = data;
            console.log(DeletedDevice);
            var toast = _this.toastCtrl.create({
                message: 'Vehicle deleted successfully!',
                position: 'bottom',
                duration: 2000
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
                // this.navCtrl.push(AddDevicesPage);
                _this.viewCtrl.dismiss();
            });
            toast.present();
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    PopoverPage.prototype.shareItem = function () {
        var that = this;
        console.log("share");
        var prompt = this.alertCtrl.create({
            title: 'Share Vehicle',
            // message: "Enter a name for this new album you're so keen on adding",
            inputs: [
                {
                    name: 'device_name',
                    value: that.vehData.Device_Name
                },
                {
                    name: 'shareId',
                    placeholder: 'Enter Email Id/Mobile Number'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Share',
                    handler: function (data) {
                        console.log('Saved clicked');
                        console.log("clicked=> ", data);
                        that.sharedevices(data);
                    }
                }
            ]
        });
        prompt.present();
    };
    PopoverPage.prototype.sharedevices = function (data) {
        var _this = this;
        var that = this;
        console.log(data.shareId);
        var devicedetails = {
            "did": that.vehData._id,
            "email": data.shareId,
            "uid": that.islogin._id
        };
        that.apiCall.startLoading().present();
        that.apiCall.deviceShareCall(devicedetails)
            .subscribe(function (data) {
            that.apiCall.stopLoading();
            var editdata = data;
            console.log(editdata);
            var toast = that.toastCtrl.create({
                message: data.message,
                position: 'bottom',
                duration: 2000
            });
            // toast.onDidDismiss(() => {
            //   console.log('Dismissed toast');
            // });
            toast.present();
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                title: 'Oops!',
                message: msg.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    PopoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "\n    <ion-list>\n      <ion-item class=\"text-palatino\" (click)=\"editItem()\">\n        <ion-icon name=\"create\"></ion-icon>&nbsp;&nbsp;Edit\n      </ion-item>\n      <ion-item class=\"text-san-francisco\" (click)=\"deleteItem()\">\n        <ion-icon name=\"trash\"></ion-icon>&nbsp;&nbsp;Delete\n      </ion-item>\n      <ion-item class=\"text-seravek\" (click)=\"shareItem()\">\n        <ion-icon name=\"share\"></ion-icon>&nbsp;&nbsp;Share\n      </ion-item>\n\n    </ion-list>\n  "
        })
        // <ion-item id="step1" class="text-seravek" (click)="upload()">
        //        <ion-icon name="cloud-upload"></ion-icon>&nbsp;&nbsp;Docs
        //        </ion-item>
        ,
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], PopoverPage);
    return PopoverPage;
}());

//# sourceMappingURL=add-devices.js.map

/***/ }),

/***/ 419:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(420);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(424);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 424:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export MyErrorHandler */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(499);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_network_network__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_menu_menu__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_google_maps__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_add_devices_add_devices__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__node_modules_ion_bottom_drawer__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_push__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_groups_update_group_update_group__ = __webpack_require__(501);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_all_notifications_filter_filter__ = __webpack_require__(412);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_geolocation__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_app_version__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_social_sharing__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_text_to_speech__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_call_number__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_pro__ = __webpack_require__(502);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_pro___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_26__ionic_pro__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_storage__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_camera__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_transfer__ = __webpack_require__(379);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_file_path__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_file__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__ionic_native_file_transfer__ = __webpack_require__(413);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_live_single_device_live_single_device__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_native_native_geocoder__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_view_doc_view_doc__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_add_devices_upload_doc_upload_doc__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_ac_report_ac_report__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_fuel_fuel_consumption_fuel_consumption__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__ionic_native_android_permissions__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__providers_geocoder_geocoder__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__ionic_native_native_page_transitions__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_history_device_modal__ = __webpack_require__(415);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



















// Custom components

























__WEBPACK_IMPORTED_MODULE_26__ionic_pro__["Pro"].init('d2e7bf2e', {
    appVersion: '0.0.1'
});
var MyErrorHandler = /** @class */ (function () {
    function MyErrorHandler(injector) {
        try {
            this.ionicErrorHandler = injector.get(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"]);
        }
        catch (e) {
            // Unable to get the IonicErrorHandler provider, ensure
            // IonicErrorHandler has been added to the providers list below
        }
    }
    MyErrorHandler.prototype.handleError = function (err) {
        __WEBPACK_IMPORTED_MODULE_26__ionic_pro__["Pro"].monitoring.handleNewError(err);
        // Remove this if you want to disable Ionic's auto exception handling
        // in development mode.
        this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
    };
    MyErrorHandler = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injector"]])
    ], MyErrorHandler);
    return MyErrorHandler;
}());

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_19__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */],
                __WEBPACK_IMPORTED_MODULE_14__pages_add_devices_add_devices__["b" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_groups_update_group_update_group__["a" /* UpdateGroup */],
                __WEBPACK_IMPORTED_MODULE_18__pages_all_notifications_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["b" /* ServiceProviderPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["c" /* UpdatePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_live_single_device_live_single_device__["b" /* PoiPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_view_doc_view_doc__["a" /* ViewDoc */],
                __WEBPACK_IMPORTED_MODULE_36__pages_add_devices_upload_doc_upload_doc__["a" /* DocPopoverPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_ac_report_ac_report__["a" /* ACDetailPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_fuel_fuel_consumption_fuel_consumption__["b" /* FuelEntryPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_history_device_modal__["a" /* ModalPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_6__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/ac-report/ac-report.module#AcReportPageModule', name: 'AcReportPage', segment: 'ac-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/immobilize/immobilize.module#ImmobilizePageModule', name: 'ImmobilizePage', segment: 'immobilize', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/payment-greeting/payment-greeting.module#PaymentGreetingPageModule', name: 'PaymentGreetingPage', segment: 'payment-greeting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/payment-secure/payment-secure.module#PaymentSecurePageModule', name: 'PaymentSecurePage', segment: 'payment-secure', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/paytmwalletlogin/paytmwalletlogin.module#PaytmwalletloginPageModule', name: 'PaytmwalletloginPage', segment: 'paytmwalletlogin', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/upload-doc/add-doc/add-doc.module#AddDocPageModule', name: 'AddDocPage', segment: 'add-doc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/upload-doc/upload-doc.module#UploadDocPageModule', name: 'UploadDocPage', segment: 'upload-doc', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/wallet/wallet.module#WalletPageModule', name: 'WalletPage', segment: 'wallet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact-us/contact-us.module#ContactUsPageModule', name: 'ContactUsPage', segment: 'contact-us', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/group-modal/group-modal.module#GroupModalPageModule', name: 'GroupModalPage', segment: 'group-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/update-cust/update-cust.module#UpdateCustModalPageModule', name: 'UpdateCustModalPage', segment: 'update-cust', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/add-dealer/add-dealer.module#AddDealerPageModule', name: 'AddDealerPage', segment: 'add-dealer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/edit-dealer/edit-dealer.module#EditDealerPageModule', name: 'EditDealerPage', segment: 'edit-dealer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/device-summary-repo/device-summary-repo.module#DeviceSummaryRepoPageModule', name: 'DeviceSummaryRepoPage', segment: 'device-summary-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/feedback/feedback.module#FeedbackPageModule', name: 'FeedbackPage', segment: 'feedback', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/distance-report/distance-report.module#DistanceReportPageModule', name: 'DistanceReportPage', segment: 'distance-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-chart/fuel-chart.module#FuelChartPageModule', name: 'FuelChartPage', segment: 'fuel-chart', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-consumption-report/fuel-consumption-report.module#FuelConsumptionReportPageModule', name: 'FuelConsumptionReportPage', segment: 'fuel-consumption-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-consumption/fuel-consumption.module#FuelConsumptionPageModule', name: 'FuelConsumptionPage', segment: 'fuel-consumption', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fuel/fuel-events/fuel-events.module#FuelEventsComponentModule', name: 'FuelEventsComponent', segment: 'fuel-events', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence-report/geofence-report.module#GeofenceReportPageModule', name: 'GeofenceReportPage', segment: 'geofence-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/geofence-show/geofence-show.module#GeofenceShowPageModule', name: 'GeofenceShowPage', segment: 'geofence-show', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/geofence.module#GeofencePageModule', name: 'GeofencePage', segment: 'geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groups/groups.module#GroupsPageModule', name: 'GroupsPage', segment: 'groups', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/idle-report/idle-report.module#IdleReportPageModule', name: 'IdleReportPage', segment: 'idle-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ignition-report/ignition-report.module#IgnitionReportPageModule', name: 'IgnitionReportPage', segment: 'ignition-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live-single-device/device-settings/device-settings.module#DeviceSettingsPageModule', name: 'DeviceSettingsPage', segment: 'device-settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live/expired/expired.module#ExpiredPageModule', name: 'ExpiredPage', segment: 'expired', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/over-speed-repo/over-speed-repo.module#OverSpeedRepoPageModule', name: 'OverSpeedRepoPage', segment: 'over-speed-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-list/poi-list.module#PoiListPageModule', name: 'PoiListPage', segment: 'poi-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-report/poi-report.module#POIReportPageModule', name: 'POIReportPage', segment: 'poi-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/settings/settings.module#SettingsPageModule', name: 'SettingsPage', segment: 'settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route-map-show/route-map-show.module#RouteMapShowPageModule', name: 'RouteMapShowPage', segment: 'route-map-show', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route-voilations/route-voilations.module#RouteVoilationsPageModule', name: 'RouteVoilationsPage', segment: 'route-voilations', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/route/route.module#RoutePageModule', name: 'RoutePage', segment: 'route', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup-otp/signup-otp.module#SignupOtpPageModule', name: 'SignupOtpPage', segment: 'signup-otp', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sos-report/sos-report.module#SosReportPageModule', name: 'SosReportPage', segment: 'sos-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/speed-repo/speed-repo.module#SpeedRepoPageModule', name: 'SpeedRepoPage', segment: 'speed-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/stoppages-repo/stoppages-repo.module#StoppagesRepoPageModule', name: 'StoppagesRepoPage', segment: 'stoppages-repo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trip-report/trip-report.module#TripReportPageModule', name: 'TripReportPage', segment: 'trip-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/your-trips/your-trips.module#YourTripsPageModule', name: 'YourTripsPage', segment: 'your-trips', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/update-device/update-device.module#UpdateDevicePageModule', name: 'UpdateDevicePage', segment: 'update-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/all-notifications/all-notifications.module#AllNotificationsPageModule', name: 'AllNotificationsPage', segment: 'all-notifications', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/create-trip/create-trip.module#CreateTripPageModule', name: 'CreateTripPage', segment: 'create-trip', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/customers.module#CustomersPageModule', name: 'CustomersPage', segment: 'customers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/add-device-modal.module#AddDeviceModalPageModule', name: 'AddDeviceModalPage', segment: 'add-device-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/modals/add-customer-modal/add-customer-modal.module#AddCustomerModalModule', name: 'AddCustomerModal', segment: 'add-customer-modal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/daily-report/daily-report.module#DailyReportPageModule', name: 'DailyReportPage', segment: 'daily-report', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dealers/dealers.module#DealerPageModule', name: 'DealerPage', segment: 'dealers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/geofence/add-geofence/add-geofence.module#AddGeofencePageModule', name: 'AddGeofencePage', segment: 'add-geofence', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trip-report/trip-review/trip-review.module#TripReviewPageModule', name: 'TripReviewPage', segment: 'trip-review', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/poi-list/add-poi/add-poi.module#AddPoiPageModule', name: 'AddPoiPage', segment: 'add-poi', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/history-device/history-device.module#HistoryDevicePageModule', name: 'HistoryDevicePage', segment: 'history-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live/live.module#LivePageModule', name: 'LivePage', segment: 'live', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/add-devices.module#AddDevicesPageModule', name: 'AddDevicesPage', segment: 'add-devices', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-devices/vehicle-details/vehicle-details.module#VehicleDetailsPageModule', name: 'VehicleDetailsPage', segment: 'vehicle-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/live-single-device/live-single-device.module#LiveSingleDeviceModule', name: 'LiveSingleDevice', segment: 'live-single-device', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_27__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_15__node_modules_ion_bottom_drawer__["b" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_14__pages_add_devices_add_devices__["b" /* PopoverPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_groups_update_group_update_group__["a" /* UpdateGroup */],
                __WEBPACK_IMPORTED_MODULE_18__pages_all_notifications_filter_filter__["a" /* FilterPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["b" /* ServiceProviderPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_profile_profile__["c" /* UpdatePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_live_single_device_live_single_device__["b" /* PoiPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_add_devices_upload_doc_view_doc_view_doc__["a" /* ViewDoc */],
                __WEBPACK_IMPORTED_MODULE_36__pages_add_devices_upload_doc_upload_doc__["a" /* DocPopoverPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_ac_report_ac_report__["a" /* ACDetailPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_fuel_fuel_consumption_fuel_consumption__["b" /* FuelEntryPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_history_device_modal__["a" /* ModalPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"],
                [{ provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: MyErrorHandler }],
                // { provide: ErrorHandler, useClass: IonicErrorHandler },
                __WEBPACK_IMPORTED_MODULE_10__providers_api_service_api_service__["a" /* ApiServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_11__providers_network_network__["a" /* NetworkProvider */],
                __WEBPACK_IMPORTED_MODULE_12__providers_menu_menu__["a" /* MenuProvider */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_google_maps__["b" /* GoogleMaps */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_google_maps__["h" /* Spherical */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_app_version__["a" /* AppVersion */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_text_to_speech__["a" /* TextToSpeech */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_transfer__["a" /* Transfer */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_transfer__["b" /* TransferObject */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_32__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_32__ionic_native_file_transfer__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_34__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_call_number__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_39__ionic_native_android_permissions__["a" /* AndroidPermissions */],
                __WEBPACK_IMPORTED_MODULE_40__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
                __WEBPACK_IMPORTED_MODULE_41__ionic_native_native_page_transitions__["a" /* NativePageTransitions */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 447:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 180,
	"./af.js": 180,
	"./ar": 181,
	"./ar-dz": 182,
	"./ar-dz.js": 182,
	"./ar-kw": 183,
	"./ar-kw.js": 183,
	"./ar-ly": 184,
	"./ar-ly.js": 184,
	"./ar-ma": 185,
	"./ar-ma.js": 185,
	"./ar-sa": 186,
	"./ar-sa.js": 186,
	"./ar-tn": 187,
	"./ar-tn.js": 187,
	"./ar.js": 181,
	"./az": 188,
	"./az.js": 188,
	"./be": 189,
	"./be.js": 189,
	"./bg": 190,
	"./bg.js": 190,
	"./bm": 191,
	"./bm.js": 191,
	"./bn": 192,
	"./bn.js": 192,
	"./bo": 193,
	"./bo.js": 193,
	"./br": 194,
	"./br.js": 194,
	"./bs": 195,
	"./bs.js": 195,
	"./ca": 196,
	"./ca.js": 196,
	"./cs": 197,
	"./cs.js": 197,
	"./cv": 198,
	"./cv.js": 198,
	"./cy": 199,
	"./cy.js": 199,
	"./da": 200,
	"./da.js": 200,
	"./de": 201,
	"./de-at": 202,
	"./de-at.js": 202,
	"./de-ch": 203,
	"./de-ch.js": 203,
	"./de.js": 201,
	"./dv": 204,
	"./dv.js": 204,
	"./el": 205,
	"./el.js": 205,
	"./en-au": 206,
	"./en-au.js": 206,
	"./en-ca": 207,
	"./en-ca.js": 207,
	"./en-gb": 208,
	"./en-gb.js": 208,
	"./en-ie": 209,
	"./en-ie.js": 209,
	"./en-il": 210,
	"./en-il.js": 210,
	"./en-nz": 211,
	"./en-nz.js": 211,
	"./eo": 212,
	"./eo.js": 212,
	"./es": 213,
	"./es-do": 214,
	"./es-do.js": 214,
	"./es-us": 215,
	"./es-us.js": 215,
	"./es.js": 213,
	"./et": 216,
	"./et.js": 216,
	"./eu": 217,
	"./eu.js": 217,
	"./fa": 218,
	"./fa.js": 218,
	"./fi": 219,
	"./fi.js": 219,
	"./fo": 220,
	"./fo.js": 220,
	"./fr": 221,
	"./fr-ca": 222,
	"./fr-ca.js": 222,
	"./fr-ch": 223,
	"./fr-ch.js": 223,
	"./fr.js": 221,
	"./fy": 224,
	"./fy.js": 224,
	"./gd": 225,
	"./gd.js": 225,
	"./gl": 226,
	"./gl.js": 226,
	"./gom-latn": 227,
	"./gom-latn.js": 227,
	"./gu": 228,
	"./gu.js": 228,
	"./he": 229,
	"./he.js": 229,
	"./hi": 230,
	"./hi.js": 230,
	"./hr": 231,
	"./hr.js": 231,
	"./hu": 232,
	"./hu.js": 232,
	"./hy-am": 233,
	"./hy-am.js": 233,
	"./id": 234,
	"./id.js": 234,
	"./is": 235,
	"./is.js": 235,
	"./it": 236,
	"./it.js": 236,
	"./ja": 237,
	"./ja.js": 237,
	"./jv": 238,
	"./jv.js": 238,
	"./ka": 239,
	"./ka.js": 239,
	"./kk": 240,
	"./kk.js": 240,
	"./km": 241,
	"./km.js": 241,
	"./kn": 242,
	"./kn.js": 242,
	"./ko": 243,
	"./ko.js": 243,
	"./ky": 244,
	"./ky.js": 244,
	"./lb": 245,
	"./lb.js": 245,
	"./lo": 246,
	"./lo.js": 246,
	"./lt": 247,
	"./lt.js": 247,
	"./lv": 248,
	"./lv.js": 248,
	"./me": 249,
	"./me.js": 249,
	"./mi": 250,
	"./mi.js": 250,
	"./mk": 251,
	"./mk.js": 251,
	"./ml": 252,
	"./ml.js": 252,
	"./mn": 253,
	"./mn.js": 253,
	"./mr": 254,
	"./mr.js": 254,
	"./ms": 255,
	"./ms-my": 256,
	"./ms-my.js": 256,
	"./ms.js": 255,
	"./mt": 257,
	"./mt.js": 257,
	"./my": 258,
	"./my.js": 258,
	"./nb": 259,
	"./nb.js": 259,
	"./ne": 260,
	"./ne.js": 260,
	"./nl": 261,
	"./nl-be": 262,
	"./nl-be.js": 262,
	"./nl.js": 261,
	"./nn": 263,
	"./nn.js": 263,
	"./pa-in": 264,
	"./pa-in.js": 264,
	"./pl": 265,
	"./pl.js": 265,
	"./pt": 266,
	"./pt-br": 267,
	"./pt-br.js": 267,
	"./pt.js": 266,
	"./ro": 268,
	"./ro.js": 268,
	"./ru": 269,
	"./ru.js": 269,
	"./sd": 270,
	"./sd.js": 270,
	"./se": 271,
	"./se.js": 271,
	"./si": 272,
	"./si.js": 272,
	"./sk": 273,
	"./sk.js": 273,
	"./sl": 274,
	"./sl.js": 274,
	"./sq": 275,
	"./sq.js": 275,
	"./sr": 276,
	"./sr-cyrl": 277,
	"./sr-cyrl.js": 277,
	"./sr.js": 276,
	"./ss": 278,
	"./ss.js": 278,
	"./sv": 279,
	"./sv.js": 279,
	"./sw": 280,
	"./sw.js": 280,
	"./ta": 281,
	"./ta.js": 281,
	"./te": 282,
	"./te.js": 282,
	"./tet": 283,
	"./tet.js": 283,
	"./tg": 284,
	"./tg.js": 284,
	"./th": 285,
	"./th.js": 285,
	"./tl-ph": 286,
	"./tl-ph.js": 286,
	"./tlh": 287,
	"./tlh.js": 287,
	"./tr": 288,
	"./tr.js": 288,
	"./tzl": 289,
	"./tzl.js": 289,
	"./tzm": 290,
	"./tzm-latn": 291,
	"./tzm-latn.js": 291,
	"./tzm.js": 290,
	"./ug-cn": 292,
	"./ug-cn.js": 292,
	"./uk": 293,
	"./uk.js": 293,
	"./ur": 294,
	"./ur.js": 294,
	"./uz": 295,
	"./uz-latn": 296,
	"./uz-latn.js": 296,
	"./uz.js": 295,
	"./vi": 297,
	"./vi.js": 297,
	"./x-pseudo": 298,
	"./x-pseudo.js": 298,
	"./yo": 299,
	"./yo.js": 299,
	"./zh-cn": 300,
	"./zh-cn.js": 300,
	"./zh-hk": 301,
	"./zh-hk.js": 301,
	"./zh-tw": 302,
	"./zh-tw.js": 302
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 447;

/***/ }),

/***/ 475:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 481:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mapStyle; });
var mapStyle = [
    {
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#242f3e"
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#746855"
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#242f3e"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#263c3f"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#6b9a76"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#38414e"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#212a37"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9ca5b3"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#746855"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#1f2835"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#f3d19c"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#2f3948"
            }
        ]
    },
    {
        "featureType": "transit.station",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#d59563"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#17263c"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#515c6d"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#17263c"
            }
        ]
    }
];
//# sourceMappingURL=map-style.model.js.map

/***/ }),

/***/ 499:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_network_network__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_menu_menu__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_push__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_side_menu_content_side_menu_content_component__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_text_to_speech__ = __webpack_require__(133);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, 
        // private network: Network,
        events, networkProvider, menuProvider, menuCtrl, modalCtrl, push, alertCtrl, app, apiCall, toastCtrl, tts) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.events = events;
        this.networkProvider = networkProvider;
        this.menuProvider = menuProvider;
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.push = push;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.apiCall = apiCall;
        this.toastCtrl = toastCtrl;
        this.tts = tts;
        this.islogin = {};
        // Settings for the SideMenuContentComponent
        this.sideMenuSettings = {
            accordionMode: true,
            showSelectedOption: true,
            selectedOptionClass: 'active-side-menu-option'
        };
        this.unreadCountObservable = new __WEBPACK_IMPORTED_MODULE_8_rxjs_ReplaySubject__["ReplaySubject"](0);
        this.headerContent = "header";
        // this.backgroundMode.enable();
        this.events.subscribe('user:updated', function (udata) {
            _this.islogin = udata;
            _this.isDealer = udata.isDealer;
            console.log("islogin=> " + JSON.stringify(_this.islogin));
        });
        this.events.subscribe('notif:updated', function (notifData) {
            console.log("text to speech updated=> ", notifData);
            // this.notfiD = notifData;
            localStorage.setItem("notifValue", notifData);
        });
        platform.ready().then(function () {
            statusBar.styleDefault();
            statusBar.hide();
            _this.splashScreen.hide();
            platform.registerBackButtonAction(function () {
                var nav = _this.app.getActiveNavs()[0];
                var activeView = nav.getActive();
                if (activeView.name == "DashboardPage") {
                    var alert_1 = _this.alertCtrl.create({
                        title: 'App termination',
                        message: 'Do you want to close the app?',
                        buttons: [{
                                text: 'Cancel',
                                role: 'cancel',
                                handler: function () {
                                    console.log('Application exit prevented!');
                                }
                            }, {
                                text: 'Close App',
                                handler: function () {
                                    _this.platform.exitApp(); // Close this application
                                }
                            }]
                    });
                    alert_1.present();
                }
                else {
                    if (nav.canGoBack()) {
                        nav.pop(); // IF IT'S NOT THE ROOT, POP A PAGE.
                    }
                    else {
                        platform.exitApp(); // IF IT'S THE ROOT, EXIT THE APP.
                    }
                }
            });
        });
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin=> " + JSON.stringify(this.islogin));
        this.setsmsforotp = localStorage.getItem('setsms');
        this.DealerDetails = JSON.stringify(localStorage.getItem('dealer')) || {};
        console.log("DealerDetails=> " + this.DealerDetails._id);
        this.dealerStatus = this.islogin.isDealer;
        console.log("dealerStatus=> " + this.dealerStatus);
        this.getSideMenuData();
        this.initializeApp();
        if (localStorage.getItem("loginflag")) {
            // debugger;
            if (localStorage.getItem("SCREEN") != null) {
                if (localStorage.getItem("SCREEN") === 'live') {
                    this.rootPage = 'LivePage';
                }
                else {
                    if (localStorage.getItem("SCREEN") === 'dashboard') {
                        this.rootPage = 'DashboardPage';
                    }
                }
            }
            else {
                this.rootPage = 'DashboardPage';
            }
        }
        else {
            this.rootPage = 'LoginPage';
        }
    }
    MyApp.prototype.getSideMenuData = function () {
        this.pages = this.menuProvider.getSideMenus();
    };
    MyApp.prototype.pushNotify = function () {
        var that = this;
        that.push.hasPermission() // to check if we have permission
            .then(function (res) {
            if (res.isEnabled) {
                console.log('We have permission to send push notifications');
                that.pushSetup();
            }
            else {
                console.log('We do not have permission to send push notifications');
                that.pushSetup();
            }
        });
    };
    MyApp.prototype.pushSetup = function () {
        var _this = this;
        // to initialize push notifications
        var that = this;
        var options = {
            android: {
                senderID: '644983599736',
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'true'
            },
            windows: {},
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            }
        };
        var pushObject = that.push.init(options);
        pushObject.on('notification').subscribe(function (notification) {
            if (localStorage.getItem("notifValue") != null) {
                if (localStorage.getItem("notifValue") == 'true') {
                    _this.tts.speak(notification.message)
                        .then(function () { return console.log('Success'); })
                        .catch(function (reason) { return console.log(reason); });
                }
            }
            if (notification.additionalData.foreground) {
                var toast = _this.toastCtrl.create({
                    message: notification.message,
                    duration: 2000
                });
                toast.present();
            }
        });
        pushObject.on('registration')
            .subscribe(function (registration) {
            // alert(registration.registrationId)
            console.log("device token => " + registration.registrationId);
            // console.log("reg type=> " + registration.registrationType);
            localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
            // that.storage.set("DEVICE_TOKEN", registration.registrationId);
        });
        pushObject.on('error').subscribe(function (error) {
            console.error('Error with Push plugin', error);
            // alert('Error with Push plugin' + error)
        });
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        var that = this;
        that.platform.ready().then(function () {
            that.pushNotify();
            that.networkProvider.initializeNetworkEvents();
            // Offline event
            that.events.subscribe('network:offline', function () {
                // alert('network:offline ==> ' + this.networkProvider.getNetworkType());
                alert("Internet is not connected... please make sure the internet connection is working properly.");
            });
            // Online event
            that.events.subscribe('network:online', function () {
                alert('network:online ==> ' + _this.networkProvider.getNetworkType());
            });
            // that.backBtnHandler();
        });
        // Initialize some options
        that.initializeOptions();
        // Change the value for the batch every 5 seconds
        setInterval(function () {
            _this.unreadCountObservable.next(Math.floor(Math.random() * 10) + 1);
        }, 5000);
    };
    MyApp.prototype.initializeOptions = function () {
        var _this = this;
        this.options = new Array();
        debugger;
        if ((this.islogin.role == undefined || this.islogin.role == 'supAdm' || this.islogin.role == 'adm') && this.islogin.isSuperAdmin == true && (this.islogin.isDealer == false || this.islogin.isDealer == undefined)) {
            this.options.push({
                iconName: 'home',
                displayText: 'Home',
                component: 'DashboardPage',
            });
            this.options.push({
                iconName: 'people',
                displayText: 'Groups',
                component: 'GroupsPage'
            });
            this.options.push({
                iconName: 'people',
                displayText: 'Dealers',
                component: 'DealerPage'
            });
            this.options.push({
                iconName: 'contacts',
                displayText: 'Customers',
                component: 'CustomersPage'
            });
            this.options.push({
                iconName: 'notifications',
                displayText: 'Notifications',
                component: 'AllNotificationsPage'
            });
            this.options.push({
                iconName: 'walk',
                displayText: 'Your Trips',
                component: 'YourTripsPage'
            });
            this.options.push({
                iconName: 'list',
                displayText: 'POI List',
                component: 'PoiListPage'
            });
            this.options.push({
                displayText: 'Fuel',
                iconName: 'arrow-dropright',
                suboptions: [
                    {
                        iconName: 'custom-fuel',
                        displayText: 'Fuel Entry',
                        component: 'FuelConsumptionPage'
                    },
                    {
                        iconName: 'custom-fuel',
                        displayText: 'Fuel Consumption',
                        component: 'FuelConsumptionReportPage'
                    },
                    {
                        iconName: 'custom-fuel',
                        displayText: 'Fuel Chart',
                        component: 'FuelChartPage'
                    }
                ]
            });
            // Load options with nested items (with icons)
            // -----------------------------------------------
            this.options.push({
                displayText: 'Reports',
                iconName: 'arrow-dropright',
                suboptions: [
                    {
                        iconName: 'clipboard',
                        displayText: 'Idle Report',
                        component: 'IdleReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'AC Report',
                        component: 'AcReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Daily Report',
                        component: 'DailyReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Summary Report',
                        component: 'DeviceSummaryRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Geofenceing Report',
                        component: 'GeofenceReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Overspeed Report',
                        component: 'OverSpeedRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Ignition Report',
                        component: 'IgnitionReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Stoppage Report',
                        component: 'StoppagesRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Distance Report',
                        component: 'DistanceReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Trip Report',
                        component: 'TripReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Route Violation Report',
                        component: 'RouteVoilationsPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'Speed Variation Report',
                        component: 'SpeedRepoPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'SOS Report',
                        component: 'SosReportPage'
                    },
                    {
                        iconName: 'clipboard',
                        displayText: 'POI Report',
                        component: 'POIReportPage'
                    }
                ]
            });
            // Load options with nested items (without icons)
            // -----------------------------------------------
            this.options.push({
                displayText: 'Routes',
                iconName: 'map',
                component: 'RoutePage'
            });
            // Load special options
            // -----------------------------------------------
            this.options.push({
                displayText: 'Customer Support',
                suboptions: [
                    {
                        iconName: 'star',
                        displayText: 'Rate Us',
                        component: 'FeedbackPage'
                    },
                    {
                        iconName: 'mail',
                        displayText: 'Contact Us',
                        component: 'ContactUsPage'
                    }
                ]
            });
            this.options.push({
                displayText: 'Profile',
                iconName: 'person',
                component: 'ProfilePage'
            });
        }
        else {
            if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == false) {
                this.options.push({
                    iconName: 'home',
                    displayText: 'Home',
                    component: 'DashboardPage',
                });
                this.options.push({
                    iconName: 'people',
                    displayText: 'Groups',
                    component: 'GroupsPage'
                });
                this.options.push({
                    iconName: 'notifications',
                    displayText: 'Notifications',
                    component: 'AllNotificationsPage'
                });
                this.options.push({
                    iconName: 'walk',
                    displayText: 'Your Trips',
                    component: 'YourTripsPage'
                });
                this.options.push({
                    iconName: 'list',
                    displayText: 'POI List',
                    component: 'PoiListPage'
                });
                this.options.push({
                    displayText: 'Fuel',
                    iconName: 'arrow-dropright',
                    suboptions: [
                        {
                            iconName: 'custom-fuel',
                            displayText: 'Fuel Entry',
                            component: 'FuelConsumptionPage'
                        },
                        {
                            iconName: 'custom-fuel',
                            displayText: 'Fuel Consumption',
                            component: 'FuelConsumptionReportPage'
                        },
                        {
                            iconName: 'custom-fuel',
                            displayText: 'Fuel Chart',
                            component: 'FuelChartPage'
                        }
                    ]
                });
                // Load options with nested items (with icons)
                // -----------------------------------------------
                this.options.push({
                    displayText: 'Reports',
                    iconName: 'arrow-dropright',
                    suboptions: [
                        {
                            iconName: 'clipboard',
                            displayText: 'Idle Report',
                            component: 'IdleReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'AC Report',
                            component: 'AcReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Daily Report',
                            component: 'DailyReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Summary Report',
                            component: 'DeviceSummaryRepoPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Geofenceing Report',
                            component: 'GeofenceReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Overspeed Report',
                            component: 'OverSpeedRepoPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Ignition Report',
                            component: 'IgnitionReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Stoppage Report',
                            component: 'StoppagesRepoPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Distance Report',
                            component: 'DistanceReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Trip Report',
                            component: 'TripReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Route Violation Report',
                            component: 'RouteVoilationsPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'Speed Variation Report',
                            component: 'SpeedRepoPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'SOS Report',
                            component: 'SosReportPage'
                        },
                        {
                            iconName: 'clipboard',
                            displayText: 'POI Report',
                            component: 'POIReportPage'
                        }
                    ]
                });
                // Load options with nested items (without icons)
                // -----------------------------------------------
                this.options.push({
                    displayText: 'Routes',
                    iconName: 'map',
                    component: 'RoutePage'
                });
                // Load special options
                // -----------------------------------------------
                this.options.push({
                    displayText: 'Customer Support',
                    suboptions: [
                        {
                            iconName: 'star',
                            displayText: 'Rate Us',
                            component: 'FeedbackPage'
                        },
                        {
                            iconName: 'mail',
                            displayText: 'Contact Us',
                            component: 'ContactUsPage'
                        }
                    ]
                });
                this.options.push({
                    displayText: 'Profile',
                    iconName: 'person',
                    component: 'ProfilePage'
                });
            }
            else {
                if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == true) {
                    this.options.push({
                        iconName: 'home',
                        displayText: 'Home',
                        component: 'DashboardPage',
                    });
                    this.options.push({
                        iconName: 'people',
                        displayText: 'Groups',
                        component: 'GroupsPage'
                    });
                    this.options.push({
                        iconName: 'contacts',
                        displayText: 'Customers',
                        component: 'CustomersPage'
                    });
                    this.options.push({
                        iconName: 'notifications',
                        displayText: 'Notifications',
                        component: 'AllNotificationsPage'
                    });
                    this.options.push({
                        iconName: 'walk',
                        displayText: 'Your Trips',
                        component: 'YourTripsPage'
                    });
                    this.options.push({
                        iconName: 'list',
                        displayText: 'POI List',
                        component: 'PoiListPage'
                    });
                    this.options.push({
                        displayText: 'Fuel',
                        iconName: 'arrow-dropright',
                        suboptions: [
                            {
                                iconName: 'custom-fuel',
                                displayText: 'Fuel Entry',
                                component: 'FuelConsumptionPage'
                            },
                            {
                                iconName: 'custom-fuel',
                                displayText: 'Fuel Consumption',
                                component: 'FuelConsumptionReportPage'
                            },
                            {
                                iconName: 'custom-fuel',
                                displayText: 'Fuel Chart',
                                component: 'FuelChartPage'
                            }
                        ]
                    });
                    // Load options with nested items (with icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Reports',
                        iconName: 'arrow-dropright',
                        suboptions: [
                            {
                                iconName: 'clipboard',
                                displayText: 'Idle Report',
                                component: 'IdleReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'AC Report',
                                component: 'AcReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Daily Report',
                                component: 'DailyReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Summary Report',
                                component: 'DeviceSummaryRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Geofenceing Report',
                                component: 'GeofenceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Overspeed Report',
                                component: 'OverSpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Ignition Report',
                                component: 'IgnitionReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Stoppage Report',
                                component: 'StoppagesRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Distance Report',
                                component: 'DistanceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Trip Report',
                                component: 'TripReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Route Violation Report',
                                component: 'RouteVoilationsPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Speed Variation Report',
                                component: 'SpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'SOS Report',
                                component: 'SosReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'POI Report',
                                component: 'POIReportPage'
                            }
                        ]
                    });
                    // Load options with nested items (without icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Routes',
                        iconName: 'map',
                        component: 'RoutePage'
                    });
                    // Load special options
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Customer Support',
                        suboptions: [
                            {
                                iconName: 'star',
                                displayText: 'Rate Us',
                                component: 'FeedbackPage'
                            },
                            {
                                iconName: 'mail',
                                displayText: 'Contact Us',
                                component: 'ContactUsPage'
                            }
                        ]
                    });
                    this.options.push({
                        displayText: 'Profile',
                        iconName: 'person',
                        component: 'ProfilePage'
                    });
                }
                else {
                    this.options.push({
                        iconName: 'home',
                        displayText: 'Home',
                        component: 'DashboardPage',
                    });
                    this.options.push({
                        iconName: 'people',
                        displayText: 'Groups',
                        component: 'GroupsPage'
                    });
                    this.options.push({
                        iconName: 'notifications',
                        displayText: 'Notifications',
                        component: 'AllNotificationsPage'
                    });
                    this.options.push({
                        iconName: 'walk',
                        displayText: 'Your Trips',
                        component: 'YourTripsPage'
                    });
                    this.options.push({
                        iconName: 'list',
                        displayText: 'POI List',
                        component: 'PoiListPage'
                    });
                    this.options.push({
                        displayText: 'Fuel',
                        iconName: 'arrow-dropright',
                        suboptions: [
                            {
                                iconName: 'custom-fuel',
                                displayText: 'Fuel Entry',
                                component: 'FuelConsumptionPage'
                            },
                            {
                                iconName: 'custom-fuel',
                                displayText: 'Fuel Consumption',
                                component: 'FuelConsumptionReportPage'
                            },
                            {
                                iconName: 'custom-fuel',
                                displayText: 'Fuel Chart',
                                component: 'FuelChartPage'
                            }
                        ]
                    });
                    // Load options with nested items (with icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Reports',
                        iconName: 'arrow-dropright',
                        suboptions: [
                            {
                                iconName: 'clipboard',
                                displayText: 'Idle Report',
                                component: 'IdleReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'AC Report',
                                component: 'AcReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Daily Report',
                                component: 'DailyReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Summary Report',
                                component: 'DeviceSummaryRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Geofenceing Report',
                                component: 'GeofenceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Overspeed Report',
                                component: 'OverSpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Ignition Report',
                                component: 'IgnitionReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Stoppage Report',
                                component: 'StoppagesRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Distnace Report',
                                component: 'DistanceReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Trip Report',
                                component: 'TripReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Route Violation Report',
                                component: 'RouteVoilationsPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'Speed Variation Report',
                                component: 'SpeedRepoPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'SOS Report',
                                component: 'SosReportPage'
                            },
                            {
                                iconName: 'clipboard',
                                displayText: 'POI Report',
                                component: 'POIReportPage'
                            }
                        ]
                    });
                    // Load options with nested items (without icons)
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Routes',
                        iconName: 'map',
                        component: 'RoutePage'
                    });
                    // Load special options
                    // -----------------------------------------------
                    this.options.push({
                        displayText: 'Customer Support',
                        suboptions: [
                            {
                                iconName: 'star',
                                displayText: 'Rate Us',
                                component: 'FeedbackPage'
                            },
                            {
                                iconName: 'mail',
                                displayText: 'Contact Us',
                                component: 'ContactUsPage'
                            },
                        ]
                    });
                    this.options.push({
                        displayText: 'Profile',
                        iconName: 'person',
                        component: 'ProfilePage'
                    });
                }
            }
        }
        console.log("check localstorage for is dealer value=> ", localStorage.getItem("isDealervalue"));
        var _DealerStat = localStorage.getItem("dealer_status");
        var _CustStat = localStorage.getItem("custumer_status");
        var onlyDeal = localStorage.getItem("OnlyDealer");
        if (_DealerStat != null || _CustStat != null) {
            if (_DealerStat == 'ON' && _CustStat == 'OFF') {
                this.options[2].displayText = 'Admin';
                this.options[2].iconName = 'person';
                this.options[2].component = 'DashboardPage';
                this.options[3].displayText = 'Customers';
                this.options[3].iconName = 'contacts';
                this.options[3].component = 'CustomersPage';
            }
            else {
                if (_DealerStat == 'OFF' && _CustStat == 'ON') {
                    this.options[2].displayText = 'Dealers';
                    this.options[2].iconName = 'person';
                    this.options[2].component = 'DashboardPage';
                }
                else {
                    if (_DealerStat == 'OFF' && _CustStat == 'OFF' && onlyDeal == null) {
                        this.options[2].displayText = 'Dealers';
                        this.options[2].iconName = 'person';
                        this.options[2].component = 'DealerPage';
                        this.options[3].displayText = 'Customers';
                        this.options[3].iconName = 'contacts';
                        this.options[3].component = 'CustomersPage';
                    }
                    else {
                        if (onlyDeal == 'true') {
                            this.options[2].displayText = 'Customers';
                            this.options[2].iconName = 'contacts';
                            this.options[2].component = 'CustomersPage';
                        }
                    }
                }
            }
        }
        this.events.subscribe("sidemenu:event", function (data) {
            console.log("sidemenu:event=> ", data);
            if (data) {
                _this.options[2].displayText = 'Dealers';
                _this.options[2].iconName = 'person';
                _this.options[2].component = 'DashboardPage';
            }
        });
    };
    MyApp.prototype.onOptionSelected = function (option) {
        var _this = this;
        this.menuCtrl.close().then(function () {
            if (option.custom && option.custom.isLogin) {
                _this.presentAlert('You\'ve clicked the login option!');
            }
            else if (option.custom && option.custom.isLogout) {
                _this.presentAlert('You\'ve clicked the logout option!');
            }
            else if (option.custom && option.custom.isExternalLink) {
                var url = option.custom.externalUrl;
                window.open(url, '_blank');
            }
            else {
                var params = option.custom && option.custom.param;
                if (option.displayText == 'Admin' && option.component == 'DashboardPage') {
                    localStorage.setItem("dealer_status", 'OFF');
                    localStorage.setItem('details', localStorage.getItem("superAdminData"));
                    localStorage.removeItem('superAdminData');
                }
                if (option.displayText == 'Dealers' && option.component == 'DashboardPage') {
                    if (localStorage.getItem('custumer_status') == 'ON') {
                        var _dealdata = JSON.parse(localStorage.getItem("dealer"));
                        if (localStorage.getItem("superAdminData") != null || _this.islogin.isSuperAdmin == true) {
                            localStorage.setItem("dealer_status", 'ON');
                        }
                        else {
                            if (_dealdata.isSuperAdmin == true) {
                                localStorage.setItem("dealer_status", 'OFF');
                            }
                            else {
                                localStorage.setItem("OnlyDealer", "true");
                            }
                        }
                        localStorage.setItem("custumer_status", 'OFF');
                        localStorage.setItem('details', localStorage.getItem("dealer"));
                    }
                    else {
                        console.log("something wrong!!");
                    }
                }
                _this.nav.setRoot(option.component, params);
            }
        });
    };
    MyApp.prototype.collapseMenuOptions = function () {
        this.sideMenu.collapseAllOptions();
    };
    MyApp.prototype.presentAlert = function (message) {
        var alert = this.alertCtrl.create({
            title: 'Information',
            message: message,
            buttons: ['Ok']
        });
        alert.present();
    };
    MyApp.prototype.openPage = function (page, index) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component) {
            this.nav.setRoot(page.component);
            this.menuCtrl.close();
        }
        else {
            if (this.selectedMenu) {
                this.selectedMenu = 0;
            }
            else {
                this.selectedMenu = index;
            }
        }
    };
    MyApp.prototype.resize = function () {
        var offset = this.headerTag.nativeElement.offsetHeight;
        this.scrollableTag.nativeElement.style.marginTop = offset + 'px';
    };
    MyApp.prototype.chkCondition = function () {
        var _this = this;
        this.resize();
        this.events.subscribe("event_sidemenu", function (data) {
            _this.islogin = JSON.parse(data);
            _this.options[2].displayText = 'Dealers';
            _this.options[2].iconName = 'person';
            _this.options[2].component = 'DashboardPage';
            _this.initializeOptions();
        });
        this.initializeOptions();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('headerTag'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], MyApp.prototype, "headerTag", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('scrollableTag'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], MyApp.prototype, "scrollableTag", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Nav"])
    ], MyApp.prototype, "nav", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_7__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_7__shared_side_menu_content_side_menu_content_component__["a" /* SideMenuContentComponent */])
    ], MyApp.prototype, "sideMenu", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-main-page',template:/*ion-inline-start:"D:\Pro\flex_track\src\app\app.html"*/'<!-- <ion-menu [content]="content" [swipeEnabled]="false" (ionOpen)=\'chkCondition()\'>\n\n  <ion-header>\n\n  </ion-header>\n\n  <ion-content id=outerNew>\n\n    <div class="headProf">\n\n      <img src="assets/imgs/dummy-user-img.png">\n\n      <div>\n\n        <h4>{{islogin.fn}}&nbsp;{{islogin.ln}}</h4>\n\n        <p style="font-size: 12px">\n\n          <ion-icon name="mail"></ion-icon>&nbsp;{{islogin.email}}</p>\n\n        <p style="font-size: 12px">\n\n          <ion-icon name="call"></ion-icon>&nbsp;{{islogin.phn}}</p>\n\n      </div>\n\n    </div>\n\n    <side-menu-content [settings]="sideMenuSettings" [options]="options" (change)="onOptionSelected($event)"></side-menu-content>\n\n  </ion-content>\n\n</ion-menu>\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav> -->\n\n\n\n<ion-menu [content]="content" [swipeEnabled]="false" (ionOpen)="chkCondition()">\n\n  <ion-header> </ion-header>\n\n  <ion-content id="outerNew">\n\n    <div class="headProf" #headerTag ion-fixed>\n\n      <img src="assets/imgs/dummy-user-img.png" />\n\n\n\n      <div>\n\n        <h4>{{islogin.fn}}&nbsp;{{islogin.ln}}</h4>\n\n        <p style="font-size: 12px">\n\n          <ion-icon name="mail"></ion-icon>&nbsp;{{islogin.email}}\n\n        </p>\n\n        <p style="font-size: 12px">\n\n          <ion-icon name="call"></ion-icon>&nbsp;{{islogin.phn}}\n\n        </p>\n\n      </div>\n\n    </div>\n\n    <!-- <ion-scroll style="width:100%; height:100vh" scrollY="true"> -->\n\n    <div #scrollableTag>\n\n      <side-menu-content style="width:100%; height:100vh" [settings]="sideMenuSettings" [options]="options"\n\n        (change)="onOptionSelected($event)"></side-menu-content>\n\n    </div>\n\n    <!-- </ion-scroll> -->\n\n\n\n  </ion-content>\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"D:\Pro\flex_track\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_4__providers_network_network__["a" /* NetworkProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_menu_menu__["a" /* MenuProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["MenuController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["App"],
            __WEBPACK_IMPORTED_MODULE_9__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_text_to_speech__["a" /* TextToSpeech */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 500:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SideMenuOptionSelect; });
// SideMenuOptionSelect constant
var SideMenuOptionSelect = 'sideMenu:optionSelect';
//# sourceMappingURL=side-menu-option-select-event.js.map

/***/ }),

/***/ 501:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateGroup; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UpdateGroup = /** @class */ (function () {
    function UpdateGroup(navCtrl, navParams, apigroupupdatecall, alertCtrl, modalCtrl, formBuilder, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apigroupupdatecall = apigroupupdatecall;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.viewCtrl = viewCtrl;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("_id=> " + this.islogin._id);
        this.GroupStatus = [
            {
                name: "Active"
            },
            {
                name: "InActive"
            }
        ];
        this.groupForm = formBuilder.group({
            group_name: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            status: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            grouptype: ['']
        });
    }
    UpdateGroup.prototype.ngOnInit = function () {
    };
    UpdateGroup.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    UpdateGroup = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-update-model',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\groups\update-group\update-group.html"*/'<ion-header>\n\n        <ion-navbar>\n\n            <ion-title>Update Group</ion-title>\n\n            <ion-buttons end>\n\n                <button ion-button icon-only (click)="dismiss()">\n\n                    <ion-icon name="close-circle"></ion-icon>\n\n                </button>\n\n            </ion-buttons>\n\n        </ion-navbar>\n\n    </ion-header>\n\n    <ion-content>\n\n     \n\n        <form [formGroup]="groupForm">\n\n\n\n            <ion-item>\n\n                    <ion-label fixed fixed style="min-width: 50% !important;">Group Name</ion-label>\n\n                    <ion-input formControlName="group_name" type="text"></ion-input>\n\n           </ion-item>\n\n           \n\n\n\n           <ion-item>\n\n                <ion-label >Group Status*</ion-label>\n\n                <ion-select formControlName="status" style="min-width:50%;">\n\n                    <ion-option *ngFor="let statusname of GroupStatus" [value]="statusname.name" (ionSelect)="GroupStatusdata(statusname)">{{statusname.name}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n    \n\n      \n\n    </form>\n\n    <!-- <button ion-button block (click)="AddGroup()">ADD</button> -->\n\n        \n\n    </ion-content>\n\n    <ion-footer class="footSty">\n\n    \n\n            <ion-toolbar>\n\n                <ion-row no-padding>\n\n                    <ion-col width-50 style="text-align: center;">\n\n                        <button ion-button clear color="light" (click)="UpdateGroup()">UPDATE</button>\n\n                    </ion-col>\n\n                </ion-row>\n\n            </ion-toolbar>\n\n        </ion-footer>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\groups\update-group\update-group.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_3__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"]])
    ], UpdateGroup);
    return UpdateGroup;
}());

//# sourceMappingURL=update-group.js.map

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeocoderProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_geocoder__ = __webpack_require__(128);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GeocoderProvider = /** @class */ (function () {
    function GeocoderProvider(_GEOCODE) {
        this._GEOCODE = _GEOCODE;
        console.log('Hello GeocoderProvider Provider');
    }
    GeocoderProvider.prototype.reverseGeocode = function (lat, lng) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._GEOCODE.reverseGeocode(lat, lng)
                .then(function (result) {
                var a = result[0].thoroughfare ? result[0].thoroughfare : null;
                var b = result[0].subThoroughfare ? result[0].subThoroughfare : null;
                var c = result[0].subLocality ? result[0].subLocality : null;
                var d = result[0].subAdministrativeArea ? result[0].subAdministrativeArea : null;
                var e = result[0].postalCode ? result[0].postalCode : null;
                var f = result[0].locality ? result[0].locality : null;
                var g = result[0].countryName ? result[0].countryName : null;
                var h = result[0].administrativeArea ? result[0].administrativeArea : null;
                var str = '';
                if (a != null)
                    str = a + ', ';
                if (b != null)
                    str = str + b + ', ';
                if (c != null)
                    str = str + c + ', ';
                if (d != null)
                    str = str + d + ', ';
                if (e != null)
                    str = str + e + ', ';
                if (f != null)
                    str = str + f + ', ';
                if (g != null)
                    str = str + g + ', ';
                if (h != null)
                    str = str + h + ', ';
                resolve(str);
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    GeocoderProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_native_geocoder__["a" /* NativeGeocoder */]])
    ], GeocoderProvider);
    return GeocoderProvider;
}());

//# sourceMappingURL=geocoder.js.map

/***/ })

},[419]);
//# sourceMappingURL=main.js.map