webpackJsonp([53],{

/***/ 505:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentGreetingPageModule", function() { return PaymentGreetingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment_greeting__ = __webpack_require__(737);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PaymentGreetingPageModule = /** @class */ (function () {
    function PaymentGreetingPageModule() {
    }
    PaymentGreetingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__payment_greeting__["a" /* PaymentGreetingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__payment_greeting__["a" /* PaymentGreetingPage */]),
            ],
        })
    ], PaymentGreetingPageModule);
    return PaymentGreetingPageModule;
}());

//# sourceMappingURL=payment-greeting.module.js.map

/***/ }),

/***/ 737:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentGreetingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { FeedbackPage } from '../feedback/feedback';
var PaymentGreetingPage = /** @class */ (function () {
    function PaymentGreetingPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.paymentTime = new Date();
    }
    PaymentGreetingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PaymentGreetingPage');
        this.paymentTime;
    };
    PaymentGreetingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-payment-greeting',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\add-devices\payment-greeting\payment-greeting.html"*/'<ion-content no-padding>\n\n  <ion-grid style="height:45%;background:#8eda4a;">\n\n    <ion-card style="margin: 0px;width: 28%;border-radius: 15px;padding-top: 46ppx;position: fixed;margin-top: 30px;margin-left: 37%;margin-bottom: 20px;">\n\n      <img src="assets/icon/zogologo.png" style="width: 60%;margin: 18%;">\n\n    </ion-card>\n\n    <h5 style="margin: 10px 0px 0px 18px;;color: white;font-weight: 400;padding-top: 41%;text-align: center;font-size: 1.8rem;">Ride Closed</h5>\n\n  </ion-grid>\n\n  <ion-card style="width: 80%;height: 30%;margin-top: -54px;margin-left: 12%;border-radius: 8%;">\n\n    <p style="text-align: left;font-size: 15px;color: #767373;margin: 12% 0% 0% 15%;">Paid Successfully </p>\n\n    <h4 style="text-align: center;color: #8eda4a;font-family: fantasy;font-size: 36px;margin-top: 6px;letter-spacing: 3px;">\n\n      <i class="fa fa-rupee"></i> 100.00\n\n      <span>\n\n        <ion-icon name="md-checkmark-circle"></ion-icon>\n\n      </span>\n\n    </h4>\n\n    <p style="margin: 8% 0% 0% 14%;color: #b1adad;">{{paymentTime | date:\'medium\'}}</p>\n\n  </ion-card>\n\n  <ion-grid no-padding style="text-align: center;">\n\n    <p style="color: #8f8e8e;font-size: 16px;">Need Help?</p>\n\n    <h3 style="color: #8eda4a;font-family: fantasy;margin: 0%;font-size: 45px;letter-spacing: 4px;">Thank</h3>\n\n    <h3 style="color: #70ac3a;font-family: fantasy;margin: 0%;font-size: 61px;letter-spacing: 4px;">You</h3>\n\n  </ion-grid>\n\n</ion-content>\n\n<ion-footer style="width: 90%;\n\nheight: 100px;\n\nbackground: #ffffff;\n\nmargin-left: 5%;\n\nborder-top-left-radius: 8px;\n\nborder-top-right-radius: 8px;\n\nbox-shadow: 0px 1px 15px 3px #cfcfcf;">\n\n  <ion-row style="margin-top: 22px;width: 90%;margin-left: 5%;">\n\n    <button ion-button full style="margin: 0%;height:50px;background:#87c23f;font-size: 14px;border-radius: 6px;" (click)="feedback()">Feedback</button>\n\n  </ion-row>\n\n</ion-footer>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\add-devices\payment-greeting\payment-greeting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], PaymentGreetingPage);
    return PaymentGreetingPage;
}());

//# sourceMappingURL=payment-greeting.js.map

/***/ })

});
//# sourceMappingURL=53.js.map