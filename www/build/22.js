webpackJsonp([22],{

/***/ 1065:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPoiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_native_geocoder__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_api_service_api_service__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AddPoiPage = /** @class */ (function () {
    function AddPoiPage(navParam, alertCtrl, events, nativeGeocoder, geoLocation, toastCtrl, navCtrl, navParams, viewCtrl, apicall) {
        this.navParam = navParam;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.nativeGeocoder = nativeGeocoder;
        this.geoLocation = geoLocation;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.apicall = apicall;
        this.autocomplete = {};
        this.autocompleteItems = [];
        this.portstemp = [];
        this.arrOBJ = [];
        this.sortedArr = [];
        this.beforSortArr = [];
        this.contains = function (needle) {
            // Per spec, the way to identify NaN is that it is not equal to itself
            var findNaN = needle !== needle;
            var indexOf;
            if (!findNaN && typeof Array.prototype.indexOf === 'function') {
                indexOf = Array.prototype.indexOf;
            }
            else {
                indexOf = function (needle) {
                    var i = -1, index = -1;
                    for (i = 0; i < this.length; i++) {
                        var item = this[i];
                        if ((findNaN && item !== item) || item === needle) {
                            index = i;
                            break;
                        }
                    }
                    return index;
                };
            }
            return indexOf.call(this, needle) > -1;
        };
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        if (this.navParam.get("param") != null) {
            this.text = "Edit";
            this.navParameters = this.navParam.get("param");
            this.radius = this.navParameters.radius;
            this.poi_name = this.navParameters.poi.poiname;
            this.getdevices();
            console.log("navparams: ", JSON.stringify(this.navParam.get("param")));
        }
        else {
            this.text = "Add";
        }
        this.acService = new google.maps.places.AutocompleteService();
    }
    AddPoiPage.prototype.ngOnInit = function () {
        this.loadMap();
    };
    AddPoiPage.prototype.onchange = function (veh) {
        console.log("onchange: ", veh);
        console.log("selected multiple value: ", this.mappedveh);
    };
    AddPoiPage.prototype.ionViewDidLoad = function () { };
    AddPoiPage.prototype.loadMap = function () {
        // if (this.map != undefined) {
        //   this.map.clear();
        // }
        this.map = __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas');
        if (this.navParam.get("param") == null) {
            this.onButtonClick();
        }
        else {
            this.callthis();
        }
    };
    AddPoiPage.prototype.callthis = function () {
        var _this = this;
        this.map.clear();
        // Move the map camera to the location with animation
        this.map.animateCamera({
            target: {
                "lat": this.navParameters.poi.location.coordinates[1],
                "lng": this.navParameters.poi.location.coordinates[0]
            },
            zoom: 17,
            tilt: 30
        })
            .then(function () {
            _this.map.addMarker({
                title: 'Add POI here(Drag Me)',
                // snippet: 'This plugin is awesome!',
                position: {
                    "lat": _this.navParameters.poi.location.coordinates[1],
                    "lng": _this.navParameters.poi.location.coordinates[0]
                },
                animation: __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["c" /* GoogleMapsAnimation */].BOUNCE,
                draggable: true
            }).then(function (marker) {
                _this.markerObj = marker;
                marker.on(__WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_DRAG_END)
                    .subscribe(function () {
                    _this.markerObj = undefined;
                    // this.markerObj = marker.getPosition();
                    _this.markerObj = marker;
                });
            });
        });
    };
    AddPoiPage.prototype.onButtonClick = function () {
        var _this = this;
        this.map.clear();
        // Get the location of you
        this.map.getMyLocation()
            .then(function (location) {
            console.log(JSON.stringify(location, null, 2));
            // Move the map camera to the location with animation
            _this.map.animateCamera({
                target: location.latLng,
                zoom: 17,
                tilt: 30
            })
                .then(function () {
                _this.map.addMarker({
                    title: 'Add POI here(Drag Me)',
                    position: location.latLng,
                    animation: __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["c" /* GoogleMapsAnimation */].BOUNCE,
                    draggable: true
                }).then(function (marker) {
                    _this.markerObj = marker;
                    marker.on(__WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_DRAG_END)
                        .subscribe(function () {
                        _this.markerObj = undefined;
                        _this.markerObj = marker;
                    });
                });
            });
        });
    };
    AddPoiPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    AddPoiPage.prototype.updateSearch = function () {
        // debugger
        console.log('modal > updateSearch');
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        var that = this;
        var config = {
            //types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'
            input: that.autocomplete.query,
            componentRestrictions: {}
        };
        this.acService.getPlacePredictions(config, function (predictions, status) {
            console.log('modal > getPlacePredictions > status > ', status);
            console.log("lat long not find ", predictions);
            that.autocompleteItems = [];
            predictions.forEach(function (prediction) {
                that.autocompleteItems.push(prediction);
            });
            console.log("autocompleteItems=> " + that.autocompleteItems);
        });
    };
    AddPoiPage.prototype.chooseItem = function (item) {
        var _this = this;
        var that = this;
        that.autocomplete.query = item.description;
        console.log("console items=> " + JSON.stringify(item));
        that.autocompleteItems = [];
        var options = {
            useLocale: true,
            maxResults: 5
        };
        this.nativeGeocoder.forwardGeocode(item.description, options)
            .then(function (coordinates) {
            console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude);
            that.newLat = coordinates[0].latitude;
            that.newLng = coordinates[0].longitude;
            var pos = {
                target: new __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["f" /* LatLng */](that.newLat, that.newLng),
                zoom: 15,
                tilt: 30
            };
            _this.map.moveCamera(pos);
            _this.markerObj.remove();
            _this.map.addMarker({
                title: 'Add POI here(Drag Me)',
                position: new __WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["f" /* LatLng */](that.newLat, that.newLng),
                draggable: true
            }).then(function (data) {
                console.log("Marker added");
                // this.markerObj = marker;
                data.on(__WEBPACK_IMPORTED_MODULE_2__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_DRAG_END)
                    .subscribe(function () {
                    // this.markerObj = marker.getPosition();
                    that.newLat = data.getPosition().lat;
                    that.newLng = data.getPosition().lng;
                });
            });
        })
            .catch(function (error) { return console.log(error); });
    };
    AddPoiPage.prototype.createPOI = function () {
        var _this = this;
        if (this.poi_name == undefined || this.radius == undefined) {
            var alert_1 = this.alertCtrl.create({
                message: "Please fill all the fields first and try again..",
                buttons: ["Okay"]
            });
            alert_1.present();
        }
        else {
            var that_1 = this;
            if (this.newLat == undefined || this.newLng == undefined) {
                this.newLat = that_1.markerObj.getPosition().lat;
                this.newLng = that_1.markerObj.getPosition().lng;
                var options = {
                    useLocale: true,
                    maxResults: 5
                };
                this.nativeGeocoder.reverseGeocode(this.newLat, this.newLng, options)
                    .then(function (result) {
                    console.log("reverse geocode: " + JSON.stringify(result[0]));
                    var addressStr = result[0].locality + ' ' + result[0].subLocality + ' ' + result[0].subAdministrativeArea + ' ' + result[0].countryName;
                    _this.autocomplete.query = "";
                    _this.autocomplete.query = addressStr;
                    var payload = {
                        "poi": [
                            {
                                "location": {
                                    "type": "Point",
                                    "coordinates": [
                                        _this.newLng,
                                        _this.newLat
                                    ]
                                },
                                "poiname": that_1.poi_name,
                                "status": "Active",
                                "user": that_1.islogin._id,
                                "address": _this.autocomplete.query,
                                "radius": that_1.radius
                            }
                        ]
                    };
                    _this.apicall.startLoading().present();
                    _this.apicall.addPOIAPI(payload)
                        .subscribe(function (data) {
                        console.log("response adding poi: ", data);
                        _this.apicall.stopLoading();
                        _this.events.publish('reloadpoilist');
                        var toast = _this.toastCtrl.create({
                            message: "POI added successfully!",
                            duration: 2000,
                            position: 'top'
                        });
                        toast.present();
                        _this.navCtrl.pop();
                    }, function (err) {
                        console.log("error adding poi: ", err);
                        _this.apicall.stopLoading();
                    });
                })
                    .catch(function (error) { return console.log(error); });
            }
            else {
                var payload = {
                    "poi": [
                        {
                            "location": {
                                "type": "Point",
                                "coordinates": [
                                    that_1.newLng,
                                    that_1.newLat
                                ]
                            },
                            "poiname": that_1.poi_name,
                            "status": "Active",
                            "user": that_1.islogin._id,
                            "address": that_1.autocomplete.query,
                            "radius": that_1.radius
                        }
                    ]
                };
                this.apicall.startLoading().present();
                this.apicall.addPOIAPI(payload)
                    .subscribe(function (data) {
                    console.log("response adding poi: ", data);
                    _this.apicall.stopLoading();
                    _this.events.publish('reloadpoilist');
                    var toast = _this.toastCtrl.create({
                        message: "POI added successfully!",
                        duration: 2000,
                        position: 'top'
                    });
                    toast.present();
                    _this.navCtrl.pop();
                }, function (err) {
                    console.log("error adding poi: ", err);
                    _this.apicall.stopLoading();
                });
            }
        }
    };
    AddPoiPage.prototype.getdevices = function () {
        var _this = this;
        this.apicall.startLoading().present();
        this.apicall.livedatacall(this.islogin._id, this.islogin.email)
            .subscribe(function (data) {
            _this.apicall.stopLoading();
            _this.portstemp = data.devices;
            for (var i = 0; i < data.devices.length; i++) {
                _this.arrOBJ.push(data.devices[i]._id);
            }
        }, function (err) {
            _this.apicall.stopLoading();
            console.log(err);
        });
    };
    AddPoiPage.prototype.onchangeeev = function (ev) {
        console.log("ev: " + ev);
        this.changedEvent = undefined;
        this.changedEvent = ev;
    };
    AddPoiPage.prototype.update = function () {
        var _this = this;
        var that = this;
        this.sortedArr = [];
        this.beforSortArr = [];
        console.log("array obj list: ", JSON.stringify(this.arrOBJ));
        // console.log("selected array: ", JSON.stringify(that.changedEvent))
        debugger;
        if (this.newLat == undefined || this.newLng == undefined) {
            this.newLat = that.markerObj.getPosition().lat;
            this.newLng = that.markerObj.getPosition().lng;
            var options = {
                useLocale: true,
                maxResults: 5
            };
            this.nativeGeocoder.reverseGeocode(this.newLat, this.newLng, options)
                .then(function (result) {
                console.log("reverse geocode: " + JSON.stringify(result[0]));
                var addressStr = result[0].locality + ' ' + result[0].subLocality + ' ' + result[0].subAdministrativeArea + ' ' + result[0].countryName;
                _this.autocomplete.query = "";
                _this.autocomplete.query = addressStr;
                var pay = {};
                if (_this.changedEvent != undefined) {
                    for (var t = 0; t < _this.portstemp.length; t++) {
                        for (var r = 0; r < _this.changedEvent.length; r++) {
                            if (_this.changedEvent[r] == _this.portstemp[t].Device_Name) {
                                _this.beforSortArr.push(_this.portstemp[r]._id);
                            }
                        }
                    }
                    for (var rt = 0; rt < _this.arrOBJ.length; rt++) {
                        var myArray = _this.beforSortArr, needle = _this.arrOBJ[rt], index = _this.contains.call(myArray, needle); // true
                        if (!index) {
                            _this.sortedArr.push(_this.arrOBJ[rt]);
                        }
                    }
                    console.log("sorted array: ", JSON.stringify(_this.sortedArr));
                    pay = {
                        "location": {
                            "type": "Point",
                            "coordinates": [
                                _this.newLng,
                                _this.newLat
                            ]
                        },
                        "poiid": _this.navParameters._id,
                        "address": _this.autocomplete.query,
                        "radius": _this.radius,
                        "deviceArr": _this.sortedArr
                    };
                }
                else {
                    pay = {
                        "location": {
                            "type": "Point",
                            "coordinates": [
                                _this.newLng,
                                _this.newLat
                            ]
                        },
                        "poiid": _this.navParameters._id,
                        "address": _this.autocomplete.query,
                        "radius": _this.radius,
                        "deviceArr": []
                    };
                }
                _this.apicall.startLoading().present();
                _this.apicall.updatePOIAPI(pay)
                    .subscribe(function (data) {
                    console.log("response adding poi: ", data);
                    _this.apicall.stopLoading();
                    _this.events.publish('reloadpoilist');
                    var toast = _this.toastCtrl.create({
                        message: "POI updated successfully!",
                        duration: 2000,
                        position: 'top'
                    });
                    toast.present();
                    _this.navCtrl.pop();
                }, function (err) {
                    console.log("error adding poi: ", err);
                    _this.apicall.stopLoading();
                });
            })
                .catch(function (error) { return console.log(error); });
        }
        else {
            var pay = {};
            if (this.changedEvent != undefined) {
                for (var t = 0; t < this.portstemp.length; t++) {
                    for (var r = 0; r < this.changedEvent.length; r++) {
                        if (this.changedEvent[r] == this.portstemp[t].Device_Name) {
                            this.beforSortArr.push(this.portstemp[r]._id);
                        }
                    }
                }
                for (var rt = 0; rt < this.arrOBJ.length; rt++) {
                    var myArray = this.beforSortArr, needle = this.arrOBJ[rt], index = this.contains.call(myArray, needle); // true
                    if (!index) {
                        this.sortedArr.push(this.arrOBJ[rt]);
                    }
                }
                pay = {
                    "location": {
                        "type": "Point",
                        "coordinates": [
                            that.newLng,
                            that.newLat
                        ]
                    },
                    "poiid": that.navParameters._id,
                    "address": that.autocomplete.query,
                    "radius": that.radius,
                    "deviceArr": this.sortedArr
                };
            }
            else {
                pay = {
                    "location": {
                        "type": "Point",
                        "coordinates": [
                            that.newLng,
                            that.newLat
                        ]
                    },
                    "poiid": that.navParameters._id,
                    "address": that.autocomplete.query,
                    "radius": that.radius,
                    "deviceArr": []
                };
            }
            this.apicall.startLoading().present();
            this.apicall.updatePOIAPI(pay)
                .subscribe(function (data) {
                console.log("response adding poi: ", data);
                _this.apicall.stopLoading();
                _this.events.publish('reloadpoilist');
                var toast = _this.toastCtrl.create({
                    message: "POI updated successfully!",
                    duration: 2000,
                    position: 'top'
                });
                toast.present();
                _this.navCtrl.pop();
            }, function (err) {
                console.log("error adding poi: ", err);
                _this.apicall.stopLoading();
            });
        }
    };
    AddPoiPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-add-poi',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\poi-list\add-poi\add-poi.html"*/'<ion-header>\n\n    <ion-navbar>\n\n        <ion-title>{{text}} POI</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n    <div id="map_canvas">\n\n        <div style="padding: 10px;">\n\n            <ion-row>\n\n                <ion-col col-50 style="padding-right: 5px; padding-left: 0px; padding-top: 0px; padding-bottom: 0px;">\n\n                    <ion-row class="rowsty">\n\n                        <ion-label style="padding: 0px; margin: 0px; color: gray;">POI Name</ion-label>\n\n                        <input type="text" class="searchbar-input" placeholder="Enter POI name" name="poi_name"\n\n                            [(ngModel)]="poi_name">\n\n                    </ion-row>\n\n                </ion-col>\n\n                <ion-col col-50 style="padding: 0px;">\n\n                    <ion-row class="rowsty">\n\n                        <ion-label style="padding: 0px; margin: 0px; color: gray;">Radius</ion-label>\n\n                        <input type="number" class="searchbar-input" placeholder="Enter radius" name="radius"\n\n                            [(ngModel)]="radius">\n\n                    </ion-row>\n\n                </ion-col>\n\n            </ion-row>\n\n        </div>\n\n        <div style="padding-top: 0px; padding-right: 10px; padding-left: 10px; padding-bottom: 2px;"\n\n            *ngIf="portstemp.length > 0">\n\n            <ion-item style="padding-left: 5px; padding-right: 0px;">\n\n\n\n                <ion-label style="padding: 0px; margin: 0px; color: gray;">Mapped Vehicles</ion-label>\n\n                <ion-select multiple okText="Okay" cancelText="Dismiss" (cancel)="onCancel($event)"\n\n                    [(ngModel)]="mappedveh" name="mappedveh" (ionChange)="onchangeeev($event)">\n\n                    <ion-option selected *ngFor="let veh of portstemp" [value]="veh.Device_Name"\n\n                        (ionSelect)="onchange(veh)">{{veh.Device_Name}}</ion-option>\n\n                </ion-select>\n\n            </ion-item>\n\n        </div>\n\n        <ion-searchbar class="search_bar" [(ngModel)]="autocomplete.query" (ionInput)="updateSearch()"\n\n            (ionCancel)="onCancel($event)" placeholder="Enter a location..">\n\n        </ion-searchbar>\n\n        <ion-list>\n\n            <ion-item *ngFor="let item of autocompleteItems" (click)="chooseItem(item)">\n\n                {{ item.description }}\n\n            </ion-item>\n\n        </ion-list>\n\n    </div>\n\n</ion-content>\n\n<ion-footer class="footSty">\n\n    <ion-toolbar>\n\n        <ion-row>\n\n            <ion-col col-50 style="text-align: center; border-right: 1px solid white;" *ngIf="text == \'Add\'">\n\n                <button ion-button clear color="light" (click)="createPOI()">SUBMIT</button>\n\n            </ion-col>\n\n            <ion-col col-50 style="text-align: center; border-right: 1px solid white;" *ngIf="text != \'Add\'">\n\n                <button ion-button clear color="light" (click)="update()">SAVE</button>\n\n            </ion-col>\n\n            <ion-col col-50 style="text-align: center">\n\n                <button ion-button clear color="light" (click)="back()">Cancel</button>\n\n            </ion-col>\n\n        </ion-row>\n\n    </ion-toolbar>\n\n</ion-footer>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\poi-list\add-poi\add-poi.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"], __WEBPACK_IMPORTED_MODULE_4__ionic_native_native_geocoder__["a" /* NativeGeocoder */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"], __WEBPACK_IMPORTED_MODULE_5__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], AddPoiPage);
    return AddPoiPage;
}());

//# sourceMappingURL=add-poi.js.map

/***/ }),

/***/ 558:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddPoiPageModule", function() { return AddPoiPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_poi__ = __webpack_require__(1065);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// import { NativeGeocoder } from '@ionic-native/native-geocoder';
var AddPoiPageModule = /** @class */ (function () {
    function AddPoiPageModule() {
    }
    AddPoiPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__add_poi__["a" /* AddPoiPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__add_poi__["a" /* AddPoiPage */]),
            ],
            providers: [],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["NO_ERRORS_SCHEMA"]]
        })
    ], AddPoiPageModule);
    return AddPoiPageModule;
}());

//# sourceMappingURL=add-poi.module.js.map

/***/ })

});
//# sourceMappingURL=22.js.map