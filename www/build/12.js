webpackJsonp([12],{

/***/ 1049:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StoppagesRepoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var StoppagesRepoPage = /** @class */ (function () {
    function StoppagesRepoPage(navCtrl, navParams, apicallStoppages, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallStoppages = apicallStoppages;
        this.toastCtrl = toastCtrl;
        this.portstemp = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        if (navParams.get('param') != null) {
            this.vehicleData = navParams.get('param');
        }
    }
    StoppagesRepoPage.prototype.ngOnInit = function () {
        if (this.vehicleData == undefined) {
            this.getdevices();
        }
        else {
            this.Ignitiondevice_id = this.vehicleData._id;
            this.getStoppageReport();
        }
    };
    StoppagesRepoPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicallStoppages.startLoading().present();
        this.apicallStoppages.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicallStoppages.stopLoading();
            _this.devices = data;
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apicallStoppages.stopLoading();
            console.log(err);
        });
    };
    StoppagesRepoPage.prototype.getStoppagesdevice = function (data) {
        console.log("selectedVehicle=> ", data);
        this.Ignitiondevice_id = data._id;
    };
    StoppagesRepoPage.prototype.getStoppageReport = function () {
        var _this = this;
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        var that = this;
        this.apicallStoppages.startLoading().present();
        this.apicallStoppages.getStoppageApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.Ignitiondevice_id, this.islogin._id)
            .subscribe(function (data) {
            _this.apicallStoppages.stopLoading();
            _this.stoppagesReport = data;
            _this.Stoppagesdata = [];
            if (_this.stoppagesReport.length > 0) {
                _this.innerFunc(_this.stoppagesReport);
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: "Report(s) not found for selected dates/vehicle.",
                    duration: 1500,
                    position: 'bottom'
                });
                toast.present();
            }
        }, function (error) {
            _this.apicallStoppages.stopLoading();
            console.log(error);
        });
    };
    StoppagesRepoPage.prototype.innerFunc = function (stoppagesReport) {
        var outerthis = this;
        outerthis.locationEndAddress = undefined;
        var i = 0, howManyTimes = stoppagesReport.length;
        function f() {
            var arrivalTime = new Date(outerthis.stoppagesReport[i].arrival_time).toLocaleString();
            var departureTime = new Date(outerthis.stoppagesReport[i].departure_time).toLocaleString();
            var fd = new Date(arrivalTime).getTime();
            var td = new Date(departureTime).getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60;
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            var Durations = rhours + 'hrs : ' + rminutes + 'mins';
            outerthis.Stoppagesdata.push({
                'arrival_time': outerthis.stoppagesReport[i].arrival_time,
                'departure_time': outerthis.stoppagesReport[i].departure_time,
                'Durations': Durations,
                'device': outerthis.stoppagesReport[i].device ? outerthis.stoppagesReport[i].device.Device_Name : 'N/A'
            });
            if (outerthis.stoppagesReport[i].lat != null && outerthis.stoppagesReport[i].long != null) {
                var latEnd = outerthis.stoppagesReport[i].lat;
                var lngEnd = outerthis.stoppagesReport[i].long;
                var latlng = new google.maps.LatLng(latEnd, lngEnd);
                var geocoder = new google.maps.Geocoder();
                var request = {
                    latLng: latlng
                };
                geocoder.geocode(request, function (data, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (data[1] != null) {
                            outerthis.locationEndAddress = data[1].formatted_address;
                        }
                    }
                    outerthis.Stoppagesdata[outerthis.Stoppagesdata.length - 1].address = outerthis.locationEndAddress;
                });
            }
            else {
                outerthis.Stoppagesdata[outerthis.Stoppagesdata.length - 1].address = 'N/A';
            }
            console.log(outerthis.Stoppagesdata);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    StoppagesRepoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-stoppages-repo',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\stoppages-repo\stoppages-repo.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Stoppages Report</ion-title>\n\n  </ion-navbar>\n\n\n\n  <ion-item style="background-color: #fafafa;" *ngIf="portstemp.length != 0">\n\n    <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getStoppagesdevice(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n\n    <ion-col width-20>\n\n      <ion-label>\n\n        <span style="font-size: 13px">From Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20 padding-left>\n\n      <ion-label>\n\n        <span style="font-size: 13px">To Date</span>\n\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n\n      </ion-label>\n\n    </ion-col>\n\n\n\n    <ion-col width-20>\n\n      <div style="margin-top: 9px; float: right">\n\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;"\n\n          (click)="getStoppageReport();"></ion-icon>\n\n      </div>\n\n    </ion-col>\n\n  </ion-row>\n\n</ion-header>\n\n<ion-content [ngClass]="{ \'masters\': portstemp.length != 0,\'masters1\': portstemp.length == 0 }">\n\n\n\n  <ion-card *ngFor="let stopdata of Stoppagesdata ">\n\n\n\n    <ion-item style="border-bottom: 2px solid #dedede;">\n\n      <ion-avatar item-start>\n\n        <img src="assets/imgs/car_red_icon.png">\n\n      </ion-avatar>\n\n      <ion-label>{{stopdata.device}}</ion-label>\n\n      <ion-badge item-end color="gpsc">{{stopdata.Durations}}</ion-badge>\n\n    </ion-item>\n\n    <ion-card-content>\n\n      <ion-row style="padding-top:5px">\n\n        <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;">\n\n\n\n          <ion-icon ios="ios-time" md="md-time" style="color:#5edb82; font-size: 15px;"></ion-icon> &nbsp;\n\n          {{stopdata.arrival_time | date: \'short\'}}\n\n\n\n        </ion-col>\n\n        <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;">\n\n\n\n          <ion-icon ios="ios-time" md="md-time" style="color:#ee7272; font-size: 15px;"></ion-icon>&nbsp;\n\n          {{stopdata.departure_time | date: \'short\'}}\n\n\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row style="padding-top:5px">\n\n        <ion-col col-1 style="font-size: 15px;">\n\n          <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;"></ion-icon>\n\n        </ion-col>\n\n        <ion-col col-11 style="color:gray;font-size:11px;font-weight: 400;">\n\n          {{stopdata.address}}\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-card-content>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\stoppages-repo\stoppages-repo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], StoppagesRepoPage);
    return StoppagesRepoPage;
}());

//# sourceMappingURL=stoppages-repo.js.map

/***/ }),

/***/ 542:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StoppagesRepoPageModule", function() { return StoppagesRepoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__stoppages_repo__ = __webpack_require__(1049);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var StoppagesRepoPageModule = /** @class */ (function () {
    function StoppagesRepoPageModule() {
    }
    StoppagesRepoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__stoppages_repo__["a" /* StoppagesRepoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__stoppages_repo__["a" /* StoppagesRepoPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], StoppagesRepoPageModule);
    return StoppagesRepoPageModule;
}());

//# sourceMappingURL=stoppages-repo.module.js.map

/***/ })

});
//# sourceMappingURL=12.js.map