webpackJsonp([41],{

/***/ 1058:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailyReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DailyReportPage = /** @class */ (function () {
    function DailyReportPage(navCtrl, navParams, apicalldaily) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalldaily = apicalldaily;
        this.deviceReport = [];
        this.deviceReportSearch = [];
        this.page = 0;
        this.limit = 10;
        this.portstemp = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.from = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.from);
        this.to = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.to);
        if (navParams.get('param') != null) {
            this.vehicleData = navParams.get('param');
        }
    }
    DailyReportPage.prototype.ngOnInit = function () {
        this.getdevices();
        this.getDailyReportData();
    };
    DailyReportPage.prototype.getItems = function (ev) {
        var val = ev.target.value.trim();
        this.deviceReportSearch = this.deviceReport.filter(function (item) {
            return (item.device.Device_Name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
    };
    DailyReportPage.prototype.getDailyReportData = function () {
        var _this = this;
        console.log("entered");
        this.page = 0;
        var baseUrl;
        baseUrl = "https://www.oneqlik.in/devices/daily_report";
        var that = this;
        var currDay = new Date().getDay();
        var currMonth = new Date().getMonth();
        var currYear = new Date().getFullYear();
        var selectedDay = new Date(that.to).getDay();
        var selectedMonth = new Date(that.to).getMonth();
        var selectedYear = new Date(that.to).getFullYear();
        var devname, devid, today_odo, today_running, today_stopped, today_trips, maxSpeed, mileage;
        if ((currDay == selectedDay) && (currMonth == selectedMonth) && (currYear == selectedYear)) {
            devname = "Device_Name";
            devid = "Device_ID";
            today_odo = "today_odo";
            today_running = "today_running";
            today_stopped = "today_stopped";
            today_trips = "today_trips";
            maxSpeed = "maxSpeed";
            mileage = "Mileage";
        }
        else {
            console.log("else block called");
            devid = "imei";
            devname = "ID.Device_Name";
            today_odo = "today_odo";
            today_running = "today_running";
            today_stopped = "today_stopped";
            today_trips = "today_trips";
            maxSpeed = "ID.maxSpeed";
            mileage = "Mileage";
        }
        // debugger
        var payload = {};
        if (this.vehicleData == undefined) {
            payload = {
                "draw": 2,
                "columns": [
                    {
                        "data": devname
                    },
                    {
                        "data": devid
                    },
                    {
                        "data": today_odo
                    },
                    {
                        "data": today_running
                    },
                    {
                        "data": today_stopped
                    },
                    {
                        "data": today_trips
                    },
                    {
                        "data": maxSpeed
                    },
                    {
                        "data": mileage
                    },
                    { "data": "t_running" },
                    { "data": "t_stopped" },
                    { "data": "t_idling" },
                    { "data": "t_ofr" },
                    { "data": "t_noGps" },
                    {
                        "data": null,
                        "defaultContent": ""
                    }
                ],
                "order": [
                    {
                        "column": 0,
                        "dir": "asc"
                    }
                ],
                "start": 0,
                "length": this.limit,
                "search": {
                    "value": "",
                    "regex": false
                },
                "op": {},
                "select": [],
                "find": {
                    "user_id": this.islogin._id,
                    "date": new Date(this.to).toISOString()
                }
            };
        }
        else {
            payload = {
                "draw": 2,
                "columns": [
                    {
                        "data": devname
                    },
                    {
                        "data": devid
                    },
                    {
                        "data": today_odo
                    },
                    {
                        "data": today_running
                    },
                    {
                        "data": today_stopped
                    },
                    {
                        "data": today_trips
                    },
                    {
                        "data": mileage
                    },
                    {
                        "data": maxSpeed
                    },
                    { "data": "t_running" },
                    { "data": "t_stopped" },
                    { "data": "t_idling" },
                    { "data": "t_ofr" },
                    { "data": "t_noGps" },
                    {
                        "data": null,
                        "defaultContent": ""
                    }
                ],
                "order": [
                    {
                        "column": 0,
                        "dir": "asc"
                    }
                ],
                "start": 0,
                "length": this.limit,
                "search": {
                    "value": "",
                    "regex": false
                },
                "op": {},
                "select": [],
                "find": {
                    "user_id": this.islogin._id,
                    "devId": this.vehicleData.Device_ID,
                    "date": new Date(this.to).toISOString()
                }
            };
        }
        this.deviceReport = [];
        this.apicalldaily.startLoading().present();
        this.apicalldaily.getDailyReport1(baseUrl, payload)
            .subscribe(function (data) {
            _this.apicalldaily.stopLoading();
            console.log("daily report data: ", data);
            for (var i = 0; i < data.data.length; i++) {
                var ignOff = 86400000 - parseInt(data.data[i].today_running);
                // var ign_off = that.millisecondConversion(ignOff);
                // debugger
                _this.deviceReport.push({
                    _id: data.data[i]._id,
                    Device_ID: data.data[i].Device_ID ? data.data[i].Device_ID : data.data[i].imei,
                    Device_Name: data.data[i].Device_Name ? data.data[i].Device_Name : (data.data[i].ID ? data.data[i].ID.Device_Name : 'N/A'),
                    maxSpeed: data.data[i].maxSpeed ? data.data[i].maxSpeed : (data.data[i].ID ? data.data[i].ID.maxSpeed : '0'),
                    today_odo: data.data[i].today_odo,
                    today_running: _this.millisToMinutesAndSeconds(data.data[i].today_running),
                    today_stopped: _this.millisToMinutesAndSeconds(ignOff),
                    today_trips: data.data[i].today_trips,
                    mileage: data.data[i].Mileage ? ((data.data[i].today_odo) / Number(data.data[i].Mileage)).toFixed(2) : 'N/A'
                    // avgSpeed: this.calcAvgSpeed(odo1[0], data.data[i].today_running)
                });
            }
        }, function (error) {
            _this.apicalldaily.stopLoading();
            console.log("error in service=> " + error);
        });
    };
    DailyReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicalldaily.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            // this.apicalldaily.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            // this.apicalldaily.stopLoading();
            console.log(err);
        });
    };
    DailyReportPage.prototype.getSelectedId = function (pdata) {
        console.log(pdata);
        this.vehicleData = pdata;
        this.getDailyReportData();
    };
    DailyReportPage.prototype.millisToMinutesAndSeconds = function (millis) {
        var ms = millis;
        ms = 1000 * Math.round(ms / 1000); // round to nearest second
        var d = new Date(ms);
        debugger;
        var min1;
        var min = d.getUTCMinutes();
        if ((min).toString().length == 1) {
            min1 = '0' + (d.getUTCMinutes()).toString();
        }
        else {
            min1 = min;
        }
        return d.getUTCHours() + ':' + min1;
    };
    DailyReportPage.prototype.calcAvgSpeed = function (distance, time) {
        distance = distance / 1000; //1000 = km
        time = time / 3600000; // 1000 = sec, 60000 = min, 3600000 = hrs
        return distance * 3600000 / time;
    };
    DailyReportPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        var that = this;
        that.page = that.page + 10;
        setTimeout(function () {
            var baseUrl, payload = {};
            baseUrl = "https://www.oneqlik.in/devices/daily_report";
            var that = _this;
            var currDay = new Date().getDay();
            var currMonth = new Date().getMonth();
            var currYear = new Date().getFullYear();
            var selectedDay = new Date(that.to).getDay();
            var selectedMonth = new Date(that.to).getMonth();
            var selectedYear = new Date(that.to).getFullYear();
            if ((currDay == selectedDay) && (currMonth == selectedMonth) && (currYear == selectedYear)) {
                var devname = "Device_Name";
                var devid = "Device_ID";
                var today_odo = "today_odo";
                var today_running = "today_running";
                var today_stopped = "today_stopped";
                var today_trips = "today_trips";
                var maxSpeed = "maxSpeed";
            }
            else {
                console.log("else block called");
                var devid = "imei";
                var devname = "ID.Device_Name";
                var today_odo = "today_odo";
                var today_running = "today_running";
                var today_stopped = "today_stopped";
                var today_trips = "today_trips";
                var maxSpeed = "ID.maxSpeed";
            }
            var payload = {};
            if (that.vehicleData == undefined) {
                payload = {
                    "draw": 2,
                    "columns": [
                        {
                            "data": devname
                        },
                        {
                            "data": devid
                        },
                        {
                            "data": today_odo
                        },
                        {
                            "data": today_running
                        },
                        {
                            "data": today_stopped
                        },
                        {
                            "data": today_trips
                        },
                        {
                            "data": maxSpeed
                        },
                        {
                            "data": null,
                            "defaultContent": ""
                        }
                    ],
                    "order": [
                        {
                            "column": 0,
                            "dir": "asc"
                        }
                    ],
                    "start": that.page,
                    "length": 10,
                    "search": {
                        "value": "",
                        "regex": false
                    },
                    "op": {},
                    "select": [],
                    "find": {
                        "user_id": _this.islogin._id,
                        "date": new Date(_this.to).toISOString()
                    }
                };
            }
            else {
                payload = {
                    "draw": 2,
                    "columns": [
                        {
                            "data": devname
                        },
                        {
                            "data": devid
                        },
                        {
                            "data": today_odo
                        },
                        {
                            "data": today_running
                        },
                        {
                            "data": today_stopped
                        },
                        {
                            "data": today_trips
                        },
                        {
                            "data": maxSpeed
                        },
                        {
                            "data": null,
                            "defaultContent": ""
                        }
                    ],
                    "order": [
                        {
                            "column": 0,
                            "dir": "asc"
                        }
                    ],
                    "start": that.page,
                    "length": 10,
                    "search": {
                        "value": "",
                        "regex": false
                    },
                    "op": {},
                    "select": [],
                    "find": {
                        "user_id": _this.islogin._id,
                        "devId": _this.vehicleData.Device_ID,
                        "date": new Date(_this.to).toISOString()
                    }
                };
            }
            _this.apicalldaily.getDailyReport1(baseUrl, payload)
                .subscribe(function (data) {
                // this.apicalldaily.stopLoading();
                console.log("daily report data: ", data);
                for (var i = 0; i < data.data.length; i++) {
                    that.deviceReport.push({
                        _id: data.data[i]._id,
                        Device_ID: data.data[i].Device_ID ? data.data[i].Device_ID : data.data[i].imei,
                        Device_Name: data.data[i].Device_Name ? data.data[i].Device_Name : (data.data[i].ID ? data.data[i].ID.Device_Name : 'N/A'),
                        maxSpeed: data.data[i].maxSpeed ? data.data[i].maxSpeed : (data.data[i].ID ? data.data[i].ID.maxSpeed : '0'),
                        today_odo: data.data[i].today_odo,
                        today_running: _this.millisToMinutesAndSeconds(data.data[i].today_running),
                        today_stopped: _this.millisToMinutesAndSeconds(data.data[i].today_stopped),
                        today_trips: data.data[i].today_trips,
                        avgSpeed: _this.calcAvgSpeed(data.data[i].today_odo, data.data[i].today_running)
                    });
                }
                console.log('Async operation has ended');
                infiniteScroll.complete();
            });
        }, 200);
    };
    DailyReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-daily-report',template:/*ion-inline-start:"D:\Pro\flex_track\src\pages\daily-report\daily-report.html"*/'<ion-header color="gpsc">\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <ion-title>Daily Report</ion-title>\n\n        <ion-buttons end>\n\n            <ion-datetime class="dateStyle" displayFormat="DD/MM/YYYY" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="to"\n\n                (ionChange)="getDailyReportData()"></ion-datetime>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n    <ion-item style="background-color: #fafafa;">\n\n        <ion-label style="margin-top: 15px;">Select Vehicle</ion-label>\n\n        <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n            itemTextField="Device_Name" [canSearch]="true" (onChange)="getSelectedId(selectedVehicle)">\n\n        </select-searchable>\n\n    </ion-item>\n\n</ion-header>\n\n\n\n<ion-content>\n\n    <ion-card *ngFor="let item of deviceReport">\n\n        <ion-item style="border-bottom: 2px solid #dedede;">\n\n            <ion-avatar item-start>\n\n                <img src="assets/imgs/car_img1.png">\n\n            </ion-avatar>\n\n            <p style="color:black; font-size:16px; padding-left: 4px;">{{item.Device_Name }}</p>\n\n            <ion-row style="margin-top:6%;">\n\n\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.today_running">{{item.today_running}}</span>\n\n                        <span *ngIf="!item.today_running">00.00</span>&nbsp;</p>\n\n                    <p style="color:#53ab53;margin-left:0%;font-size:11px;font-weight: 350;">Running</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.today_stopped">{{item.today_stopped}}</span>\n\n                        <span *ngIf="!item.today_stopped">00.00</span>&nbsp;</p>\n\n                    <p style="color:#5dd17f;text-align:left;font-size: 11px;color:red;font-weight:350;">Stop</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span>{{ item.mileage }}</span>\n\n                    </p>\n\n\n\n                    <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;">\n\n                        Fuel Con(Litre)\n\n                    </p>\n\n                </ion-col>\n\n\n\n            </ion-row>\n\n            <ion-row style="margin-top: 2%;">\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                        <span *ngIf="item.today_odo">{{item.today_odo | number : \'1.0-2\'}}</span>\n\n                        <span *ngIf="!item.today_odo">00.00</span>&nbsp;</p>\n\n                    <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;">Distance(Km)</p>\n\n                </ion-col>\n\n                <ion-col>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight:400;">\n\n                        <span *ngIf="item.maxSpeed">{{item.maxSpeed}}&nbsp;Km/h</span>\n\n                        <span *ngIf="!item.maxSpeed">00.00</span>&nbsp;</p>\n\n                    <p style="color:#009688;margin-left:0%;font-size: 11px;font-weight: 350;">Max Speed</p>\n\n                </ion-col>\n\n                <ion-col center text-center>\n\n                    <p style="color:gray;font-size:11px; margin-top: -9px; text-align:left;font-weight: 400;">\n\n                        <span *ngIf="item.today_trips">{{item.today_trips}}</span>\n\n                        <span *ngIf="!item.today_trips">00.00</span>&nbsp;</p>\n\n                    <p style="text-align:left;font-size: 11px;color:#11c1f3;font-weight:350;">Trips</p>\n\n                </ion-col>\n\n            </ion-row>\n\n        </ion-item>\n\n    </ion-card>\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n        <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data...">\n\n        </ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"D:\Pro\flex_track\src\pages\daily-report\daily-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], DailyReportPage);
    return DailyReportPage;
}());

//# sourceMappingURL=daily-report.js.map

/***/ }),

/***/ 551:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DailyReportPageModule", function() { return DailyReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__daily_report__ = __webpack_require__(1058);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DailyReportPageModule = /** @class */ (function () {
    function DailyReportPageModule() {
    }
    DailyReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__daily_report__["a" /* DailyReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__daily_report__["a" /* DailyReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"]
            ],
        })
    ], DailyReportPageModule);
    return DailyReportPageModule;
}());

//# sourceMappingURL=daily-report.module.js.map

/***/ })

});
//# sourceMappingURL=41.js.map